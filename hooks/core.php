<?php

function UniqGeneratePaginator($data, $qString = false, $onclick = false) {
        
    if ($data['pagesCount'] > 1) {
	if ($qString == false)
	    $qString = new queryString();

	echo '<div id="paginator-container"><div class="navigation rounded"><div class="navigationIn text-center splashy rounded"><ul class="pagination">';
	if ($data['currentPage'] != 1)
	    echo '<li class="prev" style="position: absolute; top: 0; left: 0;"><a href="' . strtok($_SERVER["REQUEST_URI"], '?') . $qString->setParam('page', $data['currentPage'] - 1) . '" title="" class="prev-page"' . ($onclick ? 'onclick="' . str_replace('[page]', $data['currentPage'] - 1, $onclick) . '"' : '') . '><span></span>Предыдущая</a></li>';

	for ($i = 1; $i <= $data['pagesCount']; $i++) {

	    $last_item_x = ($i == $data['pagesCount'] ) ? 'last-item' : false;
	    $last_item_y = ($last_item_x !== false) ? ' class="' . $last_item_x . '"' : false;
	    if ($i == $data['currentPage']) {
		echo '<li class="current ' . $last_item_x . '"><span>' . $i . '</span></li>';
	    } else {
		echo '<li' . $last_item_y . '><a href="' . strtok($_SERVER["REQUEST_URI"], '?') . $qString->setParam('page', $i) . '" title=""' . ($onclick ? 'onclick="' . str_replace('[page]', $i, $onclick) . '"' : '') . '>' . $i . '</a></li>';
	    }
	}
	if ($data['currentPage'] != $data['pagesCount'])
	    echo '<li class="next" style="position: absolute; top: 0; right: 0;"><a href="' . strtok($_SERVER["REQUEST_URI"], '?') . $qString->setParam('page', $data['currentPage'] + 1) . '" title="" class="next-page"' . ($onclick ? 'onclick="' . str_replace('[page]', $data['currentPage'] + 1, $onclick) . '"' : '') . '>Следующая<span></span></a></li>';
	echo '</ul></div></div></div>';
    }
}
