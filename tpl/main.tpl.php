<?php 


?>
<div class="white_line">
	<div class="inner">


			<div class="left_tree">
			<h2>Каталог товаров</h2>	
				<ul>
									<?php
												$i = 0;
												$len = count($menu);
												
												foreach ($menu as $item) {$i++;?>
													<li class="info-box box1" id="box<?=$i?>" <?=($i == $len) ? 'style="margin-right: 0px !important"' : '';?>><a href="<?=!empty($item['cat_url'])?"/category/":"/mcat/" ?><?=$item['chpu'] ?>" >
														<span <?=(strlen($item['title']) < 30) ? 'style="padding-top:10px;"' : '';?>><?=$item['title']?></span></a>
														<div class="submenu">
														<ul>
														<?php	$count=0;
														
														foreach ($item['submenu'] as $subitem) {
															
															$count++;
															?>
															<li><a href="/<?php echo ($subitem['tag_status']!=NULL)?"tags":"category"; echo "/".$subitem['chpu']?>/"><?=$subitem['title']?></a></li>
														<?php 
														 if($count==13)
														{
															echo '</ul><ul>';
															$count=0;
														} 
														}?>
														</ul>
														</div>
													</li>
												<?php }?>
				</ul>
			</div>
		<div class="caro">	
			<div id="carousel">
					<a href="/category/557/">
						<div class="content_slid s1">
							
							<h3>Электропростыни <br><span class="text_red">Beurer</span></h3>
							<p>Если вы хотите уюта, тепла и комфорта в вашей постели – приобретите электропростынь от Beurer. </p>
							<p><span class="text_red">Перейти к товарам</span></p>
						</div>
					</a>
					<!--
					<div class="content_slid s2">
						
						 <p>An excursion in which the final destination is the same as the starting point.</p> 
					</div>

					<div class="content_slid s3">
						
						<p>jQuery  is a cross-browser JavaScript library designed to simplify the client-side scripting.</p>
					</div>
					-->
			</div>
			<div id="pager"></div>
		
		</div>
		
	</div>	
</div> 
<div class="dop"> 
	<div class="inner">
		<ul class="advantages">
					<li>Бесплатная доставка по Москве от <?=Config::get('site.delivery_price_limit') ?> руб</li>
					<li>Обмен и возврат <br> в течении 14 дней</li>
					<li>Официальные сертификаты на товары</li>
					<li>Удобные способы оплаты: Visa, Mastercard</li>
				</ul>	
	</div>	
</div>	
	

<div class="popular_products">
	<div class="inner">
		<h2 class="h2_main">Выбор покупателей <div id="pager_popular"></div></h2>
		<div class="tovarCont">
			<?php foreach ($products as $item){ ?>
			
			
			 <?php include("blocks/one_product_listing.tpl.php");  ?>
		
			<?php  } ?>
		</div>
		
	</div>
</div>
	
	<div class="popular_cat">
		<div class="inner">
		<h2 class="h2_main">Популярные категории</h2>
		<div class="block_1_p">
			<div class="block_category">
			<a href="/category/557/">
				<span class="text_pop">Электрические<br> простыни</span>
				</a>
			</div>
			<div class="block_category">
			<a href="/category/vesy">
				<span class="text_pop">Весы</span>
				</a>
			</div>
			<div class="block_category">
			<a href="/category/564/">
				<span class="text_pop">Косметические <br>зеркала</span>
				</a>
			</div>
		</div>
		<div class="block_2_p">
			<div class="block_category">
			<a href="/category/560/">
				<span class="text_pop">Массажные<br>
накидки</span>
</a>
			</div>
		</div>
		<div class="block_3_p">
			<div class="block_category">
			<a href="/category/563/">
				<span class="text_pop">Маникюрные<br> наборы</span>
				</a>
			</div>
			<div class="block_category">
			<a href="/category/574/">
				<span class="text_pop">Тонометры</span>
				</a>
			</div>
		</div>
		</div>
	</div>
	
	
	<div class="inner">
	<h1>Beurer-lux - фирменный интернет-магазин немецкой техники</h1>
	
	<p>Интернет-магазин «Бойрер Люкс» предлагает покупателям товары для дома, кухонную технику, а также продукцию для красоты и здоровья высокого качества. Все изделия произведены под маркой известной немецкой компании Beurer, надежны, имеют необходимые сертификаты и гарантийные талоны.</p>
	
	<h2>Ассортимент</h2>
		<p>На сайте магазина покупатели найдут следующие группы товаров:</p>
		<ul>
		<li>увлажнители, очистители воздуха и термометры;</li>
<li>электрические грелки, простыни и пледы;</li>
<li>массажеры и миостимуляторы;</li>
<li>тонометры;</li>
<li>эпиляторы;</li>
<li>маникюрно-педикюрные наборы;</li>
<li>фитнес-браслеты и пульсометры;</li>
<li>аппараты для ухода за кожей лица;</li>
<li>кухонные и напольные весы;</li>
 <li>тостеры и электрочайники.</li>
		</ul>
		<h3>Преимущества</h3>
		
				<p>Сотрудники магазина «Бойрер Люкс» стремятся максимально удовлетворить потребности покупателей. Если вы испытываете затруднения при выборе товара, позвоните по контактному телефону для получения профессиональной консультации. Клиенты, не нуждающиеся в помощи менеджеров, могут оставить заказ на сайте компании.</p>
		
			<p>Оплатить покупку можно наличными, банковской картой, переводом или с  помощью системы электронных платежей. Для оптовых покупателей предусмотрены скидки. Компания имеет пункты самовывоза, а также осуществляет доставку товаров в любые населенные пункты РФ.
</p>
		
		
		</ul>
	
	
	</div>
                            
                                <div class="clear"></div>
                                <div class="cpt_product_lists"></div>
                                <div class="cpt_product_lists"></div>

