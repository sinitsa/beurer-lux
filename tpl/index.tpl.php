<?php foreach (explode('|', $_COOKIE['cart_products']) as $productData) {
	$temp = explode(':', $productData);
    $temp[1] = isset($temp[1]) ? $temp[1] : 0 ;
	$summCount += $temp[1];
}
 ?>
<!DOCTYPE html>
<html  lang="ru">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="<?=$seo_des ?>" />
    <meta name="keywords" content="<?=$seo_key ?>" /> 
    <title><?=$seo_title ?></title>
    <link rel="icon" href="/favicon.ico" >
	<link rel="stylesheet" href="/frontend/css/magnific-popup.css">
    <link rel="stylesheet" href="/frontend/css/main.css">
	 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script src="/js/jquery.magnific-popup.min.js"></script>
	<script src="/js/jquery.carouFredSel-6.2.0-packed.js"></script>
    <script src="/js/jquery.maskedinput.min.js"></script>
	<script src="/js/basket.js"></script>
	<?php if($tab[0]=="category" || $tab[0]=="tags"){?>
	<script src="/js/filters.js"></script>
	<script src="/js/jquery.form.js"></script>
	<?php } ?>
	<script src="/js/main.js"></script>
	

	<!--<script src="/js/jquery.selectbox-0.2.min.js"></script>-->
	
	<!-- retailrocket.ru -->
<script>
       var rrPartnerId = "571e39491e99470aace8942b";
       var rrApi = {}; 
       var rrApiOnReady = rrApiOnReady || [];
       rrApi.addToBasket = rrApi.order = rrApi.categoryView = rrApi.view = 
           rrApi.recomMouseDown = rrApi.recomAddToCart = function() {};
       (function(d) {
           var ref = d.getElementsByTagName('script')[0];
           var apiJs, apiJsId = 'rrApi-jssdk';
           if (d.getElementById(apiJsId)) return;
           apiJs = d.createElement('script');
           apiJs.id = apiJsId;
           apiJs.async = true;
           apiJs.src = "//cdn.retailrocket.ru/content/javascript/api.js";
           ref.parentNode.insertBefore(apiJs, ref);
       }(document));
</script>
<!--  END --> 

</head>
<body>
<header>
<div id="top_line">  
		<div class="inner">
			<ul>

                <li><a href="<?=getTemplateLink(array('chpu'=>'dostavka-i-oplata'), 'page');?>">Доставка</a></li>
                <li><a href="<?=getTemplateLink(array('chpu'=>'oplata'), 'page');?>">Оплата</a></li>
                <li><a href="<?=getTemplateLink(array('chpu'=>'garantiya'), 'page');?>">Гарантия</a></li>
                <li><a href="<?=getTemplateLink(array('chpu'=>'punkty-samovyvoza'), 'page');?>">Самовывоз</a></li>
                <!--li><a href="<?=getTemplateLink(array('chpu'=>'o-magazine'), 'page');?>">О магазине</a></li-->
                <li><a href="<?=getTemplateLink(array('chpu'=>'kontakty'), 'page');?>">Контакты</a></li>
                <li><a href="<?=getTemplateLink(array('chpu'=>'wholesale'), 'page');?>">Оптовикам</a></li>
			</ul>	
			<div class="right_block top_line">
			<!--
			<a href="https://market.yandex.ru/shop/356620/reviews" class="rating_m"><img src="/img/rating.jpg" alt="rating"></a> 
			-->
				<a href="#" class="call_back">Заказать звонок</a>
				<div class="conta_bask"><a href="/cart/" class="small_basket">
					<span id="basket-summ-products-text"><?//= declOfNum($summCount, array('товар', 'товара', 'товаров')) ?>Корзина </span>  
					<span id="basket-summ-products"><?php echo $summCount; ?></span></a>
					<div id="dropBasket">
						<ul>
						<?php foreach($productsCart as $productCart){ ?>
							<li>
								<a href="/product/<?=$productCart['id'] ?>" class="img-cont">
								<img src="<?=getImageWebPath('product_small') . $productCart['id']?>.jpg" alt="<?=$productCart['title'] ?>"></a>
									<a href="/product/<?=$productCart['id'] ?>" class="name"><?=$productCart['title'] ?>
								</a>
								<span class="price"><span><?=moneyFormat($productCart['price']) ?></span> р.</span>
							</li>
						<?php } ?>
						</ul>
						<div id="itog">
							<span id="oplata_small">Итого к оплате: <span><?=$totalPrice ?>  р.</span> </span>
							<span id="delivery_small">Доставка: <span><?=$deliveryText ?></span></span>
							<a href="/cart/" class="oform">Перейти в корзину</a>
						</div>
						
					</div>
				</div>
			</div>
		</div>
</div>
    <?php $search_string = isset($search_string) && $search_string ? $search_string : 'Поиск товаров' ;  ?>
    <div class="top_scroll default">
        <div class="inner_scroll">
            <div class="logo"><a href="/"></a></div>
            <div class="search">
                <form action="/search" method="get" role="search">
                    <input id="searchBox" type="text" name="search" placeholder="<?=$search_string?>" >
                    <input type="submit" name="ok">
                </form>
            </div>
            <div class="phone">
                <span class="phone"><span class="text_red">+7 (499)</span> 653-70-14</span>
            </div>
            <div class="basket">
                <div class="conta_bask"><a href="/cart/" class="small_basket">
                        <span id="basket-summ-products-text">Корзина </span>
                        <span id="basket-summ-products" class="productsCount"><?php echo $summCount; ?></span></a>
                </div>
            </div>
        </div>
    </div>
<div id="bottom_line">
	<div class="inner">	
		<a href="/"><img src="/frontend/img/logo_lux.png" alt="logo"></a>
		<div class="search_block">
		<form action="/search" method="get" role="search">

			<input id="searchBox" type="text" name="search" placeholder="<?=$search_string?>" >
			<input type="submit" name="ok">
		</form>
		<ul id="search_sugestion_1"></ul>
		</div>

		<!--<<div class="phone_block">
				<span class="text_clock">Звонок по России бесплатный</span><br>
				<!--<span class="phone"><span class="text_red">8 (800)</span> 555-78-65</span>
		</div>-->

		<div class="phone_block">
				<span class="text_clock">Часы работы: 9:00-22:00 Мск</span><br>
				<span class="phone"><span class="text_red">+7 (499)</span> 653-70-14</span>  
		</div>
		
	</div>
</div>						
</header>
<div id="content">
	<div class="inner">
		
		
	</div>
</div>

 
                            
                                <?php 
								$menu = getMenu(true);
						//	var_dump($tab[0]); //echo "fdfd";
                                    switch ($tab[0]) {

                                        case 'page':

                                            include('page.tpl.php');
                                            break;
                                        case 'wholesale':
                                            include('wholesale.tpl.php');
                                            break;
                                         
                                        case 'category':
                                            include('cat.tpl.php');
                                            break;
										 case 'tags':
                                            include('cat.tpl.php');
                                            break;	
										 case 'mcat':
                                            include('mcat.tpl.php');
                                            break;	
                                        
                                        case 'product':
                                            include('product.tpl.php');
                                            break;

                                        case 'main': 
                                            include('main.tpl.php');
                                            break;
											
										case 'cart': 
                                            include('finish.tpl.php');
                                            break;	
										case 'search': 
                                            include('search.tpl.php');
                                            break;
                                        default:
                                            include('404.tpl.php');
                                            break;
                                    }?>
    
<footer>
	<div id="top_footer">
		<div class="inner">
		<div class="soc_work">
		Beurer-lux в соц. сетях:
		</div>

			<ul>
								<li>
									<li><a href="<?=getTemplateLink(array('chpu'=>'dostavka-i-oplata'), 'page');?>">Доставка</a></li>
										<li><a href="<?=getTemplateLink(array('chpu'=>'oplata'), 'page');?>">Оплата</a></li>
								<li><a href="<?=getTemplateLink(array('chpu'=>'garantiya'), 'page');?>">Гарантия</a></li>
									<!--li><a href="<?=getTemplateLink(array('chpu'=>'o-magazine'), 'page');?>">О магазине</a></li-->
							
			</ul>	
			
			<ul>
								<li>
								
									<li><a href="<?=getTemplateLink(array('chpu'=>'punkty-samovyvoza'), 'page');?>">Самовывоз</a></li>
									<!--li><a href="<?=getTemplateLink(array('chpu'=>'o-magazine'), 'page');?>">О магазине</a></li-->
									<li><a href="<?=getTemplateLink(array('chpu'=>'kontakty'), 'page');?>">Контакты</a></li>
			</ul>	
			<div class="contact_footer">
				<span>+7 (499) 653-70-14</span><br>
				<span>info@beurer-lux.ru</span>
                <a href="javascript:void(0);" class="to_mobile">Мобильная версия</a>
			</div>
			
			
			  
		</div>
		
		
	</div>
	<div id="bot_footer">
		<div class="inner">
			<div class="copyright">© 2016. Официальный представитель<br> «Beurer-lux».</div>
			
			<div class="visa"><img src="/frontend/img/master_card.png" alt="master_card"><img src="/frontend/img/visa.png" alt="master_card"></div>
		</div>
	</div>
</footer>

                          

   <div style="display:none;">
			<div class="answers">
				<form>
				<span class="zagol">Ответы на все вопросы!</span>
				<p>Оставьте заявку и наш специалист ответит на все ваши вопросы</p>
					<input type="text" value="" placeholder="Имя" name="name">
					<input type="text" value="" placeholder="+ 7 (___) ___-__-__" name="phone">
					<input type="submit" value="Отправить" class="call_back_order">
				</form>
			</div>
   </div>
   
   	<div class="quickBuy mfp-hide"  id="linkBuyClick">
           
            <h2>Быстрый заказ</h2>
            <div class="left">
			<div class="cont_link">
				<a href="" class="img-cont">  <img src="<?= getImageWebPath('product_small') . $product['id']; ?>.jpg" alt="s"></a>
                <a href="" class="name"> </a>
			</div>
				
				<span class="price"><span>6 500</span> руб.</span>
				<p>Указажите Ваше имя и номер телефона, наши операторы свяжутся с Вами и уточнят условия доставки в ближайшее время!</p>
            </div>
			<div class="left">
            <form id="fastorder-form" action="#" method="post">
                <input type="text" name="name" placeholder="Ваше Имя:">
                <input type="tel" name="phone" placeholder="Ваш Телефон:">
				<input type="hidden" name="product_id" value="173" id="prod_id">
                <input type="hidden" name="comp" value="">
                <button type="submit" class="oneClickBuy">Оформить заказ</button>
            </form>
			  </div>
    </div>
	
	 <div class="quickBuy mfp-hide"  id="add_product_form">
           
            <h2>Товар добавлен в корзину!</h2>
            <div class="left">
			<div class="cont_link">
			<a href="" class="img-cont"><img src="<?= getImageWebPath('product_small') . $product['id']; ?>.jpg" alt="s"> </a>

                <a href="" class="name"> </a>
			</div>
				
				<span class="price"><span>6 500</span> руб.</span>
				<a href="#" class="next">Продолжить покупки</a>
            </div>
			<div class="left">
				<div class="basketInform">
				<span class="col">В корзине <span ></span></span><br>
				<span class="sum">Сумма покупки <span ></span></span>
                <a href="/cart/" class="oform">Оформление заказа</a>
				</div>
			  </div>
    </div>

   <noscript>
   <div>
    <img src="/watch/10893718" style="position:absolute; left:-9999px;" alt="" />
   </div>
   </noscript>
   <!--  END -->     

<!-- Scripts -->
 
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter37425065 = new Ya.Metrika({
                    id:37425065,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/37425065" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<div class="hide_cont">
	<div class="form_pop white-popup mfp-hide">
		<p>Товар добавлен в корзину!</p>
		<a href="/cart/" class="small_basket">Перейти в корзину!</a>
	</div>
</div>

<div class="quickBuy mfp-hide"  id="ans">
           
            <h2>Заказать звонок</h2>
			
			
            <form>
			 <div class="left">
                <input type="text" name="name" placeholder="Ваше Имя:">
				 </div>
			 <div class="left">	 
                <input type="tel" name="phone" placeholder="+ 7 (__) ___-__-__">
			</div>
             
            
			 
            <div class="left">
			
				<p>Оператор перезванивает в рабочее время в течение 2 часов</p>
            </div>
			 <div class="left">
			   <button  type="submit" class="call_back_order">Заказать звонок</button>
			</div>
			</form>
    </div>
	
	
</body>
</html>