<?php
$my_ip = $_SERVER['REMOTE_ADDR'];
$debug = false;
if ($my_ip == "217.194.255.193") {
	$debug = true;
}



	

?>
<script>
  $(document).ready(function() {
      $('.sbOptions').last().find('li').eq(1).hide();
  });
</script>
<?php include("blocks/bread.tpl.php"); ?>
<div id="basket_fon">
  <div class="inner">

    <div class="container with-theme-delimiter">
      <h1 class="with-theme-delimiter">Товары в корзине</h1>
    </div>

    <div class="container">

      <?php

?>

      <script>var orderPrice = {
          "discount": <?=!empty($discount['value'])?$discount['value']:0;?>,
          "summ": <?=$priceSumm;?>,
          "delivery": <?=($deliveryPrice === false ? 'false' : $deliveryPrice);?>,
          "total": <?=$total;?>
        };</script>
		 <div class="right">
		 
	  	<div class="advantages">
					<ul>
						<li>Бесплатная доставка по Москве от <?=Config::get('site.delivery_price_limit') ?> руб.</li>
						<li>Обмен и возврат в течении 14 дней</li>
						<li>Официальные сертификаты на товары</li>
						<li>Удобные способы оплаты: Visa, Mastercard</li>
					</ul>
	 
				</div>
	  </div>
      <div class="left">
        <div id="cart-items" class="cart">
          <div class="head">
            <table>
             <th class="photo"> <!--Фото--></th>
              <th class="name">Товар</th>
              <th class="price"><!--Цена--></th>
              <th class="count">Кол-во</th>
              <th class="sum">Цена</th>
              <th class="delete"></th>
            </table>
          </div>

          <?php foreach ($productsCart as $product) {?>
            <div class="row basket-item rounded">
              <div class="rowIn splashy rounded">
                <table>
                  <td class="photo"><a href="/product/<?=$product['id'] ?>"><img
                        src="<?=getImageWebPath('product_small') . $product['id']?>.jpg"
                        alt="<?=$product['title']?>"></a></td>
                  <td class="name"><a href="/product/<?=$product['id'] ?>"><?=$product['title']?></a>
                  </td>
                  <!--<td class="price"><span><?=moneyFormat($product['price_after_discount']);?></span> руб.</td>-->
                  <td class="count"><a href="javascript:void()" class="k_dec k_font">-</a>
				  <input type="text" class="amount-input" name="amount" value="<?=$product['amount']?>">
				  <a href="javascript:void()" class="k_inc k_font">+</a>
				  </td>
                  <td class="sum">
                    <span><?=moneyFormat(round($product['price_after_discount'] * $product['amount']));?></span> руб.
                  </td>
                  <td class="delete"><a href="#" class="deleteLink">×</a></td>
                </table>
                <input type="hidden" name="price" value="<?=$product['price_after_discount']?>"/> <input type="hidden"
                                                                                                           name="id"
                                                                                                           value="<?=$product['id']?>"/>
              </div>
            </div>
          <?php }?>
        </div>
		<div class="itog">
		   <div class="summ_Price">Итоговая стоимость:
                    <span class="num" id="totalItemPrice"><?=moneyFormat($priceSumm);?> руб.</span>
                   </div>
	
		Доставка:
                    <span class="num"><?=$deliveryText?></span>
                    <span class="unit" style="<?=$deliveryUnitShow ? '' : 'display:none;'?>"></span>
		</div>			
		
		<h2>Оформление заказа</h2>
        <div class="order">
          <div class="orderIn rounded">

            <form id="order-form" action="" method="post">
			<div class="main_date">	<input class="rounded" type="text" name="name" placeholder="ФИО, Ваше имя: *">
				<input class="rounded" type="tel" name="phone" placeholder="Телефон: *">
				<input class="rounded" type="email" name="email" placeholder="E-mail:"></div>
              <div class="">

              <!-- <table class="overall" id="overall">
                  <tr>
                 
                  </tr>
                  <tr class="delivery msk">
                    
                  </tr>
                  <tr class="delivery msk-region" style="display: none;">
                    <th>Доставка:</th>
                    <td style="width: 130px;font-size: 13px;font-weight: normal;position: absolute;right: 0px;"><span class="section">Базовая <span class="value">1 200</span> руб<br/> + </span>30 руб за каждый км
                      от МКАД
                    </td>
                  </tr>
                  <tr class="delivery individual" style="display: none;">
                    <th>Доставка:</th>
                    <td style="width: 130px;font-size: 13px;font-weight: normal;position: absolute;right: 0px;">Расчитывается индивидуально</td>
                    <td></td>
                  </tr>
                  <tr id="discount-field" style="<?=($discountValue <= 0 ? 'display:none;' : '')?>">
                    <th>Скидка:</th>
                    <td class="num" id="cartDiscountPersent"><?=$discountValue?></td>
                    <td>%</td>
                  </tr>
                  <tr id="total-summ">
                      <th>Итого:</th>
                      <td class="num"><?=moneyFormat($total)?></td>
                      <td>руб.</td>
                  </tr>
                </table>-->
				
				
                <div class="deliveryMethod">
                  <div class="caption">Способ доставки</div>
                  <!--<select name="delivery_type">-->

                    <?php foreach ($delivery as $d) {?>
					
					  <input id="<?=$d['id']?>" type="radio" name="radio_delivery" value="<?=$d['id']?>" <?=($d['id'] == 1 ? 'checked="checked"' : '')?>>

						<label for="<?=$d['id']?>"><?=$d['name']?></label>
						
                      <!--<option data-id="<?=$d['id']?>" value="<?=$d['id']?>"
                              name="delivery_type" <?=($d['id'] == 1 ? 'selected="selected"' : '')?>><?=$d['name']?></option>-->
                    <?php }?>
                 <!-- </select>-->
                </div>
               <!-- <div class="deliveryMethod">
                  <div class="caption">Способ оплаты:</div>
                  <select name="oplata_type">
                    <?php foreach ($oplata as $op) {?>
                      <option value="<?=$op['id']?>" name="oplata_type"><?=$op['name']?></option>
                    <?php }?>
                  </select>
                </div>-->
                <!--<div class="deliveryMethod">
                  <div class="caption">Дата доставки и время:</div>
				<input name="date" id='date' type="date" style=" width: 135px; "><label>C</label><input type="time" name="time_with"><label>До</label><input type="time" name="time_to">
                </div>-->
                <a href="#" class="doneBtn">Продолжить</a>


              </div>

              <div class="orderForm">

               
                <div class="">
                 <div class="caption">Адрес доставки:</div>
                   

                    <p><input class="rounded" type="text" name="adress" placeholder="Город"></p>
					
                    <p><input class="rounded" type="text" name="post_index" placeholder="Улица, дом, кварира"></p>

                    <p><textarea class="rounded" name="extra_information"
                                 placeholder="Пожелания к заказу:"></textarea></p>
                       <div class="details">
                        <span class="add-details">Прикрепить реквизиты (pdf,doc):</span>
                        <input type="file" name="details" id="details">
                       </div>


                    <div class="btnWrap">
                      <div class="loading" style="display: none; margin-left: 50px;"><img
                          src="/assets/gfx/ajax-loader.gif" alt="Обработка..."/></div>
                      
                    	   	<div class="itog">
		   <div class="summ_Price">Итого к оплате:
                    <span class="num" id="totalBasketPrice"><?=moneyFormat($totalPrice);?> руб.</span>
                   </div>
	
		Доставка:
                    <span class="num"><?=$deliveryText?></span>
                    <span class="unit" style="<?=$deliveryUnitShow ? '' : 'display:none;'?>"></span>
		</div>
			  <button type="submit">Заказать
                      </button>
                  </div>
                </div>

              </div>
		
            </form>
          </div>
		 	
		
        </div>
      </div>
	 

    </div>
  </div>
</div>
<!-- / Content -->

