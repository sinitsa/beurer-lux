<?php  include("blocks/bread.tpl.php");  ?>
<?php $product['video_link'] = preg_replace('/.*\/embed\/(.*?)&quot;.*/','$1',$product['video_link']); ?>
<?php //header('Access-Control-Allow-Origin: *'); ?>
<div id="product_cont" itemscope itemtype="http://schema.org/Product">
	<div class="inner">
		<div class="right_product">
			<div class="right_block1" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				<div class="price"><span class="label">Цена:</span>
                    <span class="num"><?=moneyFormat($product['price']) ?> <meta itemprop="priceCurrency" content="RUB">руб.</meta></span>
                    <span  itemprop="price" content="<?=$product['price']?>"></span>
                </div>
				<div class="availability">
					<?php if($product["active_rests"] > 0 ){ ?>
					
							<span class="green_text">Есть в наличии</span>
						
					<?php } else { ?>
						
								<!-- <span class="grey_text">Под заказ</span>-->
					<?php } ?>
				</div>
				<div class="buy_buttons">
					<a class="buttonInBasket" data-product_id="<?=$product["id"]; ?>" data-product_price="<?=$product["price"]; ?>" data-name="<?=$product["title"];?>" onclick="try { rrApi.addToBasket(<?=$product["id"]; ?>) } catch(e) {}">В корзину</a>
					<a class="oneLinkBuy" data-product_id="<?=$product["id"]; ?>" data-product_price="<?=$product["price"]; ?>" data-name="<?=$product["title"]; ?>">Купить в 1 клик</a>
				</div>
			</div>
			<div class="right_block1">
				<div class="delivery">
					
					<table>
						<tr>
							<td>Доставка, Москва</td>
						</tr>
						<tr>
							<td>Курьером </td>
							<td>
							<?
							if($product["price"]>Config::get('site.delivery_price_limit'))
							{
								
								echo "бесплатно";
							}
							else
							{
								
								echo Config::get('site.delivery_price')." руб.";
							}			
							?> 
							</td>
						</tr>
						<tr>
							<td>Срок доставки </td>
							<td>1-2 дня </td>
						</tr>
						<!--<tr>
							<td>Пункт выдачи </td>
							<td>520 руб.</td>
						</tr>-->
					</table>
	
				</div>
			</div>
			<div class="right_block1">
				<div class="advantages">
					<ul>
						<li>Бесплатная доставка по Москве от <?=Config::get('site.delivery_price_limit') ?> руб.</li>
						<li>Обмен и возврат в течении 14 дней</li>
						<li>Официальные сертификаты на товары</li>
						<li>Удобные способы оплаты: Visa, Mastercard</li>
					</ul>
	 
				</div>
			</div>
			<div class="answers">
				<form>
				<span class="zagol">Ответы на все вопросы!</span>
				<p>Оставьте заявку и наш специалист ответит на все ваши вопросы</p>
					<input type="text" value="" placeholder="Имя" name="name">
					<input type="text" value="" placeholder="+ 7 (___) ___-__-__" name="phone">
					<input type="submit" value="Отправить" class="call_back_order">
				</form>
			</div>
		</div>
		<div class="left_product">
				 <div class="container with-theme-delimiter">
				<h1 class="with-theme-delimiter" itemprop="name"><?=$product['title'] ?></h1>
					<div class="fhoto">
						<div class="beurer-icon"></div>
						<div class="big_fhoto_cont">
							<a itemprop="image" href="/upload/<?=$product['id'] ?>.jpg" class="full_fhoto">
							<img id="img-current_picture" src="/upload/<?=$product['id'] ?>.jpg" title="<?=$product['title'] ?>" alt="<?=$product['title'] ?>" border="0">
							</a>
						</div>
						<div class="small_fhoto_cont">
							<ul>
								  <li class="selected"><a class="rounded" data-original="<?= getImageWebPath('product_original') . $product['id']; ?>.jpg"
										 href="<?= getImageWebPath('product_preview') . $product['id']; ?>.jpg"><img
										src="<?= getImageWebPath('product_small') . $product['id']; ?>.jpg" title="<?= $product['title']; ?>" alt="<?= $product['title']; ?>"></a></li>
								  <? foreach ($product['extra_photo'] as $id) { ?>
									<li><a class="rounded" data-original="<?= getImageWebPath('product_extra_original') . $id; ?>.jpg"
										   href="<?= getImageWebPath('product_extra_medium') . $id; ?>.jpg"><img
										  src="<?= getImageWebPath('product_extra_preview') . $id; ?>.jpg" alt="" class="rounded"></a></li>
								  <? } ?>
							</ul>
							<span id="cl_left"></span>
							<span id="cl_right"></span>
						</div>
					
					</div>
				<div class="prem_block">
					<div class="cont_hidden">
				<?php if($product['video_link']) { ?>
						<div class="video_small">
							<a href="http://www.youtube.com/watch?v=<?=$product['video_link'] ?>">
							<img src='https://i.ytimg.com/vi/<?=$product['video_link'] ?>/hqdefault.jpg'>
							<div class='podl'></div>
						</a>
						</div>
			
				<?php } ?>
				
				<?php if($product['instruction']) { ?>
						<div class="instruct_small">
						<a class="instruct" href="/upload/files/<?php echo $product['instruction']; ?>" target="__blank"><?php echo $product['instruction']; ?></a>
						</div>
			
				<?php } ?>
					</div>
				<p class="dop_info">Вы можете купить <?=$product['title'] ?> в магазине Beurer-lux по доступной цене. <?=$product['title'] ?>: описание, фото, характеристики, отзывы покупателей, инструкция и аксессуары.</p>
				
				</div>
			</div>
		
			<?php ?>
			<div id="tabs">
			   <ul>
				  <li><a href="#tabs-1">Описание </a></li>
				  <li><a href="#tabs-2">Характеристики</a></li>
				  <li><a href="#tabs-3">Отзывы </a></li>
				  <?php if($product['video_link']) { ?>
				  <li><a href="#tabs-4">Видео </a></li>
				  <?php } ?>
				  <?php if($product['instruction']) { ?>
				 <li><a href="#tabs-5">Инструкция </a></li>
				  <?php } ?>
			   </ul>
			   <div id="tabs-1">
				 <?php echo $product['des']; ?>
			   </div>
			   <div id="tabs-2" itemprop="description">
				<div class="teh_har">
				 <?php echo html_entity_decode($product['like_desc']); ?>
				 </div>
			   </div>
			   <div id="tabs-3">
				  <noindex>
				  	<div id="mneniyapro_feed"><a href="//mneniya.pro">Mneniya.Pro</a></div>
				  	<script type="text/javascript" src="//dev.mneniya.pro/js/beurer-luxru/mneniyafeed.js"></script>
				  	<div class="mp-prod_id" style="display:none;"><?= $product["id"] ?></div>
					<div class="mp-prod_name" style="display:none;"><?= $product['title'] ?>f</div>
				  	<?php //echo $product['comments']; ?>
				  </noindex>
			   </div>
			   <?php if($product['video_link']) { ?>
			     <div id="tabs-4">
				 <iframe width="560" height="315" src="https://www.youtube.com/embed/<?=$product['video_link'] ?>" frameborder="0" allowfullscreen></iframe>
			   </div>
			    <?php } ?>
				<?php if($product['instruction']) { ?>
			    <div id="tabs-5">
				  <p> <a class="instruct" href="/upload/files/<?php echo $product['instruction']; ?>" target="__blank"><?php echo $product['instruction']; ?></a></p>
			   </div>
			    <?php } ?>
			  
			</div>
			<div class="personal_block">
				<h2>Персональные рекомендации</h2>
				<div id="block_similar">
				</div>
			</div>
				<!--<div class="rr-widget" 
		 data-rr-widget-product-id="<?=$product['id'] ?>"
		 data-rr-widget-id="571e39491e99470aace8942d"
		 data-rr-widget-width="100%"></div>-->
		</div>
		
	</div>

</div>

<script type="text/javascript">
	rrApiOnReady.push(function() {
		try{ rrApi.view(<?=$product['id'] ?>); } catch(e) {}
	})
	$(function(){
			$(".personal_block").hide();			
			$.ajax({
				type: "POST",
				url: "/ajax/retail_hack.php",
				data:"idPr=<?=$product['id'] ?>",
				// передача в качестве объекта
				// поля будут закодированые через encodeURIComponent автоматически
				success: function(msg){
					if(msg!="")
					{
					$("#block_similar").html(msg);
					$(".personal_block").show();		
					}
					
					//console.log(msg);
				}
			});

		
	});
	
</script>


