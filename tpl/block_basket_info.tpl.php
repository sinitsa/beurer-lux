<?php
$summPrice = isset($_COOKIE['total_amount']) && !empty($_COOKIE['total_amount']) ? htmlspecialchars($_COOKIE['total_amount']) : 0;
$summCount = 0;
foreach (explode('|', $_COOKIE['cart_products']) as $productData) {
    $temp = explode(':', $productData);
    $summCount += $temp[1];
}
?>

			    <!-- Small cart -->
			    <div id="small-cart">
				<!-- optional class "inactive"-->
				<a href="<?= getTemplateLink(array('chpu' => 'finish'), 'cart'); ?>" class="inactive">
				    <dl>
					<dt>Ваша корзина:</dt>
					<dd>
					    <span>В корзине: <span id="basket-summ-products"><?php echo $summCount; ?></span> <span class="text" id="basket-summ-products-text"><?= declOfNum($summCount, array('товар', 'товара', 'товаров')) ?></span></span>
					    <span>На сумму: <span id="basket-summ-price"><?= moneyFormat($summPrice); ?></span> руб.</span>
					</dd>
				    </dl>
				</a>
			    </div>
			    <!-- / Small cart -->
