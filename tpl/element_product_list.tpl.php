<?php
$discount = '';
if ($product['discount_value'] > 0) {
    if ($product['discount_type'] == 'percent') {
        $discount = '-' . $product['discount_value'] . '%';
    } elseif ($product['discount_type'] == 'value') {
        $discount = '-' . getPercentForDiscount($product['price'], $product['discount_value']) . '%';
    }
}
$hadOldPrice = $product['discount_value'] > 0;
$product['deliveryPrice'] = deliveryPrice::getByCatAndPrice($product['cat'], $product['price_after_discount'], $product['id']);
?>					<li class=" rounded"> <!-- <? //print_r($product); ?> -->
    <div class="item rounded splashy">
        <div class="itemIn">
            <!--div class="timer">
                <ul>
                    <li>
                        <span class="num">5</span> дней
                    </li>
                    <li>
                        <span class="num">18</span> часов
                    </li>
                    <li>
                        <span class="num">58</span> минут
                    </li>
                    <li>
                        <span class="num">02</span> секунд
                    </li>
                </ul>
            </div-->
            <div class="caption">
                <a href="<?= getTemplateLink($product, 'catalog'); ?>"><?= $product['title']; ?></a>
            </div>
            <div class="pic">
                <a href="<?= getTemplateLink($product, 'catalog'); ?>">                    
                    <? if ($product['sale']) { ?>

                        <span class="badge-sale"></span>
                    <? } elseif ($product['best']) { ?>

                        <span class="badge-hit"></span>
                    <? } ?>

                    <img src="<?= getImageWebPath('product_medium') . $product['id']; ?>.jpg" alt="<?= $product['title']; ?>">
                </a>
            </div>

            <?php
            $prShuffleEq = getRndItemPans(rand(3, 4));
            ?>

            <div class="panels">
                <?php foreach ($prShuffleEq as $prShuffleEq_key => $prShuffleEq_value) {
                    ?>

                    <div class="panels-sprite <?= $prShuffleEq_key; ?>"><span><?= $prShuffleEq_value; ?></span></div>
                <?php }
                ?>
            </div>

            <? unset($prShuffleEq); ?>

            <div class="pricebox">
                <? if ($product['discount_value'] > 0) { ?>
                    <dl class="oldprice">
                        <dt>Старая цена:</dt>
                        <dd><span> <?= moneyFormat($product['price']) ?> </span> руб.</dd>
                    </dl>
                <? } ?>
                <dl class="price">
                    <dt>Цена:</dt>
                    <dd><span><?= (moneyFormat($product['price_after_discount'])) ?></span> руб.</dd>
                </dl>
            </div>

            <div class="buyLinkWrap">
                <a data-product_id="<?= $product['id'] ?>" data-product_price="<?= $product['price_after_discount'] ?>" href="<?= getTemplateLink(array('chpu' => 'finish'), 'cart'); ?>" class="buyLink buy-link">В корзину</a>
            </div>

            <ul class="list-inline tips">
                <li style="width:30px;">
                    <? if (($product['rests_main']['summ'] > 0)||($product['fake_in_stock'] == 1)) { ?>

                        <img src="/assets/gfx/icons/instock-small.png" alt="Есть в наличии">
                    <? } else { ?>

                        <img src="/assets/gfx/icons/no-instock-small.png" alt="На заказ">
                    <? } ?>

                </li>
                <li>
                    <? if (!empty($product['like_desc'])) { ?>

                        <img src="/assets/gfx/icons/video-small.png" alt="Видеоролик">
                    <? } else { ?>

                        <img src="/assets/gfx/icons/no-video-small.png" alt="Нет видео">
                    <? } ?>

                </li>
                <li class="last-item">
                    <? if ($product['deliveryPrice'] === 0) { ?>

                        <img src="/assets/gfx/icons/freedel-small.png" alt="Бесплатная доставка по Москве">
                    <? } else { ?>

                        <img src="/assets/gfx/icons/no-freedel-small.png" alt="Доставка платная">
                    <? } ?>
                </li>
            </ul>

        </div>
        <div class="description">
            <?= preg_replace('/<a href=\"(.*?)\">(.*?)<\/a>/', "\\2", getShortDesc($product, SHORT_IF_EXISTS_ELSE_CROP, 300)) ?>			
        </div>
    </div>
</li>