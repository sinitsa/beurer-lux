
<?php
//echo $tab ; ?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<title><?php echo $seo_title; ?></title>
<meta name="keywords" content="<?php echo $seo_key; ?>">
<meta name="description" content="<?php echo $seo_des; ?>">
<link href="/css.css" rel="stylesheet" type="text/css">
<!--[if lte IE 6]>
<link href="/cssie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if lte IE 7]>
<link href="/cssie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<link rel="SHORTCUT ICON" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">


<!--JQuery<script src="http://cdn.jquerytools.org/1.1.2/jquery.tools.min.js"></script>


 -->
<script type="text/javascript" src="http://yandex.st/jquery/1.6.0/jquery.min.js" ></script>

<?php if ($tab == "catalog")
{ echo ('<script src="/js/jquery.watermarkinput.js" type="text/javascript"></script>
');}
?>
<?php if ($tab == "catalog")
{ echo ('<script type="text/javascript" src="/js/form-validator.js" ></script>
<script type="text/javascript" src="/js/jquery.lightbox-0.5.min.js" ></script>
<link href="/css/jquery.lightbox-0.5.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript">
    $(function() {
        $(\'#gallery a\').lightBox();
    });
</script>
');}
?>
<?php if ($tab == "finish1" or $tab == "finish2" or $tab == "finish3" or $tab == "feed" or $tab == "obratnyi")
{ echo ('<script type="text/javascript" src="/js/form-validator-all.js" ></script>
');}
?>
<?php if ($tab == "korzina")
{ echo ('<script type="text/javascript" src="/js/jquery.cookie.js" ></script>');}
?>
 

<!-- Корзина -->
    <script type="text/javascript">
            $(function () {
                read_cart();
                update_cart_info();
                $('.pay_button').click(function() {
                    //Вытаскиваем необходимые координаты
                    var x0 = $(".fly_container").offset().left;
                    var y0 = $(".fly_container").offset().top;
                    var x = $("#korz_image").offset().left;
                    var y = $("#korz_image").offset().top;
                    var width = $("#korz_image").width();
                    var height = $("#korz_image").height();

                    $('.flyimage').css('top', y0);
                    $('.flyimage').css('left', x0);

                    dx = x0-x;
                    dy = y0-y;

                    //Скроллим к верху
                    pagetop = $('html').scrollTop();
                    $("html").animate({ scrollTop: 0}, {
                        complete: function () {
                            //Первоначально раздуваем
                            $('.flyimage').animate({
                                width: "+=50"
                            })

                            //Сворачиваем к корзине
                            $('.flyimage').animate({
                                opacity: 0.3,
                                left: "-="+dx,
                                top: "-="+dy,
                                width: width,
                                height: height
                            }, "fast")

                            //Исчезновение
                            $('.flyimage').animate({
                                opacity: "0"
                            }, "very fast");

                            //Анимация корзины
                            $("#korz_image").delay(400);
                            $("#korz_image").animate({width: "+=20" });
                            $("#korz_image").animate({width: "-=20" },{

                                complete: function () {
                                    $("html").animate({ scrollTop: pagetop});
                                }

                            }
                        );
                            
                        }
                    });


                    var product_id = $('#product_id').attr('value');
                    cart_add(product_id, <?php 
							 if ($catalog_price == "") 
							 $catalog_price = 0; 
							 echo $catalog_price; ?>);
                    cart_save();
                    update_cart_info();
                    return false;
                });
            });

            function read_cart()
            {
                product_count = 0;
                total_amount = parseInt(getCookie('total_amount'));
                if (!total_amount) total_amount = 0;
                cart_products = new Array();
                var products_string = getCookie('cart_products');

                var regex = /([\d]+):([\d]+)/g;
                var match;
                //match = regex.exec("234:3|845:2");
                while (match = regex.exec(products_string)){
                    cart_products[parseInt(match[1])] = parseInt(match[2]);
                    product_count+= parseInt(match[2]);
                }
            }

            function update_cart_info()
            {
                $("#total_cart_amount").text(total_amount);
                $("#total_cart_products").text(product_count);
            }

            function cart_add(product_id, sum)
            {
                if (cart_products[product_id] > 0)
                    cart_products[product_id]++
                else
                    cart_products[product_id] = 1;
                product_count++;
                total_amount+= sum;
            }
			
			function setCookie (name, value, expires, path, domain, secure) {
      document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}


			function cart_save()
            {
                products_string = '';
                for (var i=0; i<cart_products.length; i++)
                    if (cart_products[i] > -1)
                {
                    products_string+= i + ':' + cart_products[i] + '|';
                }
                
setCookie('cart_products', products_string, "", '/');
setCookie('total_amount', total_amount, "", '/');
            }

            function getCookie(c_name)
            {
                if (document.cookie.length>0)
                {
                    c_start=document.cookie.indexOf(c_name + "=");
                    if (c_start!=-1)
                    {
                        c_start=c_start + c_name.length+1;
                        c_end=document.cookie.indexOf(";",c_start);
                        if (c_end==-1) c_end=document.cookie.length;
                        return unescape(document.cookie.substring(c_start,c_end));
                    }
                }
                return "";
            }
        </script>
        
<!-- Автоподсказки  -->        
<?php if ($tab == "search")
{ echo ('<script type="text/javascript" src="/js/jquery.autocomplete.js"></script>');}
?>
<?php if ($tab == "korzina")
{ echo ('<script type="text/javascript" src="/js/jquery.cookie.js" ></script>');}
?>



</head>
<body>


<div style="width:100%;" align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="max-width:1280px; min-width:1075px;">
  <tr>
    <td width="20" class="logo_left"><div style="width:20px;">&nbsp;</div></td>
    <td><table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td height="14"></td>
      </tr>
      <tr>
        <td height="72" bgcolor="#ABE400" class="top_rast"><table width="100%" height="72" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="225" align="left"><a href="/"><img src="/img/logo.gif" alt="Интернет - магазин &quot;Мед-Сердце&quot;" width="232" height="72" border="0"></a></td>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="33%" align="center">
                  <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td><img src="/img/tel.gif" width="36" height="36"></td>
                      <td class="tel"><?php 
					  $hour = date ("H"); 
					  if ($hour == "19" or $hour == "20" or $hour == "21" or $hour == "22") 
					  { echo ('&nbsp; 8 (926) 856-64-53');} 
					  else 
					  {echo $config_tel;} ?></td>
                    </tr>
                  </table></td>
                <td width="30%" align="center" class="main"><div align="left" style="width:280px;"><b>Режим работы <?php echo $config_rezhim; ?> по МСК</b><br>
                  <a href="/obratnyi/zvonok.html">Не можете дозвониться? Мы перезвоним!</a>
                </div></td>
                <td align="right" class="txt_dost" >Доставка по России и СНГ !</td>
                </tr>
            </table></td>
            <td width="25">&nbsp;</td>
            <td width="6" align="right"><img src="/img/top_left.gif" width="6" height="72"></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="89"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="25%" align="center"><table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="98" align="left"><a href="/pay/korzina.html"><img src="/img/korz.gif" name="korz_image" width="94" height="57" border="0" id="korz_image"></a></td>
                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td align="left"><b><i><span class="txt_korz"><span id="total_cart_amount"> </span> Р<br>
                                                                                <span id="total_cart_products"></span> товаров</span></i></b></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="25" align="left"><b><i><a href="/pay/korzina.html" class="txt_new">Оформить заказ</a></i></b></td>
                                                            </tr>
                                                        </table>                  <b><i><a href="/" class="txt_new"></a></i></b></td>
                                                </tr>
                                            </table></td>
            <td width="25%"  align="center"><table border="0" cellspacing="0" cellpadding="0" >
              <tr>
                <td width="32" height="30"><img src="/img/new.gif" width="26" height="27"></td>
                <td align="left" class="txt_new"><a href="/novoe/index.html">Новинки</a></td>
              </tr>
              <tr>
                <td height="30"><img src="/img/super.gif" width="23" height="24"></td>
                <td align="left" class="txt_new"><a href="/spec/index.html">Лидеры продаж</a></td>
              </tr>
            </table></td>
            <td>
            
            <?php 
switch ($tab)
{
	case "0" :
	//case "catlist" :
		include ('tpl/block_up1.tpl.php');
				 
	break;
	
	
	
	default:
	echo('<img src="/img/right_bg_w.jpg" width="567" height="89">');  
	break;
}

             ?>    
            
            
            
            
            
            
            
            
            
            </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="38" bgcolor="#ABE400" class="main_rast"><table width="100%" height="38" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="1%" align="left"><img src="/img/left.gif" width="3" height="38"></td>
            <td align="center"><span class="main"><a href="/">Каталог товаров</a></span></td>
            <td width="2%" align="center"><img src="/img/mail_line.gif" width="2" height="38"></td>
            <td align="center"><span class="main"><a href="/pay/korzina.html">Мои покупки</a></span></td>
            <td width="2%" align="center"><img src="/img/mail_line.gif" width="2" height="38"></td>
            <td align="center"><span class="main"><a href="/page/dostavka.html">Доставка и Оплата</a></span></td>
            <td width="2%" align="center"><img src="/img/mail_line.gif" width="2" height="38"></td>
            <td align="center"><span class="main"><a href="/page/skidka.html">О Скидках</a></span></td>
            <td width="2%" align="center"><img src="/img/mail_line.gif" width="2" height="38"></td>
            <td align="center"><span class="main"><a href="/page/garantiya.html">Гарантия качества</a></span></td>
            <td width="2%" align="center"><img src="/img/mail_line.gif" width="2" height="38"></td>
            <td align="center"><span class="main"><a href="/page/about.html">О компании</a></span></td>
            <td width="2%" align="center"><img src="/img/mail_line.gif" width="2" height="38"></td>
            <td align="center"><span class="main"><a href="/feed/svyaz.html">Обратная связь</a></span></td>
            <td width="2%" align="center"><img src="/img/mail_line.gif" width="2" height="38"></td>
            <td align="center"><span class="main"><a href="/page/contacts.html">Контакты</a></span></td>
            <td width="1%" align="right"><img src="/img/right.gif" width="3" height="38"></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td valign="top"><br>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td align="left" valign="top" width="248"><table width="248" border="0" cellspacing="0" cellpadding="0">
  
  
  <?php  
  
	 switch ($tab)
	{
		case "korzina" :					 
		break;
		
		case "search" :					 
		break;		
				
		default:
		include('block_search.tpl.php'); 
		break;
	}
 ?>    
  
  
                <tr>
                  <td align="left" valign="top" >			  <div  style=" width:235px; padding-left:7px;">
                    <div class="kat_div" align="left"><span class="zag_kat">Каталог товаров</span></div>
                    <ul id="my-menu">
  <?php for ($i=0; $i<count ($cat_id); $i++)	{ ?>
                      
  <div class="box">
    <div class="top">
      <div></div>
      </div>
    <li  class=" bg_li"><a href="/cat/<?php echo (''.$cat_chpu[$i].''); ?>.html"><?php echo (''.$cat_title[$i].'');?> </a>
      
      <ul>
  <?php for ($k=0; $k<count ($pod_cat); $k++)	{
	  if ($pod_cat[$k] == $cat_id[$i]){ ?>
  <li class="bg_uli"><a href="/cat/<?php echo (''.$pod_chpu[$k].''); ?>.html"><?php echo (''.$pod_title[$k].''); ?></a></li>					
  <?php	} } ?> 
        </ul>
      </li>
    <div class="bottom">
      <div></div>
      </div>
  </div>
  <?php } ?>
                      </ul>
                    </div>
                    
                    </td>
                  </tr>
                </table>
                
              </td>
              
              <td width="15"><div style="width:15px;"></div></td>
              <td valign="top" align="left">
                
  <?php include ('tpl/'.$tab.'.tpl.php'); ?>
                
              </td>
                
<?php 
switch ($tab)
{
	case "pay" :
	case "dostavka" :
	case "page" :
	case "vybor" :
	case "spec" :
	case "catalog" :
	case "novoe" :
	case "korzina" :
	case "finish1" :
	case "finish2" :
	case "finish3" :

	//case "catlist" :
	case "ok" :
	break;
	
	case "catalog2" :
	$query = ("SELECT * FROM catalog WHERE cat = $catalog_cat and novinka='1'");
$result = @mysql_query($query);
$chekis = mysql_num_rows($result);

	echo ('
              <td width="20"><div style="width:23px;"></div></td>
              <td valign="top">
			  <div  style=" width:235px; padding-left:7px;">  
			<strong>Новинки в этой категории</strong><br><br>
				  ');  
while ($line = @mysql_fetch_assoc($result)) 
{
	$title = $line['title'];
	$price = $line['price'];
	$chpu = $line['chpu'];
	$nov_id = $line['id'];

	if ($catalog_id != $nov_id){
	echo ('
		  <img src= "/upload/small/'.$nov_id.'.jpg">
			<br> <a href = "/catalog/'.$chpu.'.html">'.$title.'</a><br> '.$price.' рублей  <br><br>

				  ');  
	}
}; 

	echo (' 
                  </div>
                  </td>
				  ');  

	break;
	
	
	
	
	default:
	
	
		$query = ("SELECT * FROM catalog WHERE hit='1' LIMIT 6");
		$result = @mysql_query($query);
		while ($line = @mysql_fetch_assoc($result)) 
		{
			$hit_title[] = $line['title'];
			$hit_chpu[] = $line['chpu'];
			$hit_id[] = $line['id'];
		}
	
	
	
	echo ('
   <td width="220" valign="top">
              
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><a href="/page/dostavka.html" target="_blank"><img src="/img/dost.jpg" width="200" height="77" border="0"></a><span class="txt_skidki"><br>
                    Юбилей или День Рождения и срочно нужен подарок? Не проблема! Звоните, доставим!<br>
                    <br>
                  </span></td>
                </tr>
                <tr>
                  <td><a href="/page/skidka.html" target="_blank"><img src="/img/skidki.jpg" width="200" height="82" border="0"></a><br>
                    <span class="txt_skidki">Совершите покупку в нашем магазине на сумму свыше 5 000 рублей и  получите скидку.<br>
                    <br>
                    </span></td>
                </tr>
                <tr>
                  <td><a href="/page/garantiya.html" target="_blank"><img src="/img/garant.jpg" width="200" height="72" border="0"></a><br>
                    <span class="txt_skidki">На все наши товары мы даём гарантию более чем на год!<br>
                    <br>
                    </span></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td valign="top"><table width="220" border="0" cellspacing="0" cellpadding="0" background="/img/rast.jpg">
                    <tr>
                      <td height="113"><a href="/spec/index.html"><img src="/img/top_hit.jpg" width="220" height="113" border="0"></a></td>
                    </tr>
                    <tr>
                      <td align="center" valign="top"  class="fon_hit_top"><table width="170" border="0" cellpadding="0" cellspacing="0">
                       
						');
						
						
for ($k=0; $k<count($hit_title); $k++){						
						
						
echo ('  <tr>   

		<td align="center">
			<a href=/catalog/'.$hit_chpu[$k].'.html >
			<img src="/upload/small/'.$hit_id[$k].'.jpg"  border="0">
             </a>  
			 <br>   <a href=/catalog/'.$hit_chpu[$k].'.html >'.$hit_title[$k].'</a><br><br><br>
		</td> 
		</tr>');
}
							
					echo('		
                       
                     
                      </table></td>
                    </tr>
                    <tr>
                      <td height="69"><img src="/img/bot_hit.jpg" width="220" height="69"></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table>
              
              
              
              
              
              </td>
				  ');  
	break;
}
/* 
if    ($tab != "pay")
			{
			echo ('
              <td width="20"><div style="width:23px;"></div></td>
              <td valign="top">
			  <div  style=" width:235px; padding-left:7px;">  
			 '.$config_banner.'  
                  </div>
                  </td>
				  ');  
			} */
             ?>    
            </tr>
          </table></td>
      </tr>
      <tr>
        <td align="center" ><div style=" width:97%; padding-top:20px; padding-bottom:30px;" class="txt"><span class="zag"><h2><?php echo $seo_title_text; ?></h2></span>
          <p class="txt"> <?php echo $seo_text; ?></p></div></td>
      </tr>
      <tr>
        <td class="s_main_rast"><table width="100%" height="38" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="1%" align="left"><img src="/img/s_left.gif" width="3" height="38"></td>
            <td align="center"><a href="/">Каталог товаров</a></td>
            <td width="2%" align="center"><img src="/img/s_mail_line.gif" width="2" height="38"></td>
            <td align="center"><a href="/pay/korzina.html">Мои покупки</a></td>
            <td width="2%" align="center"><img src="/img/s_mail_line.gif" width="2" height="38"></td>
            <td align="center"><a href="/page/dostavka.html">Доставка и Оплата</a></td>
            <td width="2%" align="center"><img src="/img/s_mail_line.gif" width="2" height="38"></td>
            <td align="center"><a href="/page/skidka.html">О Скидках</a></td>
            <td width="2%" align="center"><img src="/img/s_mail_line.gif" width="2" height="38"></td>
            <td align="center"><a href="/page/garantiya.html">Гарантия качества</a></td>
            <td width="2%" align="center"><img src="/img/s_mail_line.gif" width="2" height="38"></td>
            <td align="center"><a href="/page/about.html">О компании</a></td>
            <td width="2%" align="center"><img src="/img/s_mail_line.gif" width="2" height="38"></td>
            <td align="center"><a href="/feed/svyaz.html">Обратная связь</a></td>
            <td width="2%" align="center"><img src="/img/s_mail_line.gif" width="2" height="38"></td>
            <td align="center"><a href="/page/contacts.html">Контакты</a></td>
            <td width="1%" align="right"><img src="/img/s_right.gif" width="3" height="38"></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="100" align="center"><table width="96%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="168" align="left" valign="top"><img src="/img/logo_sm.gif" width="150" height="39"></td>
            <td width="234" align="left" valign="top">&copy; 2010 Мед-Сердце.<br>
              <span class="txt_coper">Все права защищены.</span></td>
            <td width="152" align="left" valign="top"><i><img src="/img/tel_ico.gif" width="9" height="14"> <?php echo $config_tel; ?><br>
             &nbsp;&nbsp;<span class="txt_coper"> ПН-ВС, <?php echo $config_rezhim; ?></span><br></td>
            <td width="214" align="left" valign="top"><a href="http://www.flexum.ru" target="_blank">Поиск по сайту - Flexum</a></td>
            <td width="211" align="left" valign="top">

<!-- begin of Top100 code -->

<img src="http://counter.rambler.ru/top100.cnt?2142555" alt="" width="1" height="1" border="0" />

<!-- end of Top100 code -->

<!-- begin of Top100 logo -->

<a href="http://top100.rambler.ru/home?id=2142555">
<img src="http://top100-images.rambler.ru/top100/banner-88x31-rambler-gray2.gif" alt="Rambler's Top100"
width="88" height="31" border="0" /></a>

<!-- end of Top100 logo -->

&nbsp;<!--Rating@Mail.ru counter-->
<script language="javascript"><!--
d=document;var a='';a+=';r='+escape(d.referrer);js=10;//--></script>
<script language="javascript1.1"><!--
a+=';j='+navigator.javaEnabled();js=11;//--></script>
<script language="javascript1.2"><!--
s=screen;a+=';s='+s.width+'*'+s.height;
a+=';d='+(s.colorDepth?s.colorDepth:s.pixelDepth);js=12;//--></script>
<script language="javascript1.3"><!--
js=13;//--></script><script language="javascript" type="text/javascript"><!--
d.write('<a href="http://top.mail.ru/jump?from=1834088" target="_top">'+
'<img src="http://dc.cf.bb.a1.top.mail.ru/counter?id=1834088;t=235;js='+js+
a+';rand='+Math.random()+'" alt="Рейтинг@Mail.ru" border="0" '+
'height="31" width="88"><\/a>');if(11<js)d.write('<'+'!-- ');//--></script>
<noscript><a target="_top" href="http://top.mail.ru/jump?from=1834088">
<img src="http://dc.cf.bb.a1.top.mail.ru/counter?js=na;id=1834088;t=235" 
height="31" width="88" border="0" alt="Рейтинг@Mail.ru"></a></noscript>
<script language="javascript" type="text/javascript"><!--
if(11<js)d.write('--'+'>');//--></script>
<!--// Rating@Mail.ru counter-->

</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
    <td width="20"><div style="width:20px;">&nbsp;</div></td>
  </tr>
<!-- Google Analitics<script type="text/javascript">
$("/img[rel]").overlay();
</script> -->  
 <script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-2391431-68");
pageTracker._addOrganic("mail.ru", "q");
pageTracker._addOrganic("rambler.ru", "words");
pageTracker._addOrganic("nova.rambler.ru", "query");
pageTracker._addOrganic("nigma.ru", "s");
pageTracker._addOrganic("aport.ru", "r");
pageTracker._addOrganic("blogs.yandex.ru", "text");
pageTracker._addOrganic("webalta.ru", "q");
pageTracker._addOrganic("gogo.ru", "q");
pageTracker._addOrganic("akavita.by", "z");
pageTracker._addOrganic("meta.ua", "q");
pageTracker._addOrganic("bigmir.net", "q");
pageTracker._addOrganic("tut.by", "query");
pageTracker._addOrganic("all.by", "query");
pageTracker._addOrganic("i.ua", "q");
pageTracker._addOrganic("online.ua", "q");
pageTracker._addOrganic("a.ua", "s");
pageTracker._addOrganic("ukr.net", "search_query");
pageTracker._addOrganic("search.com.ua", "q");
pageTracker._addOrganic("search.ua", "query");
pageTracker._addOrganic("poisk.ru", "text");
pageTracker._addOrganic("km.ru", "sq");
pageTracker._addOrganic("liveinternet.ru", "ask");
pageTracker._addOrganic("gde.ru", "keywords");
pageTracker._addOrganic("quintura.ru", "request");
pageTracker._addOrganic("search.qip.ru", "query");
pageTracker._initData();
pageTracker._trackPageview();
} catch(err) {}</script>


<!-- Yandex.Metrika -->
<div style="display:none;"><script type="text/javascript">
(function(w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter2241217 = new Ya.Metrika(2241217);
             yaCounter2241217.clickmap(true);
             yaCounter2241217.trackLinks(true);
        
        } catch(e) {}
    });
})(window, 'yandex_metrika_callbacks');
</script>


<!-- WebVisor -->
<script type="text/javascript" id="__visorCode"></script>
<script type="text/javascript">
    var __visorInit = new Function('', 'if (typeof window["__visor"] != "undefined") __visor.init(10745);');
    setTimeout('document.getElementById("__visorCode").src="//c1.web-visor.com/c.js";',10);
</script>
<noscript><img src="//c1.web-visor.com/noscript?sid=10745" alt="" 
style="position:absolute;width:1px;height:1px;left:-999px;top:-999px;"></noscript>
<!-- WebVisor -->

</div>
<script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript" defer="defer"></script>
<noscript><div style="position:absolute"><img src="//mc.yandex.ru/watch/2241217" alt="" /></div></noscript>
<!-- /Yandex.Metrika -->

</table>
</div>

</body>
</html>