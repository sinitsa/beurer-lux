<?php  include("blocks/bread.tpl.php");  ?>

  <script type="text/javascript">
		price = {
		    "min": <?php echo isset($data['price_range']['min']) ? $data['price_range']['min'] : 0 ?>,
		    "max": <?php echo isset($data['price_range']['max']) ? $data['price_range']['max'] : 0 ?>
		};
		
		var defaultPage = <?php echo $_GET['page'] == 'all' ? "'all'" : (is_numeric($_GET['page']) ? $_GET['page'] : 1); ?>;
		var defaultSort = '<?php echo isset($_GET['sort']) ? $_GET['sort'] : 'none'; ?>';
		var ajaxMode = false;
		
            </script>
<div class="inner">
<div id="column">
	<div class="filter open" ><div class="filterIn splashy rounded">
							<form id="filter-form" action="/ajax/products.php?action=<?php echo $data['paramsFormAction']; ?>" method="post">
					<?php
					if (isset($data['paramsHidden']) && count($data['paramsHidden']) > 0) {
					foreach ($data['paramsHidden'] as $name => $value) {
						echo '<input type="hidden" name="' . $name . '" value="' . $value . '" />';
					}
					
					if(sizeof($data['params']) > 0):
					echo '<div class="filterToggle"><span>Больше параметров</span></div>';
					endif;	
					
					}
					?>
								<input type="hidden" name="paginator_type" value="uniq" />
								
								
									<div class="range">
										<div class="caption">ЦЕНА, руб</div>
										<div class="value">от  <input name="min_price" type="text" value="<?= $data['price_range']['min']; ?>" class="price-min range-min rounded" id="price-min"></div>
										<div class="value">до  <input name="max_price" type="text" value="<?= $data['price_range']['max']; ?>" class="price-max range-max rounded" id="price-max"></div>
									</div>
									<div id="price-slider"></div>
									<div class="sort my_sort">
										<div class="caption">Сортировка:</div>
										<select id="sort-filter">
											<option data-uri="<?php echo $qString->setParam('sort', 'news_sale'); ?>" value="news_sale" <?= (empty($_GET['sort']) || $_GET['sort'] == 'news_sale' ? 'selected="selected"' : '') ?>>По популярности</option>
											<option data-uri="<?php echo $qString->setParam('sort', 'price_asc'); ?>" value="price_asc" <?= ($_GET['sort'] == 'price_asc' ? 'selected="selected"' : '') ?>>По возрастанию цены</option>
											<option data-uri="<?php echo $qString->setParam('sort', 'price_desc'); ?>" value="price_desc" <?= ($_GET['sort'] == 'price_desc' ? 'selected="selected"' : '') ?>>По убыванию цены</option>
										</select>
									</div>
								
								
					<div class="filters-field" style="z-index: 100; height: auto; zoom:1; position: relative; display: none;">
					
					
					
					<?php
					foreach ($data['params'] as $param) {
						if ($param['info']['type'] == PARAM_VALUE || $param['info']['type'] == PARAM_SET) {
						?>
						<fieldset class="producers">
							<?php if ($param['info']['show_title']) { ?>
								<div class="caption"><?= $param['info']['title'] ?>:</div>
							<?php } ?>
							<?php foreach ($param['params'] as $value) { ?>
								<a href="javascript:void(0)" class="checkbox-param"><?= $value['value'] ?><input type="checkbox" name="filter[<?= $value['id']; ?>]" /></a>
							<?php } ?>
						</fieldset>
						<?php
						} elseif ($param['info']['type'] == PARAM_RANGE) {
						$p = getMinAndMaxParams($param['params']);
						?>
						<fieldset class="range-slider">
							<div class="range">
							<div class="caption"><?= $param['info']['title']; ?>:</div>
							<div class="value">от: <input type="text" value="<?= $p['min']['value_float'] ?>" class="weight-min range-min" id="weight-min"></div>
							<div class="value">до: <input type="text" value="<?= $p['max']['value_float'] ?>" class="weight-max range-max" id="weight-max"></div>
							</div>
							<div>
							<div class="slider-field" data-min="<?= $p['min']['value_float'] ?>" data-max="<?= $p['max']['value_float'] ?>" data-param_id="<?= $param['info']['param_id'] ?>"></div>
							<div class="slider-data-field"></div>
							</div>
							<div class="slider-body"></div>
						</fieldset>
						<?php
						}
					}
					?>
								</div>
							</form>
						</div>
					</div>

		<?php if (count($data['tags'])) {
			
	   //$NeedUrl = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	   //$urlElements = explode('.', end(array_values(array_filter(explode('/', $NeedUrl)))));



	   ?>
	   
	   <div class="tags_cloud">
		<h2 class='title_cat'>Подкатегории</h2>
			<div class="tags_hidden">
				<div class="tags_slide">
				<ul>
			<?php foreach($data['tags'] as $tags){?>
			<li>
				<a href="/tags/<?=$tags['chpu'];?>" class="<?php echo $tags['chpu'] === $tag['chpu']?"selected":"";?> <?php if ( strlen($tags['title']) > 60 ){echo "long";} ?>"><?=$tags['title'];?></a>
			</li>
			<?}?>
			</ul>
				</div>
			</div>

			 <div id="showTags">Показать еще</div> 
			
		</div>
		
		
			
			<?php } ?>  
				<div class="advantages">
					<ul>
						<li>Бесплатная доставка по Москве от <?=Config::get('site.delivery_price_limit') ?> руб.</li>
						<li>Обмен и возврат в течении 14 дней</li>
						<li>Официальные сертификаты на товары</li>
						<li>Удобные способы оплаты: Visa, Mastercard</li>
					</ul>
	 
				</div>
			
			<div class="answers">
				<form>
				<span class="zagol">Ответы на все вопросы!</span>
				<p>Оставьте заявку и наш специалист ответит на все ваши вопросы</p>
					<input type="text" value="" placeholder="Имя" name="name">
					<input type="text" value="" placeholder="+ 7 (___) ___-__-__" name="phone">
					<input type="submit" value="Отправить" class="call_back_order">
				</form>
			</div>
</div>
	<div class="content">
	
		<div class="clearfix" id="cat_top_tree">
		 
		  
		  <div id="cat_info_left_block">
		  <div class="new_filters">
			Сортировать:
			<input id="1" type="radio" name="radio_sort" value="0" checked="checked">
			<label for="1">популярные</label>
			<input id="2" type="radio" name="radio_sort" value="1">
			<label for="2">дешевые</label>
			<input id="3" type="radio" name="radio_sort" value="2">
			<label for="3">дорогие</label>
			</div>
		  </div>
			<h1>
			  <?=$title ?>
			</h1>
			
		  
		</div>



		  
		<div class="tovarCont">

			<?php foreach ($data['products'] as $item){ ?>
			
			
			 <?php include("blocks/one_product_listing.tpl.php");  ?>
		
			<?php  } ?>
			
			<?if( $data['page']['pagesCount'] > 1 ){?>
						<a class="bt_eshe" id='loadMore' data-page='<?=$data['page']['currentPage']?>' data-pagescount="<?=$data['page']['pagesCount']?>" href="#">Показать еще товары</a>
					<?}?>
		</div>	
		
		
		
		<div class="desCat"><?php echo $seo_text; ?></div>
	</div>		  
</div>
 <script type="text/javascript">
    rrApiOnReady.push(function() {
		try { rrApi.categoryView(<?=$data['catInfo']['id'] ?>); } catch(e) {}
	})
</script> 
  

  <div class="clear"></div>
  
 
  
  
</center>