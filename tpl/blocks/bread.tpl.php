<div class="clearfix" id="cat_path">
		 <div class="inner">
			<div class="left_tree">
				<h2>Каталог товаров</h2>	
					<ul>
										<?php
												$i = 0;
												$len = count($menu);
												
												foreach ($menu as $item) {$i++;?>
													<li class="info-box box1" id="box<?=$i?>" <?=($i == $len) ? 'style="margin-right: 0px !important"' : '';?>><a href="<?=!empty($item['cat_url'])?"/category/":"/mcat/" ?><?=$item['chpu'] ?>" >
														<span <?=(strlen($item['title']) < 30) ? 'style="padding-top:10px;"' : '';?>><?=$item['title']?></span></a>
														<div class="submenu">
														<ul>
														<?php	$count=0;
														
														foreach ($item['submenu'] as $subitem) {
															
															$count++;
															?>
															<li><a href="/<?php echo ($subitem['tag_status']!=NULL)?"tags":"category"; echo "/".$subitem['chpu']?>/"><?=$subitem['title']?></a></li>
														<?php 
														 if($count==13)
														{
															echo '</ul><ul>';
															$count=0;
														} 
														}?>
														</ul>
														</div>
													</li>
												<?php }?>
					</ul>
			</div>
			<div itemscope itemtype="http://schema.org/BreadcrumbList" class="bread">
                <div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" >
                    <a itemprop="item" href="/">
                        <span itemprop="name">Главная</span>
                    </a>
                </div>

                <?php if (isset($breadItems)): ?>
                    <?php foreach($breadItems as $item):?>
                        -
                        <?php if (isset($item['chpu'])): ?>
                            <div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" >
                                <a itemprop="item" href="<?=$item['chpu']?>">
                                    <span itemprop="name"> <?=$item['title']?></span>
                                </a>
                            </div>
                        <?php else: ?>
                            <div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" >
                                <span itemprop="name"> <?=$item['title']?></span>
                            </div>
                        <?php endif ?>
                    <?php endforeach ?>
                <?php endif ?>
			</div>
		</div>
	</div>
