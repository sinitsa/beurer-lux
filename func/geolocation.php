<?php

/**
 * <pre>
 * Fringe Interactive
 * Kinetic Management System (KMS) v0.1.8 (beta)
 * Geolocation Component Executable Wrapper
 * Last Updated: 14.01.14 0:04
 * </pre>
 *
 * @author 		$Author: V. Nesterov $
 * @copyright		(c) 2014 Fringe Interactive
 * @license		http://www.kinetic.pro/info/license
 * @package		KMS
 * @link		http://www.kinetic.pro
 * @version		$Rev: 3 $
 *
 */
if (!defined('IN_KINETIC')) {
    define('IN_KINETIC', TRUE);
}

error_reporting('E_ALL, E_NOTICE');

mb_internal_encoding('utf-8');
header('Content-Type: text/html; charset=utf-8');


// We are not in shell!
if (!defined('IN_KINETIC_SHELL')) {
    define('IN_KINETIC_SHELL', FALSE);
}

/**
 * Path to the front controller of Geolocation Component (this file)
 */
if (!defined('KMS_GLC_ROOT')) {
    define('KMS_GLC_ROOT', dirname(__FILE__) . '/');
    //echo 'Set init file dir: <br />' . KMS_GLC_ROOT . '<br /><br />';
}

/**
 * Path to the KMS Geolocation Component Directory
 */
if (!defined('KMS_GLC_CORE')) {
    define('KMS_GLC_CORE', KMS_GLC_ROOT . 'geolocation/');
    //echo 'Set core dir: <br />' . KMS_GLC_CORE . '<br /><br />';
}

/**
 * Path to the KMS Geolocation Regions Directory
 */
if (!defined('KMS_GLC_RGS')) {
    define('KMS_GLC_RGS', KMS_GLC_CORE . 'regions/');
    //echo 'Set regions dir: <br />' . KMS_GLC_RGS . '<br /><br />';
}

/**
 * Path to the Geolocation Component cache directory
 */
if (!defined('KMS_GLC_CACHE')) {
    define('KMS_GLC_CACHE', KMS_GLC_CORE . 'data_cache/');
    //echo 'Set cache dir: <br />' . KMS_GLC_CACHE . '<br /><br />';
}

/*
 *  Load the KMS Geolocation Component config file
 * -----------------------------------------------------------------------------
 */
$GLConfig = array();
$GLConfigFile = KMS_GLC_CORE . 'geolocation.config.php';
if (file_exists($GLConfigFile)) {
    require ( $GLConfigFile );
    //echo 'Get config: <br />' . $GLConfigFile . '<br /><br />';
}


/*
 * Get current php version
 */
$php_version = PHP_VERSION;
//echo 'Get current php version: <br />' . $php_version . '<br /><br />';

/*
 * Get current time
 */
$php_time = date('l jS \of F Y h:i:s A');
//echo 'Get current time: <br />' . $php_time . '<br /><br />';

/*
 *  Load the KMS Geolocation Component Class file
 * -----------------------------------------------------------------------------
 */
$GLClassFile = KMS_GLC_CORE . 'geolocation.class.php';
if (file_exists($GLClassFile)) {
    require ( $GLClassFile );
    //echo 'Get class: <br />' . $GLClassFile . '<br /><br />';
    $GLObj = new kmsGeolocation();
} else {
    exit();
}

/*
 *  Set UTF-8 data encoding
 * -----------------------------------------------------------------------------
 */

function to_utf8($in) {
    if (is_array($in)) {
	foreach ($in as $key => $value) {
	    $out[to_utf8($key)] = to_utf8($value);
	}
    } elseif (is_string($in)) {
	return iconv('CP1251', 'UTF8', $in);
    } else {
	return $in;
    }
    return $out;
}

// Get current data
$GLDataResult = $GLObj->getRecord($GLObj->get_client_ip());

//print_r($GLDataResult);
