<?php
class GetImage {
	var $source;
	var $save_to;
	var $new_name;
	var $set_extension;
	var $quality;
	var $error;

	function download($method = 'curl',$name_randomize = true){
		$info = @GetImageSize($this->source);
		$mime = $info['mime'];

		// Какой тип изображения?
		$type = substr(strrchr($mime, '/'), 1);

		switch ($type){
			case 'jpeg':
				$image_create_func = 'ImageCreateFromJPEG';
				$image_save_func = 'ImageJPEG';
				$new_image_ext = 'jpg';

				// Наилучшее качество: 100
				$quality = isSet($this->quality) ? $this->quality : 100;
				break;

			case 'png':
				$image_create_func = 'ImageCreateFromPNG';
				$image_save_func = 'ImagePNG';
				$new_image_ext = 'png';

				// Уровень компрессии: от&nbsp; 0&nbsp; (без компрессии) до&nbsp; 9
				$quality = isSet($this->quality) ? $this->quality : 0;
				break;

			case 'bmp':
				$image_create_func = 'ImageCreateFromBMP';
				$image_save_func = 'ImageBMP';
				$new_image_ext = 'bmp';
				break;

			case 'gif':
				$image_create_func = 'ImageCreateFromGIF';
				$image_save_func = 'ImageGIF';
				$new_image_ext = 'gif';
				break;

			case 'vnd.wap.wbmp':
				$image_create_func = 'ImageCreateFromWBMP';
				$image_save_func = 'ImageWBMP';
				$new_image_ext = 'bmp';
				break;

			case 'xbm':
				$image_create_func = 'ImageCreateFromXBM';
				$image_save_func = 'ImageXBM';
				$new_image_ext = 'xbm';
				break;

			default:
				$image_create_func = 'ImageCreateFromJPEG';
				$image_save_func = 'ImageJPEG';
				$new_image_ext = 'jpg';
		}
		
		if($name_randomize){
			$this->new_name = md5(microtime()).'.'.$new_image_ext;
		}else{
			if(isSet($this->set_extension)){
				$ext = strrchr($this->source, ".");
				$strlen = strlen($ext);
				$this->new_name = basename(substr($this->source, 0, -$strlen)).'.'.$new_image_ext;
			}else{
				$this->new_name = basename($this->source);
			}
		}

		$save_to = $this->save_to.$this->new_name;
		try{
			if($method == 'curl'){
				$save_image = $this->LoadImageCURL($save_to);
			}elseif($method == 'gd'){
				$img = $image_create_func($this->source);
				//echo($save_to);
				if(isSet($quality)){
					$save_image = $image_save_func($img, $save_to, $quality);
				}else{
					$save_image = $image_save_func($img, $save_to);
					//echo("!");
				}
				//echo($save_image);
			}
		}catch(Exception $e){
			$error = $e->getMessage();
		}
		return $save_image;
	}


	function LoadImageCURL($save_to){
		$ch = curl_init($this->source);
		$fp = fopen($save_to, "wb");

		// устанавливаем URL и другие функции.
		$options = array(CURLOPT_FILE => $fp,
							CURLOPT_HEADER => 0,
							CURLOPT_FOLLOWLOCATION => 1,
							CURLOPT_TIMEOUT => 60); // Таймаут в 1 минуту должен быть достаточен

		curl_setopt_array($ch, $options);

		curl_exec($ch);
		curl_close($ch);
		fclose($fp);
	}
}

?>