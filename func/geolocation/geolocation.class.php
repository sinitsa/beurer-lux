﻿<?php



defined('IN_KINETIC') OR exit('No direct script access allowed');



/**

 * <pre>

 * Fringe Interactive

 * Kinetic Management System (KMS) v0.1.8 (beta)

 * Geolocation Component Class

 * Last Updated: 14.01.14 0:04

 * </pre>

 *

 * @author 		$Author: V. Nesterov $

 * @copyright		(C) 2013, Vladislav Ross

 * @copyright		(c) 2014 Fringe Interactive

 * @license		http://www.kinetic.pro/info/license

 * @package		KMS

 * @link		http://www.kinetic.pro

 * @version		$Rev: 3 $

 *

 */

class kmsGeolocation {



    private $fhandleCIDR, $fhandleCities, $fSizeCIDR, $fsizeCities;



    function __construct($CIDRFile = false, $CitiesFile = false) {

	if (!$CIDRFile) {

	    $CIDRFile = KMS_GLC_CACHE . 'global/cidr_optim.txt';

	}

	if (!$CitiesFile) {

	    $CitiesFile = KMS_GLC_CACHE . 'global/cities.txt';

	}

	$this->fhandleCIDR = fopen($CIDRFile, 'r') or die("Cannot open $CIDRFile");

	$this->fhandleCities = fopen($CitiesFile, 'r') or die("Cannot open $CitiesFile");

	$this->fSizeCIDR = filesize($CIDRFile);

	$this->fsizeCities = filesize($CitiesFile);

    }



    private function getCityByIdx($idx) {

	rewind($this->fhandleCities);

	while (!feof($this->fhandleCities)) {

	    $str = fgets($this->fhandleCities);

	    $arRecord = explode("\t", trim($str));

	    if ($arRecord[0] == $idx) {

		return array('city' => $arRecord[1],

		    'region' => $arRecord[2],

		    'district' => $arRecord[3],

		    'lat' => $arRecord[4],

		    'lng' => $arRecord[5]);

	    }

	}

	return false;

    }



    function getRecord($ip) {
	
// $ip = "88.201.128.11";

	$ip = sprintf('%u', ip2long($ip));



	rewind($this->fhandleCIDR);

	$rad = floor($this->fSizeCIDR / 2);

	$pos = $rad;

	while (fseek($this->fhandleCIDR, $pos, SEEK_SET) != -1) {

	    if ($rad) {

		$str = fgets($this->fhandleCIDR);

	    } else {

		rewind($this->fhandleCIDR);

	    }



	    $str = utf8_encode(fgets($this->fhandleCIDR));



	    if (!$str) {

		return false;

	    }



	    $arRecord = explode("\t", trim($str));



	    $rad = floor($rad / 2);

	    if (!$rad && ($ip < $arRecord[0] || $ip > $arRecord[1])) {

		return false;

	    }



	    if ($ip < $arRecord[0]) {

		$pos -= $rad;

	    } elseif ($ip > $arRecord[1]) {

		$pos += $rad;

	    } else {

		$result = array('range' => $arRecord[2], 'cc' => $arRecord[3]);



		if ($arRecord[4] != '-' && $cityResult = $this->getCityByIdx($arRecord[4])) {

		    $result += $cityResult;

		}



		return $result;

	    }

	}

	return false;

    }



    function get_client_ip() {

	$ipaddress = '';

	if ($_SERVER['HTTP_CLIENT_IP'])

	    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];

	else if ($_SERVER['HTTP_X_FORWARDED_FOR'])

	    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];

	else if ($_SERVER['HTTP_X_FORWARDED'])

	    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];

	else if ($_SERVER['HTTP_FORWARDED_FOR'])

	    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];

	else if ($_SERVER['HTTP_FORWARDED'])

	    $ipaddress = $_SERVER['HTTP_FORWARDED'];

	else if ($_SERVER['REMOTE_ADDR'])

	    $ipaddress = $_SERVER['REMOTE_ADDR'];

	else

	    $ipaddress = 'UNKNOWN';



	return $ipaddress;

    }

	function getCitiesArray()
	{
		$data_geo_path = 'func/geolocation/data_cache/global/cities.txt';
		$arGeoCities = file($data_geo_path);
		return $arGeoCities;
	}

}

