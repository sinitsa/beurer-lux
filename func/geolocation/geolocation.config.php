<?php



defined('IN_KINETIC') OR exit('No direct script access allowed');



/**

 * <pre>

 * Fringe Interactive

 * Kinetic Management System (KMS) v0.1.8 (beta)

 * Geolocation Component Configuration File

 * Last Updated: 13.01.14 23:36

 * </pre>

 *

 * @author 		$Author: V. Nesterov $

 * @copyright		(c) 2014 Fringe Interactive

 * @license		http://www.kinetic.pro/info/license

 * @package		KMS

 * @link		http://www.kinetic.pro

 * @version		$Rev: 12 $

 *

 */

// Global settings

$GLConfig['local'] = TRUE; // TRUE or FALSE? If false -> use global xml(json) service;

$GLConfig['type'] = 'json'; // use global xml or json service format? 

$GLConfig['force_type'] = FALSE; // force global xml or json service format if currently php extension doesn`t load or doesn`t exists? 

$GLConfig['dbcon'] = FALSE; // TRUE or FALSE? -> use database provided data in script? 

$GLConfig['cache'] = FALSE; // TRUE or FALSE? -> write to cache results array? 

// Database settings

$GLConfig['database'] = 'aqq13885_beta';

$GLConfig['username'] = 'aqq13885_beta';

$GLConfig['password'] = 'd@NJpHOTB+0P';

$GLConfig['hostname'] = 'localhost';

$GLConfig['port'] = 3306;

