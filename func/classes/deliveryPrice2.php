<?php
class cart {
	public static function getList($id = false)
	{
		if ($id === false)
		{
			if ($_COOKIE['cart_products'])
			{
				$cart = array();
				$cart_products = explode('|', $_COOKIE['cart_products']);
				foreach ($cart_products as $value)
				{
					if ($value)
					{
						list($id, $quantity) = explode(':', $value);
						$cart[$id] = $quantity;
					}
				}
				$id = array_keys($cart);
			}
		}
		if (is_array($id))
			$id = implode(',', $id);
		
		$sql =
"SELECT c.`id` AS id, c.`cat` AS category, c.`title` AS name, c.`short_des` AS description, c.`chpu` AS url, c.`price_after_discount` AS price_after_discount, dg.`cat_gabarits_id` as gabarit, cg.`sort` as gabarit_sort
FROM `catalog` AS c
LEFT JOIN `category_2_delivery_group` AS c2dg
ON (c2dg.`category_id` = c.`cat`)
LEFT JOIN `delivery_group` AS dg
ON (dg.`id` = c2dg.`delivery_group_id`)
LEFT JOIN `cat_gabarits` AS cg
ON (cg.`id` = dg.`cat_gabarits_id`)
WHERE c.`id` IN ($id)";

		//var_dump($sql);
		//die();


		if ($result = DB::query($sql))
		{
			while ($row = $result->fetch_assoc())
			{
				$row['name'] = strip_tags($row['name']);
				//$row['name'] = utf($row['name']);
				$row['description'] = strip_tags($row['description']);
				$row['description'] = html_entity_decode($row['description']);
				
				//$row['description'] = utf($row['description']);
				$row['quantity'] = $cart[$row['id']];
				$row['sum'] = $row['quantity'] * $row['price'];
				$rows[$row['id']] = $row;
			}
			return $rows;
		}
		return false;
	}
	/*
		$products = array(
			array(
				'id' => 23,
				'amount' => 1
			),
			...
		);
	*/
	public static function getDeliveryAndDiscount($products,$typeDelivery) {
		
		$productsIDS = array();
		$productsAmount = 0;
		
		foreach($products as $p) {
			if (is_numeric($p['id']) && is_numeric($p['amount'])) {
				$productsIDS[] = $p['id'];
				$productsAmount += $p['amount'];
				
				$tempProducts[$p['id']] = $p;
			}
		}
		if (count($productsIDS) == 0) {
			return array(
				'deliveryPrice' => false,
				'discount' => 0
			);
		}
		$cart = self::getList($productsIDS);
		$priceSumm = 0;
		$productsAmount = 0;
		$gabarit = 0;
		$minSort = false;

		foreach ($cart as $item) {
			
			if ($minSort === false || $minSort > $item['gabarit_sort']) {
				$gabarit = $item['gabarit'];
				$minSort = $item['gabarit_sort'];
			}
			
			$priceSumm += $item['price_after_discount'] * $tempProducts[$item['id']]['amount'];
			$productsAmount += $tempProducts[$item['id']]['amount'];
			
			$category = $item['category'];
			$itemId = $item['id'];
		}
		//�������� ��������� �������� �������� �� ������
		var_dump($priceSumm);
		if (intval($priceSumm)>(int)Config::get('site.delivery_price_limit')) {
			$dp = Config::get('site.delivery_price');
		} else {
			$dp = false;
		}
		if ($productsAmount == 1) {
			$deliveryPrice = deliveryPrice::getByCatAndPrice($category, $priceSumm, $itemId);
		} elseif ($productsAmount > 1) {
			$deliveryPrice = $dp;
		} else {
			$deliveryPrice = false;
		}
		
		//������ ������
		if ($productsAmount > 1) {
			$discount['value'] = discountPrice::getByGabaritAndSum($gabarit, $priceSumm);
		} else {
			$discount['value'] = 0;
		}
		$discount['type'] = 'percent';
		
		return array(
				'deliveryPrice' => $deliveryPrice,
				'discount' => $discount['value']
			);
	}
}
?>