<?php
class deliveryPrice {
	static $default = 300;
	
	
	
	public static function getByCatAndPrice($catalog_cat = 0, $catalog_price = 0, $catalog_id = 0) {
		//Хак с выводом цены доставки для конкретного товара
		// if ($catalog_id > 0) {
			// $result = DB::query("SELECT `cat`,`price`,`delivery_cost` FROM `catalog` WHERE id = '{$catalog_id}'");
			// $line = $result->fetch_assoc();
			// if ($line['delivery_cost'] != '')
				// return $line['delivery_cost'];
			// $catalog_cat = $line['cat'];
			// $catalog_price = $line['price'];
		// }
		if ($catalog_cat > 0 && $catalog_price > 0) {
			$sql =
"SELECT dp.`price` AS price, dg.`cat_gabarits_id` AS gabarit
FROM `category_2_delivery_group` c2dg
INNER JOIN `delivery_group_2_delivery_price` dp2dp
ON (dp2dp.`delivery_group_id` = c2dg.`delivery_group_id`)
INNER JOIN `delivery_price` dp 
	ON (dp.`id` = dp2dp.`delivery_price_id`)
INNER JOIN `delivery_group` dg
	ON (dg.`id` = dp2dp.`delivery_group_id`)
WHERE c2dg.`category_id` = $catalog_cat AND dp.`from` <= $catalog_price 
ORDER BY dp.`from` DESC LIMIT 1";
			if ($result = DB::query($sql)) {
				if ($line = $result->fetch_assoc()) {
					return $line['price'];
				}
			}
		}
		return self::$default;
	}
	public static function getByGabaritAndSum($gabarit, $sum) {
		if ($gabarit > 0 && $sum > 0) {
			$sql = 
"SELECT dp.`price` AS price
FROM `cat_gabarits_2_delivery_price` cg2dp
INNER JOIN `delivery_price` dp
ON (dp.`id`=cg2dp.`delivery_price_id`)
WHERE cg2dp.`cat_gabarits_id` = $gabarit AND dp.`from` <= $sum
ORDER BY dp.`from` DESC LIMIT 1";
			if ($result = DB::query($sql)) {
				if ($line = $result->fetch_assoc()) {
					return $line['price'];
				}
			}
		}
		return self::$default;
	}
}
?>