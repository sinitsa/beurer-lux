<?php
include(dirname(__FILE__).'/../../config.db.php');

$mysqli = new mysqli($host, $login, $pass, $db);
/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}
/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

setlocale(LC_ALL, 'ru_RU.UTF-8');

error_reporting(E_ALL);
ini_set('display_errors','Off');
#set_time_limit(0);
DB::$instance = $mysqli;
DB::query('SET NAMES UTF-8');
class DB{
	static $instance;
	public static function query($sql){
		return self::$instance->query($sql);
	}
	public static function prepare($sql){
		return self::$instance->prepare($sql);
	}
	public static function stop(){
		@DB::$instance->close();
	}
}
function utf($str){
	return iconv('Windows-1251', 'UTF-8', $str);
}
function cp($str){
	return iconv('UTF-8', 'Windows-1251', $str);
}
function template($template, $search)
{
	$data = file_get_contents($template);
	return str_replace(array_keys($search), array_values($search), $data);
}


function __autoload($class_name) {
    require_once $_SERVER['DOCUMENT_ROOT'].'/func/classes/'.$class_name.'.php';
}
function type($row){
	if ($row['Key']) $type = 'hidden';
	else switch ($row['Type']) {
		 	case 'tinyint':
		 	case 'smallint':
		 	case 'int':
		 	case 'bigint':
				$type = 'number';
		 		break;
		 	case 'tinytext':
		 	case 'text':
		 	default:
		 		$type = 'text';
		 		break;
		 }
	return $type;
}

function bind_type($row){
	switch(type($row)){
		case 'number':
			$type = 'i';
			break;
		case 'text':
		default:
			$type = 's';
			break;
	}
	return $type;
}

?>