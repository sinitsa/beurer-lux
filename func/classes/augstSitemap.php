<?php
//                        _    _____ _ _                             
//                       | |  / ____(_) |                            
//   __ _ _   _  __ _ ___| |_| (___  _| |_ ___ _ __ ___   __ _ _ __  
//  / _` | | | |/ _` / __| __|\___ \| | __/ _ \ '_ ` _ \ / _` | '_ \ 
// | (_| | |_| | (_| \__ \ |_ ____) | | ||  __/ | | | | | (_| | |_) |
//  \__,_|\__,_|\__, |___/\__|_____/|_|\__\___|_| |_| |_|\__,_| .__/ 
//               __/ |                                        | |    
//              |___/                                         |_|    
// 
//	РљР»Р°СЃСЃ РґР»СЏ Р±С‹СЃС‚СЂРѕРіРѕ СЃРѕСЃС‚Р°РІР»РµРЅРёСЏ РєР°СЂС‚С‹ СЃР°Р№С‚Р°.
//	РСЃРїРѕР»СЊР·РѕРІР°РЅРёРµ:
//	$map = new augstSitemap();
//	...
//		$map->insertUrl($location[, ...]);
//	...
//	$map->save($path);
//

class augstSitemap {

	//	РЁР°Р±Р»РѕРЅ РІСЃРµРіРѕ РґРѕРєСѓРјРµРЅС‚Р° СЃ РєР°СЂС‚РѕР№
	static $documentTemplate = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
</urlset>
XML;
	//	РЁР°Р±Р»РѕРЅ РѕРґРЅРѕР№ СЃСЃС‹Р»РєРё
	static $urlTemplate = <<<XML
<url>
	<loc></loc>
    <lastmod></lastmod>
	<changefreq></changefreq>
    <priority></priority>
</url>
XML;

	private $_map    = null;	//	DOMDocument
	private $_urlSet = null;	//	DOMElement

	/**
	 * РЎС‚Р°С‚РёС‡РµСЃРєРёР№ РјРµС‚РѕРґ РґР»СЏ СЃРѕР·РґР°РЅРёСЏ СѓР·Р»Р°-СЃСЃС‹Р»РєРё РґР»СЏ РєР°СЂС‚С‹ СЃР°Р№С‚Р°.
	 * @param  DOMDocument $dom   DOM-РґРѕРєСѓРјРµРЅС‚, РІ РєРѕС‚РѕСЂС‹Р№ РёРјРїРѕСЂС‚РёСЂСѓРµС‚СЃСЏ СѓР·РµР»
	 * @param  string $location   URL СЃС‚СЂР°РЅРёС†С‹ СЃР°Р№С‚Р°
	 * @param  string $lastmod    Р”Р°С‚Р° РёР·РјРµРЅРµРЅРёСЏ СЃС‚СЂР°РЅРёС†С‹. Р—РЅР°С‡РµРЅРёРµ 'today'
	 *                            РїРѕ-СѓРјРѕР»С‡Р°РЅРёСЋ - РёСЃРїРѕР»СЊР·РѕРІР°С‚СЊ РґР°С‚Сѓ РіРµРЅРµСЂР°С†РёРё 
	 *                            РєР°СЂС‚С‹. Р¤РѕСЂРјР°С‚ - Р“Р“Р“Р“-РњРњ-Р”Р”.
	 * @param  string $changefreq Р§Р°СЃС‚РѕС‚Р° РёР·РјРµРЅРµРЅРёСЏ СЃС‚СЂР°РЅРёС†С‹.
	 *                            РЎРј. www.sitemaps.org
	 * @param  float  $priority   РџСЂРёРѕСЂРёС‚РµС‚ РѕР±СЂР°Р±РѕС‚РєРё СЂРѕР±РѕС‚РѕРј РїРѕ РѕС‚РЅРѕС€РµРЅРёСЋ Рє
	 *                            РґСЂСѓРіРёРј СЃС‚СЂР°РЅРёС†Р°Рј.
	 * @return DOMNode|bool       Р’РѕР·РІСЂР°С‰Р°РµС‚ СѓР·РµР» DOM СЃ Р·Р°РїРѕР»РЅРµРЅРЅРѕР№ РёРЅС„РѕСЂРјР°С†РёРµР№
	 *                            РёР»Рё false РІ СЃР»СѓС‡Р°Рµ РЅРµСѓРґР°С‡Рё.
	 */
	static public function url($dom, $location, $lastmod = 'today',
									$changefreq = 'daily', $priority = 0.5) {
		$url = new SimpleXMLElement(self::$urlTemplate);
		$url->loc = $location;
		$url->lastmod = ($lastmod == 'today') ? date('Y-m-d') : $lastmod;
		$url->changefreq = $changefreq;
		$url->priority = $priority;

		$url = dom_import_simplexml($url);
		if (!$url) return false;
		return $dom->importNode($url, true);
	}
	
	/**
	 * РЎРѕР·РґР°РµС‚ XML-РґРѕРєСѓРјРµРЅС‚ РєР°СЂС‚С‹ СЃР°Р№С‚Р° РїРѕ С€Р°Р±Р»РѕРЅСѓ.
	 */
	function __construct() {
		$this->_map = new DOMDocument();
		$this->_map->loadXML(self::$documentTemplate);
		$this->_urlSet = $this->_map->getElementsByTagName('urlset')->item(0);
		return $this;
	}

	/**
	 * Р’СЃС‚Р°РІР»СЏРµС‚ РґР°РЅРЅС‹Рµ Рѕ СЃС‚СЂР°РЅРёС†Рµ СЃР°Р№С‚Р° РІ РєР°СЂС‚Сѓ.
	 * @param  string $location   URL СЃС‚СЂР°РЅРёС†С‹ СЃР°Р№С‚Р°
	 * @param  string $lastmod    Р”Р°С‚Р° РёР·РјРµРЅРµРЅРёСЏ СЃС‚СЂР°РЅРёС†С‹. Р—РЅР°С‡РµРЅРёРµ 'today'
	 *                            РїРѕ-СѓРјРѕР»С‡Р°РЅРёСЋ - РёСЃРїРѕР»СЊР·РѕРІР°С‚СЊ РґР°С‚Сѓ РіРµРЅРµСЂР°С†РёРё 
	 *                            РєР°СЂС‚С‹. Р¤РѕСЂРјР°С‚ - Р“Р“Р“Р“-РњРњ-Р”Р”.
	 * @param  string $changefreq Р§Р°СЃС‚РѕС‚Р° РёР·РјРµРЅРµРЅРёСЏ СЃС‚СЂР°РЅРёС†С‹.
	 *                            РЎРј. www.sitemaps.org
	 * @param  float  $priority   РџСЂРёРѕСЂРёС‚РµС‚ РѕР±СЂР°Р±РѕС‚РєРё СЂРѕР±РѕС‚РѕРј РїРѕ РѕС‚РЅРѕС€РµРЅРёСЋ Рє
	 *                            РґСЂСѓРіРёРј СЃС‚СЂР°РЅРёС†Р°Рј.
	 * @return DOMNode|bool       Р’РѕР·РІСЂР°С‰Р°РµС‚ РІСЃС‚Р°РІР»РµРЅРЅС‹Р№ СѓР·РµР» РёР»Рё false.
	 */
	public function insertUrl($location, $lastmod = 'today',
									$changefreq = 'daily', $priority = 0.5) {
		$url = self::url($this->_map, $location, $lastmod, $changefreq, $priority);
		return $this->_urlSet->appendChild($url);
	}

	/**
	 * РЎРѕС…СЂР°РЅСЏРµС‚ РєР°СЂС‚Сѓ СЃР°Р№С‚Р° РІ СѓРєР°Р·Р°РЅРЅРѕРј РјРµСЃС‚Рµ.
	 * @param  string   $path РџРѕР»РЅС‹Р№ РїСѓС‚СЊ Рє С„Р°Р№Р»Сѓ СЃ РєР°СЂС‚РѕР№ СЃР°Р№С‚Р°
	 * @return int|bool       РљРѕР»РёС‡РµСЃС‚РІРѕ Р·Р°РїРёСЃР°РЅРЅС‹С… Р±Р°Р№С‚ РёР»Рё false.
	 */
	public function save($path) {
		return $this->_map->save($path);
	}
}
?>