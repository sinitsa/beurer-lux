<?
function api_10med($method, $params) {
	global $config;
	//В запросе к API 10med нам нужно обязательно передать api ключ 
	$data['api_key'] = $config['10med_apikey'];
	//и идентификатор магазина, который совершает запрос к апи
	$data['shop_id'] = $config['10med_shopid'];
	//а ну еще название метода передаем
	$data['method'] = $method;
	//а тут у нас параметры метода и все такое. ну ты понел
	$data['data'] = $params;
	
	//подготавливаем запрос для передачи
	$data = http_build_query($data);
	file_put_contents('data.txt', var_export($data, true));
	file_put_contents('config.txt', var_export($config, true));
	$context = stream_context_create(array(
         'http' => array(
             'method' => 'POST',
             'content' => $data,
         ),
    ));
	//отправляем запрос
	$response = file_get_contents($config['10med_apiurl'], false, $context);

	//var_dump($response);
	//die();
	//разбираем ответ, перегоняем его в массив и возвращаем
	return unserialize($response);
}