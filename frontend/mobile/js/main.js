/**
 * [Basket ]
 */
function Basket() {

    // флаг что корзина пуста    
    this.emptyBasket = null;

    //
    function parseJson(str) {
        try {
            str = JSON.parse(str);
        } catch (e) {
            return false;
        }
        return str;
    }

    // валидация формы оформления заказа
    function orderValidate(name, phone, email, basket_list) {
        var errors = [];

        // проверка имени
        name.match(/^[a-zA-Zа-яА-Я]+$/) ? false : errors.push('name');
        // проверка телефоне (только на пустоту, т.к филтруется маской)
        (phone.length > 0) ? false : errors.push('phone');
        // проверка почтового адреса
        if (email.length) {
            email.match(/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/) ? false : errors.push('email');
        } 
        if (errors.length) {
            return errors;  
        } else {
            return null;
        }
    }
	


    // добавление товара в корзину
    this.addProduct = function(id, count, price) {
        var products = parseJson($.cookie('basket'));
        if (products) {
            var tsumm = 0,
                tcount = 0,
                flag = true;
            for (var i = products.items.length - 1; i >= 0; i--) {
                if  (products.items[i].id == id) {
                    products.items[i].count += parseInt(count);
                    flag = false;
                } 
                tcount += products.items[i].count;
                tsumm += products.items[i].count*products.items[i].price
            }
            // если товара нет в корзине
            if (flag) {
                products.items.push({
                    id: id,
                    count: parseInt(count),
                    price: price 
                });
                tcount += parseInt(count); 
                tsumm += price;
            }
            products.tcount = tcount;
            products.tprice = tsumm;

        } else {
            products = {
                tcount: parseInt(count),
                tprice: parseInt(count) * parseInt(price),
                items: [{
                    id: id,
                    count: count,
                    price: price
                }]
            };
        }
        $.cookie('basket', JSON.stringify(products), {expires: 30, path: '/' });
    };
    // удаление товара из корзины
    this.delProduct = function(id) {
        var products = parseJson($.cookie('basket'));
        if (products) {
            var tsumm = 0,
                tcount = 0;
            for (var i = products.items.length - 1; i >= 0; i--) {
                if  (products.items[i].id == id) {
                    products.items.splice(i,1);
                } else {
                    tcount += parseInt(products.items[i].count);
                    tsumm += parseInt(products.items[i].count*products.items[i].price);
                }
            }
            products.tcount = tcount;
            products.tprice = tsumm;

        } else {
            products = {
                tcount: 0,
                tprice: 0,
                items: []
            } 
        }
        $.cookie('basket', JSON.stringify(products), {expires: 30, path: '/' }); 
        return products.tcount;
    };
    // изменение количества товара в корзине
    this.changeProduct = function(id, count) {
        var products = parseJson($.cookie('basket'));
        if (products) {
            var tsumm = 0,
                tcount = 0;
            for (var i = products.items.length - 1; i >= 0; i--) {
                if  (products.items[i].id == id) {
                    products.items[i].count = parseInt(count);
                } 
				
                tcount += parseInt(products.items[i].count);
                tsumm += products.items[i].count*products.items[i].price
            }
            products.tcount = tcount;
            products.tprice = tsumm;
            $.cookie('basket', JSON.stringify(products), {expires: 30, path: '/' });
        } 
    };
    // Оформление заказа
    this.makeOrder = function(name, phone, email, delivery_type, comment, basket_list) {
        errors = orderValidate(name, phone, email, basket_list);
        if (errors) {
            return {errors: true , messages: errors};
        } else {

            var data,
                ref = $.cookie('ref');
            $.ajax({
               url: '/ajax/makeorder',
               type: 'POST',
               async: false,
               dataType: 'json',
               data: {
                   'name'      :name, 
                   'phone'     :phone, 
                   'email'     :email, 
				   //'adress'    :adress,
				   //'date_delivery'  :date_delivery,
				   //'time_delivery'   :time_delivery,
				   //'index'     :index,
                   //'ptype'     :pay_type, 
                   'dtype'     :delivery_type, 
                   'comment'   :comment,
                   'ref'       :ref,
                   'list'      :basket_list,
                   'other'     : {}
               },
               success: function(input) {
                    data = input;
               },
			   error: function(data){
				   data = data ;
			   }
           });
           return {errors: false, data: data}; 
        }
    };
}
// получение всех параметров GET запроса
function parseQuery(search) {
    var args = search.substring(1).split('&');
    var argsParsed = {};
    var i, arg, kvp, key, value;
    for (i = 0; i < args.length; i++) {
        arg = args[i];
        if (-1 === arg.indexOf('=')) {
            argsParsed[decodeURIComponent(arg).trim()] = true;
        } else {
            kvp = arg.split('=');
            key = decodeURIComponent(kvp[0]).trim();
            value = decodeURIComponent(kvp[1]).trim();
            argsParsed[key] = value;
        }
    }
    return argsParsed;
}

// валидация формы оформления заказа
function feedbackValidate(name, email, des) {
    var errors = [];

    // проверка имени
    name.match(/^[a-zA-Zа-яА-Я]+$/) ? false : errors.push('name');
    // проверка почтового адреса
    if (email.length > 5) {
        email.match(/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/) ? false : errors.push('email');
    } else {
		errors.push('email');
	}
    des.length > 5 ? false : errors.push('des');
    
    if (errors.length > 0) {
        return errors;  
    } else {
        return false;
    }
}

// посылаем новый отзыв
function sendFeedback(name, email, des){
    
    var errors = feedbackValidate(name, email, des);
    
    if (errors != false) {
        return {errors: true , messages: errors};
    } else {
        var data,
            ref = $.cookie('ref');
        $.ajax({
           url: '/index.php?t=ajax&c=newfeedback',
           type: 'POST',
           async: false,
           dataType: 'json',
           data: {
               'name'      :name, 
               'des'       :des, 
               'email'     :email, 
           },
           success: function(input) {
                data = input;
           },
           error:function(input , stat, msg){
               data = {'msg': msg};
           }
       });
       return {errors: false, data: data}; 
    }
}
	
// обновление виджета корзины
function updateBasketWidget() {
    var basket = JSON.parse($.cookie('basket')),
        makeOrder = $('.make_order');
    $('.mainpanel').find('.counter').text(basket.tcount);

    if (makeOrder.length) {
        makeOrder.find('.tsumm').text(basket.tprice);
        if (basket.tcount) {
            makeOrder.fadeIn('400');
            $('.count_order').text(basket.tcount);    
        } else {
            $('.count_order').text('пуста');
            makeOrder.fadeOut('400');
        }
        var del_type = makeOrder.find('.del_type_item.active').data('id');
		
		//$('.pay_type').children('.check_pay_type').show();
		$('div.info').find('input').show();
		
		// убираем или добавляем способы оплаты в зависимости от доставки
		/*
		switch(del_type) {
			case '1':
			case 1:
			case '2':
			case 2:
				$('.check_pay_type').removeClass('active');
				$('.check_pay_type').filter('[data-type="2"]').hide();
				$('.check_pay_type').filter('[data-type="1"]').addClass('active');
			break;
			case '4':
			case 4:
				
				$('.check_pay_type').removeClass('active');
				$('.check_pay_type').filter('[data-type="1"]').hide();
				$('.check_pay_type').filter('[data-type="2"]').addClass('active');
			break;
			case 5:
			case '5':
				$('input.inp_index, input.inp_adress').hide();
			break;
		}
		*/
        updateDeliveryPrice(basket.tprice, del_type);
    }
} 
// расчет стоимости доставки
function updateDeliveryPrice(summ, del_type) {
    var string;
    var pr;
	$('.delivery_info').show();
        switch (del_type) {
            case 1:
					if (summ < 950) {
					 pr = 300;
					}
					if (summ >= 950 && summ < 3000) {
					 pr = 250;
					}
					if (summ >= 3000 && summ < 5000) {
						pr = 200;
					}
					if (summ >= 5000 && summ < 7000) {
						pr = 150;
					}
					
					string = '<span class="red">'+pr+'</span> руб';
					 
					if (summ >= 7000) {
						string = "Бесплатно"
					}
               
                break;
            case 2:
					if (summ < 950) {
					 pr = 300;
					}
					if (summ >= 950 && summ < 3000) {
					 pr = 250;
					}
					if (summ >= 3000 && summ < 5000) {
						pr = 200;
					}
					if (summ >= 5000 && summ < 7000) {
						pr = 150;
					}
					if (summ >= 7000) {
						pr = 0;
					}
                string = 'Базовая <span class="red">'+pr+' + 30 руб</span> за каждый км от МКАД';
                break;
            case 3:
				string = 'Расчитывается индивидуально';
                //string = 'Бесплатно';
                break;
            case 4:
                string = 'Расчитывается индивидуально';
                break;
            case 5:
				$('.delivery_info').hide();
                string = '';
                break;
            case 6:
                string = 'Расчитывается индивидуально';
                break;
        }
    
    $('.make_order').find('.delivery_summ').html(string);

}

// функция меняет селект времени на диваки
function createSelectHolder(elem){
	
	var body = "<div class='inp_wrap'>";
	elem.find('option').each(function(i, el){
		var opt = elem.find(':eq('+i+')');
		var active = (i == 0) ? ' class="active" ' : '' ;
		body += '<div ' + active +' data-index="'+(i)+'" >'+opt.html()+'</div>';
	});
		
	body += "</div>";
	elem.after(body);
		
	elem.find('option:eq(0)').attr('selected', 'true');
		
	var wrap = $('div.inp_wrap');
		
	wrap.find('div').click(function(){
		wrap.children().removeClass('active');
		$(this).addClass('active');
		var ind = $(this).attr('data-index');
		elem.find('option:selected').each(function(){
			this.selected = false;
		});
		elem.find(':eq('+ind+')').attr('selected', 'selected');
	});
}
	
/*
 * пердача данных в Ecommerce
 * @param {string} action
 * @param {obj} obj
 * @returns {undefined}
 */
function addLayer(action, obj) {
    var objCom = {};
    var dopObj = {};
    switch (action) {
        // добавление в корзину
        case "add":
            var name = obj.name;
            dopObj.products = [{
                "id": obj.id,
                "name": obj.name,
                "category": obj.category,
                "price": obj.price,
                "quantity": 1
            }];
            break
        // просмотр полного описания товара    
        case "detail":
            dopObj.products = [{
                "id": obj.id,
                "name": obj.name,
                "category": obj.category,
                "price": obj.price,
            }];
            break
        // удаление из корзины
        case "remove":
            dopObj.products = [{
                "id": obj.id,
                "name": obj.name,
                "category": obj.category
            }];
            break
        // покупка    
        case "purchase":
            var massPro = [];
            $.each(obj.products, function(ind, val) {
                massPro.push({
                    "id": val.id,
                    "name": val.name,
                    "category": val.category,
                    "price": val.price,
                    "quantity": val.quantity
                });
            });
            dopObj.products = massPro;
            dopObj.actionField = {
                "id": obj.orderId
            };
            break
    }
    objCom[action] = dopObj;
//    console.log({
//        "ecommerce": objCom
//    });
    try {
        dataLayer.push({
            "ecommerce": objCom
        });
    } catch (e) {
        console.log('ecommerce:', objCom);
    }
    
}

/**
 * обработка событий "достижение уцелей" для yandex счетчика
 * @param {string} index
 * @param {obj} param
 * @returns false
 */
function yaReachGoal(index) {
    try {
        yaCounter38390870.reachGoal(index);
    } catch (e) {
        console.log('count not working.', ' reach: ' + index);
    }
    return false;
}

/**
 * ====================     основные обработчики событий     ====================
 */
$(document).ready(function($) {
    // открыть\закрыть всплывающее меню
    $('.main_menu').on('click', function() {
        $(this).toggleClass('menu_hide');
        $('.action_nav').toggleClass('hide');
    });
    // 'Ленивая загрузка' для всех img
    $(".lazy").Lazy();
    // раскрыть список категорий  главной категории во всплывающем меню
    $('.section_list > li > a').on('click', function(event) {
        event.preventDefault();
        var list = $(this).siblings();
        list.toggleClass('wrap');
    });
    // выбор сортировки, сохраняет страницы пагинатора
    $('.sort_filters').on('click', 'a', function(event) {
        var query = parseQuery(window.location.search);
        if (query.page == null) {
            window.location.search = '?sort=' + $(this).data('sort_type');
        } else {
            window.location.search = '?sort=' + $(this).data('sort_type') + '&page=' + query.page;
        }
    });
    // выбор страницы пагинатора, сохраняет тип сортировки
    $('.paginator').on('click', 'a', function(event) {
        var query = parseQuery(window.location.search);
        if (query.sort == null) {
            window.location.search = '?page=' + $(this).data('to');
        } else {
            window.location.search = '?sort=' + query.sort + '&page=' + $(this).
            data('to');
        }
    });
    //  открытие всех категорий на главной
    $('.show_all_catalogs').on('click', function(event) {
        event.preventDefault();
        $('.catalog_area').toggleClass('open');
    });
    // слайдер для картинок товара в карточке
    $('.lazy_slider').slick({
        lazyLoad: 'ondemand',
        arrows: false,
        dots: true,
    });
    // слайдер для отзывов
    $('.feedbacks').slick({
        dots: false,
        infinite: true,
        speed: 300,
        arrows: true,
        slidesToShow: 1,
        adaptiveHeight: true
    });
    // переключение вкладок "Особенности/Описание"
    $('.product_des').on('click', '.head > .t1', function() {
        $('.product_des').find('.wrapper > div, .button').toggleClass('active').toggleClass('t1');
    });
    // переход на полную версию
    $('.in_full_vers').on('click', function() {
        $.cookie('theme', 1, 0, '/'); 
        console.log($.cookie('theme'));
        location.reload();        
    });

    // нажата кнопка "Назад"   
    $('.top').on('click', '.back', function() {
        event.preventDefault();
        history.back(1);
    });
    // нажат телефон
    $('.phone_btn').on('click', function(){
        yaReachGoal('PHONE_IN_MOBILE');
    });
    $('.phone_btn2').on('click', function(){
        yaReachGoal('BUTTON_PHONE_RU_MOBILE');
    });

    // открыта карточка товара
    $(".cart").is(function() {
        var btn = $(this).find('.in_basket');
        addLayer('detail',{
            id: btn.data('id'),
            name: btn.data('title'),
            category: btn.data('cat_title'),
            price: btn.data('price')
        });
    });

	/**
     * ---------- Работа с отзывами ----------
     */
	 
	$('.feedback-wrapper').ready(function(){
		$('input.inp_name').focus();
		
	});

	// Обалденный костыль чтоб вывести ссылку на отзывы в футере и на главной
    /*
	$('.main_container').ready(function(){
		var feed_link = '<li><a href="/feedback/" >Отзывы</a></li>';
		$('ul.pages_list').append(feed_link);
	});
	$('footer').ready(function(){
		var feed_link = '<a href="/feedback/" >Отзывы</a>';
		$('div.footer_pages_menu').append(feed_link);
	});
    */

	$('.addComment__send').click(function(){
		
		var form = $('#feedback');
        var name = form.find('.inp_name').val(),
            email = form.find('.inp_mail').val(),
			des = form.find('textarea[name="usermsg"]').val();
		
		form.find('.warrning').removeClass('warrning');    
		$('.feedback-wrapper').find('.warning_message').addClass('hidden');
		
        var result = sendFeedback(name, email, des);
		
        // обработка показ ошибок, если есть
        if ( result.errors ) {
			$('.feedback-wrapper').find('.warning_message').removeClass('hidden');
			console.log(result);
            for (var i = 0 ; i < result.messages.length ; i++) {
                switch (result.messages[i]) {
                    case 'name':
						form.find('.inp_name').addClass('warrning');
                        break;
                    case 'email':
						form.find('.inp_mail').addClass('warrning');
						break;
                    case 'des':
						form.find('textarea[name="usermsg"]').addClass('warrning');
                        break;
                }
            }
		} else {
			console.log(result);
			form.find('input[name="success"]').val("ok");
            form.submit();
		}
		
		
	});
	
	
    /**
     * ---------- Работа с корзиной ----------
     */
    var myBasket = new Basket(),
        orderForm = $('.make_order');

    // выбор типа оплаты
    orderForm.on('click', '.check_pay_type:not(.active)', function() {
        if ($(this).data('type') == 2) {
            $('.make_order').find('.inp_email').attr("placeholder", "Email: *");
        } else {
            $('.make_order').find('.inp_email').attr("placeholder", "Email:");
        }
		$('.check_pay_type').removeClass('active');
		$(this).addClass('active');
        //$(this).toggleClass('active').siblings().toggleClass('active');
    });
    // открытие списка типов доставки
    orderForm.on('click', '.control2', function() {
        $(this).parent().toggleClass('min');
    });
    // выбор типа доставки
    orderForm.on('click', '.del_type_item', function() {
        $(this).addClass('active').siblings().removeClass('active');
        $('.delivery_wrapper').toggleClass('min');
        updateBasketWidget();
    });
	
	// рендерим список времени доставки
	createSelectHolder($('select.inp_time_delivery'));
	
    // маски инпутов
    orderForm.find('.inp_phone').mask('+7 (999) 999-99-99');
    // событие - добавление товара в корзину
    $(".in_basket").click(function() {
        var prodId = $(this).data('id'),
            prodPrice = $(this).data('price');
        myBasket.addProduct(prodId, 1, prodPrice);
        updateBasketWidget();
        $(this).addClass("active").text('открыть корзину').removeClass('smbt');
        $(this).unbind('click').click(function() {
            window.location.href = "/cart/korzina";
        });
        yaReachGoal('BUY');
        addLayer('add',{
            id: prodId,
            name: $(this).data('title'),
            category: $(this).data('cat_title'),
            price: prodPrice
        });
    }); 
    // удаление из корзины
    orderForm.on('click', '.del_product', function() {
        var prodId = $(this).data('id'),
            prodPrice = $(this).data('price'),
            count = myBasket.delProduct(prodId);
        // для метрики  события "Корзина пуста", "Корзина не пуста"        
        if ((count > 1) && (myBasket.emptyBasket === null)) {
            myBasket.emptyBasket = false;
            yaReachGoal('BASKET_NOT_EMPTY');
        } else if (count == 0) {
            yaReachGoal('BASKET_EMPTY');
        }
        
        addLayer('remove',{
            id: prodId,
            name: $(this).data('title'),
            category: $(this).data('cat_title')
        });
        
        updateBasketWidget();
        $(this).closest('.item').fadeOut(500, function() {
            $(this).remove();
        });
    });
    // изменение количества товаров в корзине
    orderForm.on('change', 'input[name="count"]', function() {
        var count = $(this).val(),
            prodId = $(this).data('id');
            if (count < 1) {
                count = 1;
                $(this).val(1);
            }
        myBasket.changeProduct(prodId, count);
        updateBasketWidget();
    });
    // декремент товара в корзине
    orderForm.on('click', '.dec', function() {
        var input = $(this).siblings("input"),
            count = input.val();
            input.val(count-1).change();
    });
    // инкремент товара в корзине
    orderForm.on('click', '.inc', function() {
        var input = $(this).siblings("input"),
            count = input.val();
            input.val(parseInt(count)+1).change();
    });
    // кнопка 'оформить заказ'
    orderForm.on('click', '.finish_order_btn', function(){
        var form = $('.make_order'),
            name = form.find('.inp_name').val(),
            phone = form.find('.inp_phone').val(),
            email = form.find('.inp_email').val(),
			//adress = form.find('.inp_adress').val(),
			//date_delivery = form.find('.inp_date_delivery').val(),
			//time_delivery = form.find('.inp_time_delivery').val(),
			//time_ot = form.find('.inp_time_ot').val(),
			//time_do = form.find('.inp_time_do').val(),
			//index = form.find('.inp_index').val(),
            //pay_type = form.find('.check_pay_type.active').data('type'),
            del_type = form.find('.del_type_item.active').data('id'),
            comment = form.find('textarea[name="order_comment"]').val(),
            basket = JSON.parse($.cookie('basket'));
        form.find('.warrning').removeClass('warrning');    
        var result = myBasket.makeOrder(name, phone, email, del_type, comment, basket.items);

        // обработка показ ошибок, если есть
        if (result.errors) {
            form.find('.error_warrning').addClass('warrning');
            for (var i = errors.length - 1; i >= 0; i--) {
                switch (errors[i]) {
                    case 'phone':
                        form.find('.inp_phone, .phone_mes_warrning').addClass('warrning');
                        break;
                    case 'name':
                        form.find('.inp_name, .name_mes_warrning').addClass('warrning');
                        break;
                    case 'email':
                        form.find('.inp_email, .email_mes_warrning ').addClass('warrning');
                    case 'email2':
                        //  при выборе оплаты  банковской картой обазательно указывается Email
                        form.find('.inp_email, .email2_mes_warrning ').addClass('warrning');
                        break;
                }
            }
        } else {
			
			console.log(result);
			
            yaReachGoal('ORDER');
            addLayer('purchase', {
                orderId: result.data.id,
                products: result.data.products
            });
			
            form.find('input[name="orderid"]').val(result.data.id);
            // очистка корзины
            $.removeCookie('basket', {path: '/' });
            form.submit();
        }
    });
	
	
	
	function supportCSS(prop) {
    var yes = false; // по умолчанию ставим ложь
    if('Moz'+prop in document.body.style) {
        yes = true; // если FF поддерживает, то правда
    }
    if('webkit'+prop in document.body.style) {
        yes = true; // если Webkit поддерживает, то правда
    }
    if('ms'+prop in document.body.style) {
        yes = true; // если IE поддерживает, то правда
    }
    if(prop in document.body.style) {
        yes = true; // если поддерживает по умолчанию, то правда
    }
    return yes; // возращаем ответ
}

	
	if(supportCSS("pointer-events")){
		
		
	/*
		 $(".snow").show().let_it_snow({
	  speed: 1, // How fast the snow falls can be define here. You can choose a number in between 0 - 5. The higher, the faster. The default value is 0.
	  interaction: false, // This option allows viewer to interact with the falling snow. Toggle this to false if you don't want the snow to be interactive. The default value is true.
	  size: 9, // You can set the size of the snow here. Choose a number between 0 - 10+. The higher, the bigger. The default size is 2.
	  count: 20, // This allows you to set the number of snows displayed at a time. The default count is 200.
	  opacity: 0, // The opacity variation of the snow. You can choose a number in between 0.00 and 1.00 to set the base opacity and the plugin will randomly generate snows with slightly varied opacity.
	  color: "#ffffff", // You can set the color of the snow here. This option only accepts HEX color code in full 6 digits. The default value is "#ffffff"
	  windPower: 0, // You can set the wind power here. If you want the wind to blow left, set a positive number in this option., if you want the wind to blow right, set a negative number in this option. The default value is 0.
	  image: "/images/flake.png" // You can define a path to an image to be used instead of a default circle here. The default value is false.
	});
	*/
	}
});