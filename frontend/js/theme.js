/**
 * Set equal height for different DOM elements
 * 
 * @param columns
 * @example <caption>Example usage of adaptScreen()</caption>
 * @example setEqualHeight('.column');
 */
function setEqualHeight(columns) {
    // Set tallest element to zero
    var tallestcolumn = 0;
    //
    columns.each(function() {
	// Clear height value to auto to current column
	// NFO: strongly recommended for resize window state
	$(this).height('auto');
	// Get current element height value
	currentHeight = $(this).height();
	if (currentHeight > tallestcolumn) {
	    // set tallest element
	    tallestcolumn = currentHeight;
	}
    });
    // set height for columns group by the tallest element height value
    columns.height(tallestcolumn);
}

/*
 * jYoutube 1.0 - YouTube video image getter plugin for jQuery
 *
 * Copyright (c) 2009 jQuery Howto
 *
 * Licensed under the GPL license:
 *   http://www.gnu.org/licenses/gpl.html
 *
 * Plugin home & Author URL:
 *   http://jquery-howto.blogspot.com
 *
 */
 

 $(document).ready(function(){
	$('.rate_stars span').hover(
		function(){ 
			var num = $(this).attr('name');
			for (var i = 1; i <= num; i++) {
			$('[name='+i+']').css('backgroundImage', 'url(../assets/gfx/star.png)');
					}
				}
			)
	
	$('.rate_stars span').mouseleave(
		function(){ 
			$('.rate_stars span').css('backgroundImage', 'url(../assets/gfx/star_off.png)');
			for (var i = 1; i <= num2; i++) {
			$('[name='+i+']').css('backgroundImage', 'url(../assets/gfx/star.png)');
					}	
				}
			)

	$('.rate_stars span').click(
		function(){ 
			num2 = $(this).attr('name');
			$('[name=product_rate]').attr('value',num2);
			for (var i = 1; i <= num; i++) {
			$('[name='+i+']').css('backgroundImage', 'url(../assets/gfx/star.png)');
					}
				}
			)
			
		});
 
(function($) {
    $.extend({
	jYoutube: function(url, size) {
	    if (url === null) {
		return "";
	    }

	    size = (size === null) ? "big" : size;
	    var vid;
	    var results;

	    results = url.match("[\\?&]v=([^&#]*)");

	    vid = (results === null) ? url : results[1];

	    if (size == "small") {
		return "http://img.youtube.com/vi/" + vid + "/2.jpg";
	    } else {
		return "http://img.youtube.com/vi/" + vid + "/0.jpg";
	    }
	}
    })
})(jQuery);


(function($) {
    $(function() {

	$('ul.new-tabs').on('click', 'li:not(.current)', function() {
	    $(this).addClass('current').siblings().removeClass('current')
		    .parents('.section').find('div.box').eq($(this).index()).show().siblings('div.box').hide();
	    return false;
	})

    })
})(jQuery)

function setCookie(name, value, expires, path, domain, secure) {
    document.cookie = name + "=" + escape(value) +
	    ((expires) ? "; expires=" + expires : "") +
	    ((path) ? "; path=" + path : "") +
	    ((domain) ? "; domain=" + domain : "") +
	    ((secure) ? "; secure" : "");
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
	c_start = document.cookie.indexOf(c_name + "=");
	if (c_start != -1) {
	    c_start = c_start + c_name.length + 1;
	    c_end = document.cookie.indexOf(";", c_start);
	    if (c_end == -1)
		c_end = document.cookie.length;
	    return unescape(document.cookie.substring(c_start, c_end));
	}
    }
    return "";
}

function parseProductsString(string) {
    var returnArray = new Array();

    if (string.length > 0) {
	var products = string.split('|');
	for (var i in products) {
	    var temp = products[i].split(':');

	    returnArray.push({
		'id': parseInt(temp[0]),
		'amount': parseInt(temp[1])
	    });
	}
    }
    return returnArray;
}

function generateProductsString(products) {
    var tempArray = new Array();
    for (var i in products) {
	tempArray.push(products[i].id + ':' + products[i].amount);
    }
    return tempArray.join('|');
}

function addProductToBasket(productId, price) {
    //получим данные из кук
    var products = parseProductsString(getCookie('cart_products'));
    var totalAmount = parseInt(getCookie('total_amount'));
    if (!isNaN(totalAmount)) {
	totalAmount += price;
    } else {
	totalAmount = price;
    }

    //Если товар уже есть в куках, добавляем ему количество
    //Заодно подсчитаем количество всех товаров
    var foundSame = false;
    var productsCount = 0;
    for (var i in products) {
	if (products[i].id == productId) {
	    products[i].amount++;
	    foundSame = true;
	}
	productsCount += products[i].amount;
    }
    if (!foundSame) {
	products.push({
	    "id": productId,
	    "amount": 1
	});
	productsCount += 1;
    }


    setCookie('cart_products', generateProductsString(products), '', '/');
    setCookie('total_amount', totalAmount, '', '/');
    //Изменить оформление
    refreshBasketInfo();
}

function deleteProductFromBasket(productId, price) {
    //получим данные из кук
    var products = parseProductsString(getCookie('cart_products'));
    var totalAmount = parseInt(getCookie('total_amount'));
    if (!isNaN(totalAmount)) {
	totalAmount -= price;
    } else {
	totalAmount = 0;
    }

    //Если товар уже есть в куках, убавляем ему количество
    //Заодно подсчитаем количество всех товаров
    var foundSame = false;
    var productsCount = 0;
    for (var i in products) {
	if (products[i].id == productId) {
	    products[i].amount--;
	    foundSame = true;
	}
	productsCount += products[i].amount;
    }
    if (!foundSame) {
	//do nothing
    }


    setCookie('cart_products', generateProductsString(products), '', '/');
    setCookie('total_amount', totalAmount, '', '/');
    //Изменить оформление
    refreshBasketInfo();
}

function setBasketInfo(amount, summ) {
    //Обратная совместимость
    refreshBasketInfo();
}

function refreshBasketInfo() {
    //получим данные из кук
    var products = parseProductsString(getCookie('cart_products'));
    var totalAmount = parseInt(getCookie('total_amount'));
    if (isNaN(totalAmount)) {
	totalAmount = 0;
    }
    var productsCount = 0;
    for (var i in products) {
	productsCount += products[i].amount;
    }

    $('#basket-summ-products').text(productsCount);
    $('#basket-summ-price').text(moneyFormat(totalAmount));
    $('#basket-summ-products-text').text(declOfNum(productsCount, ['товар', 'товара', 'товаров']));
}

function moneyFormat(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
	x1 = x1.replace(rgx, '$1' + ' ' + '$2');
    }
    return x1 + x2;
}

function declOfNum(number, titles) {
    cases = [2, 0, 1, 1, 1, 2];
    return titles[ (number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5] ];
}

function intVal(mixed_var, base) {
    var tmp;

    if (typeof (mixed_var) == 'string') {
	tmp = parseInt(mixed_var);
	if (isNaN(tmp)) {
	    return 0;
	} else {
	    return tmp.toString(base || 10);
	}
    } else if (typeof (mixed_var) == 'number') {
	return Math.floor(mixed_var);
    } else {
	return 0;
    }
}

function recalculateBasket() {
    var productsCount = 0;
    var priceSumm = 0;
    var products = new Array();

    var _break = false;
    //Блокируем изменение инпутов до завершения аякса
    $('.amount-input').attr('disabled', 'disabled');

    $('#cart-items .basket-item').each(function(index, element) {
	if (_break == false) {
	    var amount = $(element).find('input[name="amount"]').val();
	    var price = $(element).find('input[name="price"]').val();
	    var pid = $(element).find('input[name="id"]').val();

	    if (amount == '0' || amount == '') {
		$(element).find('input[name="amount"]').val(1);
		amount = '1';
	    }

	    if (amount.match(/^[0-9]{1,3}$/)) {
		amount = parseInt(amount);
		productsCount += amount;
		priceSumm += parseInt(price) * amount;
		//
		$(element).find('.sum span').text(moneyFormat(parseInt(price) * amount));
		//
		products.push({
		    "id": pid,
		    "amount": amount
		});
	    } else {
		productsCount = '-';
		priceSumm = '-';
		_break = true;
	    }
	}
    });

    //Если ошибок не было
    if (_break == false) {
	//Записываем куки
	setCookie('cart_products', generateProductsString(products), '', '/');
	setCookie('total_amount', priceSumm, '', '/');

	//Изменяем данные в блоке корзины
	setBasketInfo(productsCount, priceSumm);

	var coverField = $('#cart-items');
	var cover = $('<div></div>').css({
	    'position': 'absolute',
	    'left': coverField.offset().left,
	    'top': coverField.offset().top,
	    'width': coverField.width(),
	    'height': coverField.height(),
	    'zIndex': 100,
	    'background': '#fff',
	    'opacity': .6
	}).html('<div style="text-align:center;">Обновляются данные...</div>').appendTo('body');

	//Запрос на стоимость доставкаи и размер скидки

	$.ajax({
	    url: '/ajax/basket.php?method=get_delivery_and_discount',
	    type: 'POST',
	    dataType: 'json',
	    data: {"products": products},
	    beforeSend: function() {
		$('#delivery-1 .value, #delivery-2 .value').html('<img src="/img/ajax_loading.gif" />');
	    },
	    success: function(data, textStatus, jqXHR) {

		orderPrice.delivery = intVal(data.deliveryPrice);
		orderPrice.discount = intVal(data.discount);
		orderPrice.summ = intVal(priceSumm);
		showPriceInfo();

		//Изменяем данные на странице корзины
		$('#totalItemPrice').html(moneyFormat(priceSumm));
		$('#cartDiscountPersent').html(orderPrice.discount);
		if (orderPrice.discount > 0) {
		    $('#discount-field').show();
		} else {
		    $('#discount-field').hide();
		}

		$('#basket-products-count').html(productsCount);

	    },
	    complete: function() {
		cover.remove();
		//Разблокируем инпуты для ввода
		$('.amount-input').removeAttr('disabled');
	    },
	    error: function(jqXHR, textStatus, errorThrown) {
	    }
	});

    }
    if (_break == false && productsCount > 0) {
	$('#continue-order').removeAttr('disabled');
    } else {
	$('#continue-order').attr('disabled', 'disabled');
	$('.amount-input').removeAttr('disabled');
    }
}

function showPriceInfo() {
    var orderInfo = $('#overall');
    var discount = $('#discount-field');
    var summ = $('#total-summ');
    var delivery = $('.delivery');
    deliveryType = $('select[name="delivery_type"]').val();

    var total = $('#total-summ');


    var price = orderPrice.summ;
    //Настройки по умолчанию
    delivery.hide();

    //Инфо о скидке
    if (orderPrice.discount > 0) {
	discount.find('.num').text(orderPrice.discount);
	discount.show();
	price = Math.round(price * ((100 - orderPrice.discount) / 100));
    } else {
	discount.hide();
    }

    //Инфо общая сумма
    summ.find('.num').text(moneyFormat(price));

    switch (deliveryType) {
	case 1 :
	case '1':
	    var msk = false;
	    if (deliveryType == 1 || deliveryType == '1') {
		//По Москве
		var deliveryDiv = orderInfo.find('.msk');
		msk = true;
	    } /*else {
		//За мкад
		var deliveryDiv = orderInfo.find('.msk-region');
	    }*/

	    //Просчет доставки курьером по москве
	    if (orderPrice.delivery === false) {
		orderInfo.find('.individual').show();
	    } else if (orderPrice.delivery === 0) {
		if (msk) {
		    deliveryDiv.find('.num').text('Бесплатно');
		    deliveryDiv.find('.unit').hide();
		} else {
		    deliveryDiv.find('.value').text('');
		    deliveryDiv.find('.section').hide();
		}
		total.find('.num').text(moneyFormat(price));
	    } else {
		if (msk) {
		    deliveryDiv.find('.num').text(orderPrice.delivery);
		    deliveryDiv.find('.unit').show();
		    total.find('.num').text(moneyFormat(parseInt(price) + parseInt(orderPrice.delivery))).show();
		} else {
		    deliveryDiv.find('.value').html(orderPrice.delivery);
		    deliveryDiv.find('.section').show();
		    total.hide();
		}

	    }
	    deliveryDiv.show();
	    //delivery.show();
	    break;
		
	case 5:
	case '5':
	    //Самовывоз
	    delivery.hide();
	    total.hide();
	    break;
	default :
	    var deliveryDiv = orderInfo.find('.individual');
	    //delivery.show();
	    deliveryDiv.show();
	    total.hide();
    }
}

var deliveryType;

$(document).ready(function() {



    // FXs for search form on navbar
    var searchString = $(".navbar-right .search-query");

    searchString.focus(function() {
	if (searchString.val() === 'Введите название товара...') {
	    searchString.val('');
	}
    });

    searchString.blur(function() {
	if (searchString.val() === '') {
	    searchString.val('Введите название товара...');
	}
    });

    // Action fot splash slider tips
    $('.tips img').hover(function() {
		if(!$(this).closest('.tips').hasClass('noTooltip')){
			var text = $(this).attr('alt');
			$(this).after('<div class="tipsPopup rounded">' + text + '</div>');
			$(this).siblings('.tipsPopup').css('opacity', 1);
		}
    }, function() {
		$(this).siblings('.tipsPopup').remove();
    });

    $div = null;

    $('#splash-slider-carousel .thumbs').show();

    $('#splash-slider-carousel .thumbs').children().each(function(i) {
	if (i % 6 === 0) {
	    $div = $('<div />');
	    $div.appendTo('#splash-slider-carousel .thumbs');
	}
	$(this).appendTo($div);
	$(this).addClass('item_' + i);
	$(this).click(function() {
	    $('#splash-slider-content').trigger('slideTo', [i, 0, true]);
	});
    });

    $('#splash-slider-carousel .thumbs .item_0').addClass('active');

    $('#splash-slider-content').carouFredSel({
	circular: false,
	infinite: false,
	width: 710,
	height: 343,
	items: 1,
	auto: false,
	prev: '#splash-slider-wrap .arrow.left',
	next: '#splash-slider-wrap .arrow.right',
	scroll: {
	    fx: 'directscroll',
	    onBefore: function() {
		var pos = $(this).triggerHandler('currentPosition');
		$('#splash-slider-carousel .thumbs a').removeClass('active');
		$('#splash-slider-carousel .thumbs a.item_' + pos).addClass('active');
		var page = Math.floor(pos / 6);
		$('#splash-slider-carousel .thumbs').trigger('slideToPage', page);
	    }
	}
    });

    $('#splash-slider-carousel .thumbs').after('<div class="pager" />').carouFredSel({
	circular: false,
	infinite: false,
	width: 196,
	height: 305,
	items: 1,
	align: false,
	auto: false,
	pagination: '#splash-slider-carousel .pager'
    });

    $("#catalog-carousel-list").carouFredSel({
	circular: true,
	infinite: true,
	scroll: 1,
	auto: false,
	prev: "#catalog-carousel-list-prev",
	next: "#catalog-carousel-list-next"
    });


    // Product photo thumbs carousel
    $('.mPhotoThumbs ul').after('<div class="mPhotoThumbsNav"><div class="arrow prev" /><div class="pager" /><div class="arrow next" /></div>').carouFredSel({
	items: 3,
	circular: false,
	infinite: false,
	auto: false,
	prev: '.mPhotoThumbs .prev',
	next: '.mPhotoThumbs .next',
	pagination: '.mPhotoThumbs .pager',
    });
    $('.mPhotoThumbs ul a').click(function() {
	//if(!$(this).parent('li').is('.current')){
	var curImg = $('<img src="' + $(this).attr("href") + '" title="' + $('.mPhotoThumbs ul a img').attr("alt")  + '" alt="' + $('.mPhotoThumbs ul a img').attr("alt") + '" style="display:none;">');
	$(this).parents('.mPhotoThumbs ul').find('li.current').removeClass('current').find('.mask').remove();
	$(this).parent('li').addClass('current').append('<span class="mask" />');
	var links = new Array();
	$(this).closest('li').siblings().each(function(index, el) {
	    links.push($(el).find('a').data('original'));
	});
	var a = $('<a>').attr('href', $(this).data('original')).attr('rel', 'fancygroup').addClass('fgallery');
	a.append(curImg);

	var container = $(this).parents('.photos').find('.mPhoto');

	container.empty();
	container.append(a);
	container.append('<div class="loader" />');

	curImg.load(function() {
	    $(this).fadeIn(200);
	    //$(this).siblings('img').remove();
	    //$(this).siblings('.loader').remove();
	    $(this).closest('a').siblings().remove();

	    for (var i in links) {
		container.append(
			$('<a>').attr('href', links[i]).attr('rel', 'fancygroup').addClass('fgallery').css('display', 'none')
			);
	    }
	    $("a.fgallery").fancybox();
	});
	//};
	return false;
    });


    // Set equal heights to footer columns
    setEqualHeight($("#theme-footer .column"));

    // Buy link
    $('a.buy-link').click(function() {
	if (!$(this).is('.active')) {
	    $('.confirmation-add').css({
		'top': $(document).scrollTop() + $(window).height() / 2 - 600 / 2
	    });
	    $(this).addClass('active');
	    //$(this).text('В корзине');
		// yaCounter23697286.reachGoal('ADD_TO_CART');
	    addProductToBasket($(this).data('product_id'), $(this).data('product_price'));
	    return false;
	}
    });
	
    //add feedback
    $('.add-feed-button a').on('click', function() {

    	$('#fastorder-form2 input[name="product_id"]').val($(this).parent().data('product_id'));
    	// alert($(this).parent().data('product_id'));
        // <input type="hidden" name="method" value="addfeedback">
    	$('.feed-back-popup').css({
    		'top': $(document).scrollTop() + $(window).height() / 2 - $('.quickBuy').outerHeight() / 2
    	});
    	$('.overlay, .feed-back-popup').show();

    	var win = $('.feed-back-popup');
    	var form = $('#fastorder-form2');
    	var loading = win.find('.loading');

    	form.show();
    	//win.find('.success').hide();
    	//win.find('.send-button').show();

    	win.find('.send-button').unbind('click').on('click', function() {
    	loading.show();
		
		    $.ajax({
		    	url: '/ajax/system.php',
		    	data: form.serialize(),
		    	type: "post",
		    	dataType: "json",
		    	beforeSend: function() {
		    		form.find('input, textarea').attr('disabled', 'disabled');
		    		win.find('.error').hide();
		    	},
		    	success: function(data) {
		    		//alert(data);
		    		if (!data.error) {
		    			console.log(1);
		    			win.find('.close').trigger('click');
		    			form.find('input, textarea').each(function(index, el) {
		    				$(el).val(el.defaultValue);
		    			});
		    			form.find("input, textarea").hide();
		    			form.find('.success').show();
		    			form.find('label').hide();
		    			form.find('.rate_stars').hide();
		    			form.find('.send-button').hide();
		    		} else {
		    			win.find('.error-code-' + data.error_code).show();
		    		}
		    	},
		    	complete: function() {
		    		loading.hide();
		    		form.find('input, textarea').removeAttr('disabled');
		    	}
		    });
		    return false;
		});
    	return false;
    });


    //Order delete button
    $('#cart-items .basket-item .delete a').click(function() {

    	$('.confirmation-delete').css({
    		'top': $(document).scrollTop() + $(window).height() / 2 - 600 / 2
    	});

    	var basketItem = $(this).closest('.basket-item');

    	var win = $('.confirmation-delete');

    	win.find('.name')
    	.attr('href', basketItem.find('.name a').attr('href'))
    	.text(basketItem.find('.name a').text());

    	win.find('.price span').text(moneyFormat(basketItem.find('input[name="price"]').val()));

	//Вешаем обработчик на событие удалить

	win.find('.cartDelLink.y').unbind('click').bind('click', function() {
		basketItem.remove();
		win.find('.closenow').trigger('click');
		recalculateBasket();
	});

	win.find('.cartDelLink.n').unbind('click').bind('click', function() {
		win.find('.closenow').trigger('click');
	});

	$('.overlay, .confirmation-delete').show();
	return false;

});

    // Order
    $('.order a.doneBtn').click(function() {
			if (!$(this).is('.disabled')) {
				$(this).addClass('disabled');
				var curPos=$(document).scrollTop();
					var height=$("body").height();
					var scrollTime=(height-curPos)/1.73;
					$("body,html").animate({"scrollTop":height},scrollTime);
				$('.order .orderForm').slideDown();
			}
    	return false;
    });

    $(".tabContents").hide(); // Hide all tab content divs by default
    $(".tabContents:first").show(); // Show the first div of tab content by default

    $(".ww_list ul li a").click(function() { //Fire the click event

	var activeTab = $(this).attr("href"); // Catch the click link
	$(".ww_list ul li a").removeClass("active"); // Remove pre-highlighted link
	$(this).addClass("active"); // set clicked link to highlight state
	$(".tabContents").hide(); // hide currently visible tab content div
	$(activeTab).fadeIn(); // show the target tab content div by matching clicked link.

	return false; //prevent page scrolling on tab click
    });

    $('.popup .closenow').click(function() {
	$('.overlay, .popup').hide();
	$('.overlay').css('z-index', 10000);
    });

    $('.buy-link').bind("click", function() {

	$('.overlay, .confirmation-add').show();
	return false;

    });

    $("a.fgallery").fancybox();
    $('.mPhotoThumbs ul a:first').trigger('click');

    // Tabs
    //$('#tabs').tabs();

    $('.modal_worldwide_init').bind("click", function() {

	$('.overlay').css('z-index', 1000);
	$('.overlay, .modal_worldwide_wrap').show();
	return false;

    });

    // Custom select
    $('.sort select, .deliveryMethod select').selectbox({
	speed: 0,
	onOpen: function() {
	    $(this).next().addClass('focus');
	},
	onClose: function() {
	    $(this).next().removeClass('focus');
	}
    });

    $('#sort-filter').bind('change', function() {
	var uri = $(this).find('option:checked').data('uri');
	window.location = uri;
    });
// Sliders
    // Price slider
    if ($('#price-slider').length) {
	$('#price-slider').slider({
	    range: true,
	    min: price.min,
	    max: price.max,
	    //step: 500,
	    values: [price.min, price.max],
	    slide: function(event, ui) {
		$(".price-min").val(ui.values[0]);
		$(".price-max").val(ui.values[1]);
	    },
	    change: function() {
		defaultSort = 'price_asc';
		reloadProducts();
	    }
	});
	$('#price-slider').find('.ui-slider-range').append('<input type="text" class="price-min" disabled /><input type="text" class="price-max" disabled />');
	$('.price-min').val($('#price-slider').slider('values', 0));
	$('.price-max').val($('#price-slider').slider('values', 1));

	//Range slider
	$('.range-slider').each(function(index, element) {
	    var _this = $(element);
	    var sliderBody = _this.find('.slider-body');

	    var dataField = _this.find('.slider-field');
	    var minValue = dataField.data('min');
	    var maxValue = dataField.data('max');
	    var paramId = dataField.data('param_id');
	    sliderBody.slider({
		range: true,
		min: minValue,
		max: maxValue,
		step: 1,
		values: [minValue, maxValue],
		slide: function(event, ui) {
		    sliderBody.find(".weight-min").val(ui.values[0]);
		    sliderBody.find(".weight-max").val(ui.values[1]);

		    _this.find(".weight-min").val(ui.values[0]);
		    _this.find(".weight-max").val(ui.values[1]);
		},
		change: function(event, ui) {
		    dataField.empty();
		    dataField.append(
			    $('<input>').attr({
			"type": "hidden",
			"name": "filter_range[" + paramId + "][min]",
			"value": ui.values[0]
		    })
			    );
		    dataField.append(
			    $('<input>').attr({
			"type": "hidden",
			"name": "filter_range[" + paramId + "][max]",
			"value": ui.values[1]
		    })
			    );
		    reloadProducts();
		    return false;
		}
	    });
	    /*sliderBody.find('.ui-slider-range').append('<input type="text" class="weight-min" disabled /><input type="text" class="weight-max" disabled />');
	    sliderBody.find('.weight-min').val(sliderBody.slider('values', 0));
	    sliderBody.find('.weight-max').val(sliderBody.slider('values', 1));*/
	});

	$('.filters-field .checkbox-param').bind('click', function() {
	    var _this = $(this);

	    if (_this.hasClass('active')) {
		_this.removeClass('active');
		_this.find('input[type="checkbox"]').removeAttr('checked');
	    } else {
		_this.addClass('active');
		_this.find('input[type="checkbox"]').attr('checked', 'checked');
	    }
	    reloadProducts();
	});

	// On change inputs
	$('.range-min').change(function() {
	    var wrapper = $(this).closest('.range-slider');
	    var sliderBody = wrapper.find('.slider-body');

	    var newValue = $(this).val();
	    var min = sliderBody.slider('option', 'min');
	    var max = sliderBody.slider('option', 'max');
	    var values = sliderBody.slider('option', 'values');
	    if (newValue <= values[1]) {
		if (newValue >= min) {
		    sliderBody.slider('option', 'values', [newValue, values[1]]);
		    wrapper.find('.weight-min').val(newValue);
		} else {
		    sliderBody.slider('option', 'values', [min, values[1]]);
		    wrapper.find('.weight-min').val(min);
		}
	    } else {
		if (newValue <= max) {
		    sliderBody.slider('option', 'values', [newValue, newValue]);
		    wrapper.find('.weight-min').val(newValue);
		    wrapper.find('.weight-max').val(newValue);
		} else {
		    sliderBody.slider('option', 'values', [max, max]);
		    wrapper.find('.weight-min').val(max);
		    wrapper.find('.weight-max').val(max);
		}
	    }
	});
	$('.range-max').change(function() {
	    var wrapper = $(this).closest('.range-slider');
	    var sliderBody = wrapper.find('.slider-body');

	    var newValue = $(this).val();
	    var min = sliderBody.slider('option', 'min');
	    var max = sliderBody.slider('option', 'max');
	    var values = sliderBody.slider('option', 'values');
	    if (newValue >= values[0]) {
		if (newValue <= max) {
		    sliderBody.slider('option', 'values', [values[0], newValue]);
		    wrapper.find('.weight-max').val(newValue);
		} else {
		    sliderBody.slider('option', 'values', [values[0], max]);
		    wrapper.find('.weight-max').val(max);
		}
	    } else {
		if (newValue >= min) {
		    sliderBody.slider('option', 'values', [newValue, newValue]);
		    wrapper.find('.weight-min').val(newValue);
		    wrapper.find('.weight-max').val(newValue);
		} else {
		    sliderBody.slider('option', 'values', [min, min]);
		    wrapper.find('.weight-min').val(min);
		    wrapper.find('.weight-max').val(min);
		}
	    }
	});

	// Filter     

	function filterToggle() {
	    if ($('.filter').is('.open')) {
		$('.filterToggle').find('span').text('Больше параметров');
		$('.filter .filters-field').slideUp();
		$('.filter').removeClass('open');
	    } else {
		$('.filterToggle').find('span').text('Скрыть параметры');
		$('.filter .filters-field').slideDown();
		$('.filter').addClass('open');
	    }
	}

	$('.filterToggle').click(function() {
	    filterToggle();
	});

	if ($('.filter').is('.open')) {
	    $('.filter').removeClass('open');
	    filterToggle();
	}


    }

    // Catalogue description text
    var timer = null;

    $('ul.catalogue>li').hover(function() {
	var _this = $(this);
	if (_this.position().left >= 716) {
	    $(this).addClass('leftDescription');
	}
	;
	timer = setTimeout(function() {
	    _this.addClass('active');
	}, 500);
    }, function() {
	clearTimeout(timer);
	$(this).removeClass('active');
    });

    $('#catalog-carousel-list .item').hover(function() {
	var _this = $(this);
	if (_this.position().left >= 716) {
	    $(this).addClass('leftDescription');
	}
	_this.addClass('active');
    }, function() {
	$(this).removeClass('leftDescription');
	$(this).removeClass('active');
    });




    $('#tabs li a').click(function(e) {

	$('#tabs li, #content .current').removeClass('current').removeClass('fadeInLeft');
	$(this).parent().addClass('current');
	var currentTab = $(this).attr('href');
	$(currentTab).addClass('current fadeInLeft');
	e.preventDefault();

    });

});