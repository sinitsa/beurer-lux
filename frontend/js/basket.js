function setCookie(name, value, expires, path, domain, secure) {
    document.cookie = name + "=" + escape(value) +
    ((expires) ? "; expires=" + expires : "") +
    ((path) ? "; path=" + path : "") +
    ((domain) ? "; domain=" + domain : "") +
    ((secure) ? "; secure" : "");
}
function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}

function parseProductsString(string) {
    var returnArray = new Array();

    if (string.length > 0) {
        var products = string.split('|');
        for (var i in products) {
            var temp = products[i].split(':');

            returnArray.push({
                'id': parseInt(temp[0]),
                'amount': parseInt(temp[1])
            });
        }
    }
    return returnArray;
}
function generateProductsString(products) {
    var tempArray = new Array();
    for (var i in products) {
        tempArray.push(products[i].id + ':' + products[i].amount);
    }
    return tempArray.join('|');
}
function addProductToBasket(productId, price) {
    //получим данные из кук
    var products = parseProductsString(getCookie('cart_products'));
    var totalAmount = parseInt(getCookie('total_amount'));
    if (!isNaN(totalAmount)) {
        totalAmount += price;
    } else {
        totalAmount = price;
    }

    //Если товар уже есть в куках, добавляем ему количество
    //Заодно подсчитаем количество всех товаров
    var foundSame = false;
    var productsCount = 0;
    for (var i in products) {
        if (products[i].id == productId) {
            products[i].amount++;
            foundSame = true;
        }
        productsCount += products[i].amount;
    }
    if (!foundSame) {
        products.push({
            "id": productId,
            "amount": 1
        });
        productsCount += 1;
    }


    setCookie('cart_products', generateProductsString(products), '', '/');
    setCookie('total_amount', totalAmount, '', '/');
    //Изменить оформление
    refreshBasketInfo();
    
}
function deleteProductFromBasket(productId, price) {
    //получим данные из кук
    var products = parseProductsString(getCookie('cart_products'));
    var totalAmount = parseInt(getCookie('total_amount'));
    if (!isNaN(totalAmount)) {
        totalAmount -= price;
    } else {
        totalAmount = 0;
    }

    //Если товар уже есть в куках, убавляем ему количество
    //Заодно подсчитаем количество всех товаров
    var foundSame = false;
    var productsCount = 0;
    for (var i in products) {
        if (products[i].id == productId) {
            products[i].amount--;
            foundSame = true;
        }
        productsCount += products[i].amount;
    }
    if (!foundSame) {
        //do nothing
    }


    setCookie('cart_products', generateProductsString(products), '', '/');
    setCookie('total_amount', totalAmount, '', '/');
    //Изменить оформление
    refreshBasketInfo();
}

function setBasketInfo(amount, summ) {
    //Обратная совместимость
    refreshBasketInfo();
}
function refreshBasketInfo() {
    //получим данные из кук
    var products = parseProductsString(getCookie('cart_products'));
    var totalAmount = parseInt(getCookie('total_amount'));
    if (isNaN(totalAmount)) {
        totalAmount = 0;
    }
    var productsCount = 0;
    for (var i in products) {
        productsCount += products[i].amount;
    }

    $('#basket-summ-products').text(productsCount);
    $('#basket-summ-price').text(moneyFormat(totalAmount));
    $('#basket-summ-products-text').text(declOfNum(productsCount, ['товар', 'товара', 'товаров']));
}

function recalculateBasket() {
    var productsCount = 0;
    var priceSumm = 0;
    var products = new Array();

    var _break = false;
    //Блокируем изменение инпутов до завершения аякса
    $('.amount-input').attr('disabled', 'disabled');

    $('#cart-items .basket-item').each(function (index, element) {
        if (_break == false) {
            var amount = $(element).find('input[name="amount"]').val();
            var price = $(element).find('input[name="price"]').val();
            var pid = $(element).find('input[name="id"]').val();

            if (amount == '0' || amount == '') {
                $(element).find('input[name="amount"]').val(1);
                amount = '1';
            }

            if (amount.match(/^[0-9]{1,3}$/)) {
                amount = parseInt(amount);
                productsCount += amount;
                priceSumm += parseInt(price) * amount;
                //
                $(element).find('.sum span').text(moneyFormat(parseInt(price) * amount));
                //
                products.push({
                    "id": pid,
                    "amount": amount
                });
            } else {
                productsCount = '-';
                priceSumm = '-';
                _break = true;
            }
        }
    });

    //Если ошибок не было
    if (_break == false) {
        //Записываем куки
        setCookie('cart_products', generateProductsString(products), '', '/');
        setCookie('total_amount', priceSumm, '', '/');

        //Изменяем данные в блоке корзины
        setBasketInfo(productsCount, priceSumm);

        var coverField = $('#cart-items');
        var cover = $('<div></div>').css({
            'position': 'absolute',
            'left': coverField.offset().left,
            'top': coverField.offset().top,
            'width': coverField.width(),
            'height': coverField.height(),
            'zIndex': 100,
            'background': '#fff',
            'opacity': .6
        }).html('<div style="text-align:center;">Обновляются данные...</div>').appendTo('body');

        //Запрос на стоимость доставкаи и размер скидки

        $.ajax({
            url: '/ajax/basket.php?method=get_delivery_and_discount',
            type: 'POST',
            dataType: 'json',
            data: {"products": products},
            beforeSend: function () {
                $('#delivery-1 .value, #delivery-2 .value').html('<img src="/img/ajax_loading.gif" />');
            },
            success: function (data, textStatus, jqXHR) {

                orderPrice.delivery = intVal(data.deliveryPrice);
                orderPrice.discount = intVal(data.discount);
                orderPrice.summ = intVal(priceSumm);
                showPriceInfo();

                //Изменяем данные на странице корзины
                $('#totalItemPrice').html(moneyFormat(priceSumm));
                $('#cartDiscountPersent').html(orderPrice.discount);
                if (orderPrice.discount > 0) {
                    $('#discount-field').show();
                } else {
                    $('#discount-field').hide();
                }

                $('#basket-products-count').html(productsCount);

            },
            complete: function () {
                cover.remove();
                //Разблокируем инпуты для ввода
                $('.amount-input').removeAttr('disabled');
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });

    }
    if (_break == false && productsCount > 0) {
        $('#continue-order').removeAttr('disabled');
    } else {
        $('#continue-order').attr('disabled', 'disabled');
        $('.amount-input').removeAttr('disabled');
    }
}

function showPriceInfo() {
    var orderInfo = $('#overall');
    var discount = $('#discount-field');
    var summ = $('#total-summ');
    var delivery = $('.delivery');
    deliveryType = $('select[name="delivery_type"]').val();

    var total = $('#total-summ');


    var price = orderPrice.summ;
    //Настройки по умолчанию
    delivery.hide();

    //Инфо о скидке
    if (orderPrice.discount > 0) {
        discount.find('.num').text(orderPrice.discount);
        discount.show();
        price = Math.round(price * ((100 - orderPrice.discount) / 100));
    } else {
        discount.hide();
    }

    //Инфо общая сумма
    summ.find('.num').text(moneyFormat(price));

    switch (deliveryType) {
        case 1 :
        case '1':
        case 2:
        case '2':
            var msk = false;
            if (deliveryType == 1 || deliveryType == '1') {
                //По Москве
                var deliveryDiv = orderInfo.find('.msk');
                msk = true;
            } else {
                //За мкад
                var deliveryDiv = orderInfo.find('.msk-region');
            }

            //Просчет доставки курьером по москве
            if (orderPrice.delivery === false) {
                orderInfo.find('.individual').show();
            } else if (orderPrice.delivery === 0) {
                if (msk) {
                    deliveryDiv.find('.num').text('Бесплатно');
                    deliveryDiv.find('.unit').hide();
                } else {
                    deliveryDiv.find('.value').text('');
                    deliveryDiv.find('.section').hide();
                }
                total.find('.num').text(moneyFormat(price));
            } else {
                if (msk) {
                    deliveryDiv.find('.num').text(orderPrice.delivery);
                    deliveryDiv.find('.unit').show();
                    total.find('.num').text(moneyFormat(parseInt(price) + parseInt(orderPrice.delivery))).show();
                } else {
                    deliveryDiv.find('.value').html(orderPrice.delivery);
                    deliveryDiv.find('.section').show();
                    total.hide();
                }


            }
            deliveryDiv.show();
            //delivery.show();
            break;
        case 3:
        case '3':        
		case 4:
        case '4':
            //Самовывоз
            delivery.hide();
            total.hide();
            break;
        default :
            var deliveryDiv = orderInfo.find('.individual');
            //delivery.show();
            deliveryDiv.show();
            total.hide();
    }
}
var deliveryType;
$(function () {
    //Листенер изменение количества товаров в корзине
    var recalcTimer;
    $('.amount-input').bind('keyup', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);

        /*
         LEFT=   37
         UP=     38
         RIGHT=  39
         DOWN=   40
         SHIFT=  16
         CONTROL=    17
         ALT=    18
         */
        var keys = [16, 17, 18, 37, 38, 39, 40];
        //Не реагировать на нажатие клавиш, обозначеных выше
        if (recalcTimer != undefined) clearInterval(recalcTimer);
        if (!(keys.indexOf(code) >= 0)) {
            recalcTimer = setTimeout('recalculateBasket();', 1000);
        }
    });
    deliveryType = $('#order-form select[name="delivery_type"]').val();

    $('#order-form select[name="delivery_type"]').bind('change', function () {
        deliveryType = $(this, ':checked').val();
        showPriceInfo();
    });


    //Обычный заказ
    $('#order-form').bind('submit', function () {

        var form = $('#order-form');
        var name = form.find('input[name="name"]');
        var phone = form.find('input[name="phone"]');
        var loading = form.find('.loading');
        var inProcess = $(this).hasClass('in-process');
        var sendButton = $(this);

        alert('ppc');

        if (inProcess)
            return false;

        if (name.val().length <= 3 || phone.val().length < 5) {
            alert('Поля, отмеченные звездочкой, обязательны для заполнения');
            return false;
        }

        $.ajax({
            url: '/ajax/basket.php?method=make_order',
            type: 'POST',
            dataType: 'json',
            data: form.serialize(),
            beforeSend: function () {
                loading.show();
                form.find('input').attr('disabled', 'disabled');
                form.find('textarea').attr('disabled', 'disabled');
                sendButton.addClass('in-process');
            },
            success: function (data, textStatus, jqXHR) {
//                        console.dir(data);
//                        alert(1);
//                        alert(data.bank);
                $('.confirmation .name').text(name.val());
                $('.confirmation .order-code').text(data.order_code);
                $('.confirmation .close').remove();
                if (data.bank) {
                    setCookie("order_id", data.order_id);
                    $(".confirmation.popup.rounded .outer.rounded a").attr({"href": "/cart/credite.html"});
                    $(".confirmation.popup.rounded .outer.rounded a").text("Перейти к оформлению кредита");
                }
                $('.confirmation').css({
                    'top': $(document).scrollTop() + $(window).height() / 2 - $('.confirmation').outerHeight() / 2
                });
                $('.overlay, .confirmation').show();
                $('.overlay').unbind('click');

                var goodsArray = new Array();
                for (var i in data.products) {
                    goodsArray.push({
                        'id': data.products[i].id,
                        'price': data.products[i].price,
                        'name': data.products[i].converted_title,
                        'quantity': data.products[i].amount,
                    });
                }
                var yaParams = {
                    order_id: data.order_id,
                    order_price: data.price,
                    currency: "RUR",
                    exchange_rate: 1,
                    goods: goodsArray
                };
                yaCounter21201121.reachGoal('ORDER', yaParams);
            },
            complete: function () {
                loading.hide();
                form.find('input').removeAttr('disabled');
                form.find('textarea').removeAttr('disabled');
                sendButton.removeClass('in-process');
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });
        return false;
    });

    // Молниеносный заказ
    // Quick buy
    $('a.oneClickBuy').click(function () {
        //наполняем инфо о товаре
        $('.quickBuy .name').attr('href', '').text($(this).data('name'));
        $('.quickBuy .price span').text(moneyFormat($(this).data('price')));
        $('#fastorder-form input').val('');

        $('#fastorder-form input[name="product_id"]').val($(this).data('product_id'));

        $('.quickBuy').css({
            'top': $(document).scrollTop() + $(window).height() / 2 - $('.quickBuy').outerHeight() / 2
        });
        $('.overlay, .quickBuy').show();
        return false;
    });

    $('#fastorder-form').bind('submit', function () {
        var wrap = $(this);
        var loading = wrap.find('.loading');
        var name = wrap.find('input[name="name"]');
        var phone = wrap.find('input[name="phone"]');
        var email = wrap.find('input[name="email"]');
        var product_id = wrap.find('input[name="product_id"]');
        var button = $(this);

        if (name.val().length <= 1 || phone.val().length < 5 || name.val() == 'Имя:' || phone.val() == 'Телефон:') {
            alert('Введите Ваше имя и телефон');
            return false;
        }
        if (wrap.hasClass('in-process')) {
            return false;
        }
        $.ajax({
            url: '/ajax/basket.php?method=make_order',
            type: 'POST',
            dataType: 'json',
            data: {'name': name.val(), "phone": phone.val(), "product_id": product_id.val(), "fast_order": 1},
            beforeSend: function () {
                name.attr('disabled', 'disabled');
                phone.attr('disabled', 'disabled');
                email.attr('disabled', 'disabled');
                button.attr('disabled', 'disabled');
                loading.show();
                wrap.addClass('in-process');
            },
            success: function (data, textStatus, jqXHR) {

                $('.quickBuy .close').trigger('click');

                // Order accepted popup
                $('.confirmation .name').text(name.val());
                $('.confirmation .order-code').text(data.order_code);

                $('.confirmation').css({
                    'top': $(document).scrollTop() + $(window).height() / 2 - $('.confirmation').outerHeight() / 2
                });
                $('.overlay, .confirmation').show();

                var goodsArray = new Array();
                for (var i in data.products) {
                    goodsArray.push({
                        'id': data.products[i].id,
                        'price': data.products[i].price,
                        'name': data.products[i].converted_title,
                        'quantity': data.products[i].amount,
                    });
                }
                var yaParams = {
                    order_id: data.order_id,
                    order_price: data.price,
                    currency: "RUR",
                    exchange_rate: 1,
                    goods: goodsArray
                };
                yaCounter21201121.reachGoal('ORDER', yaParams);
            },
            complete: function () {
                name.removeAttr('disabled');
                phone.removeAttr('disabled');
                email.removeAttr('disabled');
                button.removeAttr('disabled');
                loading.hide();
                wrap.removeClass('in-process');
            }
        });
        return false;
    });
});
function getNewSize(oldWidth, oldHeight, destWidth, destHeight) {
    if (destWidth) {
        var iScaleW = destWidth / oldWidth;
    } else {
        var iScaleW = 1;
    }
    if (destHeight) {
        var iScaleH = destHeight / oldHeight;
    } else {
        var iScaleH = 1;
    }

    var iSizeRelation = ( (iScaleW < iScaleH) ? iScaleW : iScaleH);
    var iWidthNew = Math.round(oldWidth * iSizeRelation);
    var iHeightNew = Math.round(oldHeight * iSizeRelation);

    var iSizeW = iWidthNew;
    var iSizeH = iHeightNew;
    iDestX = 0;
    iDestY = 0;

    return {"width": iSizeW, "height": iSizeH};
}
function moneyFormat(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ' ' + '$2');
    }
    return x1 + x2;
}
function declOfNum(number, titles) {
    cases = [2, 0, 1, 1, 1, 2];
    return titles[ (number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5] ];
}
function intVal(mixed_var, base) {
    var tmp;

    if (typeof( mixed_var ) == 'string') {
        tmp = parseInt(mixed_var);
        if (isNaN(tmp)) {
            return 0;
        } else {
            return tmp.toString(base || 10);
        }
    } else if (typeof( mixed_var ) == 'number') {
        return Math.floor(mixed_var);
    } else {
        return 0;
    }
}