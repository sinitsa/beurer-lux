<?php 

$debug = false; 

$phptime = microtime(true);

if ($debug) {
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
}
/**
* 	Класс реализующий синхронизацию данных (цена, наличие) с 10med
*	Работает для Olesya CMS
*/
class dataUpdater10med {

	private $token;
	private $url;
	private $limit;
	private $myCurl;
	private $db;
	private $time;
	public $count_rows;

	function __construct($curlArr, $dbArr) {

		// $this->limit = $limit;

		$this->time = time();
		$this->count_rows = 0;
		$this->token = $curlArr['token'];
		$this->url = $curlArr['url'];
		$this->myCurl = curl_init();
		
		$this->db = new mysqli($dbArr['host'], $dbArr['login'], $dbArr['pass'], $dbArr['db']);
		if ($this->db->connect_error) {
		    die('Ошибка подключения (' .$this->db->connect_errno . ') '. $this->db->connect_error);
		}
	}

	function __destruct() {
		$this->db->close();
	}

	// Запрос данных к 10Med
	public function request($action, $params) {

		$urlString = $this->url . $action . '?access-token=' . $this->token;

		curl_setopt_array($this->myCurl, array(
		    CURLOPT_URL => $urlString,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POST => true,
		    CURLOPT_POSTFIELDS => http_build_query($params),
		));

		$res = curl_exec($this->myCurl);
		$res = json_decode($res);
		
		return $res;
	}

	// выборка 10med id из всех, товаров сайта, которые имею её
	public function getSiteProducts() {
		$output = array();
		$query = "	SELECT id, linked_with_10med
					FROM catalog
					WHERE linked_with_10med > 0";
		if ($result = $this->db->query($query)) {
			while ($row = $result->fetch_assoc()) { 
				$output[] = $row;
			}
			return $output;
		} else {
			echo "Errormessage: " . $this->db->error;
			return false;
		}
	}

	// обновление цены, наличия
	public function updateProductsRests($storage, $id10med, $rests, $price) {
		if ($rests > 0) {
			$query = "	INSERT INTO storage_rests (storage_id, catalog_id, amount, last_update, newPrice) 
						VALUES (
							{$storage},
							(SELECT id FROM catalog WHERE linked_with_10med = {$id10med}),
							{$rests},
							{$this->time},
							{$price}
						)";
			if ($result = $this->db->query($query)) {
				return true;
			} else {
				echo "Errormessage: " . $this->db->error;
				return false;
			}	
		} else {
			return true;
		}
	} 

	public function updateProductsPrice($id10med, $price) {
		$this->count_rows++;
		$query = "UPDATE catalog SET price = {$price}, price_after_discount = {$price} WHERE linked_with_10med = {$id10med}";
		if ($result = $this->db->query($query)) {
			return true;
		} else {
			echo "Errormessage: " . $this->db->error;
			return false;
		}	
	}

	// очистка таблицы storage_rests
	public function clearStorageRests() {
		$query = "TRUNCATE TABLE storage_rests";
		if ($result = $this->db->query($query)) {
			return true;
		} else {
			echo "Errormessage: " . $this->db->error;
			return false;
		}
	}
}
echo '<pre>';

// Данные подключения к БД
require_once('/var/www/beurerlux/config.db.php');

// Формиррование служебных  данных
$limit = 200;
$curlArr = array(
	'token' => '5RJrtyWrOf7KlNc4Hzzj65NQ7Ud0AOKS',
	'url'	=> 'http://46.101.204.133/api/',
);
$dbArr = array(
	'host'	=> $host,
	'login' => $login,
	'pass'	=> $pass,
	'db'	=> $db,
);

$updater = new dataUpdater10med($curlArr, $dbArr);

$products = $updater->getSiteProducts();
$updater->clearStorageRests();
if ($debug) {	
	print_r($products);
	echo "---------------------------------------- \n";	
	$inputDebug = array();
}

// разбиение на порции
$products = array_chunk($products, $limit);
foreach ($products as $part) {
	// Массив для запии id из 10med
	$params = array();
	foreach ($part as $item) {
		$params['id10med'][] = $item['linked_with_10med'];	
	}
	$input10medData = $updater->request('rest', $params);

	if ($debug) {	
		$inputDebug = array_merge($inputDebug, $input10medData); 
	}
	foreach ($input10medData as $value) {
		$updater->updateProductsRests($value->storageId, $value->id10med, $value->rests, $value->price);
		$updater->updateProductsPrice($value->id10med, $value->price);
		printf("[update row]  1C_key: %' 12s  |   sclad: %' 3d   |   10medId:%' 6d   |   price:%' 7d   |   amount: %' 4d|\n",$value->nomenclature , $value->storageId, $value->id10med, $value->price, $value->rests);
	}
}

printf("------------------------------ \n %d rows update | PHP time: %F \n", $updater->count_rows, (microtime(true)-$phptime));

if ($debug) {
	print_r($inputDebug);
}

unset($updater);
?>