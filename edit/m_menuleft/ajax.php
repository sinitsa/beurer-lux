<?php
include('../../connect.php');
include('../../func/core.php');

switch ($_GET['method']) {
	
	case 'deleteelement' :
		if (!is_numeric($_POST['id'])){
			echo json_encode(array('error' => true));
			break;
		}
		$id = $_POST['id'];
		mysql_query("SET NAMES utf8");
//mysql_query("SET CHARACTER SET utf8");
//mysql_query("SET CHARSET utf8");
		mysql_query("DELETE FROM menuleft WHERE id = {$id} LIMIT 1");
		echo json_encode(array('error' => false));
		break;
		
	case 'addelement' :
		
		if (!is_numeric($_POST['pod'])){
			echo json_encode(array('error' => true));
			break;
		}
		
		$pod = $_POST['pod'];
		mysql_query("SET NAMES utf8");
//mysql_query("SET CHARACTER SET utf8");
//mysql_query("SET CHARSET utf8");
		$row =	mysql_fetch_assoc(mysql_query("
						SELECT MAX(`rang`) as rang 
						FROM `menuleft` 
						WHERE `pod` = {$pod}"));
		$rang = ++$row['rang'];
		
		$query = "INSERT INTO `menuleft`
				  SET `pod` = {$pod}, `rang` = {$rang}";
				  
				  mysql_query("SET NAMES utf8");
//mysql_query("SET CHARACTER SET utf8");
//mysql_query("SET CHARSET utf8");
		mysql_query($query);		
		$id = mysql_insert_id();
		
		if (is_numeric($id)) 
			echo json_encode(array('error' => false, 'id' => $id));
		else
			echo json_encode(array('error' => true));
		break;
		
	case 'setelementvalue':
		//file_put_contents('post.txt', var_export($_POST, true));
		if (!is_numeric($_POST['id'])) {
			echo json_encode(array('error' => true));
			break;
		}
		
		$id = $_POST['id'];
		$name = mysql_real_escape_string($_POST['name']);
		$value = mysql_real_escape_string($_POST['value']);
		
		$query = "UPDATE `menuleft`
				  SET
					`{$name}` = '{$value}'
				  WHERE
					`id` = '{$id}'
				  LIMIT 1";
		mysql_query("SET NAMES utf8");
//mysql_query("SET CHARACTER SET utf8");
//mysql_query("SET CHARSET utf8");
		mysql_query($query);
		
		if (mysql_error())
			echo json_encode(array('error' => true));
		else
			echo json_encode(array('error' => false));
		break;
	case 'setmenuitemsorder' :
		$orderList = $_REQUEST['order_list'];
		$menuId = $_REQUEST['menu_id'];
		
		$list = array();
		$count = count($orderList);

		if (!(is_numeric($menuId) && $menuId > 0)) {
			echo json_encode(array('error' => true));
			break;
		}
		for ($i = 0; $i < $count; $i++) {
			if (is_numeric($orderList[$i]['id'])) {
			mysql_query("SET NAMES utf8");
//mysql_query("SET CHARACTER SET utf8");
//mysql_query("SET CHARSET utf8");
				//Элемент не новый, просто меняем ему rang
				mysql_query("
					UPDATE
						`menuleft`
					SET
						`rang` = '{$i}'
					WHERE
						`id` = '{$orderList[$i]['id']}'
					LIMIT 1
				");
			} elseif ($orderList[$i]['id'] == 'new' && is_numeric($orderList[$i]['cat_id'])) {
				//Если добавляется разделитель
				if ($orderList[$i]['cat_id'] == '0') {
				mysql_query("SET NAMES utf8");
//mysql_query("SET CHARACTER SET utf8");
//mysql_query("SET CHARSET utf8");
					mysql_query("
						INSERT INTO
							`menuleft`
						SET
							`cat_id` = '0',
							`title` = '',
							`chpu` = '',
							`rang` = '{$i}',
							`pod` = '{$menuId}'
						
					");
					$list[] = array(
						'cat_id' => 0,
						'id' => mysql_insert_id()
					);
				} else {
					//Элемент новый, добавляем в таблицу с новыми рангом, возвращаем id
					mysql_query("SET NAMES utf8");
mysql_query("SET CHARACTER SET utf8");
mysql_query("SET CHARSET utf8");
					$cat = sqlFetch("SELECT `title`, `chpu` FROM `cat` WHERE `id` = '{$orderList[$i]['cat_id']}'");
					if ($cat) {
						$cat['title'] = mysql_real_escape_string($cat['title']);
						$cat['chpu'] = mysql_real_escape_string($cat['chpu']);
						mysql_query("SET NAMES utf8");
mysql_query("SET CHARACTER SET utf8");
mysql_query("SET CHARSET utf8");
						mysql_query("
							INSERT INTO
								`menuleft`
							SET
								`cat_id` = '{$orderList[$i]['cat_id']}',
								`title` = '{$cat['title']}',
								`chpu` = '{$cat['chpu']}',
								`rang` = '{$i}',
								`pod` = '{$menuId}'
							
						");
						$list[] = array(
							'cat_id' => $orderList[$i]['cat_id'],
							'id' => mysql_insert_id()
						);
					}
				}
			}
		}
		echo json_encode(array('error' => false, 'update' => $list));
	break;
	
	case 'deletemenuitem' :
		$id = $_REQUEST['cat_item_id'];
		if (is_numeric($id) && $id > 0) {
		mysql_query("SET NAMES utf8");
mysql_query("SET CHARACTER SET utf8");
mysql_query("SET CHARSET utf8");
			mysql_query("
				DELETE
				FROM
					`menuleft`
				WHERE
					`id` = '{$id}'
			");
			echo json_encode(array('error' => false));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	
	case 'setmenuorder' :
		$orderList = $_REQUEST['order_list'];
		$count = count($orderList);
		
		for ($i = 0; $i < $count; $i++) {
			if (is_numeric($orderList[$i]) && $orderList[$i] > 0) {
			mysql_query("SET NAMES utf8");
mysql_query("SET CHARACTER SET utf8");
mysql_query("SET CHARSET utf8");
				mysql_query("
					UPDATE
						`menuleft`
					SET
						`rang` = '{$i}'
					WHERE
						`id` = '{$orderList[$i]}'
				");
			}
		}
		
		echo json_encode(array('error' => false));
		
	break;
}