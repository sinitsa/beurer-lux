<?php
include ("../connect.php");
include ("../../func/core.php");

$save = false;

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
	$id = $_GET['id'];
	if (isset($_POST['save'])) {
		$title = mysql_real_escape_string($_POST['title']);
		$chpu = mysql_real_escape_string($_POST['chpu']);
		$seo_title = mysql_real_escape_string($_POST['seo_title']);
		$seo_text = mysql_real_escape_string($_POST['seo_text']);
mysql_query("SET NAMES utf8");

		mysql_query("
			UPDATE
				`menuleft`
			SET
				`title` = '{$title}',
				`chpu` = '{$chpu}',
				`seo_title` = '{$seo_title}',
				`seo_text` = '{$seo_text}'
			WHERE
				`id` = '{$id}'
		");
		if (isset($_FILES['image'])) {
				
			$postFile = $_FILES['image'];
			$imgPath = getImagePath('menu');
			// $sFileFullPath = getImagePath('menu');
				// $uploadfile = $sFileFullPath . '0000.png';
				// if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
					// echo "Файл корректен и был успешно загружен.\n";
				// }
			$sFileDest = $id;
			$size = getConfigImageSize('menu');
			$iWidthDest = $size['width'];
			$iHeightDest = $size['height'];
			$iResizeMode = 1;
			$quality = getConfigImageQuality('menu');
			uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode);
		}
		$save = true;
	}
	
	$data = getMenuItemInfo($_GET['id']);
}
$cssOl = true;
include ("../up.php");
?>
<script type="text/javascript" src="/edit/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/edit/ckfinder/ckfinder.js"></script>
<table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td class="ol">
			<a href="/edit/m_menuleft/">К редактору меню</a>
			<?php if ($save) { ?>
				<div class="ok" style="font-size: 1.2em;">Сохранено</div>
			<?php } ?>
			<form action="" method="post" enctype="multipart/form-data">
			<div><h2>Заголовок</h2></div>
			<div><input type="" name="title" class="p_name" value="<?php echo $data['title']?>" /></div>
			<div><h2>URL</h2></div>
			<div><input type="" name="chpu" class="p_name" value="<?php echo $data['chpu']?>" /></div>
			<div><h2>SEO title</h2></div>
			<div><input type="" name="seo_title" class="p_name" value="<?php echo $data['seo_title']?>" /></div>
			<div><h2>SEO text</h2></div>
			<div>
				<textarea name="seo_text" cols="70" rows="7" style="width: 400px;"><?php echo $data['seo_text']; ?></textarea>
				<script type="text/javascript">
					var editor = CKEDITOR.replace( 'seo_text' );
					CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
				</script>
			</div>
			<div>
				Изображение
			</div>
			<div><img src="<?=getImageWebPath('menu').$data['id'];?>.png<?=(isset($save) ? '?'.rand(0,9999) : '')?>" alt="" /></div>
			<div>
				<input type="file" name="image" />
			</div>
			<div class="info-text">(Авторесайз: вкл; Размер: <?=Config::get('image_size.menu');?>)</div>
			<div>&nbsp;</div>
			<div>
				<input type="submit" name="save" value="Сохранить" class="btn" />
			</div>
			</form>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>