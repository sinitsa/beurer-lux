<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Панель администрирования</title>
<link rel="stylesheet" href="plugins/dropdown/dropdown.css" type="text/css" />
<script type="text/javascript" src="js/dropdown.js"></script>
<script type="text/javascript" src="js/MoreCSS.js"></script>
<script type="text/javascript" src="plugins/jquery/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="plugins/tablesort/jquery.tablednd.js"></script>
<style type="text/css">
<!--
.style1 {color: #9AC1C9}
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.style2 {
	font-size: 36px;
	font-weight: bold;
	color: #84B4BD;
}
.style4 {color: #CC0000}
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
}
a:link {
	color: #63A0AB;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #63A0AB;
}
a:hover {
	text-decoration: underline;
	color: #63A0AB;
}
a:active {
	text-decoration: none;
	color: #63A0AB;
}
.myDragClass {
	background-color: #C7DCE0;
}
-->
</style>
<link href="/edit/css/main.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#84B4BD" bgcolor="#FFFFFF">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
          <td width="30%"><img border="0" align="absbottom" src="/edit/logo.jpg" style = "padding-top:20px;padding-left:30px"></td>
        <td width="70%">
				 <div class="floatleft">
		   <div class="ddheader" id="zero-ddheader" onmouseover="ddMenu('zero',1)" onmouseout="ddMenu('zero',-1)"> 
		   <a href="/edit/">
			На главную
		   </a>
		   </div>
            <div class="ddcontent" id="zero-ddcontent" onmouseover="cancelHide('zero')" onmouseout="ddMenu('zero',-1)">
            </div>
          </div>		
		
		
		<div class="floatleft">
          <div class="ddheader" id="one-ddheader" onmouseover="ddMenu('one',1)" onmouseout="ddMenu('one',-1)"> Страницы</div>
          <div class="ddcontent" id="one-ddcontent" onmouseover="cancelHide('one')" onmouseout="ddMenu('one',-1)">
            <div class="ddinner">
              <ul>
                <li class="underline"><a href="m_pages/edit.php?id=1">Главная</a></li>
                <li class="underline"><a href="m_pages/edit.php?id=2">Услуги и цены</a></li>
                <li class="underline"><a href="m_pages/edit.php?id=3">О компании</a></li>
                <li class="underline"><a href="m_pages/edit.php?id=4">Информация</a></li>
                <li class="underline"><a href="m_pages/edit.php?id=5">Контакты</a></li>
                <li class="underline"><a href="m_pages/edit.php?id=6">Спец предложение</a></li>
              </ul>
            </div>
          </div>
        </div>
          <div class="floatleft">
            <div class="ddcontent" id="four-ddcontent" onmouseover="cancelHide('four')" onmouseout="ddMenu('four',-1)">
              <div class="ddinner">
                <ul>
                  <li class="underline"><a href="m_art/add.php" >Добавить</a></li>
                  <li class="underline"><a href="m_art/index.php" >Изменить</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="floatleft">
            <div class="ddheader" id="six-ddheader" onmouseover="ddMenu('six',1)" onmouseout="ddMenu('six',-1)">Фото</div>
            <div class="ddcontent" id="six-ddcontent" onmouseover="cancelHide('six')" onmouseout="ddMenu('six',-1)">
              <div class="ddinner">
                <ul>
                  <li class="underline"><a href="m_foto/add.php" >Добавить</a></li>
                  <li class="underline"><a href="m_foto/index.php" >Изменить</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="floatleft">
            <div class="ddcontent" id="seven-ddcontent" onmouseover="cancelHide('seven')" onmouseout="ddMenu('seven',-1)">
              <div class="ddinner">
                <ul>
                  <li class="underline"><a href="m_guest/add.php" >Добавить</a></li>
                  <li class="underline"><a href="m_guest/index.php" >Изменить</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="floatleft">
            <div class="ddheader" id="vosem-ddheader" onmouseover="ddMenu('vosem',1)" onmouseout="ddMenu('vosem',-1)">Настройки</div>
            <div class="ddcontent" id="vosem-ddcontent" onmouseover="cancelHide('vosem')" onmouseout="ddMenu('vosem',-1)">
              <div class="ddinner">
                <ul>
                  <li class="underline"><a href="m_config/edit.php" >Изменить</a></li>
                </ul>
              </div>
            </div>
          </div></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>