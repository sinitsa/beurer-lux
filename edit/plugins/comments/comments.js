	$(window).load( function(){
		
		$('.edit-comment-button').click( function(){
			var _bl = $(this).parent().parent().find('div.comment-block');
			$(_bl).find('.old-comment').val($(_bl).find('.comment-field').val()); 
			if($(_bl).css('display')!='none'){
				$(_bl).slideUp('fast');
			}else{
				$(_bl).slideDown();
			}
		});
		
		$('.describe-changes').click( function(){
			var _txt = $(this).parent().find('.old-comment').val();
			$(this).parent().find('.comment-field').val(_txt);
			$(this).parent().slideUp('fast');
		});
		
		$('.commit-changes').click( function(){
			var _txt = $(this).parent().find('.comment-field').val();
			var _id = $(this).parent().find('.comment-id').val();
			var _ob = this;
			$.ajax({
				type: "POST",
				url: "/edit/m_zakaz/sql_comment.php",
				data: "isajax=1&id="+_id+"&comment="+_txt,
				success: function(data) {
					//alert(data);
					var _par1 = $(_ob).parent();
					$(_par1).parent().find(".txt_zakaz").find(".comment-edit-date").html(data);
					$(_par1).parent().find(".txt_zakaz").find(".comment-text").html(_txt);
					$(_par1).find('.old-comment').val($(_par1).find('.comment-field').val());
					$(_par1).find('.describe-changes').attr('disabled','disabled');
					if(_txt!=""){
						$(_par1).parent().find(".txt_zakaz").find('.edit-comment-button').html('Изменить комментарий');
						$(_ob).html('Изменить комментарий');
					}else{
						$(_par1).parent().find(".txt_zakaz").find('.edit-comment-button').html('Добавить комментарий');
						$(_ob).html('Добавить комментарий');  
					}
				},
				error: function(jqXHR, textStatus, errorThrown){
					alert(errorThrown); 
				}
			});
		});
		
		$('.comment-field').keyup( function(){
			var _par1 = $(this).parent();
			if($(_par1).find('.old-comment').val()!=$(_par1).find('.comment-field').val()){
				$(_par1).find('.describe-changes').removeAttr('disabled');
			}else{
				$(_par1).find('.describe-changes').attr('disabled', 'disabled');
			}
		});
	});