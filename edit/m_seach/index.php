<?php
include '../connect.php';
include '../../func/core.php';

// echo __FILE__;

$cssOl = true;
include '../up.php';
?>

<style>
.mul {
	list-style: none; margin:0 0 0 5px; float: left;
}
.mul li label{
	cursor: pointer;
}
.loading, .complete, .error, .alternative, .a-error {
	display: none;
}
.loading img {
	vertical-align: middle;
}
label.page_checkbox {
	display: inline-block;
	width: 305px;
}
</style>

<script src="/js/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="/js/jquery.form.js" type="text/javascript"></script>
<script>
var handled = 0;
var errorCounter = 0;
$(function() {
	$('.sitemap-form').bind('submit', function () {
		var form = this;
		$(this).ajaxSubmit({
			dataType : 'json',
			beforeSend : function () {
				$(form).find('.loading').show();
				$(form).find('input[type="submit"]').attr('disabled', 'disabled');
			},
			success : function ( data, statusText, xhr, element) {
				$(form).find('input[type="submit"]').removeAttr('disabled');
				$(form).find('.loading').hide();
				$(form).find('.complete').html(data);
				$(form).find('.complete').show();
				$(form).find('.error').hide();
				return false;
			},
			error : function (jqXHR, textStatus, errorThrown) {
				$(form).find('input[type="submit"]').removeAttr('disabled');
				$(form).find('.loading').hide();
				$(form).find('.error blockquote').html(jqXHR.responseText);
				$(form).find('.error').show()
				$(form).find('.complete').hide();
				return false;
			}
		});
		return false;
	});

});

function typoajax(e, id){
	$.ajax({
		type: "POST",
		url: "ajax.php?method=write" + e + 	$('#t'+ id).val(),
		cache: false,
		beforeSend : function () {
			$('.loading2').show();
			$('.redwri').attr('disabled', 'disabled');
		},
		success: function(){
			$('.redwri').removeAttr('disabled');
			$('.loading2').hide();
		}
	})
}


$(document).ajaxComplete(function() { 
	$( "#updateTNG" ).bind("blur change", function() {
		var amount = $(this).val();
		var id = $(this).data('id');
		jQuery.ajax({
                        url: "ajax.php?method=updateTNG&amount=" + amount + "&id=" + id,
                    type:     "POST", //Тип запроса
                    success: function(response) { //Если все нормально
                     // alert(response);
                },
                error: function(response) { //Если ошибка
                alert('ошибка');
                }
             });
	});

		$( "#update1C" ).bind("blur change", function() {
		var amount = $(this).val();
		var id = $(this).data('id');
		jQuery.ajax({
                        url: "ajax.php?method=update1C&amount=" + amount + "&id=" + id,
                    type:     "POST", //Тип запроса
                    success: function(response) { //Если все нормально
                    // alert(response);
                },
                error: function(response) { //Если ошибка
                alert('ошибка');
                }
             });
	});
});

</script>

<table width="90%" border="0" align="center" class="txt ol">
	<tr>
		<td width="10">&nbsp;</td>
		<td>
			<div>
				<form class="sitemap-form" action="ajax.php?method=seach" method="post">
					<h4 style="padding-right: 10px;">Поиск: </h4>
					<ul class="mul">
						<li>
							<label for="inp_txt_sitemap_path" style="display: inline;">
								Введите тайтл или артикул&nbsp;&nbsp;&nbsp;
							</label>
							<input type="text" id="inp_txt_sitemap_path" name="q_request" value="<?=$q_request?>" class="span4" placeholder = "Поиск по базе"/>
						</li>
					</ul><br style="clear: both;" />

					<div class="cl"></div>
					<div class="loading">
						<img src="/img/ajax_loading.gif" alt=""/> Идет Поиск...
					</div>
					<div class="loading2" style="display: none;">
						<img src="/img/ajax_loading.gif" alt=""/> Идет Изменение...
					</div>
					<div class="complete"></div>
					<div class="error">
						<br />
						<blockquote></blockquote>
					</div>
					<input type="submit" name="" value="Поиск" />
				</form>
				<div class="error-log"></div>
			</div>
		</td>
	</tr>
</table>
<?
include('../down.php');