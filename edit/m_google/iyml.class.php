<?php
include_once("../../func/core.php");
ini_set("display_errors",1);
error_reporting(E_ALL);
if ($_POST)
{
	require_once('../../connect.php');
	$iy = new iYML;
	$iy->buildYMLFile();
}


class iYMLParam{
}

class iYML{

	var $controlAmount;
	var $controlMargin;

	var $siteName;
	var $siteCompany;
	var $currencyName;
	var $currencyRate;

	var $catalogImageType;
	var $selectedCategories;
	var $selectedCategoriesFakeRests;
	var $onRequest;
	var $includeRests;

	var $ymlFilePath;

	var $lastUpdate;

	var $controlPrice;
	var $bidInStock;
	var $bidNotAvailable;


	//	var $__params;

	function __construct(){

		$this->getYMLConfig();

		return $this;
	}

	function install(){

		$query = "
			CREATE TABLE IF NOT EXISTS `igoogle_config` (
			  `name` varchar(50) NOT NULL,
			  `definition` varchar(50) DEFAULT NULL,
			  `description` varchar(200) DEFAULT NULL,
			  `value` varchar(200) DEFAULT '',
			  `sort` int(4) unsigned NOT NULL default '0',
			  PRIMARY KEY (`name`)
			) ENGINE=MyISAM DEFAULT CHARSET=cp1251
		";
		mysql_query($query);

		return $this;
	}

	function getYMLConfig(){

		$query = "SELECT `name`, COALESCE(`value`, '') AS `value` FROM `igoogle_config` ORDER BY `sort` ASC";

		$result = @mysql_query($query);

		while($line = @mysql_fetch_assoc($result)){

			// $this->{$line['name']} = str_replace(' & ',' and ',$line['value']);
			$this->{$line['name']} = $line['value'];

		}

	}

	function saveParam($paramName, $paramValue){
		if($paramName == 'selectedCategories'){
			mysql_query('truncate table `igoogle_cat_links`');
			$arr = explode(',',$paramValue);
			$inStr = 'insert into `igoogle_cat_links`(`cat_id`,`ya_cat_id`) values';
			$inArr = array();
			foreach($arr as $a){
				$inArr[] = '('.$a.',0)';
			}
			$inStr .= implode(',', $inArr);
			unset($inArr);
			mysql_query($inStr);
		}

		$query = "INSERT INTO `igoogle_config` SET `name` = '{$paramName}', `value`= '{$paramValue}'
			ON DUPLICATE KEY UPDATE `value` = '{$paramValue}' ";

		mysql_query($query);

		return mysql_affected_rows()>0;
	}

	function saveParams(){

		foreach($this as $attrName=>$attrValue){
			if ($paramName != "lastUpdate"){
				$this->saveParam($attrName, $this->{$attrName});
			}
		}

		return $this;
	}

	function buildParamHTML(){
		$query = "SELECT `name`, COALESCE(`description`, '') AS `description`, COALESCE(`value`, '') AS `value` FROM `igoogle_config` ORDER BY `sort` ASC";

		$result = @mysql_query($query) or die(mysql_error());
		$html = "<table id=\"iYMLParamForm\">";
		while($line = @mysql_fetch_assoc($result)){
			if ($line['name'] == "lastUpdate"){
				continue;
			}
			//$this->{$line['name']} = $line['value'];
			$descr = $line['description'];
			$caption_class = "description";
			if(!$descr){
				$caption_class = "internal-name";
				$descr = $line['name'];
			}
			$disabled_attribute = "";			
			$html .= "<tr>";
			$html .= "<td class=\"col1\"><span class=\"{$caption_class}\">{$descr}</span></td>";
			if ($line['name'] == 'catalogImageType') {
				$html .= "<td class=\"col2\"><select name=\"{$line["name"]}\" {$disabled_attribute}>";
				$ar = array();
				$ar[] = array('Превью', 'product_preview');
				$ar[] = array('Средняя', 'product_medium');
				$ar[] = array('Маленькая', 'product_small');
				$ar[] = array('Оригинал', 'product_original');
				foreach ($ar as $option) {
					$html .= "<option ".($option[1] == $line['value']?'selected="selected" ':'')."value=\"{$option[1]}\">{$option[0]}</option>";
				}
				$html .= "</select></td>";
			} elseif (in_array($line['name'], array('selectedCategories', 'selectedCategoriesFakeRests'))) {
				$html .= "<td class=\"col2\"><select name=\"{$line["name"]}\" style=\"width: 380px;\" multiple=\"multiple\" size=\"16\" {$disabled_attribute}>";
				$selected = explode(',', $line['value']);
				$selected = (array) $selected;

				foreach (getCats() as $cat) {
					$html .= "<option value=\"{$cat['id']}\"".(in_array($cat['id'], $selected)?' selected="selected"':'').">{$cat['title']}</option>";
				}
				$html .= "</select></td>";
			} else {
				$html .= "<td class=\"col2\"><input {$disabled_attribute} name=\"{$line["name"]}\" type=\"text\" value=\"{$line["value"]}\" /></td>";
			}
			$html .= "</tr>";

		}
		$html .= "</table>";

		return $html;
	}

	function buildYMLFile(){

		//mysql_query('SET NAMES utf8');





		$site_title=$this->siteName;

		$xml_file= "<?xml version=\"1.0\"?>
<rss version=\"2.0\" xmlns:g=\"http://base.google.com/ns/1.0\">
<channel>
<title>$site_title</title>
<link>".Config::get('site.web_addr')."</link>
<description>$site_title</description>";



	// категории google
	/*$yQ = mysql_query('select * from `igoogle_cats`');
	$yArr = array();
	$catArr = array();
	while($yR = mysql_fetch_array($yQ)){
		$yArr[$yR['id']] = $yR['title'];
		$catArr[] = $yR['id'];
	}
	unset($yR);unset($yQ);
	
	$cats = implode(', ',$catArr);
	unset($catArr);*/
				$query = "SELECT * FROM `igoogle_config` ORDER BY `sort` ASC";
				$cc1=0;
				$result = @mysql_query($query) or die(mysql_error());
				while($line = @mysql_fetch_assoc($result)){
					if($line['name'] == 'selectedCategories' and $line['name'] != "") {$s_cats=$line['value'];$cc1=1;} 
					if($line['name'] == 'selectedCategoriesFakeRests' and $line['name'] != "") {
						if($cc1==1) {$s_cats.=','.$line['value'];}
						else {$s_cats=$line['value'];}
						}
				}

		//$conn=mysql_connect($host,$user,$pass)or die("Невозможно подключиться к базе данных!");
		//mysql_set_charset('utf8',$conn);
		//mysql_select_db($db_name)or die("Ошибка при выборе базы данных!");
		//$query=mysql_query("SELECT *,`igoogle_cat_links`.`ya_cat_id` FROM catalog LEFT JOIN `igoogle_cat_links`on `igoogle_cat_links`.`cat_id` = `catalog`.`cat` where cat in (".$cats.") ORDER BY id ")or die("Ошибка при запросе к базе данных 1!");
		$query=mysql_query("SELECT * FROM catalog WHERE cat in (".$s_cats.") ORDER BY id ")or die("Ошибка при запросе к базе данных 1!");
		while ($row = mysql_fetch_array($query))
		{
		
			//$title = str_replace('&amp;', 'and' ,$title);
			//$title = htmlspecialchars($title,ENT_QUOTES);

			$xml_file.= "<item>";
			$title=$row['title'];
			$title = str_replace(' & ', ' and ' ,$title);
			$title = str_replace(' &amp; ', ' and ' ,$title);
			$title = htmlspecialchars($title,ENT_QUOTES);
			$xml_file.= "<title>$title</title>";

			$xml_file.= "<g:price>{$row['price']} RUB</g:price>";
			$xml_file.= "<link>http://gardena-market.ru/catalog/{$row['chpu']}.html</link>";
			$xml_file.= "<g:id>{$row['id']}</g:id>";

			$descr=strip_tags($row['des']);
			$descr=$descr;
			$descr=html_entity_decode($descr,ENT_QUOTES,"UTF-8");
			$descr=htmlspecialchars($descr,ENT_QUOTES);
			$xml_file.= "<description>".$title.'. '."$descr</description>";
			$q = "SELECT DISTINCT `pav`.value 
						FROM `params_available_values`as `pav` 
						INNER JOIN  `params_catalog_links` as `pcl` 
						ON `pav`.id = `pcl`.param_id 
						INNER JOIN `catalog` as `c`
						ON `pcl`.catalog_id = `c`.id
						WHERE `c`.id  = {$row['id']} 
						AND `pav`.id IN (201, 202, 203, 204) LIMIT 1";
			$value = mysql_fetch_row(mysql_query($q));
			$brand = $value[0];
			if (!empty($brand))
				$xml_file.= "<g:brand>{$brand}</g:brand>";
				
			
			$xml_file.= "<g:condition>new</g:condition>";
			$xml_file.= "<g:availability>in stock</g:availability>";
			$xml_file.= "<g:image_link>http://gardena-market.ru/upload/{$row['id']}.jpg</g:image_link>";
			$xml_file.= "</item>";
			/*if($row['ya_cat_id']>0){
				$xml_file .= '<g:google_product_category>'.$yArr[$row['ya_cat_id']].'</g:google_product_category>';
			}*/
			//$offer->appendChild($xml->createElement('brand', $brand));


		}

		$xml_file.="</channel>
			</rss>";
		$fh=fopen($this->ymlFilePath,"w");
		fwrite($fh,$xml_file);

		// Сохраняем в YML
		//$xml->save($this->ymlFilePath);
		//mysql_query("SET NAMES 'cp1251'");



		$this->lastUpdate = date('d.m.Y H:i:s', time());
		// echo $this->lastUpdate;
		// echo $this->ymlFilePath;
		$this->saveParam("lastUpdate", $this->lastUpdate);

		return $this;
	}

	function findNearestSpace($string, $limit) {
		$substring_limited = mb_substr($string, 0, $limit, 'UTF-8');
		return mb_strrpos($substring_limited, ' ', 'UTF-8');
	}

}