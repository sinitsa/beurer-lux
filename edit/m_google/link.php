<?php
include ("../connect.php");
include ("../../func/core.php");

include ("../up.php");
include ("iyml.class.php");

$iy = new iYML; ?>

<style>
	.holder{
		margin: 0 100px;
	}
	.holder .item{
		margin: 10px 0;
	}
	.holder .item div{
		display: inline-block;
	}
	.holder .item div:nth-child(odd){
		width:400px;
	}
	select{
		width:400px;
	}
</style>

<link rel="stylesheet" href="/edit/css/nprogress/nprogress.css">
<script src='/edit/css/nprogress/nprogress.js'></script>

<script>
	$(document).ready(function(){
		$('.selector').on('change',function(){
			$.ajax({
				url: 'ajax.php?method=saveLink', 
				data: 'id='+$(this).data('id')+'&cat_id='+$(this).val(), 
				type: "POST",
				beforeSend: function(){
					$('.holder select').attr('disabled','disabled');
					NProgress.start();
				},
				success: function(data){
					$('.holder select').removeAttr('disabled');
					NProgress.done();
				}	
			});
		});
	});
</script>

<a href='/edit/m_google/' style='margin-left:100px;margin-bottom:40px;' class="btn"> Назад </a>

<div class='holder'>
<?
	$yQ = mysql_query('select * from `igoogle_cats`');
	$cats = array();
	while($res = mysql_fetch_array($yQ)){
		$cats[] = $res;
	}
	$q = mysql_query('select *,`cat`.`title`,`cat`.`chpu` from `igoogle_cat_links` LEFT JOIN `cat` on `cat`.`id`=`igoogle_cat_links`.`cat_id`');
	while($r = mysql_fetch_array($q)){?>
		<div class="item">
			<div>
				<a href='http://gardena-market.ru/cat/<?=$r['chpu']?>.html'><?=$r['title']?></a>
			</div>
			<div>
				<select data-id='<?=$r['id']?>' class='selector'>
					<option value='0' <?if(0==$r['ya_cat_id']){echo 'selected="selected"';}?>> --- </option>
					<?foreach($cats as $c){?>
						<option value='<?=$c['id']?>' <?if($c['id']==$r['ya_cat_id']){echo 'selected="selected"';}?>> <?=$c['title']?> </option>
					<?}?>
				</select>
			</div>
		</div>
		<hr />
	<?}?>
</div>
<?php include ("../down.php");	?>