<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Панель администрирования</title>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript" src="/edit/bootstrap/normal/bootstrap.min.js"></script>
<script src="/edit/js/common.js" type="text/javascript"></script>
<script src="/edit/js/jquery.highlight.js" type="text/javascript"></script>
<script src="/edit/js/jquery.scrollTo-1.4.3.1.js" type="text/javascript"></script>

<link href="/edit/bootstrap/normal/bootstrap.min.css" rel="stylesheet">
<link href="/edit/bootstrap/normal/bootstrap.responsive.min.css" rel="stylesheet">
<?php if (isset($cssOl) && $cssOl) { ?>
	<link href="/edit/css/css_ol.css" rel="stylesheet" />
	<link href="/css/filters.css" rel="stylesheet" type="text/css">
<?php } ?>


<style type="text/css">
<!--

.txt_zakaz
{
	color: #000;
	font-size: 12px;
	width: 460px;
}
	

.txt_zakaz_grey
{
	color: #c4c0b1;
	font-size: 12px;
}

.txt_zakaz_small
{
	color: #000;
	font-size: 10px;
}

.txt_small
{
	color: #000;
	font-size: 12px;
}

.txt_zag_zakaz
{
	color: #000;
	font-size: 16px;
}
.small_menu
{
	font:14px Arial; 
}
.big_menu
{
	font:24px Arial; 
}
.big_menu_grey
{
	font:24px Arial; 
	color: #c4c0b1;

}
.txt_spisok
{
	color: #000;
	text-decoration:underline;
	font-size: 14px;
	font-style:normal;
}
.myDragClass {
	background-color: #C7DCE0;
}
.partners-block {
	display: inline-block;
	margin: 15px;
	padding: 0px;
	
}
.partners-block #partner-link, .partners-block select {
	margin: 5px 0px;
	
}
.partners-block select {
	width: 225px;
}

.partners-block .alert {
	max-width: 375px;
}
-->
</style>

</head>

<body  >
<table width="100%"  cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td><span class="style2"> </span>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="30%"><a href="<?php echo Config::get('site.web_addr'); ?>"><img border="0" align="absbottom" src="/assets/gfx/logotype.png" style = "padding-top:20px;padding-left:30px"></a></td>
          <td width="70%" valign="top">

<br>

		
      <div class="btn-toolbar" style="margin-top: 18px;">
		<div class="btn-group">      
			<a class="btn btn-info btn-large" href="/edit/"><i class="icon-home"></i> На главную</a>
		</div>
		<div class="btn-group">
          <a class="btn btn-large dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-file"></i> Страницы <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <?
			$pages = getStaticPages();
			foreach ($pages as $page) { ?>
				<li><a href="/edit/m_pages/edit.php?chpu=<?=$page['chpu']?>"><?=$page['title']?></a></li>
			<? } ?>
            <li class="divider"></li>
            <li><a href="/edit/m_pages/add.php"><i class="icon-plus"></i> Добавить страницу</a></li>
          </ul>
        </div>
		<div class="btn-group">
          <a class="btn btn-large dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-inbox"></i> Статьи <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/edit/m_articles/add.php"><i class="icon-plus"></i> Добавить</a></li>
            <li><a href="/edit/m_articles/index.php"><i class="icon-pencil"></i> Редактировать</a></li>
          </ul>
        </div>
		
		
		<div class="btn-group">
          <a class="btn btn-large dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-inbox"></i> Видеоблог <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <?
			$pagess = getVideoblogPages();
			foreach ($pagess as $pagez) { ?>
				<li><a href="/edit/m_videoblog/edit.php?id=<?=$pagez['vi_id']?>"><?=$pagez['vi_title']?></a></li>
			<? } ?>
            <li class="divider"></li>
            <li><a href="/edit/m_videoblog/add.php"><i class="icon-plus"></i> Добавить</a></li>
          </ul>
        </div>
		

		
		
        <div class="btn-group">
          <a class="btn btn-info btn-large" href="/edit/m_mcat/"><i class="icon-list-alt"></i> Каталог</a>
        </div><!-- /btn-group -->
        <div class="btn-group">
          <a class="btn btn-large dropdown-toggle" data-toggle="dropdown" href="#"> Разное <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/edit/m_menuleft/"><i class="icon-list-alt"></i> Редактировать меню</a></li>
            <li><a href="/edit/m_catalog_feedback/"><i class="icon-comment"></i> Отзывы о товарах</a></li>
			<li><a href="/edit/m_feedback/feedback.php"><i class="icon-comment"></i> Отзывы о магазине</a></li>
            <li class="divider"></li>
			<li>
				<a href="/edit/m_market/">
					<i class="icon-user"></i> Генератор YML
				</a>
			</li>
            <li class="divider"></li>
            <li><a href="/edit/m_configuration/"><i class="icon-cog"></i> Настройки</a></li>
			<li class="divider"></li>
          </ul>
        </div><!-- /btn-group -->
      </div>
	  
	

	
	
	</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>