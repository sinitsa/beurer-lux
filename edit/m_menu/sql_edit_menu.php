<?php
// Подключаем библиотеки
include ("../connect.php");
include ('../../func/core.php');
// Определяем id меню
$id = get_id();
// Узнаем путь до картинокна сервере
query_path ();
// Дописываем нужный нам каталог
$dir = $folder."/upload/cat";

// ----------------------- Загружаем большую иконку
if(isset($_FILES["upfile"])) 
{ 
    $upfile      = $_FILES["upfile"]["tmp_name"]; 
    $upfile_name = $_FILES["upfile"]["name"]; 
    $upfile_size = $_FILES["upfile"]["size"]; 
    $upfile_type = $_FILES["upfile"]["type"]; 
    $error_code  = $_FILES["upfile"]["error"]; 

//echo (''.$error_code.'');
// Если ошибок нет
    if($error_code == 0) 
    {
     // дополняем имя файла 
     $upfile_names = $dir."/main/".$id.".jpg";
     copy($upfile , $upfile_names);
     } 
}

// ------------------ Загружаем маленькую иконку
if(isset($_FILES["upfile2"])) 
{ 
    $upfile2      = $_FILES["upfile2"]["tmp_name"]; 
    $upfile_name2 = $_FILES["upfile2"]["name"]; 
    $upfile_size2 = $_FILES["upfile2"]["size"]; 
    $upfile_type2 = $_FILES["upfile2"]["type"]; 
    $error_code2  = $_FILES["upfile2"]["error"]; 

//echo (''.$error_code.'');
// Если ошибок нет
    if($error_code2 == 0) 
    {
     // дополняем имя файла 
     $upfile_names2 = $dir."/main_small/".$id.".jpg";
     copy($upfile2 , $upfile_names2);
     } 
}

// Все ОК
header("Location: /edit/m_menu/index.php");
?>