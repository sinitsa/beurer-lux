<?php 
include ('../connect.php'); 
include ('../../func/core.php');

if (isset($_POST['add'])) {
	if (strlen($_POST['title']) >= 2) {
		$data['title'] = mysql_real_escape_string(slashes($_POST['title']));
		$data['cat_id'] = mysql_real_escape_string($_POST['cat']);
		$data['seo_title'] = mysql_real_escape_string($_POST['seo_title']);
		$data['seo_key'] = mysql_real_escape_string($_POST['seo_key']);
        //$data['seo_des'] = mysql_real_escape_string($_POST['seo_des']);
		$data['seo_text'] = mysql_real_escape_string($_POST['seo_desc']);
		$data['seo_description'] = mysql_real_escape_string($_POST['seo_des']);
		$data['chpu'] = mysql_real_escape_string(slashes($_POST['chpu']));
		
		$catId = is_numeric($data['cat_id']) ? $data['cat_id'] : 0;
		//Массив для подготовки sql запроса
		$paramsSqlIds = array();

		//Готовим sql запрос
		$setString = getSetString($data);
	mysql_query("SET NAMES utf8");

		//Добавляем параметры
		mysql_query("
			INSERT INTO
				`tags` 
			SET
			{$setString}
			") or die(mysql_error());
		
		$id = mysql_insert_id();
		
		$size = getConfigImageSize('tag');
		
		if (getConfigImageAutoresize('tags')) {
			$iWidthDest = $size['width'];
			$iHeightDest = $size['height'];
		} else {
			$iWidthDest = null;
			$iHeightDest = null;
		}
		$iQuality = getConfigImageQuality('tag');
		
		uploadAndResize($_FILES['image'], getImagePath('tags'), $id, $iWidthDest, $iHeightDest, 1, $iQuality);
		
		redirect("/edit/m_catalog/list.php?id=".$catId);
	}
}

$catId = is_numeric($_GET['cat_id']) ? $_GET['cat_id'] : 0;
$cssOl = true;
include ('../up.php'); 
?>
<script type="text/javascript" src="/edit/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/edit/ckfinder/ckfinder.js"></script>
<script>
	$(function(){
		$('#title').bind('change', function() {
			var title = $(this).val();
			
			$('#chpu').val(title.translit().toLowerCase());
			$('#seo_title').val(title);
		});
	});
</script>
 <table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td class="ol">
			<div><a href="/edit/m_catalog/list.php?id=<?=$catId;?>">Назад к категории</a></div>
			<div><h2>Добавление подкатегории:</h2><div>
			<form action="" method="post" enctype="multipart/form-data">
			<div><h3>Название:</h3></div>
			<div><input name="title" id="title" type="text" class="p_name" value="<?php echo schars($_POST['title']); ?>" /></div>
			<div><h3>ЧПУ:</h3></div>
			<div><input name="chpu" id="chpu" type="text" class="p_name" value="<?php echo schars($_POST['chpu']); ?>" /></div>
			<?php if (isset($_GET['cat_id'])) {
				echo '<input type="hidden" name="cat" value="'.$_GET['cat_id'].'" />';
			} else { ?>
			<div><h3>Категория:</h3></div>
			<div>
				<select name="cat" class="p_cat">
				<?php
				foreach (getCats() as $cat) {
					echo "<option value=\"{$cat['id']}\"".($cat['id'] == $_GET['cat'] ? ' selected="selected"':'').">{$cat['title']}</option>";
				}
				?>
				</select>
			</div>
			<?php } ?>
			<div><h3>SEO заголовок:</h3></div>
			<div><input name="seo_title" id="seo_title" type="text" class="p_name span9" value="<?php echo schars($_POST['seo_title']); ?>" /></div>
                <div><h3>SEO описание:</h3></div>
                <div><input name="seo_des" id="seo_des" type="text" class="p_name span9" value="<?php echo schars($_POST['seo_des']); ?>" /></div>
			<div><h3>SEO текст:</h3></div>
			<div>
				<textarea name="seo_desc" id="seo_desc" cols="70" rows="7" style="width: 400px;"><?=slashes($_POST['seo_desc'])?></textarea>
				<script type="text/javascript">
					var editor = CKEDITOR.replace( 'seo_desc' );
					CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
				</script>
			</div>
			<div><h3>SEO ключ:</h3></div>
			<div><input name="seo_key" id="seo_key" type="text" class="p_name span9" value="<?php echo schars($_POST['seo_key']); ?>" /></div>			
			<div>
			<div><h3>Изображение:</h3></div>
			<div>
				<input type="file" name="image" />
			</div>
			<div class="info-text">(Авторесайз: <?=(getConfigImageAutoresize('tags') ? 'Вкл' : 'Выкл')?>. Размер: <?=Config::get('image_size.tag');?>)</div>
			<div>
				<input class="btn" name="add" type="submit" value="Добавить" />
			</div>
			<form>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>