<?php 
include ('../connect.php'); 
include ('../../func/core.php');

$rand = '';
if (isset($_POST['edit']) && is_numeric($_GET['id'])) {
	if (strlen($_POST['title']) >= 2) {
		$data['title'] = mysql_real_escape_string(slashes($_POST['title']));
		$data['cat_id'] = mysql_real_escape_string($_POST['cat']);
		$data['seo_title'] = mysql_real_escape_string($_POST['seo_title']);
        //$data['seo_des'] = mysql_real_escape_string($_POST['seo_des']);
		$data['seo_text'] = mysql_real_escape_string($_POST['seo_desc']);
		$data['seo_description'] = mysql_real_escape_string($_POST['seo_des']);
		$data['seo_key'] = mysql_real_escape_string($_POST['seo_key']);
		$data['chpu'] = mysql_real_escape_string(slashes($_POST['chpu']));
		
		$catId = is_numeric($data['cat_id']) ? $data['cat_id'] : 1;
			mysql_query("SET NAMES utf8");

		//Добавляем параметры
		mysql_query("
			UPDATE
				`tags`
			SET
				".getSetString($data)."
			WHERE `id` = '{$_GET['id']}'
			"
			);
		$id = $_GET['id'];
		if ($_FILES['image']) {
			$size = getConfigImageSize('tag');
			
			if (getConfigImageAutoresize('tags')) {
				$iWidthDest = $size['width'];
				$iHeightDest = $size['height'];
			} else {
				$iWidthDest = null;
				$iHeightDest = null;
			}
			$iQuality = getConfigImageQuality('tag');
			
			uploadAndResize($_FILES['image'], getImagePath('tags'), $id, $iWidthDest, $iHeightDest, 1, $iQuality);
			$rand = '?'.rand(1,9999);
		}
	}
}

//Выбор данных
if (is_numeric($_GET['id'])) {
	mysql_query("SET NAMES utf8");

	$data = sqlFetch("
		SELECT
			*
		FROM
			`tags`
		WHERE
			`id` = '{$_GET['id']}'
	");
	
	$catId = $data['cat_id'];
	//$catInfo = getCatInfo($catId);
}


$cssOl = true;
include ('../up.php'); 
?>
<script type="text/javascript" src="/edit/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/edit/ckfinder/ckfinder.js"></script>

 <table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td class="ol">
			<div><a href="/edit/m_catalog/list.php?id=<?=$catId;?>">Назад к категории</a></div>
			<div><h2>Редактирование подкатегории:</h2><div>
			<form action="" method="post" enctype="multipart/form-data">
			<div><h3>Название:</h3></div>
			<div><input name="title" type="text" class="p_name" value="<?php echo schars($data['title']); ?>" /></div>
			<div><h3>ЧПУ:</h3></div>
			<div><input name="chpu" type="text" class="p_name" value="<?php echo schars($data['chpu']); ?>" /></div>
			<div><h3>Категория:</h3></div>
			<div>
				<select name="cat" class="p_cat">
				<?php
				foreach (getCats() as $cat) {
					echo "<option value=\"{$cat['id']}\"".($cat['id'] == $data['cat_id'] ? ' selected="selected"':'').">{$cat['title']}</option>";
				}
				?> 
				</select>
			</div>
			<div><h3>SEO заголовок:</h3></div>
			<div><input name="seo_title" type="text" class="p_name span9" value="<?php echo schars($data['seo_title']); ?>" /></div>

                <div><h3>SEO описание:</h3></div>
                <div><input name="seo_des" type="text" class="p_name span9" value="<?php echo schars($data['seo_description']); ?>" /></div>
			<div><h3>SEO текст:</h3></div>
			<div>
				<textarea name="seo_desc" cols="70" rows="7" style="width: 400px;"><?=slashes($data['seo_text'])?></textarea>
				<script type="text/javascript">
					var editor = CKEDITOR.replace( 'seo_desc' );
					CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
				</script>
			</div>
			<div><h3>SEO ключ:</h3></div>
			<div><input name="seo_key" type="text" class="p_name span9" value="<?php echo schars($data['seo_key']); ?>" /></div>
			<div><h3>Изображение:</h3></div>
			<div>
				<img src="<?=getImageWebPath('tags').$data['id']?>.jpg<?=$rand?>" alt="" />
			</div>
			<div>
				<input type="file" name="image" />
			</div>
			<div class="info-text">(Авторесайз: <?=(getConfigImageAutoresize('tags') ? 'Вкл' : 'Выкл')?>. Размер: <?=Config::get('image_size.tag');?>)</div>
			<div>
				<input class="btn" name="edit" type="submit" value="Сохранить" />
			</div>
			<form>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>