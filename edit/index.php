  <?php
include('connect.php');
include('../func/core.php');

//Перевод даты на русский
$days['Mon'] = 'Понедельник';
$days['Tue'] = 'Вторник';
$days['Wed'] = 'Среда';
$days['Thu'] = 'Четверг';
$days['Fri'] = 'Пятница';
$days['Sat'] = 'Суббота';
$days['Sun'] = 'Воскресенье';

$months = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
//=========

session_start();

$_SESSION['edit_hash'] = true;
$_SESSION['auth'] = 'edit';

include('up.php');

query_all('pages');

$fd = $global_massiv[0]['title'];
query_email();
?>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
  <td width="33%" valign="top">

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="70" align="left" valign="top"><img src="icons/page.jpg" width="60" height="60" alt=""/></td>
        <td align="left" valign="top">
          <table width="100%" border="0" cellpadding="1" cellspacing="0">
            <tr>
              <td align="left" valign="bottom" class="big_menu">Страницы</td>
            </tr>
            <tr>
              <td align="left"><i class="icon-plus"></i> <a href="m_pages/add.php" class="txt_spisok">Добавить
                  страницу</a></td>
            </tr>
            <tr>
              <td align="left"></td>
            </tr>

          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2" align="left" valign="top">
          <table width="80%" class="table-condensed" border="0" cellpadding="1" cellspacing="0">
            <tr>
              <td align="left"><i class="icon-pencil"></i> <em>Изменить:</em></td>
            </tr>
            <?php
            // Спивок страниц
            $pages = getStaticPages();
            foreach ($pages as $page) {
              echo('
								<tr>
									<td align="left"><a href="/edit/m_pages/edit.php?id=' . $page['id'] . '" class="txt_spisok">' . $page['title'] . '</a></td>
								</tr>
							');
            }
            ?>
          </table>
        </td>
      </tr>
    </table>

  </td>
  
		<link rel="stylesheet" href="m_edit_menu/edit_menu.css">
		<link rel="stylesheet" href="css/nprogress/nprogress.css">
		<script src='css/nprogress/nprogress.js'></script>
		<link rel="stylesheet" href="/css/animate.css">
		<script src='/js/jquery-ui-1.8.13.js'></script>
		<script src='m_edit_menu/edit_menu.js'></script>
  
  <td width="67%" valign="top">
		<div class="menuHolder">
			<?
				$edMenuQ = mysql_query('select * from `edit_menu` order by `rang` asc');
				$menuNum = 0;
				while($edR = mysql_fetch_array($edMenuQ)){
					if($edR['title']=='separator'){
						if($edR['url']==1){
							echo '<div class="allSepar separ1"><img src="/edit/m_edit_menu/menuDelBtn.jpg" data-id="',$edR['id'],'" alt="" class="menuDelBtn"></div>';
						}elseif($edR['url']==2){
							echo '<div class="allSepar separ2"><img src="/edit/m_edit_menu/menuDelBtn.jpg" data-id="',$edR['id'],'" alt="" class="menuDelBtn"></div>';
						}elseif($edR['url']==3){
							echo '<div class="allSepar separ3"><img src="/edit/m_edit_menu/menuDelBtn.jpg" data-id="',$edR['id'],'" alt="" class="menuDelBtn"></div>';
						}
					}else{
						$menuNum++;
			?>
				<div class="menuBtn">
					<img src="/edit/m_edit_menu/menuDelBtn.jpg" alt="" data-id='<?=$edR['id']?>' class='menuDelBtn'>
					<div class='menuBtnContent'>
						<div class='menuBtnImg'>
							<a href='<?=$edR['url']?>' class='menuLink'><img src="/edit/m_edit_menu/img/<?=$edR['id']?>.jpg" alt="" width="60px" height="60px" class='menuImg'></a>
						</div>
						<div class='menuBtnText'>
							<a href='<?=$edR['url']?>' class='menuLink'><?=$edR['title']?></a>
						</div>
					</div>
				</div>
			<?}}?>
				<div class="newMenuBtn" <?if($menuNum==0){echo 'style="display:block;"';}?>>
					<div class='newMenuBtnContent'>
						<div class='newMenuBtnImg'>
							<img src="/edit/m_edit_menu/menuAddBtn.jpg" alt="" class='newMenuImg'>
						</div>
						<div class='newMenuBtnText'>
							Новый элемент
						</div>
					</div>
				</div>
			</div>
  </td>
</tr>
<tr>
  <td valign="top">&nbsp;</td>
  <td valign="top">&nbsp;</td>
  <td valign="top">&nbsp;</td>
</tr>
<tr>
  <td colspan="2" valign="top">

  </td>
  <td valign="top">


  </td>
</tr>
<tr>
<td colspan="3" valign="top">

<table width="100%" border="0" cellpadding="1" cellspacing="0">
  <tr>
    <td align="left" valign="middle">
    </td>
  </tr>
  <tr>
    <td align="left" valign="middle">
      <?php include('../tpl/admin/nav_zakaz.tpl.php'); ?>
    </td>
  </tr>
</table>

<?php
// Листинг
$k = $_GET['k'];
if ($k == '') {
  $k = 0;
}


//Какие заказы выводить
switch ($_GET['status']) {
  case '0' :
  case '1' :
  case '4' :
    $status = $_GET['status'];
    break;
  default:
    $status = 0;
}
// Постраничность
$listing = 40;
$result = mysql_query("SELECT COUNT(*) AS c FROM `orders` WHERE `status` = '{$status}' ");
$all_pay = mysql_fetch_assoc($result);
$all_pay = $all_pay['c'];
$num_page = $all_pay / $listing;
$num_page = round($num_page);

$previusOrderDay = 0;

include('../tpl/admin/list_zakaz.tpl.php'); // Постраничность заказов
//Выбираем не обработанные заказы
$sel = mysql_query("
		SELECT
			`orders`.*,
			`order_delivery_types`.`name` AS `delivery`
		FROM
			`orders`
		LEFT JOIN `order_delivery_types`
			ON `orders`.`delivery_type` = `order_delivery_types`.`id`
		WHERE
			`status` = '{$status}'
		ORDER BY
			`orders`.`id` DESC
		LIMIT {$k},{$listing}");

while ($order = mysql_fetch_assoc($sel)) {
  //Подготавливаем данные для вывода
  $order['products'] = unserialize($order['products']);
  fillProductsWithInfo($order['products']);
  $order['order_price'] = unserialize($order['order_price']);

  //print_r($order);
  $day = date('d', $order['date']);
  if ($previusOrderDay != $day) echo('<div align="center" class="big_menu">' . date('d.m.Y', $order['date']) . '</div><hr> ');
  $previusOrderDay = $day;
  ?>
  <div class="order" data-id="<?= $order['id']; ?>">
  <span class="txt_zag_zakaz"><strong><?php echo getOrderCode($order['id']); ?> </strong></span> <span
    class="txt_zakaz">(<?php echo date('d.m.Y H:i', $order['date']); ?>)</span>
  <table width="900" border="0" cellspacing="0" cellpadding="0">
  <tr>
  <td width="500" align="left" valign="top" class="txt_zakaz">
    <div style="padding-top:10px">
      <?php switch ($status) {
        case '0' :
          echo '<span class="label label-info">новый </span>&nbsp;&nbsp;&nbsp;&nbsp;';
          break;
        case '1' :
          echo '<span class="label label-success">обработан </span>&nbsp;&nbsp;&nbsp;&nbsp;';
          break;
        case '4' :
          echo '<span class="label label-important">отказ </span>&nbsp;&nbsp;&nbsp;&nbsp;';
      }
      ?>
      <? if ($status != '1') { ?>
        <img src="/tpl/icons/ok.gif" align="absbottom">
        <a href="#" class="processed" style="color:#333;">Обработан</a>&nbsp;&nbsp;&nbsp;&nbsp;
      <? } ?>
      <? if ($status != '4') { ?>
        <img src="/tpl/icons/del.gif" align="absbottom">
        <a href="#" class="refusal" style="color:#333;">Отказ</a>&nbsp;&nbsp;&nbsp;&nbsp;
      <? } ?>
    </div>

    <table>
      <tr>
        <td>
          <?php
          foreach ($order['products'] as $p) {
            $info = $p['info'];?>
            <table width="100%" border="0" cellpadding="7" cellspacing="0"
                   style="border-bottom-width: 1px;border-bottom-style: dashed;border-bottom-color: #d8d8d8;">
              <tr>
                <td width="80" align="left" valign="top"><a target="_blank"
                                                            href="<?php echo getTemplateLink($info, 'catalog'); ?>"><img
                      src="<?= getImageWebPath('product_small') . $info['id'] ?>.jpg" border="0"
                      align="left"/></a></td>
                <td align="left" valign="top"><a target="_blank"
                                                 href="<?php echo getTemplateLink($info, 'catalog'); ?>"
                                                 style="color:#333;"><?= $info['title'] ?></a> <span
                    style="font-size: 1.3em;">(<?= $p['amount'] ?> шт.)</span><br/>
                  <br/>
                        <span
                          style="font-size: 17px;color: grey;font-family: Verdana, Geneva, sans-serif;"><?= $p['price_after_discount'] ?></span>
                        <span
                          style="font-size: 12px;color: #A8A8A8;font-weight: bold;font-family: Verdana, Geneva, sans-serif;">руб.</span>
                </td>
              </tr>
            </table>
          <?php } ?>
          <span style="font-size: 1.2em;">Общая сумма: <?= $order['order_price']['price_after_global_discount'] ?>
            (скидка <?= $order['order_price']['discount_value'] ?> <?= getDiscountTypeString($order['order_price']['discount_type']) ?>
            )</span><br/>
          <br/>
          <? if ($order['delivery_type'] == 1) { ?>
            <strong>Стоимость доставки:</strong>
            <? if (!isset($order['order_price']['delivery_price']) || $order['order_price']['delivery_price'] === false) {
              echo 'Рассчитать не удалось';
            } elseif ($order['order_price']['delivery_price'] == 0) {
              echo 'Бесплатно';
            } else {
              echo $order['order_price']['delivery_price'] . ' руб.';
            }
          } elseif ($order['delivery_type'] == 2) {
            echo '<strong>Базовая ставка доставки:</strong> ';
            if (!isset($order['order_price']['delivery_price']) || $order['order_price']['delivery_price'] === false) {
              echo 'Рассчитать не удалось';
            } elseif ($order['order_price']['delivery_price'] == 0) {
              echo 'Бесплатно';
            } else {
              echo $order['order_price']['delivery_price'] . ' руб.';
            }
          } ?>
          <br/>

            <script>
                $(function(){
                    $(".icon-ok.price").on("click", function(){
                        var el = $(this);
                        var newPrice = el.parent().find("input").val();
                        var idOrder = el.data('id');
                        if(newPrice === "" || newPrice === {} || newPrice === NaN || newPrice === undefined){
                            alert('Введите новую цену.');
                            return false;
                        }
                        var $this = this;
                        $.ajax({
                            url: '/edit/editOrder.php',
                            data: {method: "editPrice", newPrice: newPrice, idOrder: idOrder},
                            type: "POST",
                            success: function(data){
                                $($this).parent().parent().find(".all_price_to_pay").text("Общая сумма: " + newPrice);
                                $($this).parent().parent().find(".all_price_to_pay").append('<div class="alert alert-warning fade in" style="font-size: 12px; margin: 0px;"><button type="button" class="mini close" data-dismiss="alert" aria-hidden="true" style="font-size: 11px;">x</button>Общая сумма изменена<div>');
                            }
                        })
                    });
                    $(".icon-ok.delivery").on("click", function(){
                        var el = $(this);
                        var newDel = el.parent().find("input").val();
                        if(newDel === 0){
                            newDel = "0";
                        }
                        var idOrder = el.data('id');
                        if(newDel === "" || newDel === {} || newDel === NaN || newDel === undefined){
                            alert('Введите новую стоимость доставки.');
                            return false;
                        }
                        var $this = this;
                        $.ajax({
                            url: '/edit/editOrder.php',
                            data: {method: "editDel", newDel: newDel, idOrder: idOrder},
                            type: "POST",
                            success: function(data){
                                $($this).parent().parent().find(".dellcPr").text(newDel);
                                $($this).parent().parent().find(".all_price_to_pay").append('<div class="alert alert-warning fade in" style="font-size: 12px; margin: 0px;"><button type="button" class="mini close" data-dismiss="alert" aria-hidden="true" style="font-size: 11px;">x</button>Сумма доставки изменена<div>');
                            }
                        });
                    });
                })
            </script>

            <!-- начало изменение суммы и доставки -->
            <?php if(0 == $order['status'] && $order['extra_information'] !== "Обратный звонок"){?>
                <span>Изменить сумму
							<br/>
								<!-- <span style="color: red;"> [не работает]</span>:  -->
								<input type="text" placeholder="<?=$order['order_price']['price_after_global_discount']?>"/>
								<i class="icon-ok price" data-id= "<?=$order['id']?>" style="cursor: pointer; position: relative; top: -4px;-webkit-touch-callout: none;-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;"></i>
							</span>
                <br/>
                <span>Изменить стоимость доставки
							<br/>
								<!-- <span style="color: red;"> [не работает]</span>:  -->
								<input type="text" placeholder="<?=$order['order_price']['delivery_price']?>"/>
								<i class="icon-ok delivery" data-id="<?=$order['id']?>" style="cursor: pointer; position: relative; top: -4px;-webkit-touch-callout: none;-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;"></i>
							</span>
                <br />
            <?php }?>
            <?php if($order['paymentId'] == 2){ ?>
            <span style="text-decoreation: underline;">Учитывать стоимость доставки(YAD):<span>
								<input type="checkbox" class="i55" value="yes" name="plus_dev" <?php if($order["total_price"] == 1){echo 'checked';}?> >
							<br/>
                    <?php }?>
                    <!-- конец изменение суммы и доставки -->

  
          <strong>Имя:</strong>       <?= $order['name']?> <br/>
          <strong>Тел.:</strong>      <?= $order['phone'] ?><br/>
          <strong>Доставка:</strong>  <?= $order['delivery'] ?><br/>
          <strong>Адрес:</strong>     <?= $order['adress'] ?><br/>
          <?php
          $payment_type_text = "";
          switch ($order['payment_type']) {
            case 0:
              $payment_type_text = "Старый заказ, еще без фун-ла типа оплаты, предположительно - Наличными.";
              break;
            case 1:
              $payment_type_text = "Наличными";
              break;
            case 2:
              $button = "<button type=\"submit\" data-oid=\"{$order['id']}\" data-mail=\"{$order['email']}\" class=\"btn btn-small mail-sender\">Отправить письмо</button>";
              $payment_type_text = "Банковской картой {$button}";
              break;
            case 3:
              $payment_type_text = "Безналичный расчет";
              break;
            default:
              $payment_type_text = "Ошибка, пожалуйста, сообщие тех-специалисту.";
              break;
          }
          ?>
          <strong>Тип оплаты: </strong> <?= $payment_type_text . "<br/>" ?>
          <?php
          if ($order['email'] != '') echo('<i class="icon-envelope"></i> ' . $order['email'] . '<br />');
          ?>
         	<?php 
         	$file_path_we = ROOT_DIR . 'upload/details/' . $order['id'];
					if (file_exists($file_path_we . 'pdf')) {
								$ext = '.pdf';
						}	elseif (file_exists($file_path_we . 'doc')) {
								$ext = '.doc';
						} else {
								$ext = '.docx';
						}
					if (!file_exists($file_path_we . $ext)) {
							$file_not_found = 1; 
					}
					if ($file_not_found != 1) {
         	 ?>
         	<strong>Реквизиты: </strong><a href="<?= '/upload/details/' .  $order['id'] . $ext;?>"><?=$order['id'] . $ext?></a><br/>
         	<?php } ?>
			<strong>Дата и время доставки:</strong> <?= str_replace("T"," ",$order['time_date']) ?><br/>
          <strong>Дополнительно:</strong> <?= $order['extra_information'] ?><br/>
          <br/>
        </td>
      </tr>
    </table>

  </td>
  <td align="left" valign="top" width="30">&nbsp;</td>
  <td align="left" valign="top" width="375">


    <div class="txt_zakaz"
         style="padding-left:20px; padding-right:10px; padding-bottom:10px;  padding-top:10px;  background-color:#fdf6f0">
      <table class="table table-condensed order-log">
        <?php
        $s = mysql_query("SELECT * FROM `orders_log` WHERE `order_id`='{$order['id']}' AND `action` <> '0' ORDER BY `date` ASC");
        $lastActionTime = $order['date'];
        $actionsCount = mysql_num_rows($s);

        while ($row = mysql_fetch_assoc($s)) {
          $actionTime = strtotime($row['date']);

          $dateDiff = $actionTime - $lastActionTime;
          $dH = floor($dateDiff / 3600);
          $dM = ($dateDiff - $dH * 3600) / 60;
          $delta = $dH . ' ч. ' . round($dM) . ' м.';

          $lastActionTime = $actionTime;

          $month = $months[date('n', $actionTime) - 1];
          $day_of_week = $days[date('D', $actionTime)];
          $day = date('d', $actionTime);
          $year = date('Y', $actionTime);
          $time = date('H:i', $actionTime);
          ?>
          <tr>
            <td width='100'><b><?= $time ?></b>, <?= $day ?> <?= $month ?><br/><?= $day_of_week ?> </td>
            <td><img src='/tpl/icons/comment.jpg' border='0' align='absbottom'></td>
            <td><?= $row['descr'] ?></td>
            <td width="75px"><?= $delta ?></td>
          </tr>
        <?php
        }
        $keyw = '';
        if ($actionsCount > 1) {
          $dd = $lastActionTime - $order['date'];
          $dh = floor($dd / 3600);
          $dm = ($dd - $dh * 3600) / 60;
          $delta = $dh . ' ч. ' . round($dm) . ' м.';
          echo "<tr><td colspan='3'>&nbsp;</td><td><strong>" . $delta . "</strong></td></tr>";
        }
        //Откуда пришел
        $hrefer = explode("/", $order['h']);
        $site = "<img src=http://favicon.yandex.net/favicon/" . $hrefer[2] . " align=absmiddle  height=15 width=15>";
        if ($hrefer[2] == "yandex.ru" or $hrefer[2] == "www.yandex.ru") {
          $site = "<img src=/tpl/icons/ya_mini.jpg align=absmiddle height=15 width=15 >";
          $hrefer2 = explode("text=", $hrefer[3]);
          $keyw = $hrefer2[1];
          $keyw = urldecode($keyw);
          $keyw = iconv('UTF-8', 'Windows-1251', $keyw);
          $hrefer3 = explode("&", $keyw);
          $keyw = $hrefer3[0];

 
        }
        if ($hrefer[2] == "www.google.ru" or $hrefer[2] == "google.ru" or $hrefer[2] == "www.google.by" or $hrefer[2] == "www.google.com") {
          $site = "<img src=/tpl/icons/goog_mini.jpg align=absmiddle  height=15 width=15 >";
          $hrefer2 = explode("q=", $hrefer[3]);

          $keyw = $hrefer2[1];
          $keyw = urldecode($keyw);
          $keyw = iconv('UTF-8', 'Windows-1251', $keyw);
          $hrefer3 = explode("&", $keyw);
          $keyw = $hrefer3[0];

          //$keyw = str_replace("&hl", "", $keyw );
        }
        //echo "<pre>"; print_r($hrefer); echo "</pre>";
        ?>
      </table>
      <img src="/tpl/icons/comment.jpg" border="0" align="absbottom">
      <a href="javascript:void(0)" class="comment-button" style="color:#333;">Комментировать</a>

      <div style="display: none;" class="comment-form">
        <textarea rows="3" cols="20" class="comment-field" style="width: 300px;"></textarea><br/>
        <input type="button" name="s" class="send-button btn" value="Написать"/>
      </div>
      <br/><br/>
    </div>

    <br>
    <table width="270" border="0" cellspacing="10" cellpadding="0">
      <?php
      //Пока не работает
      if ($order['status'] == 1 AND false) {
        if (($order['remind'] != 1) && ($order['email'] != '')) {
          echo('<tr><td><a class="btn btn-warning reminder-link" pay_id="' . $order['id'] . '"><i class="icon-warning-sign"></i> Запросить отзыв</a></td></tr>');
        } elseif ($order['email'] == '') {
          //	Мыла нет, нет и отзыва
        } else {
          echo('<tr><td><a class="btn btn-success disabled"><i class="icon-ok"></i> Отзыв запрошен</a></td></tr>');
        }
      }
      ?>
      <tr>
        <td class="txt_zakaz_small"><?= $site ?> <?= $keyw ?> <a href="<?= $order['h'] ?>" style="color:#333;"
                                                                 target="_blank">>>></a></td>
      </tr>
      <?php if ($order['partner_id'] != 0) {
        echo('<tr>
						<td class="txt_zakaz_small">Заказ по партнерской ссылке.</td>
					</tr>');
      }
      ?>
    </table>
  </td>
  </tr>
  </table>
  <hr style="margin: 50px 0;"/>
  </div>
<?php
}
?>
</td>
</tr>
<tr>
  <td colspan="3" valign="top">
    <?php include('../tpl/admin/list_zakaz.tpl.php'); // Постраничность заказов   ?>
  </td>
</tr>
<tr>
  <td colspan="3" valign="top" class="big_menu">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<tr>
  <td><?php
    include('down.php');
    ?></td>
</tr>
</table>

<div class="menuCreateBack"></div>
<div class="menuCreateHolder">
	<div class="selectBtn" data-type='1'>
		Разделитель
	</div>
	<div class="selectBtn" data-type='2'>
		Новая кнопка
	</div>
	<div class='createArea'>
		<div class='createSeparator'>
			<div class='separator' data-type='1'>
				<div class='separType1'></div>
			</div>
			<div class='separator' data-type='2'>
				<div class='separType2'></div>
			</div>
			<div class='separator' data-type='3'>
				<div class='separType3'></div>
			</div>
		</div>
		<div class='createBtn'>
			<div><input type='text' name='btnTitle' class='skip' placeholder='Название' /></div>
			<div><input type='text' name='btnUrl' class='skip' placeholder='url' /></div>
			<div><input type='file' name='btnFile' class='skip' id='newFile' /></div>
			<div> <input type='button' id='saveNewBtn' class='skip' value='Добавить' /> </div>
		</div>
	</div>
</div>

<script>
  $(document).ready(function () {
    //Аякс на выставление статуса обработан
    $('.processed').bind('click', function () {
      var order = $(this).closest('.order');

      $.ajax({
        url: '/edit/m_zakaz/ajax.php',
        data: {"method": "setstatus", "order_id": order.data('id'), status: "<?=STATUS_PROCESSED;?>"},
        type: "POST",
        //dataType :
        beforeSend: function () {
          order.css('opacity', .5);
        },
        success: function (data, textStatus, jqXHR) {
          order.fadeOut('slow', function () {
            $(this).remove();
          });
        }
      });
      return false;
    });
    //Аякс на выставление статуса отказ
    $('.refusal').bind('click', function () {
      var order = $(this).closest('.order');

      $.ajax({
        url: '/edit/m_zakaz/ajax.php',
        data: {"method": "setstatus", "order_id": order.data('id'), status: "<?=STATUS_REFUSAL;?>"},
        type: "POST",
        //dataType :
        beforeSend: function () {
          order.css('opacity', .5);
        },
        success: function (data, textStatus, jqXHR) {
          order.fadeOut('slow', function () {
            $(this).remove();
          });
        }
      });
      return false;
    });

    //Отправка сообщений
    $('.comment-button').bind('click', function () {
      $(this).next('.comment-form').slideToggle();
    });

    $('.send-button').bind('click', function () {
      var _this = this;
      var order = $(this).closest('.order');
      var orderId = order.data('id');

      var field = $(this).closest('.comment-form').find('.comment-field');
      var button = $(this);

      var comment = field.val();
      $.ajax({
        url: '/edit/m_zakaz/ajax.php',
        type: "POST",
        data: {"method": "add_comment", "order_id": orderId, "comment": comment},
        dataType: 'json',
        beforeSend: function () {
          field.attr('disabled', 'disabled');
          button.attr('disabled', 'disabled');
        },
        success: function (data, textStatus, jqXHR) {
          field.removeAttr('disabled');
          button.removeAttr('disabled');

          $(_this).closest('.order').find('.order-log').append($('<tr></tr>').html(
            "<td width='100'>" + data.date + " </td>" +
            "<td><img src='/tpl/icons/comment.jpg' border='0' align='absbottom' ></td>" +
            "<td>" + comment + "</td>" +
            "<td>" + data.delta + "</td>"
          ));
          field.val('');
          $(_this).closest('.comment-form').slideToggle();
        }
      });
    });
  });
</script>
<script>
  $(function () {
    $(".mail-sender").on("click", function () {

        var delivery = $(this).parent().find("input[type=checkbox]").prop("checked");

      var mail = $(this).data('mail');
      var oid = $(this).data('oid');

      if(!mail || '' === mail){
        alert('У заказа нет почтового ящика, обратитесь в тех-подержку.');
        return false;
      }else if(!oid || '' === oid){
        alert('У заказа нет индефикатора, обратитесь в тех-подержку.');
        return false;
      }

      $.ajax({
        url : '/edit/sendMail.php',
        type : 'POST',
        dataType : 'html',
        data : {
          oid: oid, mail: mail, delivery: delivery
        },
        success: function(data){
          alert(data);
        }
      })
    });
  })
</script>
</body>
</html>
