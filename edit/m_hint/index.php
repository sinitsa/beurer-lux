<?php
class model_hint
{
    protected $default_action = 'main';
    protected $header = '../up2.php';
    protected $footer = '../down.php';
    protected $connect = '../connect.php';
    protected $data = array();
    
    // отрисовка страницы
    protected function render($view){
    	
    	function get_row($rq){return @mysql_fetch_assoc($rq);}
    	
    	extract((array)$this->data);
    	include($this->header);
        include($view);
        include($this->footer);
    }
	
    // конструктор
	public function __construct() {
		$this->data = (object)array();
	}


    //запуск нужного контролерра
	function run_action($action){
        
        $action = $action == NULL ? $this->default_action : $action;
        if(!method_exists($this, $action)){
        	$action = $this->default_action;
        }
        include($this->connect);
        $this->$action();
        
    }

    // выводим список хитовых товаров
    function main(){
	    //$this->data->hits = 'HOHOHOH';
	    $valQ = @mysql_query("SELECT * FROM  `site_variable` WHERE name LIKE 'hits_seq' LIMIT 1;");
        $val = @mysql_fetch_assoc($valQ);
        $val = $val['val'];

        $hitsQuery = "SELECT * FROM catalog WHERE hit ORDER BY FIELD(id,$val) DESC;";
        $this->data->hits = @mysql_query($hitsQuery);
	    //$this->data->cats = @mysql_query("SELECT * FROM cat limit 10;");
		$this->render('view.php');
    }

    // сохраняем последовательность вывода хитовых товаров
    function set_sequence(){
        $sequence = trim($_POST['sequence'], ',');
        $r=@mysql_query("UPDATE site_variable SET val='$sequence' WHERE `name` = 'hits_seq';");
        echo $r ?  1 : 0;
    }
    
}

$model = new model_hint();
$model->run_action($_GET['action']);

