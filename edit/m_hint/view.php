<style type="text/css">
  .hit-list{
    width: 800px;
    font-size: 12px;
    /*float: left;*/
    padding: 5px;
    margin: 0 auto;
  }

  .top-list{
    font-size: 12px;
    border: 1px solid;
    height: 500px;
  }

  .hit-elem{
    border: 1px solid #999;
    border-radius: 5px;
    margin: 5px 0px;
    background: #EEE;
    padding: 5px;
    height: 52px;
    position: relative;
  }
  .hit-elem:nth-child(odd){
    background: #FFF;
  }

  .image-blc{
    float: left;
    border: 1px solid #C7C7C7;
    width: 50px;
    overflow: hidden;
  }

  .des-conteiner{
    height: 50px;
    padding-left: 55px;
  }

  .des-conteiner h4{
    margin: 0 7px;
  }

  .ctrl-block{
    position: absolute;
    right: 5px;
    bottom: 5px;
  }

  .ctrl-block a{
    text-decoration: none;
    opacity: 0.6;
  }
  .ctrl-block a:hover{
    opacity: 1;  
  }

  .filter-line{
    width: 800px;
    margin: 0 auto;
    font-size: 13px;
    font-weight: 900;
  }

  .filter-line input[type=text]{
    width: 550px;
    font-size: 16px;
    padding: 5px;
    font-weight: 900;
    margin-left: 10px;
  }

</style>
<div class="filter-line">
  Отфильтровать по названию: <input id="title_filter_input_id" type="text">
</div>
<div class="hit-list">
  <?while($row=get_row($hits)):?> 
    <div class="hit-elem" data-id="<?=$row['id']?>">
      <div class="image-blc">
        <img height="50" src="/upload/hit/<?=$row['id']?>.jpg">
      </div>
      
      <div class="des-conteiner">
        
        <h4><?=$row['title']?></h4>
      </div>
      <div class="ctrl-block">
        
        <!--<a href="#" title="Редактировать">
          <img data-event="edit" src="/edit/m_hint/src/edit.png">
        </a>
        
        <a href="#" title="Удалить">
          <img data-event="delete" src="/edit/m_hint/src/delete2.png">
        </a>
        -->
        <a href="#" title="В начало">
          <img data-event="in_begin" src="/edit/m_hint/src/navigate_up2.png">
        </a>
        
        <a href="#" title="В конец">
          <img data-event="in_end" src="/edit/m_hint/src/navigate_down2.png">
        </a>
        
        <a href="#" title="Вверх">
          <img data-event="el_up" src="/edit/m_hint/src/arrow_up_blue.png">
        </a>
        
        <a href="#" title="Вниз">
          <img data-event="el_down" src="/edit/m_hint/src/arrow_down_blue.png">
        </a>
        
  

      </div>
    </div>
  <?endwhile;?>
</div>

<script type="text/javascript">
  $(function(){
    // определяем фильтр по названию товара
    $('#title_filter_input_id').keyup(function(){
      var re = new RegExp(this.value, 'ig')
      $('.hit-elem h4').each(function(){
        var test_res = re.test(this.innerHTML)
        this.parentNode.parentNode.style.display = test_res ? 'block' : 'none'
      })
    })
    
    // отключаем стандартное поведение ссылки
    $('.hit-elem a').click(function(e){e.preventDefault()})
    

    // вешаем события на элементы блока товара
    $('.hit-elem').click(function(e){
      var 
        me = $(this),
        target = $(e.target)
      switch(target.attr('data-event')){
        case 'edit'     : hitsApp.edit(me); break;
        case 'delete'   : hitsApp.delete(me); break;
        case 'in_begin' : hitsApp.inBegin(me); break;
        case 'in_end'   : hitsApp.inEnd(me); break;
        case 'el_up'    : hitsApp.elUp(me); break;
        case 'el_down'  : hitsApp.elDown(me); break;
      }

    })
  })

  // объект с функциями блока
  var hitsApp = {
    edit: function(){
      alert('Try to edit me!')
    }, 
    delete: function(){
      alert('Try to delete me!')
    },
    inBegin: function(me){
      me.parent().prepend(me)
      hitsApp.sentNewSeq()
    },
    inEnd: function(me){
      me.parent().append(me)
      hitsApp.sentNewSeq()
    },
    elUp: function(me){
      me.prev().before(me)
      hitsApp.sentNewSeq()
    },
    elDown: function(me){
      me.next().after(me)
      hitsApp.sentNewSeq()
    }, 
    sentNewSeq: function(){
      $('#title_filter_input_id').val('')
      var list = $('.hit-elem'), seq = ''
      for(var i = list.length-1; i>=0; i--){
        attr = list[i].attributes
        list[i].style.display='block'
        for(var a=0; a < attr.length; a++)
          if(attr[a].name === 'data-id') break;
        seq += list[i].attributes[a].value + ','
      }
      $.ajax({
        url: '/edit/m_hint/?action=set_sequence', 
        data:{sequence:seq},
        type: 'POST',
        success: function(data, textStatus, jqXHR){
          data ? 0 : alert('Error on save in SQL.')
        }
      })
      console.log(seq)
    }
  }

</script>






















