<?
include('../connect.php');
include('../../func/core.php');

//Перевод даты на русский
$days['Mon'] = 'Понедельник';
$days['Tue'] = 'Вторник';
$days['Wed'] = 'Среда';
$days['Thu'] = 'Четверг';
$days['Fri'] = 'Пятница';
$days['Sat'] = 'Суббота';
$days['Sun'] = 'Воскресенье';

$months = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
//=========

switch ($_REQUEST['method']) {
	case 'setstatus' : 
		$orderId = $_REQUEST['order_id'];
		$status = $_REQUEST['status'];
		
		if (!is_numeric($orderId)) {
			echo json_encode(array('error' => 'true'));
			break;
		}
		if ($status == STATUS_PROCESSED || $status == STATUS_REFUSAL) {
			//Апдейтим статус заказа
			mysql_query("UPDATE `orders` SET `status` = '{$status}' WHERE `id` = '{$orderId}'");
			
			//Добавляем запись в лог
			if ($status == STATUS_PROCESSED)
				$statusText = 'Обработан';
			if ($status == STATUS_REFUSAL)
				$statusText = 'Отказ';
			
			mysql_query("INSERT INTO `orders_log` (`order_id`, `date`, `action`, `descr`)
                            VALUES ('{$orderId}', NOW(), 2, '{$statusText}'");
							
			echo json_encode(array('error' => 'false'));
		}
	break;
	
	case 'add_comment' :
		$orderId = $_REQUEST['order_id'];
		$comment = mysql_real_escape_string(iconv("utf-8", "windows-1251", $_REQUEST['comment']));
		
		if (!is_numeric($orderId) || strlen($comment) <= 0) {
			echo json_encode(array('error' => 'true'));
			break;
		}
		
		$lastActionTime = strtotime(fetchOne("SELECT `date` FROM `orders_log` WHERE `order_id` = '{$orderId}' ORDER BY `id` DESC LIMIT 1"));
		if (!$lastActionTime) $lastActionTime = time();

		mysql_query("
			INSERT INTO
				`orders_log`
			SET
				`order_id` = '{$orderId}',
				`date` = NOW(),
				`action` = '1',
				`descr` = '{$comment}'
			");

		/* Расчет разницы во времени предыдущего экшена и даты */
		$actionTime = time();
				
		$dateDiff = $actionTime - $lastActionTime;
		$dH = floor($dateDiff / 3600);
		$dM = ($dateDiff - $dH * 3600) / 60;
		$delta = $dH . ' ч. ' . round($dM) . ' м.';
		
		$month = $months[date('n', $actionTime) - 1];
		$day_of_week = $days[date('D', $actionTime)];
		$day = date('d', $actionTime);
		$year = date('Y', $actionTime);
		$time = date('H:i', $actionTime);
				
		echo json_encode(array(
			'error' => 'false',
			'date' => iconv("windows-1251", "utf-8", '<b>'.$time.'</b>, '.$day.' '.$month.'<br />'.$day_of_week),
			'delta' => iconv("windows-1251", "utf-8", $delta)
		));
	break;
}