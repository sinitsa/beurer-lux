<?
	class image {
		// -----------------------------	НАСТРОЙКИ:
		//	пути фоток:
		static $big_dir;
		static $med_dir;
		static $small_dir;
		static $hit_dir;
		static $dop_dir;
		static $dop_small_dir;
		static $cat_dir;
		
		//	размеры ресайзов:
		//	большие
		static $w_big;
		static $h_big;
		//	средние
		static $w_med;
		static $h_med;
		//	маленькие
		static $w_small;
		static $h_small;
		//	Хит
		static $w_hit;
		static $h_hit;
		//	Доп. фотки уменьшенные
		static $w_dop;
		static $h_dop;
		//	Качество
		static $quality;
		
		// -----------------------------	Свойства картинок
		private $type;
		private $id;
		private $goodId;
		private $paths = array();
		private $valid;
		
		function __construct($type, $id) {
			switch($type) {
				//	Главное фото
				case 0:
					$this->id = $id;
					$this->goodId = $this->id;
					//	Пути:
					$this->paths['src'] = App::$upload_dir . $this->id . '.jpg';
					$this->paths['big'] = self::$big_dir . $this->id . '.jpg';
					$this->paths['med'] = self::$med_dir . $this->id . '.jpg';
					$this->paths['small'] = self::$small_dir . $this->id . '.jpg';
					$this->paths['hit'] = self::$hit_dir . $this->id . '.jpg';
					break;
					
				//	Дополнительное фото
				case 1:
					$this->id = $id;
					$r = App::$db->execute('SELECT `catalog_id` FROM `foto` WHERE `id`=' . $id . ' LIMIT 1');
					if(!$r) {
						$this->goodId = -1;
					} else {
						$this->goodId = $r[0]['catalog_id'];
					}
					//	Пути:
					$this->paths['src'] = self::$dop_dir . $this->id . '.jpg';
					$this->paths['small'] = self::$dop_small_dir . $this->id . '.jpg';
					break;
					
				//	Неверный тип
				default:
					break;
			}
		}
		
		function getInfo() {
			echo "<td>" . $this->id . "</td>";
			echo "<td>" . $this->goodId . "</td>";
			echo "<td>";
				foreach($this->paths as $path) {
					echo $path . "<br />";
				}
			echo "</td>";
			
			echo "<td>";
				foreach($this->paths as $path) {
					if(file_exists($path)) {
						echo filesize($path) . " КБ. <br />";
					} else {
						echo "- файла нету - <br />";
					}
				}
			echo "</td>";
			
		}
		
		//	Ресайз фото
		static function resize ($src, $dest, $w_max, $h_max, $quality) { 
			$im=imagecreatefromjpeg($src); 
			
			// Узнаем ширину исходника
			$w_src = imagesx($im); 
			
			// Узнаем высоту исходника
			$h_src = imagesy($im);
			
			if ($h_src > $h_max and $h_max != '0') {
				$ratio = $h_src/$h_max; 
				$width = round($w_src/$ratio); 
				$height = round($h_src/$ratio);
				
				if ($width > $w_max)
				{
						$ratio = $width/$w_max; 
						$width = round($width/$ratio); 
						$height = round($height/$ratio);
				}
			}
			else {
				if ($w_src > $w_max)
				{
						$ratio = $w_src/$w_max; 
						$width = round($w_src/$ratio); 
						$height = round($h_src/$ratio);
			
				}
				else
				{
						$width = $w_src; 
						$height = $h_src;
				}
			}
			// создаёт новое изображение true color.
			$im1=imagecreatetruecolor($width, $height); 
			
			// 
			imagecopyresampled($im1,$im,0,0,0,0,$width,$height,$w_src,$h_src); 
			
			imagejpeg($im1,$dest,$quality); 
			
			imagedestroy($im); 
			imagedestroy($im1); 	
		}
		
		//	Проверка мертвого груза:
		function isValid() {
			if($this->goodId == -1) {
				return false;
			}
			$r = App::$db->execute('SELECT `id`, `title`, `chpu` FROM `catalog` WHERE `id`=' . $this->goodId . ' LIMIT 1');
			if(!$r) {
				$this->valid = false;
			} else {
				$this->valid = true;
			}
			return $this->valid;
		}
		
		static function checkSite() {
			$images = array();
			$dop_images = array();
			
			//	Главные фотки:
			if(is_dir(App::$upload_dir)) {
				if($dh = opendir(App::$upload_dir)) {
					while( ($file = readdir($dh)) !== false ) {
						if( $file != '.' && $file != '..' && (substr_count($file, 'jpg') > 0) ) {
							$t = explode('.', $file);
							$id = $t[0];
							$tmp_img = new image(0, $id);
							if( !$tmp_img->isValid() ) {
								$images[] = $tmp_img;
							}
						}
					}
				}
			}
			
			//	Дополнительные фотки:
			if(is_dir(self::$dop_dir)) {
				if($dh = opendir(self::$dop_dir)) {
					while( ($file = readdir($dh)) !== false ) {
						if( $file != '.' && $file != '..' && (substr_count($file, 'jpg') > 0) ) {
							$t = explode('.', $file);
							$id = $t[0];
							//echo $id . "<br />";
							$tmp_img = new image(1, $id);
							if( !$tmp_img->isValid() ) {
								$dop_images[] = $tmp_img;
							}
						}
					}
				}
			}
			
			return array_merge($images, $dop_images);
		}
	}
?>