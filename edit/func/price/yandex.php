<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

include ("../../connect.php");
mysql_query('SET NAMES utf8');

$imp = new DOMImplementation();
$dtd = $imp->createDocumentType('yml_catalog', '', 'shops.dtd');

$xml = $imp->createDocument(null, null, $dtd);
$xml->encoding = 'utf-8';

$shop = $xml->createElement('shop');

$cat_ids = Array();

// Company
$shop->appendChild($xml->createElement('name', 'РњРµРґ-РЎРµСЂРґС†Рµ'));
$shop->appendChild($xml->createElement('company', 'РРЅС‚РµСЂРЅРµС‚ РјР°РіР°Р·РёРЅ РјРµРґС‚РµС…РЅРёРєРё РњРµРґ-СЃРµСЂРґС†Рµ'));
$shop->appendChild($xml->createElement('url', 'http://med-serdce.ru/'));


// Currencies
$currency = $xml->createElement('currency');
$currency->setAttribute('id', 'RUR');

$currencies = $xml->createElement('currencies');
$currencies->appendChild($currency);
$shop->appendChild($currencies);


// Categories
$categories = $xml->createElement('categories');

$query = ("SELECT `id`, `pod`, `title` FROM `cat` ORDER BY `id` ASC");
$result = mysql_query($query);
if ($result && mysql_num_rows($result) != 0)
{
  while ($row = mysql_fetch_assoc($result))
  {
    $category = $xml->createElement('category', $row['title']);
    $category->setAttribute('id', $row['id']);
    $cat_ids[count($cat_ids)+1] = $row['id']; 
    
    if ($row['pod'] != 0)
    {
      $category->setAttribute('parentId', $row['pod']);
    }
    
    $categories->appendChild($category);
  }
}

$shop->appendChild($categories);



// Offers
$offers = $xml->createElement('offers');

$query = ("SELECT `id`, `chpu`, `price`, `cat`, `title` FROM `catalog` ORDER BY `id` ASC");
$result = mysql_query($query);
if ($result && mysql_num_rows($result) != 0)
{
  while ($row = mysql_fetch_assoc($result))
  {
  	if(($row['price']>0)&&(array_search($row['cat'], $cat_ids))){
		$new_title = str_replace('&', '&amp;', $row['title']); 
		
		$offer = $xml->createElement('offer');
		$offer->setAttribute('id', $row['id']);
		$offer->setAttribute('available', 'true');
		
		$offer->appendChild($xml->createElement('url', 'http://med-serdce.ru/catalog/'.$row['chpu'].'.html'));
		$offer->appendChild($xml->createElement('price', $row['price']));
		$offer->appendChild($xml->createElement('currencyId', 'RUR'));
		$offer->appendChild($xml->createElement('categoryId', $row['cat']));
		$offer->appendChild($xml->createElement('picture', 'http://med-serdce.ru/upload/medium/'.$row['id'].'.jpg'));
		$offer->appendChild($xml->createElement('name', $new_title));

		$offers->appendChild($offer);
	}
  }
}

$shop->appendChild($offers);


$yml_catalog = $xml->createElement('yml_catalog');
$yml_catalog->setAttribute('date', date('Y-m-d H:i'));
$yml_catalog->appendChild($shop);

$xml->appendChild($yml_catalog);

$xml->formatOutput = true;
$xml->save('../../yandex.xml');
 
//echo $xml->saveXML();
