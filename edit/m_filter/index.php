<?php
include ("../connect.php");
include ('../../func/core.php');

include ("../up.php"); 

$params = getAvailableParams();

?>
 <table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td>
			<div><h4>Доступные параметры:</h4><div>
			<div>
				<table class="table">
					<thead>
						<tr>
							<td><strong>Название</strong></td>
							<td><strong>Тип</strong></td>
							<td><strong>Действие</strong></td>
						</tr>
					</thead>
					<?php foreach ($params as $param) { ?>
					<tr>
						<td><?php echo $param['title']; ?></td>
						<td>
							<?php
								switch ($param['type']) {
									case 'value' : echo 'Одно значение'; break;
									case 'set' : echo 'Набор значений'; break;
									default: echo 'Неизвестный тип';
								}
							?>
						</td>
						<td>
							<a href="edit.php?id=<?php echo $param['id']; ?>">Редактировать</a>&nbsp;
							<a href="delete.php?id=<?php echo $param['id']; ?>">Удалить</a>
						</td>
					</tr>
					<?php } ?>
				</table>
			</div>
			<div><a href="add.php" class="btn btn-small"><i class="icon-plus"></i> Добавить параметр</a><div>
			<div>&nbsp;</div>
			<div><h4>Редкатирование доступных значений параметров:</h4><div>
			<br />
			<?php foreach (getCats() as $cat) {
				echo "<div><a href=\"edit_values.php?id={$cat['id']}\">{$cat['title']}</a></div>";
			}
			?>
			<div>&nbsp;</div>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>