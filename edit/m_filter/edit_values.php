<?php
include ("../connect.php");
include ('../../func/core.php');

include ("../up.php"); 

$catId = $_GET['id'];
$params = getCatParams($_GET['id']);

?>
<script type="text/javascript">
$(function() {
	//Add params buttons
	$('.add-param-button').bind('click', function() {
		var addParamField = $(this).closest('.add-param-field');
		var loading = addParamField.find('.loading');
		
		var catId = addParamField.find('input[name="cat_id"]').val();
		var paramId = addParamField.find('input[name="param_id"]').val();
		var paramValue = addParamField.find('input[name="param_value"]').val();
		
		if (paramValue == undefined || paramValue == '') return;
		loading.show();
		//Аяксом добавляем параметры в базу
		addParamValue(paramId, catId, paramValue, function (id) {
			addParamField.closest('.params-list-field').find('.params-list').append(
			$('<li>' + paramValue + '&nbsp;<img class="delete-button" data-id="' + id + '" style="width:12px; vertical-align: middle;" src="/img/dopfotodel.png" alt=""/></li>')
			);
			//очистить поле ввода
			addParamField.find('input[name="param_value"]').val('');
			loading.hide();
		}, function () {
			alert('some error here');
			loading.hide();
		});
	});
	$('.delete-button').live('click', function() {
		//alert('sss' + $(this).data('id'));
		var paramId = $(this).data('id');
		var li = $(this).parent();
		if (confirm('Действительно хотите удалть параметр?')) {
			deleteParamValue(paramId, function() {
				li.remove();
			});
		}
	});
});
</script>
<style>
	.delete-button {cursor: pointer;}
</style>	
 <table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td>
			<div><a href="/edit/m_filter/">К фильтрам</a></div>
			<div><h3>Редактирование параметров:</h3></div>
			<?php foreach($params as $param) { ?>
				<div class="params-list-field">
					<div><h4><?=$param['title']?>:</h4></div>
					<ul class="params-list" style="list-style: none;">
					<?
					$values = getCatAvailableValues($param['param_id'], $catId);
					foreach ($values as $value) { ?>
						<li><?=$value['value']?>&nbsp;<img class="delete-button" data-id="<?=$value['id']?>" style="width:12px; vertical-align: middle;" src="/img/dopfotodel.png" alt=""/></li>
					<? } ?>
					</ul>
					<div class="add-param-field">
						Добавить значение:<br />
						<input type="hidden" name="cat_id" value="<?php echo $catId; ?>" />
						<input type="hidden" name="param_id" value="<?php echo $param['param_id']; ?>" />
						<input type="text" name="param_value" value="" />
						<input class="add-param-button" type="button" value="Добавить" />
						<img class="loading" style="display: none;" src="/img/ajax_loading.gif" alt="" />
					</div>
				</div>
			<?php } ?>
			<!--<pre>
			<?php
				/*
				foreach($params as $param) {
				print_r($param);
				$values = getCatAvailableValues($param['param_id'], $catId);
				print_r($values);
			}
			*/
			?>
			</pre> -->
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>