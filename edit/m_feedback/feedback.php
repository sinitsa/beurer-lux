<?php
include ('../connect.php'); 
include ('../../func/core.php');

include ('../up.php'); 
?>
	<script type="text/javascript" src="/edit/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/edit/ckfinder/ckfinder.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="http://twitter.github.com/bootstrap/assets/js/bootstrap-modal.js"></script>
	<style>
		.r {
			color: #C33B35;
			display: inline;
		}
		.g {
			color: #55AB55;
			display: inline;
		}
	</style>
	
<div class="modal hide fade" id="myModal">
	<div class="modal-header">

    </div>
    <div class="modal-body">
		<center><textarea id="modal_reply" class='reply' style="width: 95%;" rows='10'></textarea></center>
    </div>
    <div class="modal-footer">
		<a id="modalOk" class="btn btn-primary">Написать</a>
		<a data-dismiss="modal" class="btn">Закрыть</a>
    </div>
</div>

<center>
<div style="width: 90%;">
<table border="0" class="table">
		<tr>
			<th align="center">Отзывы</th>
			<th align="center">Заказы</th>
		</tr>
<?
	$query = "SELECT * FROM `shop_otzyv` ORDER BY `id` DESC";
	$result = @mysql_query($query);
	while($row = @mysql_fetch_assoc($result)) {
		?>
		<tr itemid="<? echo $row['id']; ?>">
			<td width="40%">
				<div style=" ">
					<?
						if($row['confirm'] == 1){
							$style = ('<span class="label label-success" >');
						} else {
							$style = ('<span class="label label-important" >');
						}
						echo $style.$row['name']."</span> <span style=font-size:11px>".$row['date']."</span> ";
					?><br>
					<div class="alert alert-block">
					<div class="otz_txt" itemid="<?=$row['id']?>"><? echo $row['des']; ?></div>
					</div>
					<div class="reply">
						<? if($row['otvet'] != '') { ?>
							<div class="alert alert-success" style="margin-left: 50px;">
								<a class='del_ans btn btn-danger' itemid="<? echo $row['id']; ?>"><i class="icon-trash icon-white"></i></a>&nbsp;
								<span style="margin-left:"><em>Ответ</em>: <? echo $row['otvet']; ?></span>&nbsp;
							</div>
						<? } ?>
					</div>
					<div class="btn-group">
						<a class='conf btn btn-success<?=(($row['confirm'] == 0) ?'':' disabled')?>' itemid="<? echo $row['id']; ?>" value="Одобрить"><i class="icon-ok"></i></a>
						<a class='ans btn<?=($row['otvet'] == '' ? '' :' disabled');?>' itemid="<? echo $row['id']; ?>" value="Ответить"><i class="icon-comment"></i></a>
						<a class='del btn btn-danger' itemid="<? echo $row['id']; ?>"><i class="icon-trash icon-white"></i></a>
					</div>
					<br>
				</div>
			</td>
			<td style="font-size: 14px">
				<?
					$sub_query = "
						SELECT
							`orders`.*,
							`order_delivery_types`.`name` as `delivery`
						FROM `orders`
						LEFT JOIN `order_delivery_types`
						ON `orders`.`delivery_type` = `order_delivery_types`.`id`
						WHERE `orders`.`email`='{$row['mail']}'
					";
					$sub_result = mysql_query($sub_query) or die('Ошбика запроса: '.$sub_query);
					while($order = mysql_fetch_assoc($sub_result)) {
						$order['products'] = unserialize($order['products']);
						fillProductsWithInfo($order['products']);
						$order['order_price'] = unserialize($order['order_price']);
					?>
						<i class ="icon-shopping-cart"></i> <a href="/edit/m_zakaz/zakaz.php?id=<?=$order['id']?>" target="_blank">
							<span class="txt_spisok" style="font-size:14px; font-weight: bold"><?=getOrderCode($order['id'])?></span>
						</a> (<?=date('d.m.Y H:i', $order['date'])?>)<br />
						<span style="font-size:11px; font-line:3px">
							<span style="font-size: 1.2em;">Общая сумма: <?=$order['order_price']['price_after_global_discount']?> (скидка <?=$order['order_price']['discount_value']?> <?=getDiscountTypeString($order['order_price']['discount_type'])?>)</span><br />
							<br />
							<strong>Имя:</strong> <?=$order['name']?><br />
							<strong>Тел.:</strong> <?=$order['phone']?><br />
							<strong>Доставка:</strong> <?=$order['delivery']?><br />
							<strong>Адрес:</strong> <?=$order['adress']?><br />
							<ul class="nav nav-tabs nav-stacked">
								<?php foreach ($order['products'] as $p) { ?>
								<li><a href="<?=getTemplateLink($p['info'], 'catalog')?>" target="_blank"><span class="txt_spisok"><?=$p['info']['title']?></span></a></li>
								<?php } ?>
							</ul>
						</span>
					<?
					}
				?>
			</td>
		</tr>
		<?
	}
?>
</table>
</div>
</center>

<!-------------------------------------------JAVASCRIPT------------------------------------------>
<script type="text/javascript">
function assignActions() {
	//	Подтвердить
	$('a.conf[itemid]').click( function () {
		if ($(this).hasClass('disabled')) return;
		
		otz_id = $(this).attr('itemid');
		$('.modal-header').html('<a class="close" data-dismiss="modal">x</a><h3>Одобрить отзыв</h3>');
		$('.modal-body').html('<p>Подтвердите публикацию отзыва.</p>');
		$('#modalOk').html('Одобрить');
		$('#modalOk').unbind('click');
		$('#modalOk').click( function() {
			$.post('/edit/m_feedback/ajax.php', { action: 'confirm', id: otz_id }, function (data) {
				$('tr[itemid='+otz_id+'] .label').removeClass('label-important').addClass('label-success');
				$('tr[itemid='+otz_id+'] .conf').addClass('disabled');
			}, 'text');
			$('#myModal').modal('hide');
		});
		$('#myModal').modal('show');
	});
	//	Удалить
	$('a.del[itemid]').click( function () {
		if ($(this).hasClass('disabled')) return;
		
		otz_id = $(this).attr('itemid');
		$('.modal-header').html('<a class="close" data-dismiss="modal">x</a><h3>Удалить отзыв</h3>');
		$('.modal-body').html('<p>Подтвердите удаление отзыва.</p>');
		$('#modalOk').html('Удалить');
		$('#modalOk').unbind('click');
		$('#modalOk').click( function() {
			$.post('/edit/m_feedback/ajax.php', { action: 'delete', id: otz_id }, function (data) {
				$('tr[itemid='+otz_id+']').remove();
			}, 'text');
			$('#myModal').modal('hide');
		});
		$('#myModal').modal('show');
	});
	//	Ответить
	$('a.ans[itemid]').click( function () {
		otz_id = $(this).attr('itemid');
		if ($(this).hasClass('disabled')) return;
		
		$('.modal-header').html('<a class="close" data-dismiss="modal">x</a><h3>Ответить на отзыв</h3>');
		$('.modal-body').html( $('div.otz_txt[itemid='+otz_id+']').html() );
		$('&nbsp;<br /><center><textarea id="modal_reply" class="reply" style="width: 95%;" rows="7"></textarea></center>').appendTo('.modal-body');
		//$('.modal-body').append('<center><textarea id="modal_reply" class="reply" style="width: 95%;" rows="10"></textarea></center>');
		$('#modalOk').html('Ответить');
		$('#modalOk').unbind('click');
		$('#modalOk').click( function() {
			rply = $('#modal_reply').val();
			$.post('/edit/m_feedback/ajax.php', { reply: rply, id: otz_id }, function (data) {
				$('tr[itemid='+otz_id+'] .reply').html('<div class="alert alert-success" style="margin-left: 50px;">' +
					'<a class="del_ans btn btn-danger" itemid="'+ otz_id +'"><i class="icon-trash icon-white"></i></a>&nbsp;' +
					'<span style="margin-left:"><em>Ответ</em>: ' + rply + '</span>&nbsp;' +
					'</div>');
				$('tr[itemid='+otz_id+'] .ans').addClass('disabled');
				$(this).unbind('click');
			}, 'text');
			$('#myModal').modal('hide');
		});
		$('#myModal').modal('show');
	});
	// Удалить ответ
	$('a.del_ans[itemid]').live('click',  function () {
		otz_id = $(this).attr('itemid');
		$('.modal-header').html('<a class="close" data-dismiss="modal">x</a><h3>Удалить ответ</h3>');
		$('.modal-body').html('<p>Подтвердите удаление ответа.</p>');
		$('#modalOk').html('Удалить');
		$('#modalOk').unbind('click');
		$('#modalOk').click( function() {
			$.post('/edit/m_feedback/ajax.php', { action: 'delete_answer', id: otz_id }, function (data) {
				$('tr[itemid='+otz_id+'] .reply').empty();
				$('tr[itemid='+otz_id+'] .ans').removeClass('disabled');
			}, 'text');
			$('#myModal').modal('hide');
			$(this).unbind('click');
		});
		$('#myModal').modal('show');
	});
}

$(document).ready( function () {
	assignActions();
});
</script>

<?php include ("../down.php"); ?>