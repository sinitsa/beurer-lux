<?php
include('../../connect.php');
include('../../func/core.php');

switch ($_REQUEST['method']) {
	case 'resizeproducts' :
		$id = is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : 0;
		$oneByOne = isset($_REQUEST['one_by_one']);
		
		//Настроки выполнения скрипта
		set_time_limit(0);
		
		//Настройки ресайза
		$settings = array(
			'product_preview' => isset($_REQUEST['preview']),
			'product_extra_preview' => isset($_REQUEST['preview_extra']),
			'product_small' => isset($_REQUEST['small']),
			'product_medium' => isset($_REQUEST['medium'])
		);
		//Для старта нужен хотя бы один true в настройках
		$start = false;
		foreach ($settings as $key => $value) {
			if ($value) {
				$start = true;
				break;
			}
		}
		if (!$start) {
			echo json_encode(array('resized' => 0));
			break;
		}
		//Ну. Поехали
		$resizedCounter = 0;
		$lastId = 0;
		
		//Если ресайзим для конкретного товара
		if ($id > 0 || $oneByOne) {
			if ($oneByOne) {
				$sel = mysql_query("SELECT `id` FROM `catalog` WHERE `id` > '{$id}' ORDER BY `id` ASC LIMIT 1");
			} else {
				$sel = mysql_query("SELECT ('{$id}') as `id`;");
			}
		} else {
			$sel = mysql_query("SELECT `id` FROM `catalog`");
		}
		while ($row = mysql_fetch_assoc($sel)) {
			$filename = $row['id'];
			$lastId = $row['id'];
			$sourceFile = array(
				'name' => 'unnamed',
				'tmp_name' => getImagePath('product_original').$filename.'.jpg',
				'error' => 0
			);
			if (file_exists($sourceFile['tmp_name'])) {
				//Ресайзим превью товара
				if ($settings['product_preview']) {
					try {
					$imgPath = getImagePath('product_preview');
					$size = getConfigImageSize('product_preview');
					$iWidthDest = $size['width'];
					$iHeightDest = $size['height'];
					$iResizeMode = 1;
					$quality = getConfigImageQuality('product_preview');
					$newName = uploadAndResize($sourceFile, $imgPath, $filename, $iWidthDest, $iHeightDest, $iResizeMode, $quality);
					if ($newName !== false) $resizedCounter++;
					} catch (Exception $e) {}
				}
				//Ресайзим маленькие изображения товара
				if ($settings['product_small']) {
					try {
					$imgPath = getImagePath('product_small');
					$size = getConfigImageSize('product_small');
					$iWidthDest = $size['width'];
					$iHeightDest = $size['height'];
					$iResizeMode = 1;
					$quality = getConfigImageQuality('product_small');
					$newName = uploadAndResize($sourceFile, $imgPath, $filename, $iWidthDest, $iHeightDest, $iResizeMode, $quality);
					if ($newName !== false) $resizedCounter++;
					} catch (Exception $e) {}
				}
				//Ресайзим средние изображения товара
				if ($settings['product_medium']) {
					$imgPath = getImagePath('product_medium');
					$size = getConfigImageSize('product_medium');
					$iWidthDest = $size['width'];
					$iHeightDest = $size['height'];
					$iResizeMode = 1;
					$quality = getConfigImageQuality('product_medium');
					$newName = uploadAndResize($sourceFile, $imgPath, $filename, $iWidthDest, $iHeightDest, $iResizeMode, $quality);
					if ($newName !== false) $resizedCounter++;
				}
			}
			//Ресайзим превью доп. фото
			if ($settings['product_extra_preview']) {
				$sel2 = mysql_query("SELECT `id` FROM `foto` WHERE `catalog_id` = '{$filename}'");
				while ($row2 = mysql_fetch_assoc($sel2)) {
					$filename = $row2['id'];
					$sourceFile = array(
						'name' => 'unnamed',
						'tmp_name' => getImagePath('product_extra_original').$filename.'.jpg',
						'error' => 0
					);
					if (file_exists($sourceFile['tmp_name']) && is_file($sourceFile['tmp_name'])) {
						$imgPath = getImagePath('product_extra_preview');
						$size = getConfigImageSize('product_extra_preview');
						$iWidthDest = $size['width'];
						$iHeightDest = $size['height'];
						$iResizeMode = 1;
						$quality = getConfigImageQuality('product_extra_preview');
						$newName = uploadAndResize($sourceFile, $imgPath, $filename, $iWidthDest, $iHeightDest, $iResizeMode, $quality);
						if ($newName !== false) $resizedCounter++;
					}
				}
			}
		}
		echo json_encode(array('resized' => $resizedCounter, 'last_id' => $lastId));
	break;
}