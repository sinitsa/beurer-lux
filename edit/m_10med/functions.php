<?php 	
// ini_set('error_reporting', E_ALL);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);

/** ----------------------------------------------------------------------------------
* 	Класс для работы а данными() товаров сайта
-------------------------------------------------------------------------------------*/
class site10medData {
	
	public static function getProducts($limit = NULL, $offset = 0, $filter) {

		if ($filter == 'search') {
			if ($_REQUEST['type'] == 'title') {
				$where = "WHERE `{$_REQUEST['type']}` LIKE '%{$_REQUEST['search_query']}%'";
			} else {
				$where = "WHERE `{$_REQUEST['type']}` = '{$_REQUEST['search_query']}'";
			}
		
		} else if ($filter == 'linked') {
			$where = "WHERE linked_with_10med > 0";
		} else if ($filter == 'nolinked') {
			$where = "WHERE linked_with_10med = 0";
		} else {
			$where = "";
		}

		$output = array();

		if ($limit) {
			$query = "SELECT id, title, art, price, linked_with_10med, nomencl_10med FROM  catalog $where LIMIT {$offset},{$limit}";
		} else {
			$query = "SELECT id, title, art, price, linked_with_10med, nomencl_10med FROM  catalog $where";
		}

		$result = mysql_query($query);
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			$output[0][] = $row;
		}

		$output[1][] = count($output[0]);

		return $output;
	}

	public static function  getLinkedCount() {

		$query = "SELECT COUNT(id) FROM catalog WHERE linked_with_10med > 0";
		$result = mysql_query($query);
		$output = mysql_fetch_row($result);
		return $output[0];
	}

	public static function getCountProducts($filter='') {

		if ($filter == 'search') {
			if ($_REQUEST['type'] == 'title') {
				$where = "WHERE `{$_REQUEST['type']}` LIKE '%{$_REQUEST['search_query']}%'";
			} else {
				$where = "WHERE `{$_REQUEST['type']}` = '{$_REQUEST['search_query']}'";
			}
		} else if ($filter == 'linked') {
			$where = "WHERE linked_with_10med > 0";
		} else if ($filter == 'nolinked') {
			$where = "WHERE linked_with_10med = 0";
		} else {
			$where = "";
		}

		$query = "SELECT count(id) FROM catalog {$where}";
		$result = mysql_query($query);
		$output = mysql_fetch_row($result);
		return $output[0];
	}

}
	
/** -----------------------------------------------------------------------------
*  Класс для работы  API 10med
--------------------------------------------------------------------------------*/
class api10med {
	
	// 
	public static $siteId = 21;
	private static $url = 'http://46.101.204.133/api/';
	private static $tokenParam = '?access-token=5RJrtyWrOf7KlNc4Hzzj65NQ7Ud0AOKS';

	/*
	Поиск товара в 10med
	*/
	public static function search($title,$option = true, $type = null) {
		if ($option) {
			$params = array(
				'title' => $title,
				'siteId' => self::$siteId,
			);
		} else {
			$params = array(
				'title' => $title,
				'siteId' => 0,
			);
		}
		$action = 'search';
		

		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
		    CURLOPT_URL => self::$url . $action . self::$tokenParam,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POST => true,
		    CURLOPT_POSTFIELDS => http_build_query($params)
		));
		$res = curl_exec($myCurl);
		curl_close($myCurl);
		return $res;
	}

	// Связывает  id сайта и 10мед
	public static function link($localId, $remoteId) {
		$action = 'link';
		$params = array(
			'siteId' => self::$siteId,
			'id10med' => $remoteId,
			'remoteProductId' => $localId,
		);

		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
		    CURLOPT_URL => self::$url . $action . self::$tokenParam,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POST => true,
		    CURLOPT_POSTFIELDS => http_build_query($params)
		));
		$res = curl_exec($myCurl);
		curl_close($myCurl);
		return $res;
	}

	// 
	public static function product($remoteId) {
		$action = 'product';
		$params = array(
			'siteId' => self::$siteId,
			'id10med' => $remoteId,
		);

		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
		    CURLOPT_URL => self::$url . $action . self::$tokenParam,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POST => true,
		    CURLOPT_POSTFIELDS => http_build_query($params)
		));
		$res = curl_exec($myCurl);
		curl_close($myCurl);
		return $res;
	}


	// удаляет все связи сайта в 10мед
	public static function clearLinks() {
		$action = 'clear';
		$params = array(
			'siteId' => self::$siteId,
		);

		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
		    CURLOPT_URL => self::$url . $action . self::$tokenParam,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POST => true,
		    CURLOPT_POSTFIELDS => http_build_query($params)
		));
		$res = curl_exec($myCurl);
		curl_close($myCurl);
		return $res;
	}

	// Автоматическе формирование связей 10med на основе данных сайта
	public static function createLinks($data) {
		$action = 'autolinking';
		$params = array(
			'siteId' => self::$siteId,
			'links' => $data,
		);

		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
		    CURLOPT_URL => self::$url . $action . self::$tokenParam,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POST => true,
		    CURLOPT_POSTFIELDS => http_build_query($params)
		));
		$res = curl_exec($myCurl);
		curl_close($myCurl);
		return $res;
	}


}


 ?>