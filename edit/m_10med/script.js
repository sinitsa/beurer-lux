$(document).ready(function($) {

	// переменные
	var itemId = 0,
		id10med = 0;

	var	myModal1 = $('.modal.modal-item-edit');
		inputArt = myModal1.find('input[name="art"]'),
		inputNomancl = myModal1.find('input[name="nomencl"]'),
		inputId10med = myModal1.find('input[name="id10med"]');

	var myModal2 = $('.modal.modal-10med-linking'),
		inputSearch = myModal2.find('input[name="search"]'), 
		statusBlock = myModal2.find('.item-status-text > span');
		linkBlock 	= myModal2.find('.item-link-text > a');

	$.ajaxSetup({cache: false});

	// Вызов события: Автоматическая синхронизация 
	$('.autosync').on('click', function() {
		$.ajax({
			url: 'ajax.php?method=autosync',
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				console.log(data);
				$('.alert').hide();
				if (data.code == 200 && data.errors == 0) {
					var alert = $('.alert-success');
					alert.find('span').text('Связи успешно сформированы! Успешно создано: '+data.rows+', Ошибок: '+data.errors);
					alert.fadeIn('slow');
				} else if (data.errors > 0)  {
					var alert = $('.alert-warning');
					alert.find('span').text('Связи частично сформированы! Успешно создано: '+data.rows+', Ошибок: '+data.errors);
					alert.fadeIn('slow');
				} else {
					var alert = $('.alert-danger');
					alert.find('span').text('Ошибка запрса. Код:' + data.status);
					alert.fadeIn('slow');
				}
			},
		})
	});

	// очистка связей 10мед (конкретного сайта)
	$('.autounlink').on('click', function(event) {
		if (confirm('Действие удалит всесвязи сайт на 10мед! Вы хотите продолжить???')) {
			$.ajax({
				url: 'ajax.php?method=autounlink',
				type: 'GET',
				dataType: 'json',
				success: function(data) {
					console.log(data);
					$('.alert').hide();
					if (data.code == 200) {
						var alert = $('.alert-success');
						alert.find('span').text(data.message);
						alert.fadeIn('slow');
					} else {
						var alert = $('.alert-danger');
						alert.find('span').text('Ошибка запрса. Код:' + data.status);
						alert.fadeIn('slow');
					}
				},
			})
		}
	});


	// Закрытие сообщения
	$('.alert-close').on('click', function(event) {
		$('.alert').fadeOut('slow');
	});

	// Ручное редактирование элемента (Модальник 1)
	$('.item_edit').on('click', function(event) {
		
		event.preventDefault();
		itemId = $(this).data('itemid');

		myModal1.modal();
		myModal1.find('.alert').hide();

		$.ajax({
			url: 'ajax.php?method=getItemData',
			type: 'GET',
			data:  ({'item_id' : itemId}),
			dataType: 'json',
			success: function (data) {
				var id = data.id;
				console.log(data);
				myModal1.find('h4').text(data.title);
				myModal1.find('.item-id-text > span').text(data.id);
				myModal1.find('.item-price-text > span').text(data.price);
				inputArt.val(data.art);
				inputNomancl.val(data.key1c);
				inputId10med.val(data.linked_with_10med);
			},	
		});
	});

	// Сохранение данных
	myModal1.on('click', '.save-item-data', function() {
		$.ajax({
			url: 'ajax.php?method=setItemData',
			type: 'GET',
			data:  ({
				'item_id': itemId,
				'art': 	inputArt.val(),	
				'nomencl': inputNomancl.val(),
				'id10med': inputId10med.val(),
			}),
			dataType: 'json',
			success: function(data) {
				myModal1.find('.alert').fadeOut();
				if (data.error) {
					myModal1.find('.alert-danger').fadeIn().text(data.message);
				} else {
					myModal1.find('.alert-success').fadeIn().text(data.message);
				}
					
			},
		})
	});

	// Настройка привязки 10med (Модальник 2)
	$('.linking_10med').on('click', function(event) {
		itemId = $(this).data('itemid');
		
		myModal2.modal();
		myModal2.find('.alert').hide();
		myModal2.find(".search_result_list").empty();

		$.ajax({
			async: false,
			url: 'ajax.php?method=getItemData',
			type: 'GET',
			data:  ({'item_id' : itemId}),
			dataType: 'json',
			success: function (data) {
				console.log(data);
				id10med = data.linked_with_10med
				var id = data.id;
				myModal2.find('.item-title-text > span').text(data.title);
				inputSearch.val(data.title);
				myModal2.find('.item-id-text > span').text(data.id);
				myModal2.find('.item-price-text > span').text(data.price);
				myModal2.find('.item-art-text > span').text(data.art);
				myModal2.find('.item-nomencl-text > span').text(data.key1c);
			},	
		});


		statusBlock.text(id10med);
		if (id10med > 0) {
			linkBlock.attr({
				href: 'http://10med.ru/product/'+id10med,
			});
			$('.relink').removeClass('disabled');
			$('.unlink').removeClass('disabled');
		} else {
			linkBlock.hide();
			$('.relink').addClass('disabled');
			$('.unlink').addClass('disabled');
		}
	});

	// Поиск  товаров на 10med
	myModal2.on('click', '.search10med', function(event) {
		var searhQuery = inputSearch.val();
		$.ajax({
			url: 'ajax.php?method=search',
			type: 'GET',
			data: ({search_text: searhQuery}),
			dataType: 'json',
			success: function(data) {
				console.log(data.suggestion);
				$(".search_result_list").empty();

				var result = data.suggestion;
				// alert(data.suggestion.count());
				for (var i = 0 ; i < result.length; i++) {
				 	var elTmp = '<div class=\"media list-group-item\">'+
				 					'<a class="pull-left" target="_blank" href="http://10med.ru/product/'+result[i].id+'">'+
				 					  	'<img class="media-object" src="'+result[i].image+'" alt="'+result[i].title+'">'+
				 					'</a>'+
		      					  	'<div class="media-body ">'+
		      					  		'<div class="col-md-7">'+
											'<h4 class="media-heading">'+result[i].title+'</h4>'+
											'<button type="button" class="btn btn-sm btn-success linking_submit" data-id10med="'+result[i].id+'">Связать</button>' +
		      					  		'</div>'+	
		      					  		'<div class="col-md-2">'+
		      					  			'<span class="s_result_data">id: <span >'+result[i].id+'</span></span><br/>'+
		      					  			'<span class="s_result_data">цена: <span >'+result[i].price+'</span></span>'+
		      					  		'</div>'+
		      					  		'<div class="col-md-3">'+
		      					  			'<span class="s_result_data">Номенклатура: <br/> <span >'+result[i].key1c+'</span></span><br/>'+
		      					  		'</div>'+     					  
		      					  	'</div>'+
				 				'</div>'	
	      			$(elTmp).appendTo('.search_result_list');
	      			// searchList.appendTo(elTmp);		
				 }; 	
			},
		});
	});

	// Перелинковка
	myModal2.on('click', '.relink', function(event) {
		
		// id10med = $(this).data('id10med');

		$.ajax({
			url: 'ajax.php?method=link',
			type: 'POST',
			dataType: 'json',
			data: {'local_id': itemId, 'remote_id': id10med, 'relink': true},
			success: function(data) {
				myModal2.find('.alert').fadeOut();
				if (data.error) {
					var alert = myModal2.find('.alert-danger').fadeIn();
						alert.find('span').text(data.message);
				} else {
					var alert = myModal2.find('.alert-success').fadeIn();
					alert.find('span').text(data.message);
				}
			},
		});
	});

	// Кнопка слинковки
	myModal2.on('click', '.linking_submit',function(event) {

		id10med = $(this).data('id10med');

		$.ajax({
			url: 'ajax.php?method=link',
			type: 'POST',
			dataType: 'json',
			data: {'local_id': itemId, 'remote_id': id10med},
			success: function(data) {
				myModal2.find('.alert').fadeOut();
				if (data.error) {
					var alert = myModal2.find('.alert-danger').fadeIn();
						alert.find('span').text(data.message);
				} else {
					var alert = myModal2.find('.alert-success').fadeIn();
					alert.find('span').text(data.message);
				}
			},
		});

		linkBlock.show();
		linkBlock.attr({
				href: 'http://10med.ru/product/'+id10med,
			});
		statusBlock.text(id10med);

		$('.relink').removeClass('disabled');
		$('.unlink').removeClass('disabled');
	});

	// Кнопка отвязки
	myModal2.on('click', '.unlink',function(event) {

		$.ajax({
			url: 'ajax.php?method=link',
			type: 'POST',
			dataType: 'json',
			data: {'local_id': 0, 'remote_id': id10med},
			success: function(data) {
				myModal2.find('.alert').fadeOut();
				if (data.error) {
					var alert = myModal2.find('.alert-danger').fadeIn();
						alert.find('span').text(data.message);
				} else {
					var alert = myModal2.find('.alert-success').fadeIn();
					alert.find('span').text(data.message);
				}
			},
		});

		linkBlock.hide();
		statusBlock.text('0');

		$('.relink').addClass('disabled');
		$('.unlink').addClass('disabled');


	});

	// фильтры
	$('.filter_linked').on('click', function(event) {
		document.cookie = "module10med_filter=linked";
	});

	$('.filter_nolinked').on('click', function(event) {
		document.cookie = "module10med_filter=nolinked";
	});

	$('.filter_clear').on('click', function(event) {
		document.cookie = "module10med_filter=";
	});





});