<?php
header('Content-Type: text/html; charset=utf-8');
// ВАЖнЫЕ ПЕРЕМЕННЫЕ;
$mysql_charset = 'utf8';



// ini_set('error_reporting', E_ALL);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);

include ("../connect.php");
include ("header.php");
mysql_query("SET CHARACTER SET {$mysql_charset}");
mysql_query("SET NAMES {$mysql_charset}");
require_once('functions.php');

/**
* 	Контроллер
*/
if (isset($_GET['page'])) {
	$currentPage = $_GET['page'];
} else {
	$currentPage = 1;
}

// Инициализация фильтров,если они существуют
if (isset($_REQUEST['search_query']) and isset($_REQUEST['type'])) {
	$filter = 'search';
} else if (isset($_COOKIE['module10med_filter']) and !empty($_COOKIE['module10med_filter'])) {
	$filter = $_COOKIE['module10med_filter'];
} else {
	$filter = '';
}

$limit = 30;
$data = site10medData::getProducts($limit, ($currentPage-1)*$limit, $filter);
$count = site10medData::getCountProducts();
$linked_count = site10medData::getLinkedCount();
$countpages = (int)(site10medData::getCountProducts($filter) / $limit) + 1; 

?>
<header>
	<div class="container">
		<h3>Модуль работы с 10med</h3>
		<span class="label label-primary version pull-right">Версия: 0.5.1</span>
		<div class="top-panel">
			<!-- Панель управления -->					
			<nav class="navbar navbar-default btn-group-sm">
			  <div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      	<a class="navbar-brand" href="/edit/m_10med/">
			      		<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
			      	</a>
			    </div>

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav">
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Навигация <span class="caret"></span></a>
			          <ul class="dropdown-menu">
			            <li><a href="/edit/">В админку</a></li>
			            <li><a href="/">Сайт</a></li>
			            <li><a href="/edit/m_mcat/">Каталог</a></li>
			            <li role="separator" class="divider"></li>
			            <li><a href="/edit/m_market/">Генератор YML</a></li>
			            <li role="separator" class="divider"></li>
			            <li><a href="/edit/m_configuration/">Конфигуратор сайта</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Фильтры <span class="caret"></span></a>
			          <ul class="dropdown-menu">
			          	<li><a class="filter_clear" href="index.php">Все</a></li>
			            <li><a class="filter_linked" href="index.php">Связанные</a></li>
			            <li><a class="filter_nolinked" href="index.php">Не связанные</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Управление <span class="caret"></span></a>
			          <ul class="dropdown-menu">
			            <li><a class='autosync' href="javascript: void(0);">Синхронизация по id</a></li>
			            <li><a class="autounlink" href="javascript: void(0);">Очистить все связи на 10med</a></li>
			            <li role="separator" class="divider"></li>
			            <li><li><a href="/cron_scripts/rp_update_v2.php" target="_blank">Обновление цены\наличия</a></li></li>
			          </ul>
			        </li>
			        <li><span class="info">Всего товаров: <?=$count ?> [<span class="green"><?=$linked_count ?></span>/<span class="red"><?=$count-$linked_count ?></span>]</span></li>
			      	<li><span class="info">Id сайта: <?=api10med::$siteId ?></span></li>
			      </ul>
			 	
				<ul class="nav navbar-nav navbar-right">
				  <li class="dropdown">
				    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">настройки <span class="caret"></span></a>
				    <ul class="dropdown-menu">
				      <li><a href="storages.php">Настройки складов</a></li>
				      <li><a href="#">[Пусто]</a></li>
				      <li><a href="#">[Пусто]</a></li>
				      <li role="separator" class="divider"></li>
				      <li><a href="#">[Пусто]</a></li>
				    </ul>
				  </li>
				</ul>

				<form class="input-group input-group-sm block-search">
			      	<input type="text" class="form-control" name="search_query" aria-label="..." placeholder="запрос" value="<?=isset($_REQUEST['search_query']) ? $_REQUEST['search_query'] : '' ?>">
			      	<div class="input-group-btn">
			        	<button type="submit" class="btn btn-default">Поиск</button>
				    	            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    	              <span class="caret"></span>
				    	              <span class="sr-only">Toggle Dropdown</span>
				    	            </button>
				    	            <ul class="dropdown-menu dropdown-menu-right">
				    	              	<li>
				    	              		<a class="radio" href="javascript: void(0);">
				    	              			<label><input type="radio" name="type" value="title" checked>Название</label>
											</a>
										</li>
										<li>
				    	              		<a class="radio" href="javascript: void(0);">
				    	              			<label><input type="radio" name="type" value="art">Артикул</label>
											</a>
										</li>
										<li>
				    	              		<a class="radio" href="javascript: void(0);">
				    	              			<label><input type="radio" name="type" value="nomencl_10med">номенклатура</label>
											</a>
										</li>
										<li>
				    	              		<a class="radio" href="javascript: void(0);">
				    	              			<label><input type="radio" name="type" value="linked_with_10med">10med ID</label>
											</a>
										</li>
				    	            </ul>
			      	</div><!-- /btn-group -->
			    </form><!-- /input-group -->




			    </div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>

			<!-- alerts -->
			<div>
				<div style="display: none;" class="alert alert-success" role="alert"><span>sfdgsdgsd</span><a class="alert-close alert-link" href="javascript: void(0);">Закрыть</a></div>
				<div style="display: none;" class="alert alert-info" 	role="alert"><span></span><a class="alert-close alert-link" href="javascript: void(0);">Закрыть</a></div>
				<div style="display: none;" class="alert alert-warning" role="alert"><span></span><a class="alert-close alert-link"  alert-linkref="javascript: void(0);">Закрыть</a></div>
				<div style="display: none;" class="alert alert-danger" 	role="alert"><span></span><a class="alert-close alert-link" href="javascript: void(0);">Закрыть</a></div>
			</div>

		</div>

		
	</div>
</header>
<section>
<div class="container">

    <table class="table table-bordered">
	    <thead>
	    	<tr>
	    		<th>id</th>
	    		<th>название</th>
	    		<th>артикул</th>
	    		<th>цена</th>
	    		<th>Управление</th>
	    		<th>10med id</th>
	    		<th>номенклатура</th>
	    		
	    	</tr>
	   	</thead>
	    <tbody>
	    	<?php foreach ($data[0] as $key => $value): ?>
	    	<tr <?php if (!empty($value['nomencl_10med']) and ($value['linked_with_10med'] > 0)): ?>
					class="success"
	    		<?php elseif (!empty($value['nomencl_10med']) or ($value['linked_with_10med'] > 0)): ?>
	    			class="warning"
				<?php else: ?>
					class="danger"	
	    		<?php endif ?>>
				<td><?=$value['id'] ?></td>
				<td><?=$value['title'] ?></td>
				<td><?=$value['art'] ?></td>
				<td><?=$value['price'] ?></td>
				<td width="80">	
					
					<?php if ($value['linked_with_10med'] > 0): ?>
						<span class="flag10med success">&#10004;</span>
					<?php else: ?>
						<span class="flag10med danger">&#10008;</span>
					<?php endif ?>
					
					<div class="btn-group btn-inline">
						<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    &#9776; <span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
						    <li><a class="item_edit" data-itemid=<?=$value['id'] ?> href="javascript: void(0);">Ручное редактирование</a></li>
						    <li><a class="linking_10med" data-itemid=<?=$value['id'] ?> href="javascript: void(0);">настройка связи 10med</a></li>
						    <li><a href="#">[Пусто]</a></li>
						    <li role="separator" class="divider"></li>
						    <li><a href="/edit/m_catalog/edit.php?id=<?=$value['id'] ?>" target="_blank">Редактирование в CMS</a></li>
						</ul>
					</div>
				</td>
				<td><?=$value['linked_with_10med'] ?></td>
				<td><?=$value['nomencl_10med'] ?></td>
	    	</tr>
	    	<?php endforeach ?>
	   	 </tbody>
    </table>
    <nav>
        <ul class="pagination pagination-sm" data-current=<?=$currentPage ?>>
            <?php if ($currentPage > 1): ?>
				<li><a href="index.php?page=<?=$currentPage-1 ?>" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
            <?php else: ?>	
            	<li class="disabled"><a href="javascript: void(0);" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
            <?php endif ?>
            <?php
            	$i = 1;
            	while ($i <= $countpages):?>
            	<li <?=$currentPage==$i ? 'class="active"' : '' ?>><a  href="index.php?page=<?=$i ?>"><?=$i ?></a></li>
        	<?php
        		$i++;
        		endwhile; ?>
        	<?php if ($currentPage < $countpages): ?>
        		<li><a href="index.php?page=<?= $currentPage+1 ?>" aria-label="Next"><span aria-hidden="true">»</span></a></li>
        	<?php else: ?>
        		<li class="disabled"><a href="javascript: void(0);" aria-label="Next"><span aria-hidden="true">»</span></a></li>
        	<?php endif ?>
            
        </ul>
    </nav>

	<!-- Модальник 1 (ручное радактирование) -->
	<div class="modal modal-item-edit fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	  <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Modal title</h4>

	      </div>
	      <div class="modal-body">
	      		<div class="block_info">	
					<p class="item-id-text">Ключ [id]: <span></span></p>
	       			<p class="item-price-text">Цена: <span></span> руб.</p>	
	      		</div>
	       <div class="input-group input-group-sm">
	         <span class="input-group-addon" id="sizing-addon3">Артикул</span>
	         <input type="text" name="art" class="form-control" placeholder="" aria-describedby="sizing-addon3">
	       </div>
	       <div class="clearfix"></div>
	       <div class="input-group input-group-sm">
	         <span class="input-group-addon" id="sizing-addon3">Id 10med</span>
	         <input type="text" name="id10med" class="form-control" placeholder="" aria-describedby="sizing-addon3">
	       </div>
	       <div class="input-group input-group-sm">
	         <span class="input-group-addon" id="sizing-addon3">Ключ 1С</span>
	         <input type="text"  name="nomencl" class="form-control" placeholder="" aria-describedby="sizing-addon3">
	       </div>
	       <div class="clearfix"></div>

	      </div>
	      <div class="modal-footer">
	  		<div class="alert_block">
				<div style="display: none;" class="alert alert-success" role="alert"><span></span><a class="alert-close" href="javascript: void(0);">Закрыть</a></div>
	  			<div style="display: none;" class="alert alert-info" 	role="alert"><span></span><a class="alert-close" href="javascript: void(0);">Закрыть</a></div>
	  			<div style="display: none;" class="alert alert-warning" role="alert"><span></span><a class="alert-close" href="javascript: void(0);">Закрыть</a></div>
	  			<div style="display: none;" class="alert alert-danger" 	role="alert"><span></span><a class="alert-close" href="javascript: void(0);">Закрыть</a></div>
	  		</div>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
	        <button type="button" class="btn btn-primary save-item-data">Сохранить</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<!-- Модальник 2 (Связь 10мед) -->
	<div class="modal modal-10med-linking fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	  	<div class="modal-dialog modal-lg">
	    	<div class="modal-content">
	     		<div class="modal-header">
	     			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	        		<h4 class="modal-title">Редактор связей 10мед</h4>
	      		</div>
	      		<div class="modal-body " >
      				<div class=" col-md-7">
      				<div class="panel panel-default">
      				  <div class="panel-body block_info">
      				    <span class="item-title-text">Название: <span>...</span></span>
						<hr/>
						<span class="item-id-text">Ключ [id]: <span>...</span></span>
						<span class="item-art-text">Артикул: <span>...</span></span>
						<span class="item-nomencl-text">Ключ 1С: <span>...</span></span>
			 			<span class="item-price-text">Цена: <span>...</span> руб.</span>
			 			<hr/>
			 			<span class="item-status-text">10med id: <span>не определен</span></span>
			 			<span class="item-link-text"><a href="" target="blank_">Ссылка</a></span>
      				  </div>
      				</div>	
						
      				</div>
      				<div class="block_search col-md-5">
      					<div class="input-group">
      					  <input type="text" name="search" class="form-control" name="">
      					  <div class="input-group-btn">
      					    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-search"></span><span class="caret"></span></button>
      					    <ul class="dropdown-menu pull-right">
      					      <li><a href="javascript: void(0);" class="search10med">Поиск несвязанных</a></li>
      					      <li><a href="javascript: void(0);">[пусто]</a></li>
      					      <li class="divider"></li>
      					      <li><a href="javascript: void(0);" class="search10med_nomencl">поиск по номенклатуре</a></li>
      					    </ul>
      					  </div><!-- /btn-group -->
      					</div><!-- /input-group -->
	      				<hr/>
						<div class="btn-group btn-group-justified" role="group" aria-label="...">
							<a href="javascript: void(0);" class="btn btn-default btn-sm disabled showlink" role="button">товар на 10med</a>
							<a href="javascript: void(0);" class="btn btn-default btn-sm disabled relink" role="button">Перелинковка</a>
							<a href="javascript: void(0);" class="btn btn-danger btn-sm disabled unlink" role="button">Отвязать</a>
						  <!-- <div class="btn-group" role="group">
						    <button type="button" class="btn btn-default">Проверка статуса</button>
						  </div>
						  <div class="btn-group" role="group">
						    <button type="button" class="btn btn-danger" disabled="disabled">Отвязать</button>
						  </div> -->
						</div>

      				</div>
      				<div class="clearfix"></div>
			  		<div class="alert_block">
						<div style="display: none;" class="alert alert-success" role="alert"><span></span><a class="alert-close" href="javascript: void(0);">Закрыть</a></div>
			  			<div style="display: none;" class="alert alert-info" 	role="alert"><span></span><a class="alert-close" href="javascript: void(0);">Закрыть</a></div>
			  			<div style="display: none;" class="alert alert-warning" role="alert"><span></span><a class="alert-close" href="javascript: void(0);">Закрыть</a></div>
			  			<div style="display: none;" class="alert alert-danger" 	role="alert"><span></span><a class="alert-close" href="javascript: void(0);">Закрыть</a></div>
			  		</div>
      				<hr>

      				<!--  -->
      				<div class="search_result_list list-group">
					
      				</div>	
	
	      		</div>

	    		<div class="modal-footer">
	    			<button type="button" class="btn btn-default">[пусто]</button>
	              	<button type="button" class="btn btn-default update_modal2" data-dismiss="modal">Закрыть</button>
	            </div>
	    	</div><!-- /.modal-content -->
	 	</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

</div>
</section>
<footer>
	
</footer>