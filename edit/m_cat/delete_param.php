<?php 
include ('../connect.php'); 
include ('../../func/core.php');

if (isset($_GET['id']) && isset($_GET['cat_id']) && $_GET['confirm'] == 'true') {
	$id = $_GET['id'];
	$catId = $_GET['cat_id'];
	
	deleteParamFromCat($catId, $id);
	
	header("Location: /edit/m_catalog/list.php?id=".$catId); 
	die();
}

if (isset($_GET['id']) && isset($_GET['cat_id'])) {
	$id = $_GET['id'];
	$catId = $_GET['cat_id'];
	
	$paramInfo = getParamInfo($id);
	$catInfo = getCatInfo($catId);
	
}

$cssOl = true;
include ('../up.php'); 
?>
 <table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td class="ol">
			<div>Вы действительно хотите удалить параметр?</div>
			<p>Параметр <strong><?=$paramInfo['title']?></strong> в категории <strong><?=$catInfo['title']?></strong></p>
			<div style="margin-top: 10px;">
				<a href="?id=<?=$id?>&cat_id=<?=$catId?>&confirm=true" class="btn">Удалить безвозвратно</a> <a href="/edit/m_catalog/list.php?id=<?=$catId?>" class="btn">Отмена</a>
			</div>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>