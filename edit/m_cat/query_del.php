<?
include ('../connect.php'); 
include ('../../func/core.php');
$id = get_id();
$catInfo = getCatInfo($id);

if ($catInfo == false) die('Категории не существует');

$key = 'cat_'.$id;
$code = md5(microtime(1));
	
//$cssOl = true;
include ('../up.php'); 

mysql_query("
	INSERT INTO `confirm_delete`
	SET
		`key` = '{$key}',
		`code` = '{$code}'
");
if (mysql_affected_rows() > 0) {
	//Подготоваливаем данные для отправки письма
	$params = array(
		'emailTo' => Config::get('site.email_confirm_delete_cat'),
		'subject' => 'Удаление категории товаров на сайте.',
		'body' => 'Было запрошено удаление категории "'.$catInfo['title'].'" <a href="'.Config::get('site.web_addr')."edit/m_cat/delete.php?id={$id}&code={$code}".'">подтвердить удаление</a>'
	);
	
	if(sendMail($params)) {
		echo 'Запрос на удаление категории отправлен.';
	} else {
		echo 'Не могу отправить сообщение о удалении категории.';
		mysql_query("
			DELETE FROM
				`confirm_delete`
			WHERE
				`key` = '{$key}'
			LIMIT 1
		");
	}
	
} else {
	echo 'Запрос на удаление категории уже отправлен.';
}
	
include('../down.php');
?>