<?php 
include ('../connect.php'); 
include ('../../func/core.php');
$id = get_id();

// Обработка
include('add_handler.php');
//
$pOn = array();
$pOff = array();

$pOff = getGlobalParams($id);

//Параметры их POST
$paramsIds = array();
if (isset($_POST['param']) && count($_POST['param']) > 0) {
	foreach ($_POST['param'] as $key => $value) {
		$paramsIds[] = $value;
	}
}

foreach ($pOff as $key => $value) {
	if (in_array($pOff[$key]['id'], $paramsIds)) {
		$pOn[] = $pOff[$key];
		unset($pOff[$key]);
	}
}

$cssOl = true;
include ('../up.php'); 
?>
<style>
	#cat_params, #available_params {
		padding: 5px;
		list-style-type: none;
		margin: 0;
		min-width: 100px;
		min-height: 20px;
	}
	#cat_params {
		background: #eee;
	}
	#available_params {
		border: 1px dashed #ccc;
		border: 1px dashed #fece39;
	}
	#cat_params li, #available_params li{
		background: #ddd;
		margin: 2px 0;
		padding: 3px;
		cursor: move;
	}
	#cat_params li.global, #available_params li.global {
		background: #FECE39;
	}
</style>

	<script type="text/javascript" src="/edit/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/edit/ckfinder/ckfinder.js"></script>
	<script type="text/javascript" src="/js/jquery-ui-1.8.13.js"></script>
	<script type="text/javascript">
		$(function() {
			$( "#available_params" ).sortable({
				connectWith: "ul"
			});
			
			$( "#cat_params" ).sortable({
				connectWith: "ul",
				update: function(event, ui) {
					$('#params-inputs').empty();
					
					$('#cat_params li').each( function(i, el) {
							var id = $(el).data('id');
							$('#params-inputs').append(	
								$('<input>').
									attr('type', 'hidden').
									attr('name', 'param[]').
									attr('value', id)
							);
						});
				}
			});
			$( "#cat_params, #available_params").disableSelection();
			
			//add event
			$('#title').bind('change blur', function() {
				set_value();
			})

			//Чек на дубли по заголовку
			$('#title').bind('blur',  function() {
			  var string = $('#title').val();
			  $.post('../double_check_new.php', { "type": "cat", "string_type" : "title", "string":  string }, function(data) {
				if (data.double == true) {
					$('#title-double-alert').show();
				} else {
					$('#title-double-alert').hide();
				}
			  }, 'json');
			});
			//Сразу и проверим
			$('#title').trigger('blur');
			
			//Чек на дубли по чпу
			$('#chpu').bind('blur',  function() {
			  var string = $(this).val();
			  $.post('../double_check_new.php', { "type": "cat", "string_type" : "chpu", "string":  string }, function(data) {
				if (data.double == true) {
					$('#chpu-double-alert').show();
				} else {
					$('#chpu-double-alert').hide();
				}
			  }, 'json');
			});
			//Сразу и проверим
			$('#chpu').trigger('blur');

		});
		
		function set_value() {
			var input = document.getElementById('title');
			var chpu = document.getElementById('chpu');
			var output1 = document.getElementById('seo_title');
			var output2 = document.getElementById('seo_des');
			var output3 = document.getElementById('seo_key');
			
			chpu.value = input.value.translit().toLowerCase();
			output1.value = input.value;
			output2.value = input.value;
			output3.value = input.value;
		}
		</script>

<form  method="post" action="" enctype="multipart/form-data">
<table width="90%" border="0" align="center" class="txt">
       <tr>
            <td colspan="2" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="65" align="center"><img src="../icons/cat.jpg" width="60" height="60" alt="" /></td>
						<td valign="middle"><span class="big_menu">Добавить категорию</span></td>
					</tr>
                </table>
			</td>
            <td width="12%" rowspan="9" align="left" valign="top"><p>&nbsp;</p>
              <p><br />
              </p></td>
        </tr>
		<tr>
			<td colspan="2">
				<?php if (count($errors) > 0) {
					foreach ($errors as $error) {
						echo "<div class=\"error\" style=\"font-size: 1.1em;\">{$error}</div>";
					}
				} 
				?>
			</td>
		</tr>
            <tr>
              <td colspan="2" class="txtbname"  style="padding: 7px 0;">Название</td>
            </tr>
            <tr>
              <td colspan="2">
				<input name="title" class="span6 p_name" type="text" id="title" value="<?php echo schars($_POST['title']); ?>" size="100" />
				<div id="title-double-alert" style="display: none;">Категория с таким названием уже существует</div>
			  </td>
            </tr>
			<tr>
              <td colspan="2" class="txtbname"  style="padding: 7px 0;">Чпу</td>
            </tr>
            <tr>
              <td colspan="2">
				<input id="chpu" name="chpu" class="span6 p_name" type="text" value="<?php echo schars($_POST['chpu']); ?>" size="100" />
				<div id="chpu-double-alert" style="display: none;">Категория с таким ЧПУ уже существует</div>
			  </td>
            </tr>
            <tr>
              <td colspan="2" class="txtbname"  style="padding: 7px 0;">Текст (<?php num_simvol ($seo_text); ?>)</td>
          </tr>
          <tr>
              <td height="1" colspan="2">
				<textarea name="seo_text" cols="100" id="seo_text"><?php echo slashes($_POST['seo_text']); ?></textarea>
				<script type="text/javascript">
					var editor = CKEDITOR.replace( 'seo_text' );
					CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
				</script> 
			</td>
          </tr>
		  <tr>
			<td colspan="2"  class="txtbname"  style="padding: 7px 0;">
				Доступные параметры\характеристики для товара:
			</td>
		  </tr>
		  <tr>
			<td colspan="2">
				<table border="0" cellpadding="5">
					<tr>
						<td style="text-align: center;"><strong>Используемые параметры</strong></td>
						<td style="text-align: center;"><strong>Отключенные параметры</strong></td>
						<td>
							<input type="checkbox" id="inp_chk_filters_state" name="filters_state" <?php if ($cat_filters_state == 1) echo "checked"; ?>/>
							<label for="inp_chk_filters_state" style="display: inline;">Открыты по умолчанию</label>
						</td>
					</tr>
					<tr>
						<td valign="top">
							
							<ul id="cat_params">
								<?php
									// [id] => 1 [type] => set [title] => Размер
									foreach($pOn as $param) {
										$class = $param['global'] ? 'class="global"' : '';
										echo '<li data-id="'.$param['id'].'"'.$class.'>'.$param['title']."</li>";
									}
								?>
							</ul>
						</td>
						<td valign="top">
							<ul id="available_params">
								<?php
									// [id] => 1 [type] => set [title] => Размер
									foreach($pOff as $param) {
										$class = $param['global'] ? 'class="global"' : '';
										echo '<li data-id="'.$param['id'].'"'.$class.'>'.$param['title']."</li>";
									}
								?>
							</ul>
						</td>
					</tr>
				</table>
				<div id="params-inputs">
					<?php foreach($paramsInCat as $param) { ?>
						<input type="hidden" name="param[]" value="<?php echo $param['id']; ?>">
					<?php } ?>
				</div>
				<div style="clear: both;"></div>
			</td>
		  </tr>
			<tr>
				<td colspan="2"  class="txtbname"  style="padding: 7px 0;">
				Изображения категории:
				</td>
            </tr>
            <tr>
              <td><input type="file" name="upfile" id="upfile" /></td>
              <td><input type="file" name="upfile2" id="upfile2" /></td>
            </tr>
            <tr>
              <td>Изменить картинку (<?=Config::get('image_size.cats_menu');?> | jpg | авторесайз выкл)</td>
              <td>Изменить иконку для меню (<?=Config::get('image_size.cats');?> | jpg | авторесайз выкл)</td>
            </tr>
            <tr>
              <td colspan="2"><table width="300" border="0" align="left" cellpadding="2" cellspacing="0" class="table">
                <tr>
                  <td><i class="icon-search"></i> <strong>SEO</strong></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><label onClick="return (document.getElementById('checkbox_row_4') ? false : true)" for="checkbox_row_2">&lt;title&gt;</label>
                    &nbsp;</td>
                  <td><input class="span9 p_name" name="seo_title" type="text" id="seo_title" value="<?php echo schars($_POST['seo_title']); ?>" size="100" /></td>
                </tr>
                <tr>
                  <td>&lt;description&gt;</td>
                  <td><input class="span9 p_name" name="seo_des" type="text" id="seo_des" value="<?php echo schars($_POST['seo_des']); ?>" size="100"  /></td>
                </tr>
                <tr>
                  <td>&lt;keywords&gt;</td>
                  <td><input class="span9 p_name" name="seo_key" type="text" id="seo_key" value="<?php echo schars($_POST['seo_key']); ?>" size="100" /></td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td width="41%">
			  <button type="submit" name="add" class="btn btn-large" /><i class="icon-pencil"></i> Добавить</button>	
              <td width="47%"></td>
            </tr>
  </table>
      </form>
<?php include ("../down.php"); ?>