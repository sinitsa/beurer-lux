<?
function translit ($stroka)
{

$trans = '';
$stroka = str_replace(") .", "", $stroka);
$stroka = rtrim($stroka);
$stroka = htmlspecialchars ($stroka, ENT_QUOTES);
$stroka = mb_strtolower($stroka);

$stroka = str_replace("&quot;", "", $stroka);

$stroka = str_replace("№", "", $stroka);

$stroka = str_replace("\"", "-", $stroka);
$stroka = str_replace("«", "-", $stroka);
$stroka = str_replace("»", "-", $stroka);

$stroka = str_replace("&laquo;", "-", $stroka);
$stroka = str_replace("&raquo;", "-", $stroka);
$stroka = str_replace("*", "-", $stroka);
$stroka = str_replace("&", "-", $stroka);
$stroka = str_replace("®", "", $stroka);
$stroka = str_replace("(", "-", $stroka);
$stroka = str_replace(")", "", $stroka);
$stroka = str_replace(" .", "", $stroka);
$stroka = str_replace("а", "a", $stroka);
$stroka = str_replace("б", "b", $stroka);
$stroka = str_replace("в", "v", $stroka);
$stroka = str_replace("г", "g", $stroka);
$stroka = str_replace("д", "d", $stroka);
$stroka = str_replace("е", "e", $stroka);
$stroka = str_replace("ё", "e", $stroka);
$stroka = str_replace("ж", "zh", $stroka);
$stroka = str_replace("з", "z", $stroka);
$stroka = str_replace("и", "i", $stroka);
$stroka = str_replace("й", "i", $stroka);
$stroka = str_replace("к", "k", $stroka);
$stroka = str_replace("л", "l", $stroka);
$stroka = str_replace("м", "m", $stroka);
$stroka = str_replace("н", "n", $stroka);
$stroka = str_replace("о", "o", $stroka);
$stroka = str_replace("п", "p", $stroka);
$stroka = str_replace("р", "r", $stroka);
$stroka = str_replace("с", "s", $stroka);
$stroka = str_replace("т", "t", $stroka);
$stroka = str_replace("у", "u", $stroka);
$stroka = str_replace("ф", "f", $stroka);
$stroka = str_replace("х", "h", $stroka);
$stroka = str_replace("ц", "ts", $stroka);
$stroka = str_replace("ч", "ch", $stroka);
$stroka = str_replace("Ч", "ch", $stroka);
$stroka = str_replace("ш", "sh", $stroka);
$stroka = str_replace("щ", "sh", $stroka);
$stroka = str_replace("ь", "", $stroka);
$stroka = str_replace("ы", "y", $stroka);
$stroka = str_replace("ъ", "", $stroka);
$stroka = str_replace("э", "e", $stroka);
$stroka = str_replace("ю", "yu", $stroka);
$stroka = str_replace("я", "ya", $stroka);
$stroka = str_replace("Я", "ya", $stroka);
$stroka = str_replace(" ", "-", $stroka);
$stroka = str_replace(",", "-", $stroka);
$stroka = str_replace("+", "-", $stroka);
$stroka = str_replace("/", "-", $stroka);
$stroka = str_replace('"', "-", $stroka);
$stroka = str_replace("*", "-", $stroka);
$stroka = str_replace(":", "-", $stroka);
$stroka = str_replace('–', "-", $stroka);
$stroka = str_replace("---", "-", $stroka);
$stroka = str_replace("--", "-", $stroka);
$stroka = str_replace("--", "-", $stroka);


$trans = $stroka;
return $trans;
//echo $stroka;

}

function cleanDir($dirname) {
				if(is_dir($dirname)) {
					$dir = opendir($dirname);
					while(false !== ($currentFile = readdir($dir))) {
						if($currentFile != '.' && $currentFile != '..') {
							unlink($dirname.'/'.$currentFile);
						}
					}
				}
}

	if(isset($_POST['act'])) {
		switch($_POST['act']) {
			case 'clear':
				$sources = getcwd() .'/source';
				$temp = getcwd().'/temp';
				$ready = getcwd().'/ready';
				$headers = getcwd().'/headers';
				cleanDir($sources);
				cleanDir($temp);
				cleanDir($ready);
				cleanDir($headers);
				unlink(getcwd().'/arch.zip');
				unlink(getcwd().'/result.zip');
				
				break;
			case 'compose':
				$zipArch = new ZipArchive();
				$zipArch->open('result.zip', ZIPARCHIVE::CREATE);
				
				$upfile_name = $_FILES["files"]["name"]; 
				$upfile = $_FILES["files"]["tmp_name"];
				$i = 0;
				foreach($upfile as $file) {
					move_uploaded_file($file, getcwd().'/temp/'.translit($upfile_name[$i]));
					$i++;
				}
				$temp = getcwd() . '/temp';
				if(is_dir($temp)) {
					$temp = opendir($temp);
					while(false !== ($currentFile = readdir($temp))) {
						if($currentFile != '.' && $currentFile != '..') {
							$tempHandle = fopen(getcwd().'/temp/'.$currentFile, 'rb');
							$headerHandle = fopen(getcwd().'/headers/'.$currentFile, 'rb');
							$readyHandle = fopen(getcwd().'/ready/'.$currentFile, 'w+b');
							while(!feof($headerHandle)) {
								$line = trim(fgets($headerHandle, 999));
								$test = fwrite($readyHandle, $line . "\n");
							}
							while(!feof($tempHandle)) {
								$line = trim(fgets($tempHandle, 999));
								$test = fwrite($readyHandle, $line . "\n");
							}
							$zipArch->addFile('ready/'.$currentFile, $currentFile);
						}
					}
					$zipArch->close();
				}
				echo "<a href='result.zip'>Архив</a>";
				break;
				
			case 'decompose':
				$upfile_name = $_FILES["files"]["name"]; 
				$upfile = $_FILES["files"]["tmp_name"];
				$i = 0;
				foreach($upfile as $file) {
					move_uploaded_file($file, getcwd().'/source/'.translit($upfile_name[$i]));
					$i++;
				}
			
				$zipArch = new ZipArchive();
				$zipArch->open('arch.zip', ZIPARCHIVE::CREATE);
				$sources = getcwd() .'/source';
				$temp = getcwd().'/temp';
				if(is_dir($sources) && is_dir($temp)) {
					$sources = opendir($sources);
					$temp = opendir($temp);
					while(false !== ($currentFile = readdir($sources))) {
						if($currentFile != '.' && $currentFile != '..') {
							//	С файлом из исходников производим операцию
							$fileHandle = fopen(getcwd().'/source/'.$currentFile, 'rb');
							$tempHandle = fopen(getcwd().'/temp/'.$currentFile, 'w+b');
							$headerHandle = fopen(getcwd().'/headers/'.$currentFile, 'w+b');
							$body = false;
							while(!feof($fileHandle)) {
								$line = fgets($fileHandle, 999);
								if($body == false) {
									$test = fwrite($headerHandle, trim($line) . "\n");
								} else {
									$test = fwrite($tempHandle, trim($line) . "\n");
								}
								if(trim($line) == '[__text__]') {
									$body = true;
								} 
							}
							$zipArch->addFile('temp/'.$currentFile, $currentFile);
						}
					}
					$zipArch->close();
				}
				echo "<a href='arch.zip'>Архив</a>";
				
				break;
		}
	}
?>
<form action="" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="act" value="decompose" />
	<input type="file" multiple="multiple" name="files[]" />
	<input type="submit" value="Разобрать" />
</form>
<form action="" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="act" value="compose" />
	<input type="file" multiple="multiple" name="files[]" />
	<input type="submit" value="Собрать" />
</form>
<form action="" method="POST">
	<input type="hidden" name="act" value="clear" />
	<input type="submit" value="Подчиститься" />
</form>