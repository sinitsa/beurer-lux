var inter;
		var active = false;
		var process=false;
		var changed = false;
		var posX,posY;
		$(document).ready(function(){
			$('.dropdown-toggle').on("click",function(){
				
				$(this).next(".dropdown-menu").slideToggle();
			});
			
			if($('.menuHolder .menuBtn').length==0){
				activate();
			}
			$('.menuBtnContent').each(function(i,e){
				var rand = Math.floor(Math.random()*3);
				if(rand == 0){
					$(e).addClass('fir');
				}else if(rand == 1){
					$(e).addClass('sec');
				}else{
					$(e).addClass('thr');
				}
			});
			
			$(document).on('mousemove',function(e){
				if( Math.abs(e.pageX-posX)>10 && Math.abs(e.pageY-posY)>10 ){
					if(!active){
						clearTimeout(inter);
					}
				}
			});
			
			
			$('.menuBtnImg').on('mousedown',function(e){
				clearTimeout(inter);
				posX = e.pageX;
				posY = e.pageY;
				inter = setTimeout(activate,2000);
			});
			
			$(document).on('click',function(e){
				if(e.target.className=='skip'){
				}else if( $(e.target).closest('.menuBtn').length>0 ||
						$(e.target).closest('.newMenuBtn').length>0 ||
						$(e.target).closest('.menuCreateHolder').length>0
					){
					if(active){return false;}
				}else{
					if(active && $('.menuHolder .menuBtn').length>0){
						active = false;
						clearTimeout(inter);
						$('.menuBtnContent').removeClass('animated tada');
						$('.menuHolder').sortable('destroy');
						$('.menuDelBtn').hide();
						$('.newMenuBtn').hide();
						$('.allSepar').removeClass('bordered');
						// SAVE SORT
						if(changed){
							changed = false;
							var res = [];
							var num = 0;
							$('.menuHolder').children().each(function(i,e){
								if(!$(e).hasClass('newMenuBtn')){
									res.push({'id':$(e).find('.menuDelBtn').data('id') ,'num':num});
									num++;
								}
							});
							$.ajax({
								url : "m_edit_menu/ajax.php?method=saveSort",
								type : "POST",
								dataType: 'json',
								data: {'res':res},
								beforeSend: function() {
									process = true;
									NProgress.start();
								},
								success : function (data) {
									
								},
								complete : function() {
									process = false;
									NProgress.done();
								},
								error : function () {
								}
							});
						}
					}
				}
			});
			
			$('.menuDelBtn').on('click',function(){
				if(!process){
				if(confirm('Удалить пункт меню?')){
					var elem = $(this);
					$.ajax({
						url : "m_edit_menu/ajax.php?method=deleteElement&id="+elem.data('id'),
						type : "POST",
						contentType: false,
						processData: false,
						beforeSend: function() {
							process = true;
							NProgress.start();
						},
						success : function (data) {
							if( $('.menuHolder .menuBtn').length==0 ){
								activate();
							}
						},
						complete : function() {
							elem.closest('div').remove();
							process = false;
							NProgress.done();
						},
						error : function () {
						}
					});
				}
				}
				return false;
			});
			$('.newMenuBtn').on('click',function(){
				$('.menuCreateBack').show();
				$('.menuCreateHolder').show();
				
				return false;
			});
			
			$('.selectBtn').on('click',function(){
				if( $(this).data('type')==1 ){
					$('.createSeparator').show();
					$('.createBtn').hide();
				}else if( $(this).data('type')==2 ){
					$('.createSeparator').hide();
					$('.createBtn').show();
				}
			});
			$('#saveNewBtn').on('click',function(){
				var form = $(this).closest('.createBtn');
				var name = form.find('input[name="btnTitle"]').val();
				var url = form.find('input[name="btnUrl"]').val();
				var file = form.find('input[name="btnFile"]').val();
				var formData = new FormData;
				$.each($('#newFile')[0].files, function (i, file){
				   formData.append('files[]', file);
				});
				if( name!='' && url!='' && file!='' ){
					$.ajax({
						url : "m_edit_menu/ajax.php?method=uploadFile&title="+name+"&url="+url,
						type : "POST",
						contentType: false,
						processData: false,
						dataType: 'json',
						data: formData,
						beforeSend: function() {
							NProgress.start();
							form.find('input').attr('disabled','disabled');
						},
						success : function (data) {
							if(data.err=='bad'){
								alert('Запрещенный формат');
							}else if(data.err=='exist'){
								alert('Файл с таким именем уже существует');
							}else{
								$('#newFile').val('');
								form.find('input[name="btnTitle"]').val('');
								form.find('input[name="btnUrl"]').val('');
								form.find('input[name="btnFile"]').val('');
								
								var rand = Math.floor(Math.random()*3);
								if(rand == 0){
									var randCl = 'fir';
								}else if(rand == 1){
									var randCl = 'sec';
								}else{
									var randCl = 'thr';
								}
								var newStr = '<div class="menuBtn"><img src="/edit/m_edit_menu/menuDelBtn.jpg" alt="" style="display:block;" data-id='+data.id+' class="menuDelBtn"><div class="menuBtnContent '+randCl+' animated tada"><div class="menuBtnImg"><a href="'+data.url+'" class="menuLink"><img src="/edit/m_edit_menu/img/'+data.id+'.jpg" alt="" width="60px" height="60px" class="menuImg"></a></div><div class="menuBtnText"><a href="'+data.url+'" class="menuLink">'+data.title+'</a></div></div></div>';
								$('.newMenuBtn').before(newStr);
								
								$('.menuCreateBack').hide();
								$('.menuCreateHolder').hide();
								$('.createSeparator').hide();
								$('.createBtn').hide();
							}
							form.find('input').removeAttr('disabled');
						},
						complete : function() {
							NProgress.done();
						},
						error : function (data) {
							alert('Запрещенный формат');
							form.find('input').removeAttr('disabled');
						}
					});
				}
			});
			$('.menuCreateBack').on('click',function(){
				$('.menuCreateBack').hide();
				$('.menuCreateHolder').hide();
				return false;
			});
			$('.createSeparator .separator').on('click',function(){
				if(!process){
				var type = $(this).data('type');
				if(confirm('Добавить разделитель?')){
					$.ajax({
						url : "m_edit_menu/ajax.php?method=addSeparator&type="+type,
						type : "POST",
						contentType: false,
						processData: false,
						beforeSend: function() {
							process = true;
							NProgress.start();
						},
						success : function (data) {
							process = false;
							if(type==1){
								$('.newMenuBtn').before('<div class="allSepar separ1 bordered"><img src="/edit/m_edit_menu/menuDelBtn.jpg" style="display:block;" data-id="'+data+'" alt="" class="menuDelBtn"></div>');
							}else if(type==2){
								$('.newMenuBtn').before('<div class="allSepar separ2 bordered"><img src="/edit/m_edit_menu/menuDelBtn.jpg" style="display:block;" data-id="'+data+'" alt="" class="menuDelBtn"></div>');
							}else if(type==3){
								$('.newMenuBtn').before('<div class="allSepar separ3 bordered"><img src="/edit/m_edit_menu/menuDelBtn.jpg" style="display:block;" data-id="'+data+'" alt="" class="menuDelBtn"></div>');
							}
							$('.menuCreateBack').hide();
							$('.menuCreateHolder').hide();
							$('.createSeparator').hide();
							$('.createBtn').hide();
						},
						complete : function() {
							NProgress.done();
						},
						error : function () {
						}
					});
				}
				}
			});
		});
		function activate(){
			active = true;
			$('.menuBtnContent').addClass('animated tada');
			$('.menuHolder').sortable({ tolerance: "newMenuBtn",items: "> .menuBtn,.allSepar",change: function( event, ui ) {changed = true;} });
			$('.menuDelBtn').show();
			$('.newMenuBtn').show();
			$('.allSepar').addClass('bordered');
		}