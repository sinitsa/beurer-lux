<?php
include('../../connect.php');
include('../../func/core.php');
include('../../func/classes/augstHelper.php');

function updateSitemapFilePath($path) {
	return mysql_query("UPDATE `config` SET `value`='{$path}' WHERE `key_group`='path' AND `key`='sitemap'");
}

function getPagesIndexes() {
	$pagesIndexes = array();
	$pagesResult = mysql_query("SELECT `id` FROM `pages` WHERE `title` <> ''");
	while($page = mysql_fetch_assoc($pagesResult))
		$pagesIndexes[] = $page['id'];
	return $pagesIndexes;
}

function updatePage($id, $include = true) {
	$num = ($include) ? 1 : 0;
	return mysql_query("UPDATE `pages` SET `sitemap_include`={$num} WHERE `id`={$id}");
}

function includePage($id) {
	return updatePage($id);
}

function excludePage($id) {
	return updatePage($id, false);
}

switch ($_REQUEST['method']) {
	case 'updatesitemapsettings' :
		//	Обновление пути до файла sitemap.xml
		$newSitemapFilePath = $_REQUEST['sitemap_path'];
		if (Config::get('path.sitemap') != $newSitemapFilePath)
			updateSitemapFilePath($newSitemapFilePath);

		//	Включение/исключение страниц из карты сайта
		foreach (getPagesIndexes() as $pageId) {
			if (isset($_REQUEST['include_page_' . $pageId]))
				includePage($pageId);
			else
				excludePage($pageId);
		}

		//	Генерация карты
		//	#TODO:
		echo json_encode(array(
				'result' => augstHelper::sitemap($_SERVER['DOCUMENT_ROOT']),
				'url' => Config::get('site.web_addr') . ltrim($newSitemapFilePath, '/'),
			));
	break;
}