<?
include('../connect.php');
include('../../func/core.php');

$cssOl = true;
include('../up.php');

$sitemapFilePath = Config::get('path.sitemap');

$pages = array();
$pagesStatuses = array();
$pagesResult = mysql_query('SELECT `id`, `title`, `sitemap_include` FROM `pages` WHERE `title` <> ""');
while($page = mysql_fetch_assoc($pagesResult)) {
	$pages[$page['id']] = $page['title'];
	$pagesStatuses[$page['id']] = ($page['sitemap_include'] == 1) ? true : false;
}

?>
<style>
.mul {
	list-style: none; margin:0 0 0 5px; float: left;
}
.mul li label{
	cursor: pointer;
}
.loading, .complete, .error, .alternative, .a-error {
	display: none;
}
.loading img {
	vertical-align: middle;
}
label.page_checkbox {
	display: inline-block;
	width: 305px;
}
</style>
<script src="/js/jquery.form.js" type="text/javascript"></script>
<script>
var handled = 0;
var errorCounter = 0;
$(function() {
	$('.sitemap-form').bind('submit', function () {
		var form = this;
		var oneByOneResizeMode = $(form).find('input[name="one_by_one"]').is(':checked');
		$('.error-log').empty();
		
		if (oneByOneResizeMode) {
			var continue_id = parseInt($(form).find('input[name="continue_id"]').val());
			resizeThisId(--continue_id);
		} else {
			$(this).ajaxSubmit({
				//data : {"page" : page, "sort" : sort},
				dataType : 'json',
				beforeSend : function () {
					$(form).find('.loading').show();
					$(form).find('input[type="submit"]').attr('disabled', 'disabled');
				},
				success : function ( data, statusText, xhr, element) {
					$(form).find('input[type="submit"]').removeAttr('disabled');
					$(form).find('.loading').hide();
					$(form).find('.complete span').html($('<a/>', {
						'href': data.url,
						text: 'sitemap.xml'
					}));
					$(form).find('.complete').show();
					$(form).find('.error').hide();
					return false;
				},
				error : function (jqXHR, textStatus, errorThrown) {
					$(form).find('input[type="submit"]').removeAttr('disabled');
					$(form).find('.loading').hide();
					$(form).find('.error blockquote').html(jqXHR.responseText);
					$(form).find('.error').show()
					$(form).find('.complete').hide();
					return false;
				}
			});
		}
		return false;
	});
});
</script>
<table width="90%" border="0" align="center" class="txt ol">
	<tr>
		<td width="10">&nbsp;</td>
		<td>
			<div>
				<form class="sitemap-form" action="ajax.php?method=updatesitemapsettings" method="post">

					<h4>Настройка генератора карты сайта</h4>
					<ul class="mul">
						<li>
							<label for="inp_txt_sitemap_path" style="display: inline;">
								Путь к файлу sitemap.xml от корня сайта:
							</label>
							<input type="text" id="inp_txt_sitemap_path" name="sitemap_path" value="<?=$sitemapFilePath?>" class="span4" />
						</li>
					</ul><br style="clear: both;" />

					<h5>Страницы, включаемые в карту сайта</h5>
					<ul class="mul">
					<?php foreach($pages as $id => $title): ?>
						<li>
							<label for="inp_chk_include_page_<?=$id?>" class="page_checkbox">
								<?=$title?>
							</label>
							<input type="checkbox" id="inp_chk_include_page_<?=$id?>" name="include_page_<?=$id?>" page_id="<?=$id?>" <?php if($pagesStatuses[$id]) echo "checked"; ?> />
						</li>
					<?php endforeach; ?>
					</ul>

					<div class="cl"></div>
					<div class="loading">
						<img src="/img/ajax_loading.gif" alt=""/> Идет генерация sitemap...
					</div>
					<div class="complete">
						Генерация <span>sitemap.xml</span> завершена.
					</div>
					<div class="error">
						Обработка завершена с ошибкой.<br />
							<blockquote></blockquote>
					</div>
					<div class="alternative">
						Обрабатывается товар с id = <span></span>
						<div class="a-error">Проблемы с ресайзом изображений у товара id = <span></span></div>
						<blockquote></blockquote>
					</div>

					<input type="submit" name="" value="Сохранить настройки и сгенерировать" />
				</form>
				<div class="error-log"></div>
			</div>
		</td>
	</tr>
</table>
<?
include('../down.php');