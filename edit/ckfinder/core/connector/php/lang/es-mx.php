<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Latin American Spanish language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'No ha sido posible completar la solicitud. (Error %1)',
	'Errors' => array (
		'10' => 'Comando incorrecto.',
		'11' => 'El tipo de recurso no ha sido especificado en la solicitud.',
		'12' => 'El tipo de recurso solicitado no es vГЎlido.',
		'102' => 'Nombre de archivo o carpeta no vГЎlido.',
		'103' => 'No se ha podido completar la solicitud debido a las restricciones de autorizaciГіn.',
		'104' => 'No ha sido posible completar la solicitud debido a restricciones en el sistema de archivos.',
		'105' => 'La extensiГіn del archivo no es vГЎlida.',
		'109' => 'PeticiГіn invГЎlida.',
		'110' => 'Error desconocido.',
		'115' => 'Ya existe un archivo o carpeta con ese nombre.',
		'116' => 'No se ha encontrado la carpeta. Por favor, actualice y pruebe de nuevo.',
		'117' => 'No se ha encontrado el archivo. Por favor, actualice la lista de archivos y pruebe de nuevo.',
		'118' => 'Las rutas origen y destino son iguales.',
		'201' => 'Ya existГ­a un archivo con ese nombre. El archivo subido ha sido renombrado como "%1".',
		'202' => 'Archivo invГЎlido.',
		'203' => 'Archivo invГЎlido. El tamaГ±o es demasiado grande.',
		'204' => 'El archivo subido estГЎ corrupto.',
		'205' => 'La carpeta temporal no estГЎ disponible en el servidor para las subidas.',
		'206' => 'La subida se ha cancelado por razones de seguridad. El archivo contenГ­a cГіdigo HTML.',
		'207' => 'El archivo subido ha sido renombrado como "%1".',
		'300' => 'Ha fallado el mover el(los) archivo(s).',
		'301' => 'Ha fallado el copiar el(los) archivo(s).',
		'500' => 'El navegador de archivos estГЎ deshabilitado por razones de seguridad. Por favor, contacte con el administrador de su sistema y compruebe el archivo de configuraciГіn de CKFinder.',
		'501' => 'El soporte para iconos estГЎ deshabilitado.',
	)
);
