<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Swedish language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'BegГ¤ran kunde inte utfГ¶ras eftersom ett fel uppstod. (Fel %1)',
	'Errors' => array (
		'10' => 'Ogiltig begГ¤ran.',
		'11' => 'Resursens typ var inte specificerad i fГ¶rfrГҐgan.',
		'12' => 'Den efterfrГҐgade resurstypen Г¤r inte giltig.',
		'102' => 'Ogiltigt fil- eller mappnamn.',
		'103' => 'BegГ¤ran kunde inte utfГ¶ras p.g.a. restriktioner av rГ¤ttigheterna.',
		'104' => 'BegГ¤ran kunde inte utfГ¶ras p.g.a. restriktioner av rГ¤ttigheter i filsystemet.',
		'105' => 'Ogiltig filГ¤ndelse.',
		'109' => 'Ogiltig begГ¤ran.',
		'110' => 'OkГ¤nt fel.',
		'115' => 'En fil eller mapp med aktuellt namn finns redan.',
		'116' => 'Mappen kunde inte hittas. Var god uppdatera sidan och fГ¶rsГ¶k igen.',
		'117' => 'Filen kunde inte hittas. Var god uppdatera sidan och fГ¶rsГ¶k igen.',
		'118' => 'SГ¶kvГ¤g till kГ¤lla och mГҐl Г¤r identisk.',
		'201' => 'En fil med aktuellt namn fanns redan. Den uppladdade filen har dГ¶pts om till "%1".',
		'202' => 'Ogiltig fil.',
		'203' => 'Ogiltig fil. Filen var fГ¶r stor.',
		'204' => 'Den uppladdade filen var korrupt.',
		'205' => 'En tillfГ¤llig mapp fГ¶r uppladdning Г¤r inte tillgГ¤nglig pГҐ servern.',
		'206' => 'Uppladdningen stoppades av sГ¤kerhetsskГ¤l. Filen innehГҐller HTML-liknande data.',
		'207' => 'Den uppladdade filen har dГ¶pts om till "%1".',
		'300' => 'Flytt av fil(er) misslyckades.',
		'301' => 'Kopiering av fil(er) misslyckades.',
		'500' => 'Filhanteraren har stoppats av sГ¤kerhetsskГ¤l. Var god kontakta administratГ¶ren fГ¶r att kontrollera konfigurationsfilen fГ¶r CKFinder.',
		'501' => 'StГ¶d fГ¶r tumnaglar har stГ¤ngts av.',
	)
);
