<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the German language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'Ihre Anfrage konnte nicht bearbeitet werden. (Fehler %1)',
	'Errors' => array (
		'10' => 'Unbekannter Befehl.',
		'11' => 'Der Ressourcentyp wurde nicht spezifiziert.',
		'12' => 'Der Ressourcentyp ist nicht gГјltig.',
		'102' => 'UngГјltiger Datei oder Verzeichnisname.',
		'103' => 'Ihre Anfrage konnte wegen AuthorisierungseinschrГ¤nkungen nicht durchgefГјhrt werden.',
		'104' => 'Ihre Anfrage konnte wegen DateisystemeinschrГ¤nkungen nicht durchgefГјhrt werden.',
		'105' => 'Invalid file extension.',
		'109' => 'Unbekannte Anfrage.',
		'110' => 'Unbekannter Fehler.',
		'115' => 'Es existiert bereits eine Datei oder ein Ordner mit dem gleichen Namen.',
		'116' => 'Verzeichnis nicht gefunden. Bitte aktualisieren Sie die Anzeige und versuchen es noch einmal.',
		'117' => 'Datei nicht gefunden. Bitte aktualisieren Sie die Dateiliste und versuchen es noch einmal.',
		'118' => 'Quell- und Zielpfad sind gleich.',
		'201' => 'Es existiert bereits eine Datei unter gleichem Namen. Die hochgeladene Datei wurde unter "%1" gespeichert.',
		'202' => 'UngГјltige Datei.',
		'203' => 'ungГјltige Datei. Die DateigrГ¶Гџe ist zu groГџ.',
		'204' => 'Die hochgeladene Datei ist korrupt.',
		'205' => 'Es existiert kein temp. Ordner fГјr das Hochladen auf den Server.',
		'206' => 'Das Hochladen wurde aus SicherheitsgrГјnden abgebrochen. Die Datei enthГ¤lt HTML-Daten.',
		'207' => 'Die hochgeladene Datei wurde unter "%1" gespeichert.',
		'300' => 'Verschieben der Dateien fehlgeschlagen.',
		'301' => 'Kopieren der Dateien fehlgeschlagen.',
		'500' => 'Der Dateibrowser wurde aus SicherheitsgrГјnden deaktiviert. Bitte benachrichtigen Sie Ihren Systemadministrator und prГјfen Sie die Konfigurationsdatei.',
		'501' => 'Die Miniaturansicht wurde deaktivert.',
	)
);
