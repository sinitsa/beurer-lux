<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Estonian language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'PГ¤ringu tГ¤itmine ei olnud vГµimalik. (Viga %1)',
	'Errors' => array (
		'10' => 'Vigane kГ¤sk.',
		'11' => 'Allika liik ei olnud pГ¤ringus mГ¤Г¤ratud.',
		'12' => 'PГ¤ritud liik ei ole sobiv.',
		'102' => 'Sobimatu faili vГµi kausta nimi.',
		'103' => 'Piiratud Гµiguste tГµttu ei olnud vГµimalik pГ¤ringut lГµpetada.',
		'104' => 'FailisГјsteemi piiratud Гµiguste tГµttu ei olnud vГµimalik pГ¤ringut lГµpetada.',
		'105' => 'Sobimatu faililaiend.',
		'109' => 'Vigane pГ¤ring.',
		'110' => 'Tundmatu viga.',
		'115' => 'Sellenimeline fail vГµi kaust on juba olemas.',
		'116' => 'Kausta ei leitud. Palun vГ¤rskenda lehte ja proovi uuesti.',
		'117' => 'Faili ei leitud. Palun vГ¤rskenda lehte ja proovi uuesti.',
		'118' => 'LГ¤hte- ja sihtasukoht on sama.',
		'201' => 'Samanimeline fail on juba olemas. Гњles laaditud faili nimeks pandi "%1".',
		'202' => 'Vigane fail.',
		'203' => 'Vigane fail. Fail on liiga suur.',
		'204' => 'Гњleslaaditud fail on rikutud.',
		'205' => 'Serverisse Гјleslaadimiseks pole Гјhtegi ajutiste failide kataloogi.',
		'206' => 'Гњleslaadimine katkestati turvakaalutlustel. Fail sisaldab HTMLi sarnaseid andmeid.',
		'207' => 'Гњleslaaditud faili nimeks pandi "%1".',
		'300' => 'Faili(de) liigutamine nurjus.',
		'301' => 'Faili(de) kopeerimine nurjus.',
		'500' => 'Failide sirvija on turvakaalutlustel keelatud. Palun vГµta Гјhendust oma sГјsteemi administraatoriga ja kontrolli CKFinderi seadistusfaili.',
		'501' => 'Pisipiltide tugi on keelatud.',
	)
);
