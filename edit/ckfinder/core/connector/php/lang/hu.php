<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Hungarian language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'A parancsot nem sikerГјlt vГ©grehajtani. (Hiba: %1)',
	'Errors' => array (
		'10' => 'Г‰rvГ©nytelen parancs.',
		'11' => 'A fГЎjl tГ­pusa nem lett a kГ©rГ©s sorГЎn beГЎllГ­tva.',
		'12' => 'A kГ­vГЎnt fГЎjl tГ­pus Г©rvГ©nytelen.',
		'102' => 'Г‰rvГ©nytelen fГЎjl vagy kГ¶nyvtГЎrnГ©v.',
		'103' => 'HitelesГ­tГ©si problГ©mГЎk miatt nem sikerГјlt a kГ©rГ©st teljesГ­teni.',
		'104' => 'JogosultsГЎgi problГ©mГЎk miatt nem sikerГјlt a kГ©rГ©st teljesГ­teni.',
		'105' => 'Г‰rvГ©nytelen fГЎjl kiterjesztГ©s.',
		'109' => 'Г‰rvГ©nytelen kГ©rГ©s.',
		'110' => 'Ismeretlen hiba.',
		'115' => 'A fГЎlj vagy mappa mГЎr lГ©tezik ezen a nГ©ven.',
		'116' => 'Mappa nem talГЎlhatГі. KГ©rjГјk frissГ­tsen Г©s prГіbГЎlja Гєjra.',
		'117' => 'FГЎjl nem talГЎlhatГі. KГ©rjГјk frissГ­tsen Г©s prГіbГЎlja Гєjra.',
		'118' => 'Source and target paths are equal.',
		'201' => 'Ilyen nevЕ± fГЎjl mГЎr lГ©tezett. A feltГ¶ltГ¶tt fГЎjl a kГ¶vetkezЕ‘re lett ГЎtnevezve: "%1".',
		'202' => 'Г‰rvГ©nytelen fГЎjl.',
		'203' => 'Г‰rvГ©nytelen fГЎjl. A fГЎjl mГ©rete tГєl nagy.',
		'204' => 'A feltГ¶ltГ¶tt fГЎjl hibГЎs.',
		'205' => 'A szerveren nem talГЎlhatГі a feltГ¶ltГ©shez ideiglenes mappa.',
		'206' => 'Upload cancelled due to security reasons. The file contains HTML-like data.',
		'207' => 'El fichero subido ha sido renombrado como "%1".',
		'300' => 'Moving file(s) failed.',
		'301' => 'Copying file(s) failed.',
		'500' => 'A fГЎjl-tallГіzГі biztonsГЎgi okok miatt nincs engedГ©lyezve. KГ©rjГјk vegye fel a kapcsolatot a rendszer ГјzemeltetЕ‘jГ©vel Г©s ellenЕ‘rizze a CKFinder konfigurГЎciГіs fГЎjlt.',
		'501' => 'A bГ©lyegkГ©p tГЎmogatГЎs nincs engedГ©lyezve.',
	)
);
