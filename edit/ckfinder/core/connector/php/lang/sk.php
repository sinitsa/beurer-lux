<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Slovak language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'Server nemohol dokonДЌiЕҐ spracovanie poЕѕiadavky. (Chyba %1)',
	'Errors' => array (
		'10' => 'NeplatnГЅ prГ­kaz.',
		'11' => 'V poЕѕiadavke nebol ЕЎpecifikovanГЅ typ sГєboru.',
		'12' => 'NepodporovanГЅ typ sГєboru.',
		'102' => 'NeplatnГЅ nГЎzov sГєboru alebo adresГЎra.',
		'103' => 'Nebolo moЕѕnГ© dokonДЌiЕҐ spracovanie poЕѕiadavky kvГґli nepostaДЌujГєcej Гєrovni oprГЎvnenГ­.',
		'104' => 'Nebolo moЕѕnГ© dokonДЌiЕҐ spracovanie poЕѕiadavky kvГґli obmedzeniam v prГ­stupovГЅch prГЎvach k sГєborom.',
		'105' => 'NeplatnГЎ prГ­pona sГєboru.',
		'109' => 'NeplatnГЎ poЕѕiadavka.',
		'110' => 'NeidentifikovanГЎ chyba.',
		'115' => 'ZadanГЅ sГєbor alebo adresГЎr uЕѕ existuje.',
		'116' => 'AdresГЎr nebol nГЎjdenГЅ. Aktualizujte obsah adresГЎra (ZnovunaДЌГ­taЕҐ) a skГєste znovu.',
		'117' => 'SГєbor nebol nГЎjdenГЅ. Aktualizujte obsah adresГЎra (ZnovunaДЌГ­taЕҐ) a skГєste znovu.',
		'118' => 'ZdrojovГ© a cieДѕovГ© cesty sГє rovnakГ©.',
		'201' => 'SГєbor so zadanГЅm nГЎzvom uЕѕ existuje. PrekopГ­rovanГЅ sГєbor bol premenovanГЅ na "%1".',
		'202' => 'NeplatnГЅ sГєbor.',
		'203' => 'NeplatnГЅ sГєbor - sГєbor presahuje maximГЎlnu povolenГє veДѕkosЕҐ.',
		'204' => 'KopГ­rovanГЅ sГєbor je poЕЎkodenГЅ.',
		'205' => 'Server nemГЎ ЕЎpecifikovanГЅ doДЌasnГЅ adresГЎr pre kopГ­rovanГ© sГєbory.',
		'206' => 'KopГ­rovanie preruЕЎenГ© kvГґli nedostatoДЌnГ©mu zabezpeДЌeniu. SГєbor obsahuje HTML data.',
		'207' => 'PrekopГ­rovanГЅ sГєbor bol premenovanГЅ na "%1".',
		'300' => 'Presunutie sГєborov zlyhalo.',
		'301' => 'KopГ­rovanie sГєborov zlyhalo.',
		'500' => 'Prehliadanie sГєborov je zakГЎzanГ© kvГґli bezpeДЌnosti. Kontaktujte prosГ­m administrГЎtora a overte nastavenia v konfiguraДЌnom sГєbore pre CKFinder.',
		'501' => 'MomentГЎlne nie je zapnutГЎ podpora pre generГЎciu miniobrГЎzkov.',
	)
);
