<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Slovenian language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'PriЕЎlo je do napake. (Napaka %1)',
	'Errors' => array (
		'10' => 'NapaДЌen ukaz.',
		'11' => 'V poizvedbi ni bil jasen tip (resource type).',
		'12' => 'Tip datoteke ni primeren.',
		'102' => 'NapaДЌno ime mape ali datoteke.',
		'103' => 'VaЕЎega ukaza se ne da izvesti zaradi teЕѕav z avtorizacijo.',
		'104' => 'VaЕЎega ukaza se ne da izvesti zaradi teЕѕav z nastavitvami pravic v datoteДЌnem sistemu.',
		'105' => 'NapaДЌna konДЌnica datoteke.',
		'109' => 'NapaДЌna zahteva.',
		'110' => 'Neznana napaka.',
		'115' => 'Datoteka ali mapa s tem imenom Еѕe obstaja.',
		'116' => 'Mapa ni najdena. Prosimo osveЕѕite okno in poskusite znova.',
		'117' => 'Datoteka ni najdena. Prosimo osveЕѕite seznam datotek in poskusite znova.',
		'118' => 'ZaДЌetna in konДЌna pot je ista.',
		'201' => 'Datoteka z istim imenom Еѕe obstaja. NaloЕѕena datoteka je bila preimenovana v "%1".',
		'202' => 'Neprimerna datoteka.',
		'203' => 'Datoteka je prevelika in zasede preveДЌ prostora.',
		'204' => 'NaloЕѕena datoteka je okvarjena.',
		'205' => 'Na streЕѕniku ni na voljo zaДЌasna mapa za prenos datotek.',
		'206' => 'Nalaganje je bilo prekinjeno zaradi varnostnih razlogov. Datoteka vsebuje podatke, ki spominjajo na HTML kodo.',
		'207' => 'NaloЕѕena datoteka je bila preimenovana v "%1".',
		'300' => 'Premikanje datotek(e) ni uspelo.',
		'301' => 'Kopiranje datotek(e) ni uspelo.',
		'500' => 'Brskalnik je onemogoДЌen zaradi varnostnih razlogov. Prosimo kontaktirajte upravljalca spletnih strani.',
		'501' => 'Ni podpore za majhne sliДЌice (predogled).',
	)
);
