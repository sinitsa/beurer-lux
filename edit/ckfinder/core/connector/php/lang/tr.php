<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object, for the Turkish language. Turkish translation by Abdullah M CEYLAN a.k.a. Kenan Balamir. Updated.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'Д°steДџinizi yerine getirmek mГјmkГјn deДџil. (Hata %1)',
	'Errors' => array (
		'10' => 'GeГ§ersiz komut.',
		'11' => 'Д°stekte kaynak tГјrГј belirtilmemiЕџ.',
		'12' => 'Talep edilen kaynak tГјrГј geГ§ersiz.',
		'102' => 'GeГ§ersiz dosya ya da klasГ¶r adД±.',
		'103' => 'Kimlik doДџrulama kД±sД±tlamalarД± nedeni ile talebinizi yerine getiremiyoruz.',
		'104' => 'Dosya sistemi kД±sД±tlamalarД± nedeni ile talebinizi yerine getiremiyoruz.',
		'105' => 'GeГ§ersiz dosya uzantД±sД±.',
		'109' => 'GeГ§ersiz istek.',
		'110' => 'Bilinmeyen hata.',
		'115' => 'AynД± isimde bir dosya ya da klasГ¶r zaten var.',
		'116' => 'KlasГ¶r bulunamadД±. LГјtfen yenileyin ve tekrar deneyin.',
		'117' => 'Dosya bulunamadД±. LГјtfen dosya listesini yenileyin ve tekrar deneyin.',
		'118' => 'Kaynak ve hedef yol aynД±!',
		'201' => 'AynД± ada sahip bir dosya zaten var. YГјklenen dosyanД±n adД± "%1" olarak deДџiЕџtirildi.',
		'202' => 'GeГ§ersiz dosya',
		'203' => 'GeГ§ersiz dosya. Dosya boyutu Г§ok bГјyГјk.',
		'204' => 'YГјklenen dosya bozuk.',
		'205' => 'DosyalarД± yГјklemek iГ§in gerekli geГ§ici klasГ¶r sunucuda bulunamadД±.',
		'206' => 'GГјvenlik nedeni ile yГјkleme iptal edildi. Dosya HTML benzeri veri iГ§eriyor.',
		'207' => 'YГјklenen dosyanД±n adД± "%1" olarak deДџiЕџtirildi.',
		'300' => 'Dosya taЕџД±ma iЕџlemi baЕџarД±sД±z.',
		'301' => 'Dosya kopyalama iЕџlemi baЕџarД±sД±z.',
		'500' => 'GГјvenlik nedeni ile dosya gezgini devredД±ЕџД± bД±rakД±ldД±. LГјtfen sistem yГ¶neticiniz ile irtibata geГ§in ve CKFinder yapД±landД±rma dosyasД±nД± kontrol edin.',
		'501' => 'Г–nizleme desteДџi devredД±ЕџД±.',
	)
);
