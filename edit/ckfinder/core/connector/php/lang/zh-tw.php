<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Chinese (Taiwan) language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'з„Ўжі•йЂЈжЋҐе€°дјєжњЌе™Ё ! (йЊЇиЄ¤д»Јзўј %1)',
	'Errors' => array (
		'10' => 'дёЌеђ€жі•зљ„жЊ‡д»¤.',
		'11' => 'йЂЈжЋҐйЃЋзЁ‹дё­ , жњЄжЊ‡е®љиі‡жєђеЅўж…‹ !',
		'12' => 'йЂЈжЋҐйЃЋзЁ‹дё­е‡єзЏѕдёЌеђ€жі•зљ„иі‡жєђеЅўж…‹ !',
		'102' => 'дёЌеђ€жі•зљ„жЄ”жЎ€ж€–з›®йЊ„еђЌзЁ± !',
		'103' => 'з„Ўжі•йЂЈжЋҐпјљеЏЇиѓЅжЇдЅїз”ЁиЂ…ж¬Љй™ђиЁ­е®љйЊЇиЄ¤ !',
		'104' => 'з„Ўжі•йЂЈжЋҐпјљеЏЇиѓЅжЇдјєжњЌе™ЁжЄ”жЎ€ж¬Љй™ђиЁ­е®љйЊЇиЄ¤ !',
		'105' => 'з„Ўжі•дёЉе‚іпјљдёЌеђ€жі•зљ„е‰ЇжЄ”еђЌ !',
		'109' => 'дёЌеђ€жі•зљ„и«‹ж±‚ !',
		'110' => 'дёЌжЋйЊЇиЄ¤ !',
		'115' => 'жЄ”жЎ€ж€–з›®йЊ„еђЌзЁ±й‡Ќи¤‡ !',
		'116' => 'ж‰ѕдёЌе€°з›®йЊ„ ! и«‹е…€й‡Ќж–°ж•ґзђ† , з„¶еѕЊе†Ќи©¦дёЂж¬Ў !',
		'117' => 'ж‰ѕдёЌе€°жЄ”жЎ€ ! и«‹е…€й‡Ќж–°ж•ґзђ† , з„¶еѕЊе†Ќи©¦дёЂж¬Ў !',
		'118' => 'Source and target paths are equal.',
		'201' => 'дјєжњЌе™ЁдёЉе·Іжњ‰з›ёеђЊзљ„жЄ”жЎ€еђЌзЁ± ! ж‚ЁдёЉе‚ізљ„жЄ”жЎ€еђЌзЁ±е°‡жњѓи‡Єе‹•ж›ґж”№з‚є "%1".',
		'202' => 'дёЌеђ€жі•зљ„жЄ”жЎ€ !',
		'203' => 'дёЌеђ€жі•зљ„жЄ”жЎ€ ! жЄ”жЎ€е¤§е°Џи¶…йЃЋй ђиЁ­еЂј !',
		'204' => 'ж‚ЁдёЉе‚ізљ„жЄ”жЎ€е·Із¶“жђЌжЇЂ !',
		'205' => 'дјєжњЌе™ЁдёЉжІ’жњ‰й ђиЁ­зљ„жљ«е­з›®йЊ„ !',
		'206' => 'жЄ”жЎ€дёЉе‚ізЁ‹еєЏе› з‚єе®‰е…Ёе› зґ е·Іиў«зі»зµ±и‡Єе‹•еЏ–ж¶€ ! еЏЇиѓЅжЇдёЉе‚ізљ„жЄ”жЎ€е…§е®№еЊ…еђ« HTML зўј !',
		'207' => 'ж‚ЁдёЉе‚ізљ„жЄ”жЎ€еђЌзЁ±е°‡жњѓи‡Єе‹•ж›ґж”№з‚є "%1".',
		'300' => 'Moving file(s) failed.',
		'301' => 'Copying file(s) failed.',
		'500' => 'е› з‚єе®‰е…Ёе› зґ  , жЄ”жЎ€зЂЏи¦Ѕе™Ёе·Іиў«еЃњз”Ё ! и«‹иЃЇзµЎж‚Ёзљ„зі»зµ±з®Ўзђ†иЂ…дё¦жЄўжџҐ CKFinder зљ„иЁ­е®љжЄ” config.php !',
		'501' => 'зё®ењ–й ђи¦ЅеЉџиѓЅе·Іиў«еЃњз”Ё !',
	)
);
