<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Czech language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'PЕ™Г­kaz nebylo moЕѕnГ© dokonДЌit. (Chyba %1)',
	'Errors' => array (
		'10' => 'NeplatnГЅ pЕ™Г­kaz.',
		'11' => 'Typ zdroje nebyl v poЕѕadavku urДЌen.',
		'12' => 'PoЕѕadovanГЅ typ zdroje nenГ­ platnГЅ.',
		'102' => 'Е patnГ© nГЎzev souboru, nebo sloЕѕky.',
		'103' => 'Nebylo moЕѕnГ© pЕ™Г­kaz dokonДЌit kvЕЇli omezenГ­ oprГЎvnД›nГ­.',
		'104' => 'Nebylo moЕѕnГ© pЕ™Г­kaz dokonДЌit kvЕЇli omezenГ­ oprГЎvnД›nГ­ souborovГ©ho systГ©mu.',
		'105' => 'NeplatnГЎ pЕ™Г­pona souboru.',
		'109' => 'NeplatnГЅ poЕѕadavek.',
		'110' => 'NeznГЎmГЎ chyba.',
		'115' => 'Soubor nebo sloЕѕka se stejnГЅm nГЎzvem jiЕѕ existuje.',
		'116' => 'SloЕѕka nenalezena, prosГ­m obnovte a zkuste znovu.',
		'117' => 'Soubor nenalezen, prosГ­m obnovte seznam souborЕЇ a zkuste znovu.',
		'118' => 'Cesty zdroje a cГ­le jsou stejnГ©.',
		'201' => 'Soubor se stejnГЅm nГЎzvem je jiЕѕ dostupnГЅ, nahranГЅ soubor byl pЕ™ejmenovГЎn na "%1".',
		'202' => 'NeplatnГЅ soubor.',
		'203' => 'NeplatnГЅ soubor. Velikost souboru je pЕ™Г­liЕЎ velkГЎ.',
		'204' => 'NahranГЅ soubor je poЕЎkozen.',
		'205' => 'Na serveru nenГ­ dostupnГЎ doДЌasnГЎ sloЕѕka pro nahrГЎvГЎnГ­.',
		'206' => 'NahrГЎvГЎnГ­ zruЕЎeno z bezpeДЌnostnГ­ch dЕЇvodЕЇ. Soubor obsahuje data podobnГЎ HTML.',
		'207' => 'NahranГЅ soubor byl pЕ™ejmenovГЎn na "%1".',
		'300' => 'PЕ™esunovГЎnГ­ souboru(ЕЇ) selhalo.',
		'301' => 'KopГ­rovГЎnГ­ souboru(ЕЇ) selhalo.',
		'500' => 'PrЕЇzkumnГ­k souborЕЇ je z bezpeДЌnostnГ­ch dЕЇvodЕЇ zakГЎzГЎn. ZdД›lte to prosГ­m sprГЎvci systГ©mu a zkontrolujte soubor nastavenГ­ CKFinder.',
		'501' => 'Podpora nГЎhledЕЇ je zakГЎzГЎna.',
	)
);
