<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Italian language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'Impossibile completare la richiesta. (Errore %1)',
	'Errors' => array (
		'10' => 'Commando non valido.',
		'11' => 'Il tipo di risorsa non ГЁ stato specificato nella richiesta.',
		'12' => 'Il tipo di risorsa richiesto non ГЁ valido.',
		'102' => 'Nome di file o cartella non valido.',
		'103' => 'Non ГЁ stato possibile completare la richiesta a causa di restrizioni di autorizazione.',
		'104' => 'Non ГЁ stato possibile completare la richiesta a causa di restrizioni nei permessi del file system.',
		'105' => 'L\'estensione del file non ГЁ valida.',
		'109' => 'Richiesta invalida.',
		'110' => 'Errore sconosciuto.',
		'115' => 'Un file o cartella con lo stesso nome ГЁ giГ  esistente.',
		'116' => 'Cartella non trovata. Prego aggiornare e riprovare.',
		'117' => 'File non trovato. Prego aggirnare la lista dei file e riprovare.',
		'118' => 'Il percorso di origine e di destino sono uguali.',
		'201' => 'Un file con lo stesso nome ГЁ giГ  disponibile. Il file caricato ГЁ stato rinominato in "%1".',
		'202' => 'File invalido.',
		'203' => 'File invalido. La dimensione del file eccede i limiti del sistema.',
		'204' => 'Il file caricato ГЁ corrotto.',
		'205' => 'Il folder temporario non ГЁ disponibile new server.',
		'206' => 'Upload annullato per motivi di sicurezza. Il file contiene dati in formatto HTML.',
		'207' => 'Il file caricato ГЁ stato rinominato a "%1".',
		'300' => 'Non ГЁ stato possibile muovere i file.',
		'301' => 'Non ГЁ stato possibile copiare i file.',
		'500' => 'Questo programma ГЁ disabilitato per motivi di sicurezza. Prego contattare l\'amministratore del sistema e verificare le configurazioni di CKFinder.',
		'501' => 'Il supporto alle anteprime non ГЁ attivo.',
	)
);
