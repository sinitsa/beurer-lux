<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Finnish language. Translated into Finnish 2010-12-15 by Petteri Salmela, updated.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'PyyntГ¶Г¤ ei voitu suorittaa. (Virhe %1)',
	'Errors' => array (
		'10' => 'Virheellinen komento.',
		'11' => 'PyynnГ¶n resurssityyppi on mГ¤Г¤rittelemГ¤ttГ¤.',
		'12' => 'PyynnГ¶n resurssityyppi on virheellinen.',
		'102' => 'Virheellinen tiedosto- tai kansionimi.',
		'103' => 'Oikeutesi eivГ¤t riitГ¤ pyynnГ¶n suorittamiseen.',
		'104' => 'Tiedosto-oikeudet eivГ¤t riitГ¤ pyynnГ¶n suorittamiseen.',
		'105' => 'Virheellinen tiedostotarkenne.',
		'109' => 'Virheellinen pyyntГ¶.',
		'110' => 'Tuntematon virhe.',
		'115' => 'Samanniminen tiedosto tai kansio on jo olemassa.',
		'116' => 'Kansiota ei lГ¶ydy. YritГ¤ uudelleen kansiopГ¤ivityksen jГ¤lkeen.',
		'117' => 'Tiedostoa ei lГ¶ydy. YritГ¤ uudelleen kansiopГ¤ivityksen jГ¤lkeen.',
		'118' => 'LГ¤hde- ja kohdekansio on sama!',
		'201' => 'Samanniminen tiedosto on jo olemassa. Palvelimelle ladattu tiedosto on nimetty: "%1".',
		'202' => 'Virheellinen tiedosto.',
		'203' => 'Virheellinen tiedosto. Tiedostokoko on liian suuri.',
		'204' => 'Palvelimelle ladattu tiedosto on vioittunut.',
		'205' => 'VГ¤liaikaishakemistoa ei ole mГ¤Г¤ritetty palvelimelle lataamista varten.',
		'206' => 'Palvelimelle lataaminen on peruttu turvallisuussyistГ¤. Tiedosto sisГ¤ltГ¤Г¤ HTML-tyylistГ¤ dataa.',
		'207' => 'Palvelimelle ladattu tiedosto on  nimetty: "%1".',
		'300' => 'Tiedostosiirto epГ¤onnistui.',
		'301' => 'Tiedostokopiointi epГ¤onnistui.',
		'500' => 'Tiedostoselain on kytketty kГ¤ytГ¶stГ¤ turvallisuussyistГ¤. PyydГ¤ pГ¤Г¤kГ¤yttГ¤jГ¤Г¤ tarkastamaan CKFinderin asetustiedosto.',
		'501' => 'Esikatselukuvien tuki on kytketty toiminnasta.',
	)
);
