<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Lithuanian language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'UЕѕklausos ДЇvykdyti nepavyko. (Klaida %1)',
	'Errors' => array (
		'10' => 'Neteisinga komanda.',
		'11' => 'Resurso rЕ«ЕЎis nenurodyta uЕѕklausoje.',
		'12' => 'Neteisinga resurso rЕ«ЕЎis.',
		'102' => 'Netinkamas failas arba segtuvo pavadinimas.',
		'103' => 'Nepavyko ДЇvykdyti uЕѕklausos dД—l autorizavimo apribojimЕі.',
		'104' => 'Nepavyko ДЇvykdyti uЕѕklausos dД—l failЕі sistemos leidimЕі apribojimЕі.',
		'105' => 'Netinkamas failo plД—tinys.',
		'109' => 'Netinkama uЕѕklausa.',
		'110' => 'NeЕѕinoma klaida.',
		'115' => 'Failas arba segtuvas su tuo paДЌiu pavadinimu jau yra.',
		'116' => 'Segtuvas nerastas. Pabandykite atnaujinti.',
		'117' => 'Failas nerastas. Pabandykite atnaujinti failЕі sД…raЕЎД….',
		'118' => 'Е altinio ir nurodomos vietos nuorodos yra vienodos.',
		'201' => 'Failas su tuo paДЌiu pavadinimu jau tra. Д®keltas failas buvo pervadintas ДЇ "%1"',
		'202' => 'Netinkamas failas',
		'203' => 'Netinkamas failas. Failo apimtis yra per didelД—.',
		'204' => 'Д®keltas failas yra paЕѕeistas.',
		'205' => 'NД—ra laikinojo segtuvo skirto failams ДЇkelti.',
		'206' => 'Д®kД—limas bus nutrauktas dД—l saugumo sumetimЕі. Е iame faile yra HTML duomenys.',
		'207' => 'Д®keltas failas buvo pervadintas ДЇ "%1"',
		'300' => 'FailЕі perkД—limas nepavyko.',
		'301' => 'FailЕі kopijavimas nepavyko.',
		'500' => 'FailЕі narЕЎyklД— yra iЕЎjungta dД—l saugumo nustaymЕі. PraЕЎau susisiekti su sistemЕі administratoriumi ir patikrinkite CKFinder konfigЕ«racinДЇ failД….',
		'501' => 'MiniatiЕ«rЕі palaikymas iЕЎjungtas.',
	)
);
