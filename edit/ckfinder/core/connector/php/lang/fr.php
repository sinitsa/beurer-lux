<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the French language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'La demande n\'a pas abouti. (Erreur %1)',
	'Errors' => array (
		'10' => 'Commande invalide.',
		'11' => 'Le type de ressource n\'a pas Г©tГ© spГ©cifiГ© dans la commande.',
		'12' => 'Le type de ressource n\'est pas valide.',
		'102' => 'Nom de fichier ou de dossier invalide.',
		'103' => 'La demande n\'a pas abouti : problГЁme d\'autorisations.',
		'104' => 'La demande n\'a pas abouti : problГЁme de restrictions de permissions.',
		'105' => 'Extension de fichier invalide.',
		'109' => 'Demande invalide.',
		'110' => 'Erreur inconnue.',
		'115' => 'Un fichier ou un dossier avec ce nom existe dГ©jГ .',
		'116' => 'Ce dossier n\'existe pas. Veuillez rafraГ®chir la page et rГ©essayer.',
		'117' => 'Ce fichier n\'existe pas. Veuillez rafraГ®chir la page et rГ©essayer.',
		'118' => 'Les chemins vers la source et la cible sont les mГЄmes.',
		'201' => 'Un fichier avec ce nom existe dГ©jГ . Le fichier tГ©lГ©versГ© a Г©tГ© renommГ© en "%1".',
		'202' => 'Fichier invalide.',
		'203' => 'Fichier invalide. La taille est trop grande.',
		'204' => 'Le fichier tГ©lГ©versГ© est corrompu.',
		'205' => 'Aucun dossier temporaire n\'est disponible sur le serveur.',
		'206' => 'Envoi interrompu pour raisons de sГ©curitГ©. Le fichier contient des donnГ©es de type HTML.',
		'207' => 'Le fichier tГ©lГ©chargГ© a Г©tГ© renommГ© "%1".',
		'300' => 'Le dГ©placement des fichiers a Г©chouГ©.',
		'301' => 'La copie des fichiers a Г©chouГ©.',
		'500' => 'L\'interface de gestion des fichiers est dГ©sactivГ©. Contactez votre administrateur et vГ©rifier le fichier de configuration de CKFinder.',
		'501' => 'La fonction "miniatures" est dГ©sactivГ©e.',
	)
);
