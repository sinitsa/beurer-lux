<?php
include('../../connect.php');
include('../../func/core.php');

switch ($_REQUEST['method']) {

	  case "product_lfp":
    $term = mysql_real_escape_string($_POST['term']);
    // $term = iconv("UTF-8", "CP1251", $term);
    $q = "SELECT * FROM catalog WHERE title LIKE '%{$term}%' OR art = '{$term}'";
    $result = mysql_query($q) or die(mysql_error());
    $arr = array();
    while($row = mysql_fetch_assoc($result)){
      $arr[] = $row;
    }
    $result = $arr;
    if(count($result) > 0){
      foreach($result as $key=>$r){
        ?>
        <div class="media" style="width: 200px; height: 85px; float: left; border: 1px dashed black; border-radius: 4px;margin: 10px;padding-top: 5px;">
          <a class="pull-left" href="#">
            <img class="media-object" src="<?= getImageWebPath('product_medium') . $r['id']; ?>.jpg" width="67" height="90">
          </a>
          <div class="media-body" style="height: 85px; position: relative;">
            <p style="font-size: 12px;">[<?= $r['title']; ?>]</p>
            <a href="" class="btn btn-mini addtovar" style="right: 10px; bottom: 10px; position: absolute;" data-id="<?= $r['id']; ?>">Добавить</a>
          </div>
        </div>
      <?php
      }
    }else{
      echo "Товаров не найдено";
    }
    break;
    
    case 'product_add':
    	$id = $_POST['id'];
    	$this_id = $_POST['this_id'];
    	$q = "INSERT INTO link_product(product_id, linked_product_id) VALUES('{$this_id}', '{$id}')";
    	mysql_query($q) or die(mysql_error());
    break;

    case 'product_delete':
    	$id = $_POST['id'];
    	$this_id = $_POST['this_id'];
    	$q = "DELETE FROM link_product WHERE product_id='{$this_id}' AND linked_product_id = '{$id}'";
    	mysql_query($q) or die(mysql_error());
    break;

	//Переключатель статусов товара (лучшее, новинка, распродажа)
	case 'toggle' :
		$availableAttrs = array('best','novinka','sale');
		$id = $_REQUEST['id'];
		$attr = $_REQUEST['attr'];
		$toggle = $_REQUEST['toggle'];
		if (is_numeric($id) && in_array($attr, $availableAttrs) && ($toggle == '1' || $toggle == '0')) {
			mysql_query("
				UPDATE
					`catalog`
				SET
					`{$attr}` = '{$toggle}'
				WHERE
					`id` = '{$id}'
				LIMIT 1
			");
			echo json_encode(array('error' => false));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	//Установка стоимости товару
	case 'setprice' :
		$id = $_REQUEST['id'];
		$price = $_REQUEST['price'];
		
		if (is_numeric($id) && is_numeric($price)) {
			setPrice($id, $price);
			echo json_encode(array('error' => false));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	//Установка артикла товару
	case 'setart' :
		$id = $_REQUEST['id'];
		$art = mysql_real_escape_string(iconv("utf-8", "windows-1251", urldecode($_REQUEST['art'])));
		
		if (is_numeric($id)) {
			mysql_query("
				UPDATE
					`catalog`
				SET
					`art` = '{$art}'
				WHERE
					`id` = '{$id}'
				LIMIT 1
			");
			echo json_encode(array('error' => false));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	case 'set_field' :
		$id = $_REQUEST['id'];
		$value =$_REQUEST['value'];
		$field = mysql_real_escape_string($_REQUEST['field']);
		
		if ($field == 'delivery_cost') {
			$value = empty($value) && $value != '0' ? 'NULL' : "'".mysql_real_escape_string(iconv("utf-8", "windows-1251", $value))."'";
		} else {
			$value = mysql_real_escape_string(iconv("utf-8", "windows-1251", $value));
			$value = "'{$value}'";
		}
		
		if (is_numeric($id)) {
			mysql_query("
				UPDATE
					`catalog`
				SET
					`{$field}` = {$value}
				WHERE
					`id` = '{$id}'
				LIMIT 1
			");
			echo json_encode(array('error' => (mysql_affected_rows() < 0)));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	//Установка ставки маркета товару
	case 'setbid' :
		$id = $_REQUEST['id'];
		$bid = mysql_real_escape_string($_REQUEST['bid']);
		
		if (is_numeric($id)) {
			mysql_query("
				UPDATE
					`catalog`
				SET
					`market_cost_per_click` = '{$bid}'
				WHERE
					`id` = '{$id}'
				LIMIT 1
			");
			echo json_encode(array('error' => false));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
		//Удаление товара
	case 'deleteproduct' :
		$id = $_REQUEST['id'];
		if (is_numeric($id)) {
			deleteProduct($id);
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	//Получить стоимость доставки для товара
	case 'get_delivery_cost' :
		$id = $_REQUEST['id'];
		if (is_numeric($id)) {
			echo json_encode(array(
				'error' => false,
				'delivery_price' => deliveryPrice::getByCatAndPrice(0, 0, $id)
				));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	//Загрузка главного фото к товару
	case 'uploadmainimage' :
		if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
			$id = $_REQUEST['id'];
			

			$a = saveMainImage($_FILES['main_image'], $id);
			
			if ($a) {
				echo json_encode(array('error' => true));
			} else {
				echo json_encode(array('error' => false));
			}
			
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	case 'uploadextraimages' :
		if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
			$catalogId = $_REQUEST['id'];
						
			$idAdded = array();
		
			for ($i = 0; $i < count($_FILES['extra_photos']['name']); $i++) {
				$photo = array(
					'name' => $_FILES['extra_photos']['name'][$i],
					'type' => $_FILES['extra_photos']['type'][$i],
					'tmp_name' => $_FILES['extra_photos']['tmp_name'][$i],
					'error' => $_FILES['extra_photos']['error'][$i],
					'size' => $_FILES['extra_photos']['size'][$i]
				);
				
				$idAdded[] = saveExtraPhoto($photo, $catalogId) ;
			}
			echo json_encode(array('error' => false, 'images' => $idAdded));
		} else {
			echo json_encode(array('error' => true, 'code' => 1));
		}
	break;
	
	case 'deleteextraimages' :
		$ids = array();
		
		foreach ($_POST['extra_photo'] as $id => $v) {
			$ids[] = $id;
			deleteExtraPhoto($id);
		}
		echo json_encode(array('error' => false, 'images' => $ids));
	break;
	
	case 'changecat' :
		$ids = $_REQUEST['products'];
		$catId = $_REQUEST['cat_id'];
		
		foreach($ids as $id) {
			if (catalogChangeCat($id, $catId))
				$success[] = $id;
		}
		
		echo json_encode(array('error' => false, 'products' => $success));
	break;
	//Проверка чпу на повторение
	case 'checkchpu' : 
		$chpu = mysql_real_escape_string($_REQUEST['chpu']);
		$id = is_numeric($_REQUEST['product_id']) ? $_REQUEST['product_id'] : 0;
		$count = fetchOne("SELECT COUNT(*) FROM `catalog` WHERE `chpu`='{$chpu}' AND `id` <> '{$id}'");
		
		echo json_encode(array('is_double' => $count > 0));
	break;
	//Проверка названия на повторение
	case 'checktitle' : 
		$title = mysql_real_escape_string(iconv('utf-8', 'cp1251', $_REQUEST['title']));
		$id = is_numeric($_REQUEST['product_id']) ? $_REQUEST['product_id'] : 0;
		$count = fetchOne("SELECT COUNT(*) FROM `catalog` WHERE `title`='{$title}' AND `id` <> '{$id}'");
		
		echo json_encode(array('is_double' => $count > 0));
	break;
	
	//Сохранение сортировки товаров
	case 'setsort' :
		$sort = $_REQUEST['sort_list'];
		$count = count($sort);

		for ($i = 0; $i < $count; $i++) {

			if (is_numeric($sort[$i]['id']) && is_numeric($sort[$i]['sort'])) {
				$id = $sort[$i]['id'];
				$rang = $sort[$i]['sort'];
				mysql_query ("
					UPDATE
						`catalog`
					SET
						`spec_rang` = '{$rang}'
					WHERE
						`id` = '{$id}'
				");
			}
		}
		echo json_encode(array('error' => false));
	break;
	//Слинковка товара с тегом\подкатегорией\псевдокатегорией
	case 'linkwithtag' :
		$tagId = $_REQUEST['tag_id'];
		if (!is_numeric($tagId)) {
			echo json_encode(array('error' => true, 'message' => 'No tag ID given'));
			break;
		}
		foreach ($_REQUEST['products'] as $pid) {
			linkCatalogAndTags($pid ,$tagId);
		}
		echo json_encode(array('error' => false, "products" => $_REQUEST['products']));
	break;
	//Разлинковка товара и тега\подкатегории\псевдокатегории
	case 'unlinkfromtag' :
		$tagId = $_REQUEST['tag_id'];
		if (!is_numeric($tagId)) {
			echo json_encode(array('error' => true, 'message' => 'No tag ID given'));
			break;
		}
		foreach ($_REQUEST['products'] as $pid) {
			unlinkCatalogAndTags($pid ,$tagId);
		}
		echo json_encode(array('error' => false, "products" => $_REQUEST['products']));
	break;
	case "sorttags" :
		$sort = $_REQUEST['sort_list'];
		$count = count($sort);
		
		for ($i = 0; $i < $count; $i++) {

			if (is_numeric($sort[$i]['id']) && is_numeric($sort[$i]['sort'])) {
				$id = $sort[$i]['id'];
				$rang = $sort[$i]['sort'];
				mysql_query ("
					UPDATE
						`tags`
					SET
						`rang` = '{$rang}'
					WHERE
						`id` = '{$id}'
				");
			}
		}
		echo json_encode(array('error' => false));
	break;
	
	//Возвращает шаблон с параметрами для товара (в редактор товаров)
	case 'getparams' :
		if (isset($_REQUEST['product_id']) && is_numeric($_REQUEST['product_id'])) {
			$id = $_REQUEST['product_id'];
			
			$product = getProduct($id);
			include('params_tpl.php');
			?>
			<table border="0" cellspacing="0" cellpadding="0" style="margin-top:5px;">
			  <tr>
				<td width="75" align="left" class="txtskidksm">Скидка</td>
				<td align="left" class="txtskidksm" width="65">
					<input type="text" name="discount_percent" id="discount_percent" class="txtpricesmf" value="<?php echo ($product['discount_type'] == 'percent' ? $product['discount_value']:'');?>">%
				</td>
				<td align="left" >
					<input type="text" name="discount_value" id="discount_value" class="txtpricesmfr" value="<?php echo ($product['discount_type'] == 'value' ? $product['discount_value']:'');?>">
					<span class="txtrub">руб.</span>
				</td>
			  </tr>
			</table>
			<input type="hidden" id="price" value="<?php echo $product['price']; ?>" />
			<?php
		}
	break;
	case 'setparams' :
		if (isset($_REQUEST['product_id']) && is_numeric($_REQUEST['product_id'])) {
			$id = $_REQUEST['product_id'];
			$product = getProduct($id);
			//Массив с ID параметров
			$paramsIds = array();
			
			//Собираем id параметров с чекбоксов и селектов
			if (count($_REQUEST['param_checkbox']) > 0 ) {
				foreach	($_REQUEST['param_checkbox'] as $key => $value) {
					$paramsIds[] = $key;
				}
			}
			if (count($_REQUEST['param_select']) > 0) {
				foreach	($_REQUEST['param_select'] as $key => $value) {
					if ($value > 0) 
						$paramsIds[] = $value;
				}
			}
			
			if (count($_REQUEST['param_range']) > 0) {
				foreach	($_REQUEST['param_range'] as $key => $value) {
					if ($value['value'] != '') {
						//$value['value'] = iconv("utf-8", "windows-1251", urldecode($value['value']));
						$loloId = addAvailableParam(array(
							'paramId' => $key,
							'catId' => $value['global'] ? 0 : $value['cat'],
							'value' => '',
							'valueFloat' => $value['value']
						));
						if ($loloId) $paramsIds[] = $loloId;
					}
				}
			}
			
			setProductsParams($id, $paramsIds);
			
			//Обновляем скидки.
			$data = array();
			$data['price'] = $product['price'];
			
			$dp = $_REQUEST['discount_percent'];
			$dv = $_REQUEST['discount_value'];
			if (!empty($dp) && $dp > 0) {
				$data['discount_type'] = 'percent';
				$data['discount_value'] = $dp;
			} elseif (!empty($dv) && $dv > 0 ) {
				$data['discount_type'] = 'value';
				$data['discount_value'] = $dv;
			} else {
				$data['discount_value'] = 0;
			}
			$data['price_after_discount'] = getPriceAfterDiscount($data['price'], $data['discount_type'] , $data['discount_value']);
			
			mysql_query("
				UPDATE
					`catalog`
				SET	".getSetString($data)."
				WHERE
					`id` = '{$id}'
			");
		}
	break;
	
	//Сортировка дополнительных фото к товару
	case 'sortextraphoto' :
		$sort = $_REQUEST['sort_list'];
		$count = count($sort);
		
		for ($i = 0; $i < $count; $i++) {

			if (is_numeric($sort[$i]['id']) && is_numeric($sort[$i]['sort'])) {
				$id = $sort[$i]['id'];
				$rang = $sort[$i]['sort'];
				mysql_query ("
					UPDATE
						`foto`
					SET
						`rang` = '{$rang}'
					WHERE
						`id` = '{$id}'
				");
			}
		}
		echo json_encode(array('error' => false));
	break;
	
	//Обработка запроса на начало слинковки товара с 10мед
	case 'want_to_link' :
		$id = $_REQUEST['id'];
		if (!is_numeric($id)) {
			echo json_encode(array('error' => true, 'error_code' => 0));
			break;
		}
		//Выбираем данные о товаре
		$product = getProduct($id);
		
		//Подготавливаем данные для api запроса
		$params = array(
			'base_id' => $config['10med_shopid'],
			'id' => $product['id'],
			'title' => $product['title'],
			'price' => $product['price_after_discount'],
			'article' =>$product['art']
		);
		
		//Отправляем api запрос
		$response = api_10med($method = 'want_to_link', $params);
		file_put_contents('response.txt', var_export($response, true));
		//Разбираем ответ
		if ($response['already_linked']) {
			//Если уже связан
			echo json_encode(array('error' => true, 'error_code' => 1));
			break;
		} else {
			//Если нет, выдаем подсказки
			echo json_encode(array(
				'error' => false,
				'suggestion' => $response['suggestion']
				));
		}
		
	break;
	
	case 'get_linked_product':
		//ID товара в магазине
		$localId = $_REQUEST['local_id'];
		
		if (!is_numeric($localId)) {
			echo json_encode(array('error' => true, 'error_message' => "Bad ID given"));
			break;
		}
		$remoteId = fetchOne("SELECT `linked_with_10med` FROM `catalog` WHERE `id` = '{$localId}'");
		if ($remoteId) {
			$params = array(
				'id' => $remoteId ,
			);
			
			//Отправляем запрос на слинковку
			$response = api_10med('get_product_info', $params);

			echo json_encode(array('error' => $response['error'], 'info' => $response['info'] ,'error_message' => $response['error_message']));
		} else {
			echo json_encode(array('error' => true, 'error_message' => "Product not linked"));
			break;
		}
	break;
	
	case 'link':
		//ID товара в магазине
		$localId = $_REQUEST['local_id'];
		//ID товара на 10мед
		$remoteId = $_REQUEST['remote_id'];
		
		if (!is_numeric($localId) || !is_numeric($remoteId)) {
			echo json_encode(array('error' => true, 'error_code' => 0));
			break;
		}
		
		$params = array(
			'local_id' => $localId ,
			'remote_id' => $remoteId
		);
		
		//Отправляем запрос на слинковку
		$response = api_10med('link_products', $params);
		if (!$response['error']) {
			//mysql_query("UPDATE `catalog` SET `linked_with_10med` = '1' WHERE `id` = '{$localId}' LIMIT 1");
			//do nothing, we will notified
		}
		echo json_encode(array('error' => $response['error'], 'error_code' => 0));
		break;
	break;
	
	case 'unlink' :
		//ID товара в магазине
		$localId = $_REQUEST['local_id'];
		
		if (!is_numeric($localId)) {
			echo json_encode(array('error' => true, 'error_message' => 'Bad ID given'));
			break;
		}
		
		$params = array(
			'local_id' => $localId
		);
		
		//Отправляем запрос на слинковку
		$response = api_10med('unlink_products', $params);
		if (!$response['error']) {
			//mysql_query("UPDATE `catalog` SET `linked_with_10med` = '0' WHERE `id` = '{$localId}' LIMIT 1");
		}
		
		echo json_encode(array('error' => $response['error'], 'error_message' => $response['error_message']));
		break;
	break;
	
	case 'manual_search' :
		$params = array(
			'search_text' => $_REQUEST['search_text']
		);
		
		//Отправляем запрос на поиск
		$response = api_10med('manual_search', $params);
		
		echo json_encode(array(
				'error' => false,
				'suggestion' => $response['suggestion']
				));
	break;
	case 'addicon':
		$id_icon = (int)$_POST['dataid'];
		$id_product = (int)$_POST['productid'];
		$text = mysql_real_escape_string($_POST['text']);
		$class = mysql_real_escape_string($_POST['cl']);
		$pos = (int)$_POST['pos'];
		$query = "INSERT INTO icon_product(id_icon, id_product, `text`, `class`, `pos`) VALUES($id_icon, $id_product, '{$text}', '{$class}', $pos)";
		mysql_query($query) or die(mysql_error());
		$t_id = mysql_insert_id();
		echo $t_id;
	break;
	case 'deleteicon':
		if(isset($_POST['id'])){
			$id = (int)$_POST['id'];
			if($id){
				$query = "DELETE FROM icon_product WHERE id = $id";
				mysql_query($query) or die(mysql_error());
				echo $id;
			}
		}
	break;
	case 'sorticon':
		$key = $_POST['key'];
		$id = $_POST['id'];
		foreach ($key as $t => $value) {
			if(isset($id[$t]) && $id[$t] !== "undefined"){
				$query = "UPDATE icon_product SET pos = $t WHERE id = {$id[$t]} LIMIT 1";
				mysql_query($query) or die(mysql_error()); 
			}
		}
	break;
	case 'updateicon':
		$id = $_POST['id'];
		$text = mysql_real_escape_string($_POST['text']);
		$query = "UPDATE icon_product SET `text` = '{$text}' WHERE id = {$id} LIMIT 1";
		mysql_query($query) or die(mysql_error());
	break;
	
	case 'fake_in_stock' :
		
		if (isset($_REQUEST['id']) && 
			is_numeric($_REQUEST['id']) &&
			isset($_REQUEST['check'])) {
		
			$fake_in_stock = ($_REQUEST['check'] === 'true') ? 1 : 0;
			$id = $_REQUEST['id'];
			
			$query = "UPDATE catalog 
					  SET `fake_in_stock` = {$fake_in_stock}
					  WHERE id = {$id}";			
			mysql_query($query);
			echo $query;
			// if (mysql_error()) 
				// echo json_encode(array("error" => true, 
										// "msg" => "Database error"));
		}
		else 
			echo json_encode(array("error" => true, 
									"msg" => "Wrong data")); 
	break;
	
	case 'save_attr' :
		$id = $_REQUEST['product_id'];
    	$arrPar = $_REQUEST['dataPar'];
		
		$q_del = "DELETE FROM ultra_param_links WHERE id_catalog={$id}";
		
		mysql_query($q_del) or die(mysql_error());
		
		foreach($arrPar as $par)
		{
		
			mysql_set_charset("utf-8");
			$q = "INSERT INTO ultra_param_links(id_catalog, id_param, value) VALUES({$id}, {$par['id']},'{$par['value']}')";
			var_dump($q);
			mysql_query($q) or die(mysql_error());
		}
		
    	
		
	break;
}