<?php
include ("../connect.php");
include ("../../func/core.php");
/*ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);*/
?>
<?php 
$cssOl = true;
include ("../up.php");

//Супер расчеты тут
$listingType = 'cat';

if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
	//print_r(getSort());
	//print_r(getPage());
	$id = $_REQUEST['id'];
	$cat = getCatInfo($id);
	
	$filter['cat'] = $id;

	$page = array(
			'page' => getPage(),
			'onPage' => 'all'
		);

	$data = getProducts(false, $filter, getSort(), $page);

	//Выбираем данные о псевдокатегории\подкатегории\тегам (с названием трудно определится, везде по разному)
	$tags = getTagsInCat($id);
	$listingType = 'cat';
} elseif (isset($_REQUEST['tag_id']) && is_numeric($_REQUEST['tag_id'])) {
	$tagId = $_REQUEST['tag_id'];
	
	$tagInfo = getTagInfo($tagId);
	
	$filter = array();
	$filter['tag'] = $tagId;
	
	$data = getProducts(false, $filter, getSort(), false);
	$listingType = 'tag';
}
?>
<script type="text/javascript" src="/js/jquery-ui-1.8.13.js"></script>
<script src="/js/jquery.form.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function() {
		
		//Биндим изменение цены
		$('.price').bind('blur change', function(e) {
			var id = $(this).closest('.divkatlist').data('id');
			var price = $(this).val();

			if ($.isNumeric(price) && price >= 0) {
				updatePrice(id, price);
			} else {
				alert('Неверная цена');
			}
		});
		//Биндим изменение артикла
		$('.art').bind('blur change', function(e) {
			var id = $(this).closest('.divkatlist').data('id');
			var art = $(this).val();

			updateArt(id, art);
		});
		
		//Hello products sort
		$('#catalog-field').sortable({
			//connectWith: "div.divkatlist",
			//cancel: ".cl",
			tolerance : "pointer",
			update : function (event, ui) {
			
				$('#catalog-field').css('opacity', .5);
				
				var sortList = new Array();
				
				$('#catalog-field .divkatlist').each(function (index, element) {
					sortList.push({
						'id' : $(element).data('id'),
						'sort' : index
					});
				});
				
				$.ajax({
					url: 'ajax.php?method=setsort',
					type : 'POST',
					dataType : 'json',
					data : {"sort_list" : sortList},
					beforeSend : function () {},
					success : function (data, textStatus, jqXHR) {
						if (data.error) {
							//do nothing :)
						} else {
							//хайдим
							$('#catalog-field').css('opacity', 1);
						}
					},
					error : function (jqXHR, textStatus, errorThrown) {}
				});
			}
		});
		//Hello tags sort
		$('#tags-field').sortable({
			connectWith: "div.tag",
			//cancel: ".cl",
			tolerance : "pointer",
			update : function (event, ui) {
			
				$('#tags-field').css('opacity', .5);
				
				var sortList = new Array();
				
				$('#tags-field .tag').each(function (index, element) {
					sortList.push({
						'id' : $(element).data('id'),
						'sort' : index
					});
				});
				
				$.ajax({
					url: 'ajax.php?method=sorttags',
					type : 'POST',
					dataType : 'json',
					data : {"sort_list" : sortList},
					beforeSend : function () {},
					success : function (data, textStatus, jqXHR) {
						if (data.error) {
							//do nothing :)
						} else {
							//хайдим
							$('#tags-field').css('opacity', 1);
						}
					},
					error : function (jqXHR, textStatus, errorThrown) {}
				});
			}
		});
		//Всплывающее окно параметров
		var maySendForm = false;
		$('.params-button').bind('click', function() {
			var modal = $('#params-modal');
			var shadow = modal.find('.shadow');
			var body = modal.find('.body');
			var content = modal.find('.content');
			var close = modal.find('.close-btn');
			var productId = $(this).data('id');
			
			shadow.css({
				'position' : 'absolute',
				'width' : $(document).width() > $(window).width() ? $(document).width() : $(window).width(),
				'height' : $(document).height() > $(window).height() ? $(document).height() : $(window).height(),
				'opacity' : 0.5
			});
			body.center(0, -150);
			shadow.add(close).unbind('click');
			shadow.add(close).bind('click', function () {
				modal.hide();
			});
			modal.show();
			content.empty().append($('<img />').attr('src', '/img/ajax_loading.gif'));
			$.ajax({
				'url' : 'ajax.php?method=getparams',
				'data' : {product_id : productId},
				'dataType' : 'html',
				beforeSend : function () {maySendForm = false},
				success : function (data, textStatus, jqXHR) {
					content.html(data);
					content.prepend($('<input />').attr({
						'name' : 'product_id',
						'type' : 'hidden',
						'value' : productId
						}));
					maySendForm = true;
					body.center(0, -150);
					stylizeCheckbox();
				}
			});
		});
		$('#params-modal form').bind('submit', function () {
			var modal = $('#params-modal');
			var content = modal.find('.content');
			if (maySendForm) {
				$(this).ajaxSubmit({
					url : 'ajax.php',
					data : {"method" : 'setparams'},
					beforeSend : function () {
						content.empty().append($('<img />').attr('src', '/img/ajax_loading.gif'));
						maySendForm = false;
					},
					success : function ( data, statusText, xhr, element) {
						modal.hide();
					}
				});
			}
			return false;
		});
		//Автопросчет % и значений скиндки в форме
		$('#price').live('change', function(){
			$('#discount_value').trigger('keyup');
		});
		$('#discount_percent').live('keyup', function() {
			var price = $('#price').val();
			var percent = $(this).val();
			
			if (price == '') price = 0;
			$('#discount_value').val(Math.round(percent * price / 100));
		});
		$('#discount_value').live('keyup', function() {
			// var price = $('#price').val();
			// var value = $(this).val();

			// if (price == '') price = 0;
			// $('#discount_percent').val(Math.round(value * 100 / price));
			$('#discount_percent').val("");
		});
		stylizeCheckbox();
	});
	//Удалить товар
	function deleteCatalog(id) {
		if (confirm("Вы уверены, что хотите удалить товар?")) {
			deleteProduct(id, function (data) {
				$('#catalog-' + id).fadeOut('slow', function () {
					$('#catalog-' + id).remove();
				});
			});
		}
	}
	//Удалить выбранные товары
	function deleteSelectedCatalogs() {
		if (confirm("Вы уверены, что хотите удалить выбранные товары?")) {
			$('#catalog-field input:checked').each(function (index, element) {
				var id = $(element).data('id');
				
				deleteProduct(id, function (data) {
					$('#catalog-' + id).fadeOut('slow', function () {
						$('#catalog-' + id).remove();
					});
				});
			});
		}
	}
	
	function selectAll() {
		$('#catalog-field input[type="checkbox"]').attr('checked', 'checked');
		stylizeCheckbox();
	}
	
	function changeCat() {
		var products = new Array();
		var catId = $('#change-cat-select').val();
		
		$('#catalog-field input:checked').each(function (index, element) {
				products.push($(element).data('id'));
		});
		
		$.ajax({
			url: 'ajax.php?method=changecat',
			type : 'POST',
			dataType : 'json',
			data : {"products" : products, "cat_id" : catId},
			beforeSend : function () {},
			success : function (data, textStatus, jqXHR) {
				if (data.error) {
					//do nothing :)
				} else {
					//хайдим
					for (var i in data.products) {
						$('#catalog-' + data.products[i]).fadeOut('slow', function() {$('#catalog-' + data.products[i]).remove()} );
					}
				}
			},
			error : function (jqXHR, textStatus, errorThrown) {}
		});
	}
	//Соединить товар с тегом\подкатегорией. (другими словами, положить товар в подкатегорию)
	function linkWithTag(t) {
		var products = new Array();
		var tagId = t;
		var _this = this;
		
		$('#catalog-field input:checked').each(function (index, element) {
				products.push($(element).data('id'));
		});
		
		$.ajax({
			url: 'ajax.php?method=linkwithtag',
			type : 'POST',
			dataType : 'json',
			data : {"products" : products, "tag_id" : tagId},
			beforeSend : function () {},
			success : function (data, textStatus, jqXHR) {
				if (data.error) {
					//do nothing :)
					alert(data.message);
				} else {
					//Моргаем, подтверждая что слинковка удачна
					$('#tag-' + tagId ).animate({
						opacity : 0.5}, 300, function() {
							$(this).animate({opacity:1}, 300);
						});
					$('#catalog-field input').removeAttr("checked");
					stylizeCheckbox();
				}
			},
			error : function (jqXHR, textStatus, errorThrown) {}
		});
	}
	//Убрать товар из подкатегории\тега
	function unlinkFromTag(t) {
		var products = new Array();
		var tagId = t;
		var _this = this;
		
		$('#catalog-field input:checked').each(function (index, element) {
				products.push($(element).data('id'));
		});
		
		$.ajax({
			url: 'ajax.php?method=unlinkfromtag',
			type : 'POST',
			dataType : 'json',
			data : {"products" : products, "tag_id" : tagId},
			beforeSend : function () {},
			success : function (data, textStatus, jqXHR) {
				if (data.error) {
					//do nothing :)
					alert(data.message);
				} else {
					//удаляем из листа
					for (var i in products) {
						var id = products[i];
						$('#catalog-' + id).fadeOut('slow', function () {
							$('#catalog-' + id).remove();
						});
					}
					stylizeCheckbox();
				}
			},
			error : function (jqXHR, textStatus, errorThrown) {}
		});
	}
</script>
<div class="ol" style="width:100%; height:100%; position:absolute; z-index: 0;" align="center">
<div style="width:100%; height:100%; max-width:1270px; min-width:1000px;" align="center">
<!-- Modal window -->
<div id="params-modal" class="wmodal">
	<div class="shadow"></div>
	<div class="body">
		<div style="text-align: right;">
			<img class="close-btn" src="/img/zndelete.gif" alt="" style="position: relative; top: -5px; left: 34px; margin-top: -25px;" />
		</div>
		<form action="" method="post">
			<div class="content"></div>
			<input type="image" name="save" src="/img/save.gif" width="137" height="40" />
		</form>
	</div>
</div>
<!-- /Modal window -->
	<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="99%" align="center" valign="top">
	<table width="96%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="200" align="left" valign="top">
				<div style="margin-top:-10px;"></div>
				
		<?php foreach (getCats() as $item) { ?>
				<div class="limain">
					<a href="/edit/m_catalog/list.php?id=<?php echo $item['id']; ?>"><?php echo $item['title']; ?> </a>
				</div> 
		<?php }	?>
					
				</td>
				<td align="left" valign="top">
	<?php if ($listingType == 'cat') { ?>
		<table width="250" border="0" align="right" cellpadding="0" cellspacing="0" class="bordotziv2" style="margin-top:30px;">
		<tr>
			<td align="left" valign="top">
				<div class="bordotziv3">
					<div class="txtzakser" style="margin-top:5px; margin-bottom:4px;"><a href="/edit/m_catalog/list_table_view.php?id=<?php echo $id; ?>">Переключить вид</a></div>
					<div class="limain2"><a href="javascript:void(0);" onclick="selectAll();">Выделить все</a></div> 
					<div class="limain3"><a href="javascript:void(0);" onclick="deleteSelectedCatalogs();">Удалить</a></div> 
					<div class="limain2">
						<a href="javascript:void(0);" onclick="$(this).next().slideToggle();">Перенос в другую категорию</a>
						<div style="margin: 5px 0; padding: 0; display: none;">
							<select id="change-cat-select" name="cat_id" style="width: 180px;">
								<?php
								foreach (getCats() as $f) {
									if ($id == $f['id']) continue;
									echo "<option value=\"{$f['id']}\"".($f['id'] == $product['cat'] ? ' selected="selected"':'').">{$f['title']}</option>";
									if (isset($f['subcats']) && count($f['subcats']) > 0) {
										foreach ($f['subcats'] as $subcat) {
											if ($id == $subcat['id']) continue;
											echo "<option value=\"{$subcat['id']}\"".($subcat['id'] == $product['cat'] ? ' selected="selected"':'').">- {$subcat['title']}</option>";
										}
									}
								}
								?>
							</select>
							<input type="button" value="Перенести" onclick="changeCat();" />
						</div>
					</div>
					<div class="limain2"><a href="/edit/m_cat/edit.php?id=<?=$id?>">Редактировать категорию</a></div> 
					<div class="txtzakser" style="margin-top:5px; margin-bottom:4px;"><a href="/edit/m_catalog/add.php?cat_id=<?php echo $id; ?>">Добавить новый товар</a></div>
					<div class="txtzakser" style="margin-top:5px; margin-bottom:4px;"><a href="/edit/m_tags/add.php?cat_id=<?php echo $id; ?>">Добавить подкатегорию</a></div>
					<div class="txtzakser" style="margin-top:5px; margin-bottom:4px;"><a href="/edit/m_filter/add.php?cat_id=<?php echo $id; ?>">Создать фильтр</a></div>
					<div class="txtzakser" style="margin-top:5px; margin-bottom:4px;"><a href="/edit/m_articles/add.php?cat_id=<?php echo $id; ?>">Добавить статью</a></div>
				</div>
			</td>
		</tr>
	</table>
	<?php } elseif ($listingType == 'tag') { ?>
	<table width="250" border="0" align="right" cellpadding="0" cellspacing="0" class="bordotziv2" style="margin-top:30px;">
		<tr>
			<td align="left" valign="top">
				<div class="bordotziv3">
					<div class="txtzakser" style="margin-top:5px; margin-bottom:4px;"><a href="/edit/m_catalog/list_table_view.php?tag_id=<?php echo $tagId; ?>">Переключить вид</a></div>
					<div class="limain2"><a href="javascript:void(0);" onclick="selectAll();">Выделить все</a></div> 
					<div class="limain3"><a href="javascript:void(0);" onclick="unlinkFromTag(<?=$tagId?>);">Убрать</a></div> 
				</div>
			</td>
		</tr>
	</table>
	<?php } ?>
		 <div style="margin-left:20px; margin-bottom:20px;"><h1><?php echo $cat['title']; ?></h1></div>
		<?php if ($listingType == 'cat') { ?>
			<? if (count($tags) > 0 ) { ?>
			<div id="tags-field">
			<?php foreach ($tags as $tag) { ?>
				<div id="tag-<?=$tag['id']?>" class="tag" data-id="<?=$tag['id']?>">
					<div>
						<table style="" border="0">
							<tr>
								<td rowspan="2" style="width: 1px;">
									<a href="/edit/m_catalog/list.php?tag_id=<?=$tag['id']?>">
										<img src="<?=getImageWebPath('tags').$tag['id']; ?>.jpg" alt="<?php #echo $tag['title']; ?>" />
									</a>
								</td>
								<td style="vertical-align: top; height: 1px;">
									&nbsp;<a href="/edit/m_tags/del.php?id=<?=$tag['id']?>"><img src="/img/zndelete.gif" alt="Удалить" /></a>
								</td>
							</tr>
							<tr>
								<td style="vertical-align: top;">
									&nbsp;<a href="/edit/m_tags/edit.php?id=<?=$tag['id']?>"><img src="/img/znedit.gif" alt="Редактировать" /></a>
								</td>
							</tr>
						</table>
					</div>
					<div>
						<a href="/edit/m_catalog/list.php?tag_id=<?=$tag['id']?>"><?php echo $tag['title']; ?></a>
					</div>
					<div><a style="color: gray;" href="javascript:void(0);" onclick="linkWithTag(<?=$tag['id']?>);">Прикрепить</a></div>
				</div>
			<?php } ?>
			<div class="cl" style="margin-bottom: 30px;"></div>
			</div>
			<?php } ?>
			<!-- Верстка это !@#$% -->
			<div style="float: right; clear: both; width: 230px; text-align: left; padding: 0px;">
				<div style="padding: 0px;">
					<h3 style="color: #a29d9f; font-weight: normal;">Глобальные фильтры</h3>
					<ul style="list-style: none; margin: 5px 0 0 15px;">
						<?php foreach (getGlobalParams($id) as $param) { ?>
						<li class="mcat-li-style <?=$param['enable'] ? 'mcat-li-style-on': 'mcat-li-style-off' ?>">
							<a href="/edit/m_filter/edit.php?id=<?=$param['id']?>"><?php echo $param['title']; ?></a>&nbsp;
							<a href="/edit/m_filter/delete.php?id=<?php echo $param['id']; ?>&cat_id=<?=$id?>"><img src="/img/dopfotodel.png" width="12" height="12" alt="" /></a>
						</li>
						<?php } ?>
					</ul>
				</div>
				<div style="padding: 0px;">
					<h3 style="color: #a29d9f; font-weight: normal;">Локальные фильтры</h3>
					<ul style="list-style: none; margin: 5px 0 0 15px;">
						<?php foreach (getLocalParams($id) as $param) { ?>
						<li class="mcat-li-style <?=$param['enable'] ? 'mcat-li-style-on': 'mcat-li-style-off' ?>"">
							<a href="/edit/m_filter/edit.php?id=<?=$param['id']?>&cat_id=<?=$cat['id'];?>"><?php echo $param['title']; ?></a>&nbsp;
							<a href="/edit/m_filter/delete.php?id=<?php echo $param['id']; ?>&cat_id=<?=$id?>"><img src="/img/dopfotodel.png" width="12" height="12" alt="" /></a>
						</li>
						<?php } ?>
					</ul>
				</div>
				<div style="padding: 0px;">
					<h3 style="color: #a29d9f; font-weight: normal;">Статьи</h3>
					<ul style="list-style: none; margin: 5px 0 0 15px;">
						<?php foreach (getCatArticles($id) as $article) { ?>
						<li class="mcat-li-style">
							<a href="/edit/m_articles/edit.php?id=<?php echo $article['id']; ?>"><?php echo $article['title']; ?><a>&nbsp;
							<a href="/edit/m_articles/delete.php?id=<?php echo $article['id']; ?>&cat_id=<?=$id?>"><img src="/img/dopfotodel.png" width="12" height="12" alt="" /></a>
						</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		<?php } ?>
		<div id="catalog-field">
			<?php foreach ($data['products'] as $product) { ?>
			<? $native = ($product['cat'] == $id) ? 'true' : 'false'; 
				//echo '<br>' . $native . "<br>";
			?>
			<div id="catalog-<?php echo $product['id']; ?>" class="divkatlist" data-id="<?php echo $product['id']; ?>"  data-native="<?=$native?>">
				<div  align="left" style="position:relative; z-index:1; display:table;">
					<?php if ($product['best']) { ?>
						<a href="javascript:void(0)" ><img data-enabled="1" onclick="toggleProductAttr(this, <?php echo $product['id']; ?>, 'best')" src="/img/best.png" width="42" height="43" class="imgzn"></a>
					<?php } else { ?>
						<a href="javascript:void(0)"><img data-enabled="0" onclick="toggleProductAttr(this, <?php echo $product['id']; ?>, 'best')" src="/img/bests.png" width="42" height="43" class="imgzn"></a>
					<?php } ?>
					<?php if ($product['sale']) { ?>
						<a href="javascript:void(0)"><img data-enabled="1" onclick="toggleProductAttr(this, <?php echo $product['id']; ?>, 'sale')" src="/img/sale.png" width="43" height="42" class="imgzn"></a>
					<?php } else { ?>
						<a href="javascript:void(0)"><img data-enabled="0" onclick="toggleProductAttr(this, <?php echo $product['id']; ?>, 'sale')" src="/img/sales.png" width="43" height="42" class="imgzn"></a>
					<?php } ?>
					<?php if ($product['novinka']) { ?>
						<a href="javascript:void(0)"><img data-enabled="1" onclick="toggleProductAttr(this, <?php echo $product['id']; ?>, 'novinka')" src="/img/new.png" width="42" height="43" class="imgzn"></a>
					<?php } else { ?>
						<a href="javascript:void(0)"><img data-enabled="0" onclick="toggleProductAttr(this, <?php echo $product['id']; ?>, 'novinka')" src="/img/news.png" width="42" height="43" class="imgzn"></a>
					<?php } ?>
				</div>
				
				<div align="left" style="display:table;">
					<table width="26" border="0" align="right" cellpadding="0" cellspacing="0" style="margin-right:-30px;">
						<tr valign="top">
							<td align="center">
								<div style="margin-bottom:20px; padding-top:5px;">
									<!-- <img src="/img/galvkl.gif" width="17" height="16" border="0"> -->
									<div class="filter-checkbox-wrapper">
										<div class="filter-checkbox filter-checkbox-off">
											<input class="" type="checkbox" data-id="<?php echo $product['id'];?>" name="catalog[<?php echo $product['id'];?>]" />
										</div>
									</div>
								</div>
								<a href="javascript:void(0);" onclick="deleteCatalog(<?php echo $product['id']; ?>);"><img src="/img/zndelete.gif" width="26" height="25" border="0" style="margin-bottom:5px;"></a><br />
								<a href="/edit/m_catalog/edit.php?id=<?php echo $product['id']; ?>"><img src="/img/znedit.gif" width="26" height="25" border="0"></a><br />
								<a href="javascript:void(0);" class="params-button" data-id="<?php echo $product['id']; ?>"><img style="margin-left: -5px; margin-top: 5px;" src="/img/settings.png" alt="" /></a>
							</td>
						</tr>
					</table>
					<a href="/edit/m_catalog/edit.php?id=<?php echo $product['id']; ?>">
						<img src="<?php echo getImageWebPath('product_medium').$product['id']; ?>.jpg" class="imglist">
					</a>
				</div>
				<div class="divkatlisttxt">
					<a href="/edit/m_catalog/edit.php?id=<?php echo $product['id']; ?>"><?php echo $product['title']; ?></a>
					<br />
					<? if ($product['rests_main']['available']) {
						echo '<span style="color: green;">('.$product['rests_main']['main'].' | '.$product['rests_main']['extra'].')</span>';
					}
					?>
				</div>
				<div class="divkatlisttabl">   
					 <table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="3"><img src="/img/pr_left.png" width="12" height="27"></td>
							<td><input type="text" name="price" class="txttable price" value="<?php echo $product['price']; ?>"></td>
							<td width="20" align="center" class="txtprice">.-</td>
							<td width="3"><img src="/img/pr_right.png" width="12" height="27"></td>
							<td width="5">&nbsp;</td>
							<td width="60"><input type="text" name="art" class="art" value="<?php echo $product['art']; ?>"></td>
						</tr>
					</table>   
				</div>
			</div>
		 <?php } ?>
		 </div>
			</td>
			</tr>
		</table>
	 
		<?php generatePaginator($data['page']); ?>
	
	</td>
	</tr>
	<tr>
		<td height="80">&nbsp;</td>
	</tr>
</table>

</div>
</div>
<?php include ("../down.php"); ?>