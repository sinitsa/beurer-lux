<?php
	$params = getCatParams($product['cat']);
	foreach($params as $param) {
?>
<h2><?php echo $param['title']; ?>:</h2>
<div class="params-field" style="margin-top:8px;">
	<?php
	if ($param['type'] == PARAM_SET) {
		$values = getCatAvailableValues($param['param_id'], $product['cat'], $product['id']);
		foreach($values as $value):
	?>
		<div class="filter-checkbox-wrapper">
			<label>
				<div class="filter-checkbox filter-checkbox-off">
					<input class="hidden-checkbox" type="checkbox" name="param_checkbox[<?php echo $value['id'];?>]" <?php echo ($value['enable'] ? 'checked="checked"':'')?>/>
				</div>
				<?php if($value['css']){?>
					<style>
					.panels-sprite {
						    position: relative;
							top: -5px;
							float: left;
						    display: block;
						    height: 29px;
						    width: 29px;
						    background-image: url("../../assets/gfx/panels-sprite.png");
						    background-repeat: no-repeat;
						}

						.panels-sprite.rocket {
						    background-position: 0 0;
						}

						 .panels-sprite.hearphones {
						    background-position: 0 -32px;
						}

						 .panels-sprite.handplate {
						    background-position: 0 -63px;
						}

						.panels-sprite.fire {
						    background-position: 0 -94px;
						}

						 .panels-sprite.threed {
						    background-position: 0 -125px;
						}

						 .panels-sprite.car {
						    background-position: 0 -156px;
						}

						 .panels-sprite.roller {
						    background-position: 0 -187px;
						}

						 .panels-sprite.wifi {
						    background-position: 0 -218px;
						}

						 .panels-sprite.head {
						    background-position: 0 -249px;
						}

						 .panels-sprite.ruller {
						    background-position: 0 -280px;
						}

						 .panels-sprite.power {
						    background-position: 0 -311px;
						}

						 .panels-sprite.takehand {
						    background-position: 0 -342px;
						}

						 .panels-sprite.arrowdown {
						    background-position: 0 -373px;
						}

						 .panels-sprite.grid {
						    background-position: 0 -404px;
						}

						 .panels-sprite.bag {
						    background-position: 0 -435px;
						}

						 .panels-sprite.calc {
						    background-position: 0 -466px;
						}

						 .panels-sprite.equalizer {
						    background-position: 0 -497px;
						}

						 .panels-sprite.tape {
						    background-position: 0 -528px;
						}

						 .panels-sprite.arrows {
						    background-position: 0 -559px;
						}

						 .panels-sprite.around {
						    background-position: 0 -590px;
						}

						 .panels-sprite.scales {
						    background-position: 0 -621px;
						}

						 .panels-sprite.layers {
						    background-position: 0 -652px;
						}

						 .panels-sprite.fat {
						    background-position: 0 -683px;
						}

					</style>
					<span class="panels-sprite <?=$value['css']?>"></span>
				<?php }?>
				<div class="divrazmer" style="float: left;">&nbsp;<?php echo $value['value']; ?></div>
			</label>
		</div>
		<?php endforeach; ?>
	<?php } elseif ($param['type'] == PARAM_VALUE) { ?>
		<select name="param_select[]">
			<option value="0">Не выбрано</option>
		<?php
		$values = getCatAvailableValues($param['param_id'], $product['cat'], $product['id']);
		foreach($values as $value): ?>
			<option value="<?=$value['id']?>" <?=($value['enable'] ? 'selected="selected"' : '')?>><?=$value['value']?></option>
		<?php endforeach; ?>
		</select>
	<? } elseif ($param['type'] == PARAM_RANGE) {
		$values = getCatAvailableValues($param['param_id'], $product['cat'], $product['id']);
		$currentVal = '';
		foreach($values as $value) {
			if ($value['enable']) {
				$currentVal = $value['value_float'];
				break;
			}
		}
		?>
		<div class="param-range" data-param_id="<?=$param['id']?>" data-cat_id="<?=($param['global'] ? 0 : $product['cat'])?>">
			<input type="text" name="param_range[<?=$param['id']?>][value]" class="span1 param-input" value="<?=$currentVal;?>" /> <?=$param['unit']?>
			<input type="hidden" name="param_range[<?=$param['id']?>][global]" value="<?=$param['global']?>" />
			<input type="hidden" name="param_range[<?=$param['id']?>][cat]" value="<?=$product['cat']?>" />
		</div>
	<? } ?>
	<div style="clear: both;"></div>
</div>
<?php } ?>
<!--
<script>
	$(function(){
		$('.param-range .param-input').bind('change', function() {
			var root = $(this).closest('.param-range');
			
			var value = $(this).val();
			
			var paramId = root.data('param_id');
			var catId = root.data('cat_id');
			
			if (value != '') {
				//Кидаем ajax на изменение параметра
				
			}
		});
	});
</script>
-->