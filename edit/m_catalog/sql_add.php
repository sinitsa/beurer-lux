<?php
if (isset($_POST['add'])) {
	if (strlen($_POST['title']) > 2 && strlen($_POST['chpu']) > 2 ) {
	
	mysql_query("SET NAMES utf8");


		//цикл вместо эксцепшенов
		do {
			$data['cat'] = is_numeric($_POST['cat_id']) && $_POST['cat_id'] > 0 ? $_POST['cat_id'] : 0;
			
			$data['title'] = mysql_real_escape_string($_REQUEST['title']);
			$data['price'] = is_numeric($_REQUEST['price']) ? $_REQUEST['price'] : 0;
			
			if (get_magic_quotes_gpc()){
				$data['art'] = mysql_real_escape_string(stripslashes($_REQUEST['article']));
				$data['short_des'] = mysql_real_escape_string(stripslashes($_REQUEST['short_des']));
				$data['des'] = mysql_real_escape_string(stripslashes($_REQUEST['desc']));	
			    $data['seo_title'] = mysql_real_escape_string(stripslashes($_REQUEST['seo_title']));
			    $data['seo_des'] = mysql_real_escape_string(stripslashes($_REQUEST['seo_des']));
			    $data['seo_key'] = mysql_real_escape_string(stripslashes($_REQUEST['seo_key']));				
			}
			else {
				$data['art'] = mysql_real_escape_string($_REQUEST['article']);
				$data['short_des'] = mysql_real_escape_string($_REQUEST['short_des']);
				$data['des'] = mysql_real_escape_string($_REQUEST['desc']);
				$data['seo_title'] = mysql_real_escape_string($_REQUEST['seo_title']);
				$data['seo_des'] = mysql_real_escape_string($_REQUEST['seo_des']);
				$data['seo_key'] = mysql_real_escape_string($_REQUEST['seo_key']);
			}	
			
			
			//$like_desc = trimDesc($_REQUEST['like_desc']);
			//$like_desc = $_REQUEST['like_desc'];

			/* if (!empty($like_desc)&& strpos($like_desc, 'youtube') !== false) {
				$data['like_desc'] = mysql_real_escape_string($_REQUEST['like_desc']);
				if(get_magic_quotes_gpc()) {
					$data['like_desc'] = mysql_real_escape_string(stripslashes($_REQUEST['like_desc']));
				}						
			} else {
				$data['like_desc'] = '';
			} */
			$data['chpu'] = mysql_real_escape_string($_REQUEST['chpu']);
			$data['like_desc'] = mysql_real_escape_string(stripslashes($_REQUEST['like_desc']));
			$data['sale'] = isset($_REQUEST['sale']) ? 1 : 0;
			$data['best'] = isset($_REQUEST['best']) ? 1 : 0;
			$data['novinka'] = isset($_REQUEST['new']) ? 1 : 0;
			
			$data['seo_title'] = mysql_real_escape_string($_REQUEST['seo_title']);
			$data['seo_des'] = mysql_real_escape_string($_REQUEST['seo_des']);
			$data['seo_key'] = mysql_real_escape_string($_REQUEST['seo_key']);
			
			$dp = $_REQUEST['discount_percent'];
			$dv = $_REQUEST['discount_value'];
			if (!empty($dp) && $dp > 0) {
				$data['discount_type'] = 'percent';
				$data['discount_value'] = $dp;
			} elseif (!empty($dv) && $dv > 0 ) {
				$data['discount_type'] = 'value';
				$data['discount_value'] = $dv;
			} else {
				$data['discount_value'] = 0;
			}
			$data['price_after_discount'] = getPriceAfterDiscount($data['price'], $data['discount_type'] , $data['discount_value']);
			$data['spec_rang'] = 0;
			//Проверка ЧПУ на дубль
			if ( fetchOne("SELECT * FROM `catalog` WHERE `chpu` = '{$data['chpu']}'") ) break;
			
			//Выполняем сдвиг сортировки, чтобы воткнуть новый элемент в начало списка
			mysql_query("
				UPDATE
					`catalog`
				SET
					`spec_rang` = `spec_rang` + 1
				WHERE
					`cat` = '{$data['cat']}'
			");
			//Добавляем запись в каталог
			$setString = getSetString($data);
			mysql_query("INSERT INTO
				`catalog`
				SET {$setString}
			");
			
			$id = mysql_insert_id();
			
			if ($id <= 0) break;
			
			//Сохранение основного фото
			if (isset($_FILES['main_image'])) {
				saveMainImage($_FILES['main_image'], $id);
			}
			//Сохранение дополнительных фото
			if (isset($_FILES['extra_photos'])) {
				for ($i = 0; $i < count($_FILES['extra_photos']['name']); $i++) {
					$photo = array(
						'name' => $_FILES['extra_photos']['name'][$i],
						'type' => $_FILES['extra_photos']['type'][$i],
						'tmp_name' => $_FILES['extra_photos']['tmp_name'][$i],
						'error' => $_FILES['extra_photos']['error'][$i],
						'size' => $_FILES['extra_photos']['size'][$i]
					);
					if ($photo['error'] != '0' || empty($photo['name'])) continue;
					$idAdded[] = saveExtraPhoto($photo, $id) ;
				}
			}
			//Сохранение созданных параметров
			//Массив с ID параметров
			$paramsIds = array();
			
			//Собираем id параметров с чекбоксов и селектов
			if (count($_POST['param_checkbox']) > 0 ) {
				foreach	($_POST['param_checkbox'] as $key => $value) {
					$paramsIds[] = $key;
				}
			}
			if (count($_POST['param_select']) > 0) {
				foreach	($_POST['param_select'] as $key => $value) {
					if ($value > 0) 
						$paramsIds[] = $value;
				}
			}
			
			//Массив для подготовки sql запроса
			$paramsSqlIds = array();

			//Готовим sql запрос
			foreach ($paramsIds as $pid) {
				$paramsSqlIds[] = "('{$id}', '{$pid}')";
			}
			//Добавляем параметры
			if (count($paramsSqlIds) > 0) {
				mysql_query("
					INSERT INTO
						`params_catalog_links` (`catalog_id`,`param_id`)
					VALUES 
					".implode(', ', $paramsSqlIds)
					);
			}
			
			//Добавление в доп. категории
			linkAdditionalCats($id, $_REQUEST['additional_cats']);
			//Сохранение тегов\подкатегорий
			linkCatalogAndTags($id, isset($_POST['tags']) ? array_keys($_POST['tags']) : array());
			
			header("Location: edit.php?id={$id}");
			die();
		} while (false);
	}
}