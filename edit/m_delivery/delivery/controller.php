<?php
$root = $_SERVER['DOCUMENT_ROOT'];
require_once "$root/func/classes/db.php";
$response = array();
class delivery{
	public static function getList() {
		$rows = array();
		$query = "SELECT * FROM `delivery_group` ORDER BY `sort` ASC";
		if ($result = DB::query($query)) {
		    while ($row = $result->fetch_assoc()) {
		    	//$row['name'] = utf($row['name']);
		    	$row['name'] = $row['name'];
				$rows[] = $row;
		    }
		    $result->free();
		}
		return $rows;
	}
	public static function getbyid($id){
		$query = "SELECT * FROM `delivery_group` WHERE `id`=$id";
		if ($result = DB::query($query)) {
		    if ($row = $result->fetch_assoc()) {
		    	//$row['name'] = utf($row['name']);
		    	$row['name'] = $row['name'];
			    $result->free();
		    }
		}
		return $row;
	}
}
class deliveryPrice {
	public static function getList($filter=null) {
		if (is_null($filter)) return false;
		$whereStatment = array();
		foreach ($filter as $key => $value) {
			$whereStatment[] = "`$key`=$value";
		}
		$whereStatment = implode(' AND ', $whereStatment);
		$rows	= array();
		$query	= "SELECT * FROM `delivery_price`  WHERE $whereStatment";
		if ($result = DB::query($query)) {
			while ($row = $result->fetch_assoc()) {
				$rows[] = $row;
			}
		}
		return $rows;
	}
	public static function getbyid($id){
		$rows = array();
		$query = "SELECT * FROM `delivery_price`  WHERE `delivery_group_id`=?";

		if ($result = DB::query($query)) {
		    while ($row = $result->fetch_assoc()) {
		    	//$row['name'] = utf($row['name']);
		    	$row['name'] = $row['name'];
				$rows[] = $row;
		    }
		    $result->free();
		}
		return $rows;
	}
}
extract($_GET);
extract($_POST);

switch($action) {
	case 'addDeliveryGroup':
		if (isset($name, $sort, $cat_gabarits_id)) {
			$sql = 'INSERT INTO `delivery_group`(`name`,`sort`,`cat_gabarits_id`) VALUES(?,?,?)';
			if ($stmt = DB::prepare($sql)) {
				$name = cp($name);
				$stmt->bind_param('sii', $name, $sort, $cat_gabarits_id);
				$stmt->execute();
				$response['success']['rowsAffected'] = $stmt->affected_rows;
				$response['success']['id'] = $stmt->insert_id;
				$stmt->close();
			}
		} else {
			if (!isset($name)) $response['error'][] = 'name is not set';
			if (!isset($sort)) $response['error'][] = 'sort is not set';
			if (!isset($cat_gabarits_id)) $response['error'][] = 'cat_gabarits_id is not set';
		}
		break;
	case 'getDeliveryGroup':
		if (isset($id)) {
			$ids = array();
			$rows = array();
			$sql = "SELECT * FROM `cat_gabarits`";
			if ($result = DB::query($sql)) {
				while ($row = $result->fetch_assoc()) {
					//$row['name'] = utf($row['name']);
					$row['name'] = $row['name'];
					$rows[] = $row;
				}
				$response['Gabarits'] = $rows;
			}
			
			$response['DG'] = delivery::getbyID($id);
			$sql = "SELECT `delivery_price_id` FROM `delivery_group_2_delivery_price` WHERE `delivery_group_id`=$id";
		
			if ($result = DB::query($sql)) {
				while ($row = $result->fetch_assoc()) {
					$ids[] = $row['delivery_price_id'];
				}
			}
			$rows = array();
			$row = DB::query($sql)->fetch_assoc();
			$dp_id = implode(',', $ids);
			$sql = "SELECT * FROM `delivery_price` WHERE `id` IN($dp_id)";
			//die();
			if ($result = DB::query($sql)) {
				while ($row = $result->fetch_assoc()) {
					$rows[] = $row;
				}
				$response['DPList'] = $rows;
			}
			
			
			//$response['DPList'] = deliveryPrice::getList(array('delivery_group_id'=>$id));
		} else $response['error'][] = 'id is not set';
		break;
	case 'deleteDeliveryGroup':
		if (isset($id)) {
			$sql = "DELETE from `delivery_group` WHERE `id`=?";
			if ($stmt = DB::prepare($sql)) {
				$stmt->bind_param('i', $id);
				$stmt->execute();
				$response['success']['rowsAffected'] = $stmt->affected_rows;
				$stmt->close();
			}
		} else {
			if (!isset($id)) $response['error'][] = 'id is not set';
		}
		break;
	case 'getDeliveryGroupList':
		$response['DGList'] = delivery::getList();
		
		$rows = array();
		$query = "SELECT * FROM `delivery_group_2_delivery_price`";
		if ($result = $mysqli->query($query)) {
		    while ($row = $result->fetch_assoc()) {
				$rows[$row['delivery_group_id']][] = $row['devivery_price_id'];
		    }
		    $result->free();
		}
		$response['DG2DPList'] = $rows;

		$rows = array();
		$query = "SELECT * FROM `delivery_price`";
		if ($result = $mysqli->query($query)) {
		    while ($row = $result->fetch_assoc()) {
				$rows[array_shift($row)] = $row;
		    }
		    $result->free();
		}
		$response['DPList'] = $rows;

		break;
	case 'getDeliveryGroupForMenu':
		$response['list'] = delivery::getList();
		break;
	case 'updateDeliveryGroup':

		if (isset($id, $name, $sort, $cat_gabarits_id)) {
			$sql = "UPDATE `delivery_group` SET `name`=?, `sort`=?, `cat_gabarits_id`=? WHERE `id`=?";
			if ($stmt = $mysqli->prepare($sql)) {
				$name = cp($name);
				$stmt->bind_param('siii', $name, $sort, $cat_gabarits_id, $id);
				$stmt->execute();
				$response['success']['rowsAffected'] = $stmt->affected_rows;
				$stmt->close();
			}
		} else {
			if (!isset($id))	$response['error'][] = 'id is not set';
			if (!isset($name))	$response['error'][] = 'name is not set';
			if (!isset($sort))	$response['error'][] = 'sort is not set';
			if (!isset($base_price)) $response['error'][] = 'base_price is not set';
		} 
		break;
	case 'updateDeliveryPrice':
		if (isset($id, $from, $price)) {
			$sql = "UPDATE `delivery_price` SET `from`=?, `price`=? WHERE `id`=?";
			if ($stmt = DB::prepare($sql)) {
				$stmt->bind_param('iii', $from, $price, $id);
				$stmt->execute();
				$response['success']['rowsAffected'] = $stmt->affected_rows;
				$stmt->close();
			}
		} else {
			if (!isset($id)) $response['error'][] = 'id is not set';
		}
		break;
	case 'addDeliveryPrice':
		if (isset($from, $price, $delivery_group_id)) {
			$sql = "INSERT INTO `delivery_price`(`from`, `price`) VALUES(?,?)";
			if ($stmt = DB::prepare($sql)) {
				$stmt->bind_param('ii', $from, $price);
				$stmt->execute();
				$response['success']['rowsAffected'] = $stmt->affected_rows;
				$delivery_price_id = $stmt->insert_id;
				$stmt->close();
				$sql = "INSERT INTO `delivery_group_2_delivery_price`(`delivery_group_id`, `delivery_price_id`) VALUES(?,?) ON DUPLICATE KEY UPDATE `delivery_price_id`=?";
				if ($stmt = DB::prepare($sql)) {
					$stmt->bind_param('iii', $delivery_group_id, $delivery_price_id,  $delivery_price_id);
					$stmt->execute();
					$response['success']['rowsAffected'] = $stmt->affected_rows;
					$id = $stmt->insert_id;
					$stmt->close();
				}
			}

		} else {
			if (!isset($id)) $response['error'][] = 'id is not set';
		}
		break;
	case 'deleteDeliveryPrice':
		if (isset($id)) {
			$sql = "DELETE from `delivery_price` WHERE `id`=?";
			if ($stmt = DB::prepare($sql)) {
				$stmt->bind_param('i', $id);
				$stmt->execute();
				$response['success']['rowsAffected'] = $stmt->affected_rows;
				$stmt->close();
			}
		} else {
			if (!isset($id)) $response['error'][] = 'id is not set';
		}
		break;

	case 'addGabaritGroup':
		if (isset($name)) {
			$sql = 'INSERT INTO `cat_gabarits`(`name`) VALUES(?)';
			if ($stmt = DB::prepare($sql)) {
				$name = cp($name);
				$stmt->bind_param('s', $name);
				$stmt->execute();
				$response['success']['rowsAffected'] = $stmt->affected_rows;
				$response['success']['id'] = $stmt->insert_id;
				$stmt->close();
			}
		} else {
			if (!isset($name)) $response['error'][] = 'name is not set';
		}
		break;
	case 'getGabaritGroup':
		if (isset($id)) {
			$query = "SELECT * FROM `cat_gabarits` WHERE `id`=$id";
			if ($result = DB::query($query))
			{
			    if ($row = $result->fetch_assoc())
			    {
			    	//$row['name'] = utf($row['name']);
			    	$row['name'] = $row['name'];
			    	$response['DG'] = $row;
				}
			}

			$ids = array();
			$sql = "SELECT `delivery_price_id` FROM `cat_gabarits_2_delivery_price` WHERE `cat_gabarits_id`=$id";
			if ($result = DB::query($sql))
			{
				while ($row = $result->fetch_assoc())
					$ids[] = $row['delivery_price_id'];
				if (count($ids))
				{
					$rows = array();
					if ($result = DB::query($sql))
					{
						if ($row = $result->fetch_assoc())
						{
							$dp_id = implode(',', $ids);
							$sql = "SELECT * FROM `delivery_price` WHERE `id` IN($dp_id)";
							if ($result = DB::query($sql))
							{
								while ($row = $result->fetch_assoc())
									$rows[] = $row;
								$response['DPList'] = $rows;
							}
						}
					}
				}
			}
			
			$ids = array();
			$sql = "SELECT `delivery_price_id` FROM `cat_gabarits_discount_2_delivery_price` WHERE `cat_gabarits_id`=$id";
			if ($result = DB::query($sql))
			{
				while ($row = $result->fetch_assoc())
					$ids[] = $row['delivery_price_id'];
				if (count($ids))
				{
					$rows = array();
					if ($result = DB::query($sql))
					{
						if ($row = $result->fetch_assoc())
						{
							$dp_id = implode(',', $ids);
							$sql = "SELECT * FROM `delivery_price` WHERE `id` IN($dp_id)";
							if ($result = DB::query($sql))
							{
								while ($row = $result->fetch_assoc())
									$rows[] = $row;
								$response['DiscountsList'] = $rows;
							}
						}
					}
				}
			}
		} else $response['error'][] = 'id is not set';
		break;
	case 'updateGabaritGroup':
		if (isset($id, $name, $sort)) {
			$sql = "UPDATE `cat_gabarits` SET `name`=?, `sort`=? WHERE `id`=?";
			if ($stmt = $mysqli->prepare($sql)) {
				$name = cp($name);
				$stmt->bind_param('sii', $name, $sort, $id);
				$stmt->execute();
				$response['success']['rowsAffected'] = $stmt->affected_rows;
				$stmt->close();
			}
		} else {
			if (!isset($id))	$response['error'][] = 'id is not set';
			if (!isset($name))	$response['error'][] = 'name is not set';
			if (!isset($sort))	$response['error'][] = 'sort is not set';
		} 
		break;

	case 'updateDeliveryGroupGabarit':
		if (isset($id, $name, $sort, $base_price)) {
			$sql = "UPDATE `delivery_group` SET `name`=?, `sort`=?, `base_price`=? WHERE `id`=?";
			if ($stmt = $mysqli->prepare($sql)) {
				$name = cp($name);
				$stmt->bind_param('siii', $name, $sort, $base_price, $id);
				$stmt->execute();
				$response['success']['rowsAffected'] = $stmt->affected_rows;
				$stmt->close();
			}
		} else {
			if (!isset($id))	$response['error'][] = 'id is not set';
			if (!isset($name))	$response['error'][] = 'name is not set';
			if (!isset($sort))	$response['error'][] = 'sort is not set';
			if (!isset($base_price)) $response['error'][] = 'base_price is not set';
		} 	
		break;
	case 'addDeliveryPriceGabarits':
		if (isset($from, $price, $cat_gabarits_id)) {
			$sql = "INSERT INTO `delivery_price`(`from`, `price`) VALUES(?,?)";
			if ($stmt = DB::prepare($sql)) {
				$stmt->bind_param('ii', $from, $price);
				$stmt->execute();
				$response['success']['rowsAffected'] = $stmt->affected_rows;
				$delivery_price_id = $stmt->insert_id;
				$stmt->close();
				$sql = "INSERT INTO `cat_gabarits_2_delivery_price` (`cat_gabarits_id`, `delivery_price_id`) VALUES(?,?) ON DUPLICATE KEY UPDATE `delivery_price_id`=?";
				if ($stmt = DB::prepare($sql)) {
					$stmt->bind_param('iii', $cat_gabarits_id, $delivery_price_id,  $delivery_price_id);
					$stmt->execute();
					$response['success']['rowsAffected'] = $stmt->affected_rows;
					$id = $stmt->insert_id;
					$stmt->close();
				}
			}

		} else {
			if (!isset($id)) $response['error'][] = 'id is not set';
		}
		break;
	case 'addDeliveryPriceDiscount':
		if (isset($from, $price, $cat_gabarits_id)) {
			$sql = "INSERT INTO `delivery_price`(`from`, `price`) VALUES(?,?)";
			if ($stmt = DB::prepare($sql)) {
				$stmt->bind_param('ii', $from, $price);
				$stmt->execute();
				$response['success']['rowsAffected'] = $stmt->affected_rows;
				$delivery_price_id = $stmt->insert_id;
				$stmt->close();
				$sql = "INSERT INTO `cat_gabarits_discount_2_delivery_price` (`cat_gabarits_id`, `delivery_price_id`) VALUES(?,?) ON DUPLICATE KEY UPDATE `delivery_price_id`=?";
				if ($stmt = DB::prepare($sql)) {
					$stmt->bind_param('iii', $cat_gabarits_id, $delivery_price_id,  $delivery_price_id);
					$stmt->execute();
					$response['success']['rowsAffected'] = $stmt->affected_rows;
					$id = $stmt->insert_id;
					$stmt->close();
				}
			}

		} else {
			if (!isset($id)) $response['error'][] = 'id is not set';
		}
		break;
	
	case 'deleteGabaritGroup':
		if (isset($id)) {
			$sql = "DELETE from `cat_gabarits` WHERE `id`=?";
			if ($stmt = DB::prepare($sql)) {
				$stmt->bind_param('i', $id);
				$stmt->execute();
				$response['success']['rowsAffected'] = $stmt->affected_rows;
				$stmt->close();
			}
		} else {
			if (!isset($id)) $response['error'][] = 'id is not set';
		}
		break;

	case 'add':
		if (isset($newRow)) { // INSERT
			$columns = '`'.implode('`,`', array_keys($fields)).'`'; // `field_name`,
			$values = array_fill(0, count($fields)+1, '?');
			$values = implode(',', $values);
			$sql = "INSERT INTO `$table_name` ($columns, `id`) VALUES($values)";
		} else { // UPDATE
			$sql = "UPDATE `$table_name` SET ";
			$set = array();
			foreach ($fields as $name => $value) $set[] = "`$name`=?";
			$sql .= implode(',', $set);
			$sql .= ' WHERE `id`=?';
		}

		if ($stmt = $mysqli->prepare($sql)) {
			$fields[] = $id;
			$types = str_repeat('i', count($fields));
			$refArr[] = &$types;
			foreach ($fields as &$value) $refArr[] = $value;
			$ref    = new ReflectionClass('mysqli_stmt'); 
			$method = $ref->getMethod("bind_param"); 
			$method->invokeArgs($stmt, $refArr); 
			$result = $stmt->execute();

			$response = array('result'=>$result);
			if ($newRow) $response['removeNewRow'] = $id;
			
		}	
		break;
	case 'updateCatalog2DeliveryGroup':
		if ($delivery_group_id && $category_id) {
			$sql = "INSERT INTO `category_2_delivery_group`(`delivery_group_id`, `category_id`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `delivery_group_id`=?";
			if ($stmt = DB::prepare($sql)) {
				$stmt->bind_param('iii', $delivery_group_id, $category_id, $delivery_group_id);
				$stmt->execute();
				$response['success']['rowsAffected'] = $stmt->affected_rows;
				$stmt->close();
			}
		} 
		break;
	case 'meta':
		if ($table) {
			$rows = array();
			$sql = "SHOW FULL FIELDS FROM `$table`";
			if ($result = DB::query($sql)) {
			    while ($row = $result->fetch_assoc()) {
					$rows[] = array(
						'name'		=> $row['Field'],
						'required'	=> $row['Null'] == 'NO',
						'type'		=> type($row),
						//'title'		=> utf($row['Comment'])
						'title'		=> $row['Comment']

					);
			    }
			    $result->free();
			    $rows[] = array(
			    	'name'	=> 'action',
			    	'type'	=> 'hidden',
			    );
			    $rows[] = array(
			    	'name'	=> 'table',
			    	'type'	=> 'hidden'
			    );
			}
			$response['columns'] = $rows;

		} else $response['error'][] = 'table is not set';
		break;
	case 'update':
		if ($table) {
			$rows = array();
			$sql = "SHOW FIELDS FROM `$table`";
			if ($result = DB::query($sql)) {
			    while ($row = $result->fetch_assoc()) $rows[] = $row;
				if ($rows) {
					$refArr = array();
					$types = array();
					$values = array();
					foreach ($rows as $row) {
						$field = $$row['Field'];
						if ($field){
							if ($row['Field'] != 'id') {
								$set[] = "`$field`=?";
								$bind_type = bind_type($row);
								$types[] = $bind_type;
								if ($bind_type == 's') $field = cp($field);
								$values[] = $field;
							}
						} else $response['error'] = $row['Field'] . ' is not set';
					}
					if (!$response['error']) {
						$setStatment = implode(',', $set);
						$sql = "UPDATE `$table` SET $setStatment WHERE `id`=?";
						$response['sql'] = $sql;
						$response['values'] = $values;
						if ($stmt = DB::prepare($sql)) {
							$refArr[] = &$types;
							foreach ($values as &$value) $refArr[] = $value;
							$ref    = new ReflectionClass('mysqli_stmt'); 
							$method = $ref->getMethod("bind_param"); 
							$method->invokeArgs($stmt, $refArr); 
							$result = $stmt->execute();
							$response['result'] = $result;
						}
					}
				}
			}

		} else $response['error'][] = 'table is not set';
		break;
	case 'insert':
		if ($table) {
			$rows = array();
			$sql = "SHOW FIELDS FROM `$table`";
			if ($result = DB::query($sql)) {
			    while ($row = $result->fetch_assoc()) $rows[] = $row;
				if ($rows) {
					$types = array();
					$values = array();
					foreach ($rows as $row) {
						if ($row['Field'] == 'id') continue;
						$field = $$row['Field'];
						if ($field){
							$into[] = "`{$row['Field']}`";
							$bind_type = bind_type($row);
							$types[] = $bind_type;
							if ($bind_type == 's') $field = cp($field);
							$values[] = $field;
						} else $response['error'] = $row['Field'] . ' is not set';
					}
					if (@!$response['error']) {
						$intoStatment = implode(',', $into);
						$valueStatement = array_fill(0, count($into), '?');
						$valueStatement = implode(',', $valueStatement);
						$sql = "INSERT INTO `$table`($intoStatment) VALUES($valueStatement)";
						if ($stmt = DB::prepare($sql)) {
							$refArr = array();
							$types = implode('', $types);
							$refArr[] = &$types;
							foreach ($values as &$value) $refArr[] = $value;
							$ref    = new ReflectionClass('mysqli_stmt'); 
							$method = $ref->getMethod("bind_param"); 
							$method->invokeArgs($stmt, $refArr); 
							$stmt->execute();
							$response['success']['rowsAffected'] = $stmt->affected_rows;
							$response['success']['newRow'] = $stmt->insert_id;
							$stmt->close();
						}
					}
				}
			}
		} else $response['error'][] = 'table is not set';
		break;
	case 'delete':
		if ($table) {
			if (isset($id)) {
				$sql = "DELETE from `$table` WHERE `id`=?";
				if ($stmt = DB::prepare($sql)) {
					$stmt->bind_param('i', $id);
					$stmt->execute();
					$response['success']['rowsAffected'] = $stmt->affected_rows;
					$stmt->close();
				}
			} else {
				if (!isset($id)) $response['error'][] = 'id is not set';
			}
		} else $response['error'][] = 'table is not set';
		break;
	case 'select':
		if ($table) {
			$rows = array();
			$sql = "SELECT * FROM `$table`";
			if (isset($id) && $id) {
				if (is_array($id)) {
					$id = implode(',', $id);
					$sql .= " WHERE `id` IN ($id)";
				} else {
					$id = (int) $id;
					if ($id > 0) {
						$sql .= " WHERE `id`=$id";
					}
				}
			} else if (isset($where) && $where) {
				$sql .= " WHERE $where";
			}
			if ($result = DB::query($sql)) {
				while($row = $result->fetch_assoc()) {
					if (@$row['name'])
						// $row['name'] = utf($row['name']);
						$row['name'] = $row['name'];
					$rows[] = $row;
				}
				$response['list'] = $rows;
			}
		} else $response['error'][] = 'table is not set';
		break;
	default:
		break;
}


echo json_encode($response);
?>