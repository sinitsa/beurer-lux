<?php 
include ('../connect.php'); 
include ('../../func/core.php'); 

$cssOl = true;
include ('../up.php');
?>

	<script type="text/javascript" src="/edit/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/edit/ckfinder/ckfinder.js"></script>
    
<script type="text/javascript">   
function set_value()
{

    var input = document.getElementById('title');
    var output1 = document.getElementById('seo_title');
    var output2 = document.getElementById('seo_des');
    var output3 = document.getElementById('seo_keys');
	
    output1.value = input.value;
    output2.value = input.value;
    output3.value = input.value;
}
</script>    
    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="ol">
        <tr>
          <td width="70%" align="left" valign="top">

<form  method="post" action="/edit/m_videoblog/sql_add.php">
<table width="90%" border="0" align="center" >
            <tr>
              <td width="77%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                  
                    <td valign="middle" class="big_menu"><h2>Добавить запись в блоге</h2></td>
                  </tr>
                </table></td>
          </tr>
            <tr>
              <td></td>
          </tr>
            <tr>
              <td>
			  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td align="left" valign="top" class="span6">
                <input name="vi_title" type="text" id="title" class="p_name" value="" size="100" placeholder="Заголовок" onchange="set_value()"/>
                <div id="doubleAlert">
                </div>
				</td>
					<td align="left" valign="top"></td>
				  </tr>
				</table>
			  
				
			
			  </td>
          </tr>
            <tr>
              <td></td>
          </tr>
            <tr>
              <td><textarea name="vi_text" cols="100" rows="10" id="des"></textarea>
			  
			<script type="text/javascript">
                 var editor = CKEDITOR.replace( 'des' , {
					width: 800,
					height: 500
					});
                CKFinder.setupCKEditor( editor, '/edit/ckfinder/' ) ;
            </script> 
			
			</td>
          </tr>
            <tr>
              <td><table width="600" border="0" align="left" cellpadding="2" cellspacing="0" class="txt_small">
			  <tr>
                  <td bgcolor="#f7f7f7">Ссылка на видео </td>
				  <td bgcolor="#f7f7f7" style="whitespace: nowrap;">(http://www.youtube.com/watch?v=<b style="color: red;">JcJGet-Uuyo</b>)</td>
                </tr>
                <tr>
                  <td bgcolor="#f7f7f7">&lt;title&gt;
                    &nbsp;</td>
                  <td bgcolor="#f7f7f7"><input name="vi_link" type="text" id="seo_title" size="100"  class="txt_small p_name"/></td>
                </tr>
              </table></td>
            </tr>
			<tr><td>&nbsp;</td></tr>
            <tr>
              <td><table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>				  
				  <button  type="submit" name="Submit" class="btn btn-large" /><i class="icon-plus"></i> Добавить</button>
</td>
                  <td align="right"></td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td align="center" valign="top">&nbsp;</td>
              </tr>
          </table>
      </form></td>
          <td width="30%" align="left" valign="top">
		  
		  <table width="90%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="big_menu">Все записи</td>
            </tr>
            <tr>
              <td><br>
			  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table">
				<?php
				$pages = getVideoblogPages();
				foreach ($pages as $page) { ?>
					<tr>
						<td align="left"><a href="/edit/m_videoblog/edit.php?id=<?=$page['vi_id']?>" class="small_menu"><?=$page['vi_title']?></a></td>
					</tr>
				<? } ?>              

              </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
		  </td>
        </tr>
      </table>
<script>
    $('#title').change( function() {
      var tryTitle = $('#title').val();
      $.post('../../double_check.php', { item_type: 'page', title: tryTitle }, function(data) {
        $('#doubleAlert').html(data);
      }, 'text');
    });
</script>
<?php include ("../down.php"); ?>