<?php 
include ('../connect.php'); 
include ('../../func/core.php');
$id = $_GET['id'];
$staticPage = getVideoblogPage($id);
$cssOl = true;
include ('../up.php');

//print_r($staticPage);
?>
	<script type="text/javascript" src="/edit/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/edit/ckfinder/ckfinder.js"></script>
      
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="ol">
        <tr>
          <td width="70%" align="left" valign="top">

<form  method="post" action="sql_edit.php?page_id=<?php echo $id; ?>">
<table width="90%" border="0" align="center" >
            <tr>
              <td width="77%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                  
                    <td valign="middle" class="big_menu"><h2>Редактировать запись в блоге</h2></td>
                  </tr>
                </table></td>
          </tr>
            <tr>
              <td></td>
          </tr>
            <tr>
              <td>
			  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td align="left" valign="top" class="span6"><input name="vi_title" class="span6 p_name" type="text" id="title" value="<?php echo $staticPage['vi_title']; ?>" size="100" /> </td>
					<td align="left" valign="top"><!--a href="<?php echo getTemplateLink($staticPage, 'page'); ?>" target="_blank" class="btn btn-info white"><i class="icon-play icon-white"></i> Посмотреть на сайте</a--></td>
				  </tr>
				</table>
			  
			  </td>
          </tr>
            <tr>
              <td></td>
          </tr>
            <tr>
              <td><textarea name="vi_text" cols="100" rows="10" id="des"><?php echo $staticPage['vi_text']; ?></textarea>
			  
			<script type="text/javascript">
                 var editor = CKEDITOR.replace( 'des' , {
					width: 800,
					height: 500
					});
                CKFinder.setupCKEditor( editor, '/edit/ckfinder/' ) ;
            </script> 
			
			</td>
          </tr>
            <tr>
              <td><table width="600" border="0" align="left" cellpadding="2" cellspacing="0" class="txt_small">
                <tr>
                  <td bgcolor="#f7f7f7">Ссылка на видео </td>
				  <td bgcolor="#f7f7f7" style="whitespace: nowrap;">(http://www.youtube.com/watch?v=<b style="color: red;">JcJGet-Uuyo</b>)</td>
                </tr>
                <tr>
                  <td colspan="2" bgcolor="#f7f7f7"><input name="vi_link" type="text" id="chpu" value="<?php echo $staticPage['vi_link']; ?>" size="100"  class="txt_small p_name" /></td>
                </tr>
              </table></td>
            </tr>
			
			<tr><td>&nbsp;</td></tr>
            <tr>
              <td><table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>				  
						<button  type="submit" name="Submit" class="btn btn-large" /><i class="icon-ok"></i> Изменить</button>
					</td>
                  <td align="right"></td>
                </tr>
              </table>
			  </td>
            </tr>
            <tr>
              <td align="left" valign="top">&nbsp;</td>
              </tr>
			  <tr>
				<td><a href="delete.php?id=<?=$id;?>" class="btn">Удалить</a></td>
				</tr>
          </table>
      </form></td>
          <td width="30%" align="left" valign="top">
		  
		  <table width="90%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="big_menu">Все записи</td>
            </tr>
            <tr>
              <td><br>
			  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table">
				<?php
				$staticPages = getVideoblogPages();
				foreach ($staticPages as $staticPage) {
					if ($staticPage['vi_id'] == $id) {
						echo ('
						<tr>
							<td align="left" class="small_menu"><strong>'.$staticPage['vi_title'].'</strong></td>
						</tr>
						');
					}
					else {
						echo ('
						<tr>
							<td align="left"><a href="/edit/m_videoblog/edit.php?id='.$staticPage['vi_id'].'" class="small_menu">'.$staticPage['vi_title'].'</a></td>
						</tr>
						');
					}
				}; 
				?>              
					<tr>
						<td align="left" class="small_menu">
						<a href="/edit/m_videoblog/add.php"  class="btn btn-success white"><i class="icon-plus icon-white"></i> Добавить</a>
						</td>
					</tr>
              </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
		  </td>
        </tr>
      </table>
      <br>
<?php include ("../down.php"); ?>