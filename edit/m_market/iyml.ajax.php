<?php 
include ("iyml.class.php"); 
require_once('../../connect.php');
?>

<?



class ajaxHandler{
	
	function process(){
		$str = $_REQUEST['data'];
		$str = str_replace('\\', '', $str);
		$str = str_replace('\"', '', $str);
		$data = json_decode($str);

		$this->{$data->method}($data->arguments);
	}
	
	function save($args){
		$iy = new iYML;
		foreach($args->params as $paramsName=>$paramValue){
			$iy->{$paramsName} = $paramValue;
		}
		$iy->saveParams();
	}
	
	function generate($args){
		$iy = new iYML;

		$answer = Array();
		$answer["lastUpdate"] = $iy->buildYMLFile()->lastUpdate;
		$answer["ymlFilePath"] = $iy->ymlFilePath;
		
		echo json_encode($answer);
	}
}

$a = new ajaxHandler();
$a->process();

?>