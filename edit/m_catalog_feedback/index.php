<?
include('../connect.php');
include('../../func/core.php');

$feeds = getProductsFeedback();

$cssOl = true;
include('../up.php');
?>
<style>

</style>
<script src="/js/jquery.form.js" type="text/javascript"></script>
<script>
/* JS ДЛЯ коментов к товарам */
$(function () {
	$('.feed .answer-button').bind('click', function(){
		var feed = $(this).closest('.feed');
		feed.find('.answer-form').slideDown();
	});
	$('.feed input[name="answer"]').bind('click', function () {
		var feed = $(this).closest('.feed');
		var feedId = feed.data('id');
		var loading = feed.find('.loading');
		
		var answer = feed.find('textarea');
		var sendButton = $(this);
		
		$.ajax({
				url: '../m_catalog_feedback/ajax.php?method=add_answer',
				type : 'POST',
				dataType : 'json',
				data : {
					"id" : feedId,
					"answer" : answer.val()
					},
				beforeSend : function () {
					loading.show();
					answer.attr('disabled','disabled');
					sendButton.attr('disabled','disabled');
				},
				success : function (data, textStatus, jqXHR) {
					feed.find('.answer').text(answer.val());
					feed.find('.answer-form').slideUp();
				},
				complete : function () {
					loading.hide();
					answer.removeAttr('disabled');
					sendButton.removeAttr('disabled');
				}
		});
		
	});
	
	$('.feed .delete').bind('click', function (){
		var feed = $(this).closest('.feed');
		var button = $(this);
		var feedId  = feed.data('id');
		
		$.ajax({
				url: '../m_catalog_feedback/ajax.php?method=delete_feed',
				type : 'POST',
				dataType : 'json',
				data : { "id" : feedId },
				beforeSend : function () {
					button.attr('disabled','disabled');
					feed.css({"opacity": .5});
				},
				success : function (data, textStatus, jqXHR) {
					feed.fadeOut('fast', function () {
						feed.remove();
					});
				},
				complete : function () {
					button.removeAttr('disabled');
				}
		});
	});
	$('.feed .confirm').bind('click', function (){
		var feed = $(this).closest('.feed');
		var button = $(this);
		var feedId  = feed.data('id');
		
		var confirm = button.attr('disabled') ? 0 : 1; //button.is(":disabled") ? 0 : 1;
		$.ajax({
				url: '../m_catalog_feedback/ajax.php?method=confirm_feed',
				type : 'POST',
				dataType : 'json',
				data : { "id" : feedId, "confirm" : confirm},
				beforeSend : function () {
					feed.css({"opacity": .5});
				},
				success : function (data, textStatus, jqXHR) {
					if (confirm == 1) {
						button.attr('disabled','disabled');
					} else {
						button.removeAttr('disabled');
					}
				},
				complete : function () {
					feed.css({"opacity": 1});
				}
		});
	});
});
</script>
<table width="90%" border="0" align="center" class="txt ol">
	<tr>
		<td width="10">&nbsp;</td>
		<td>
			<h4>Отзывы к товарам</h4>
			<br />
			<div id="feedback-field">
			<? foreach ($feeds as $feed) { ?>
				<div class="feed" data-id="<?=$feed['id'];?>" id="feed<?=$feed['id']?>">
					<div class="product-image">
						<img src="<?=getImageWebPath('product_small').$feed['catalog_id'];?>.jpg" />
					</div>
					<div class="feed-other">
						<div class="header">
							<div class="manage-buttons">
								<a class="confirm btn btn-success" <?=($feed['confirm']==1?'disabled="disabled"':'')?>><i class="icon-ok"></i></a>&nbsp;
								<a class="delete btn btn-danger"><i class="icon-trash icon-white"></i></a>
							</div>
							<div class="product-name"><a href="../m_catalog/edit.php?id=<?=$feed['catalog_id']?>"><?=$feed['catalog_title'];?></a></div>
							<div><span class="name"><?=$feed['name']?></span>&nbsp;<span class="date"><?=date('d.m.Y H:i', $feed['date'])?></span></div>
							<div><span class="email"><?=$feed['email']?></span></div>
						</div>
						<div class="comment"><?=$feed['comment']?></div>
						<div class="answer-field">
							<div class="answer"><?=$feed['answer']?></div>
							<a class="answer-button btn"><i class="icon-comment"></i>&nbsp;Ответить</a> 
							<div class="answer-form">
								<textarea name=""><?=$feed['answer'];?></textarea>
								<br />
								<input type="button" name="answer" value="Ответить" /> <span class="loading"><img src="/img/ajax_loading.gif" alt="Загрузка" /></span>
							</div>
						</div>
					</div>
				</div>
			<? } ?>
			</div>
		</td>
	</tr>
</table>
<?
include('../down.php');