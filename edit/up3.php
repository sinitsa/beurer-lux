<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Панель администрирования</title>
<link rel="stylesheet" href="/edit/plugins/dropdown/dropdown.css" type="text/css" />

<link rel="stylesheet" href="/edit/plugins/tooltip/jquery.tooltip.css" type="text/css" />
<script type="text/javascript" src="/edit/js/dropdown.js"></script>
<script src="http://yandex.st/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript" src="/edit/plugins/tablesort/jquery.tablednd.js"></script>

<style type="text/css">
.myDragClass {
	background-color: #C7DCE0;
}
</style>

<link href="/edit/css/main.css" rel="stylesheet" type="text/css" />
</head>

<body onload="initialize()" onunload="GUnload()">
<table width="100%"  cellpadding="0" cellspacing="0"  bgcolor="#FFFFFF">
  <tr>
    <td><span class="style2"> </span>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="30%"><img border="0" align="absbottom" src="/edit/logo.jpg" style = "padding-top:20px;padding-left:30px"></td>
          <td width="70%">
		  		 <div class="floatleft">
		   <div class="ddheader" id="zero-ddheader" onmouseover="ddMenu('zero',1)" onmouseout="ddMenu('zero',-1)"> 
		   <a href="/edit/">
			На главную
		   </a>
		   </div>
            <div class="ddcontent" id="zero-ddcontent" onmouseover="cancelHide('zero')" onmouseout="ddMenu('zero',-1)">
            </div>
          </div>		
		  
		  <div class="floatleft">
            <div class="ddheader" id="one-ddheader" onmouseover="ddMenu('one',1)" onmouseout="ddMenu('one',-1)"> Страницы</div>
            <div class="ddcontent" id="one-ddcontent" onmouseover="cancelHide('one')" onmouseout="ddMenu('one',-1)">
              <div class="ddinner">
                <ul>
                  <li class="underline"><a href="/edit/m_pages/edit.php?chpu=dostavka">Доставка</a></li>
                  <li class="underline"><a href="/edit/m_pages/edit.php?chpu=contacts">Контакты</a></li>		
                </ul>
              </div>
            </div>
          </div>
           
            <div class="floatleft">
              <div class="ddheader" id="six-ddheader" onmouseover="ddMenu('six',1)" onmouseout="ddMenu('six',-1)">Каталог</div>
              <div class="ddcontent" id="six-ddcontent" onmouseover="cancelHide('six')" onmouseout="ddMenu('six',-1)">
                <div class="ddinner">
                  <ul>
                  <li class="underline"><a href="/edit/m_cat/add.php">Добавить категорию</a></li>
                  <li class="underline"><a href="/edit/m_cat/index.php">Изменить категорию</a></li>		
   		           <li class="underline"><a href="/edit/m_catalog/add.php">Добавить товар</a></li>
                  <li class="underline"><a href="/edit/m_catalog/index.php">Изменить товар</a></li>
                  </ul>
                </div>
              </div>
            </div>
           
            <div class="floatleft">
              <div class="ddheader" id="vosem-ddheader" onmouseover="ddMenu('vosem',1)" onmouseout="ddMenu('vosem',-1)">Настройки</div>
              <div class="ddcontent" id="vosem-ddcontent" onmouseover="cancelHide('vosem')" onmouseout="ddMenu('vosem',-1)">
                <div class="ddinner">
                  <ul>
                    <li class="underline"><a href="/edit/m_config/edit.php" >Изменить</a></li>
                  </ul>
                </div>
              </div>
            </div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>