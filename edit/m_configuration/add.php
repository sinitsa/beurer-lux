<?php
die('Deprecated');
include ("../connect.php");
include("../../func/core.php");

if (isset($_POST['add']) && $_POST['add']) {
	if (strlen($_POST['p_name']) > 2 && strlen($_POST['p_key']) >= 2) {
		if (Config::add($_POST['p_key'], $_POST['p_value'], $_POST['p_name'], $_POST['p_desc'])) {
			header("Location: /edit/m_configuration/");
			die();
		}
	}
}
include ("../up.php"); 
?>

 <table width="90%" border="0" align="center" class="txt">
            <tr>
              <td width="10">&nbsp;</td>
              <td>
				  <h3>Конфигурация</h3>
				  <h4>Добавление значения</h4>
				  <form action="" method="post">
					  <div>Название*:</div>
					  <div><input type="text" name="p_name" value="<?=$_POST['p_name'];?>" /></div>
					  <div>Описание</div>
					  <div><textarea name="p_desc" cols="40" rows="2" ><?=$_POST['p_desc'];?></textarea></div>
					  <div>Ключ*:</div>
					  <div><input type="text" name="p_key" value="<?=$_POST['p_key'];?>" /></div>
					  <div>Значение</div>
					  <div><input type="text" name="p_value" value="<?=$_POST['p_value'];?>" /></div>
					  <div>&nbsp;</div>
					  <div><input type="submit" name="add" class="btn" value="Добавить" /></div>
				  </form>
				  <div><a href="index.php" class="btn btn-small"><i class="icon-chevron-left"></i> Назад к списку</a><div>
			  </td>
        </tr>
          </table>
<?php include ("../down.php");	?>