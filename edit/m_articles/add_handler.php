<?
$errors = array();

if ($catId >= 0) {
	$data = array();
	$data['cat_id'] = $catId;
	$data['chpu'] = mysql_real_escape_string(strtolower(translitIt($_REQUEST['seo_title'])));
	$data['title'] = mysql_real_escape_string($_REQUEST['title']);
	$data['seo_title'] = mysql_real_escape_string($_REQUEST['seo_title']);
	$data['text'] = mysql_real_escape_string($_REQUEST['text']);
	$data['show_in_list'] = isset($_REQUEST['show_in_list']) ? 1 : 0 ;
	$data['rang'] = 0;

	if (strlen($data['title']) < 2) $errors[] = 'Не введен заголовок';
	
	if (count($errors) <= 0) {
		mysql_query("
			INSERT INTO
				`articles`
			SET
				".getSetString($data)
		);
		if ($catId == 0)
			redirect('/edit/m_mcat/');
		else
			redirect('/edit/m_catalog/list.php?id=' . $catId);
	}
} else {
	$errors[] = 'Нет идентификатора категории';
}