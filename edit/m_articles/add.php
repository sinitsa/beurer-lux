<?php
include ("../connect.php");
include ("../../func/core.php");

$catId = isset($_GET['cat_id']) && is_numeric($_GET['cat_id']) ? $_GET['cat_id'] : 0 ;

if ($catId >= 0 && isset($_POST['add'])) {
	include('add_handler.php');
}
$cssOl = true;
include ("../up.php");
?>
<script type="text/javascript" src="/edit/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/edit/ckfinder/ckfinder.js"></script>
 <table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td>
			<div><a href="/edit/m_catalog/list.php?id=<?=$catId;?>">Назад к каталогу</a></div>
			<?php if (count($errors) > 0) {
				foreach($errors as $error) {
					echo '<div class="error">'.$error.'</div>';
				}
			}
			?>
			<form action="" method="post">
				<div class="txtbname">Заголовок статьи (вывод в списке):</div>
				<div><input class="span4 p_name" type="text" name="title" value="<?=slashes($_POST['title']);?>" /></div>
				<div><input type="hidden" name="cat_id" value="<?=$_REQUEST['cat_id'];?>" /></div>
				<div class="txtbname">СЕО title:</div>
				<div><input class="span4 p_name" type="text" name="seo_title" value="<?=slashes($_POST['seo_title']);?>" /></div>
				<div class="txtbname">Текст:</div>
				<div>
					<textarea name="text" cols="70" rows="7" style="width: 400px;"><?php echo schars($_POST['text']); ?></textarea>
					<script type="text/javascript">
						var editor = CKEDITOR.replace( 'text' );
						CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
					</script>
				</div>
				<?php
					if (isset($_POST['add'])) {
						$checked = isset($_POST['show_in_list']);
					} else {
						$checked = true;
					}
				?>
				<div>Выводить в списке статей <input type="checkbox" name="show_in_list" <?=( $checked ? 'checked="checked"' : '')?> /></div>
				<div><input type="submit" name="add" value="Добавить" /></div>
			</form>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>