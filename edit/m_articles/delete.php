<?php
include ("../connect.php");
include ("../../func/core.php");

$articleId = isset($_GET['id']) && is_numeric($_GET['id']) ? $_GET['id'] : 0 ;
$catId = isset($_GET['cat_id']) && is_numeric($_GET['cat_id']) ? $_GET['cat_id'] : 0 ;

if ($articleId > 0 && $catId >= 0 && $_GET['confirm'] == 'true') {
	deleteArticle($articleId);
	if ($catId == 0 )
		redirect('/edit/m_mcat/');
	else
		redirect('/edit/m_catalog/list.php?id=' . $catId);
}

if ($articleId) {
	$article = getArticle($articleId);
}

$cssOl = true;
include ("../up.php");
?>
<script type="text/javascript" src="/edit/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/edit/ckfinder/ckfinder.js"></script>
 <table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td>
			<div>Вы действительно хотите удалить статью "<strong><?=$article['title']?></strong>"?</div>
			<div>&nbsp;</div>
			<a class="btn" href="?id=<?=$articleId?>&cat_id=<?=$catId?>&confirm=true">Удалить безвозвратно</a> <a class="btn" href="/edit/m_catalog/list.php?id=<?=$catId?>">Отмена</a>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>