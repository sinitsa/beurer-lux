<?php
include ("../connect.php");
include ("../../func/core.php");

$articleId = isset($_GET['id']) && is_numeric($_GET['id']) ? $_GET['id'] : 0 ;

if (isset($_POST['edit'])) {
	include('edit_handler.php');
}
if ($articleId > 0) {
	$article = getArticle($articleId);	
}
$cssOl = true;
include ("../up.php");
?>
<script type="text/javascript" src="/edit/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/edit/ckfinder/ckfinder.js"></script>
 <table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td>
			<div><a href="/edit/m_catalog/list.php?id=<?=$article['cat_id'];?>">Назад к каталогу</a></div>
			<?php if (count($errors) > 0) {
				foreach($errors as $error) {
					echo '<div class="error">'.$error.'</div>';
				}
			} ?>
			<form action="" method="post">
				<div class="txtbname">Заголовок статьи (вывод в списке):</div>
				<div><input class="span4 p_name" type="text" name="title" value="<?=$article['title'];?>" /></div>
				<div class="txtbname">ЧПУ:</div>
				<div><input class="span4 p_name" type="text" name="chpu" value="<?=$article['chpu'];?>" /></div>
				<div class="txtbname">СЕО title:</div>
				<div><input class="span4 p_name" type="text" name="seo_title" value="<?=$article['seo_title'];?>" /></div>
				<div class="txtbname">Текст:</div>
				<div>
					<textarea name="text" cols="70" rows="7" style="width: 400px;"><?php echo $article['text']; ?></textarea>
					<script type="text/javascript">
						var editor = CKEDITOR.replace( 'text' );
						CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
					</script>
				</div>
				<div>Выводить в списке статей <input type="checkbox" name="show_in_list" <?=($article['show_in_list'] == 0 ? '' : 'checked="checked"')?> /></div>
				<div><input type="submit" name="edit" value="Сохранить" /></div>
			</form>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>