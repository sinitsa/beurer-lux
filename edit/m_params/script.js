$(document).ready(function(){
	
	// Переменная куда будут располагаться данные файлов
 
	var files;
	 
	// Вешаем функцию на событие
	// Получим данные файлов и добавим их в переменную
	 
	
		
	$("#param_table").on("change", "input[type=file]", function(){
		files = this.files;
		console.log(data);
		 var data = new FormData();
    $.each(files, function( key, value ){
        data.append( key, value );
    });
	var active_file = $(this);
    // Отправляем запрос
	var old_file = active_file.parents("tr.row_par").find(".file_cont img").attr("src");
	var id_par = active_file.parents("tr.row_par").data('id_par');
		$.ajax({
			url: '/edit/m_params/ajax.php?method=save_file&id_par='+id_par+'&old_file='+old_file,
			type: 'POST',
			data: data,
			cache: false,
			dataType: 'json',
			processData: false, // Не обрабатываем файлы (Don't process the files)
			contentType: false, // Так jQuery скажет серверу что это строковой запрос
			success: function( respond, textStatus, jqXHR ){
	 
				// Если все ОК
	 
				if( typeof respond.error === 'undefined' ){
					// Файлы успешно загружены, делаем что нибудь здесь
				var files_path = respond.files;
					// выведем пути к загруженным файлам в блок '.ajax-respond'
		
				active_file.parents("tr.row_par").find(".file_cont").html("<img src='/upload/icons/"+files_path+"'>");
					
					
					console.log(files_path);
				}
				else{
					console.log('ОШИБКИ ОТВЕТА сервера: ' + respond.error );
				}
			},
			error: function( jqXHR, textStatus, errorThrown ){
				console.log('ОШИБКИ AJAX запроса: ' + textStatus );
			}
		});
	});
	
	$("#add_par").click(function(){
		$.ajax({
			url: '/edit/m_params/ajax.php?method=addPar',
			type: 'POST',
			success: function( respond, textStatus, jqXHR ){
	 
				// Если все ОК

					$('#param_table').append(respond);
				
				
			},
			error: function( jqXHR, textStatus, errorThrown ){
				console.log('ОШИБКИ AJAX запроса: ' + textStatus );
			}
		});
		
		return false;
	});
	
	$("#param_table").on("click", ".del", function(){
		
		var id_par = $(this).data('id');
		var old_file = $(this).parents('.row_par').find('.file_cont img').attr("src");
		var t = $(this);
		$.ajax({
			url: '/edit/m_params/ajax.php?method=del_par&id_par='+id_par+"&old_file="+old_file,
			type: 'POST',
			success: function( respond, textStatus, jqXHR ){
	 
				// Если все ОК

					t.parents('.row_par').remove();
				
				
			},
			error: function( jqXHR, textStatus, errorThrown ){
				console.log('ОШИБКИ AJAX запроса: ' + textStatus );
			}
		});
		
		return false;
	});
	
	$("#save_all").click(function(){
		
		
		var massData = [];
		$('#param_table tr').each(function(){
			
			var activ = $(this).find('.enabled').prop('checked')?1:0;
			
			massData.push({
				'id':$(this).data('id_par'),
				'type':$(this).find('.type').val(),
				'title':$(this).find('.title').val(),
				'unit':$(this).find('.unit').val(),
				'def':$(this).find('.def').val(),
				'enabled':activ
			});

		});
		$.ajax({
			url: '/edit/m_params/ajax.php?method=update_all',
			type: 'POST',
			data:{"dataPar": massData},
			success: function( respond, textStatus, jqXHR ){
	 
				// Если все ОК

					//t.parents('.row_par').remove();
				
				alert("Сохранено");
			},
			error: function( jqXHR, textStatus, errorThrown ){
				console.log('ОШИБКИ AJAX запроса: ' + textStatus );
			}
		});
		
		return false;
	});
	
	$("#param_table").on("change", ".type", function(){
		
		
		
		//console.log($(this).val());
		if($(this).val()==2)
		{
			$(this).parents('tr.row_par').find(".defcont").html("<textarea class='def'>"+$(this).parents('tr.row_par').find(".def").val()+"</textarea>");
		}
		else
		{
			$(this).parents('tr.row_par').find(".defcont").html("<input class='def' value='"+$(this).parents('tr.row_par').find(".def").text()+"'>");
		}	
	});
	
	
	
});