<?php
include ("../connect.php");
include ("../../func/core.php");
$conn = new PDO("mysql:host=$host;dbname=$db;charset=utf8", $login, $pass);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

switch($_REQUEST['method'])
{

case "save_file":		
		$data = array();
		$error = false;
		$files = array();
	 
		$uploaddir = '/var/www/beurerlux/upload/icons/'; 
	 
		// Создадим папку если её нет
	 
		if( ! is_dir( $uploaddir ) ) mkdir( $uploaddir, 0777 );
	 
		// переместим файлы из временной директории в указанную
		foreach( $_FILES as $file ){
			if( move_uploaded_file( $file['tmp_name'], $uploaddir . basename($file['name']) ) ){
				$files[] = realpath( $uploaddir . $file['name'] );
				$file_name = $file['name'];
			}
			else{
				$error = true;
			}
		}
		//var_dump($_REQUEST['old_file']);
		if($_REQUEST['old_file']!="undefined")
		{
		unlink("/var/www/beurerlux" . $_REQUEST['old_file']);
		}
		$data = $error ? array('error' => 'Ошибка загрузки файлов.') : array('files' => $file_name );
		
		$stmt = $conn->prepare('UPDATE `ultra_param` SET icon=? WHERE id = ?');
		$stmt->execute([$file_name, $_REQUEST['id_par']]);
		echo json_encode( $data );
	break;	
	
case "addPar":		
		
		
		
		$stmt = $conn->query('INSERT INTO `ultra_param` (`title`) VALUES ("Новый параметр")');
		$lastId = $conn->lastInsertId();
		
		?>
		
		<tr data-id_par="<?=$lastId ?>" class="row_par">
		<td><?=$lastId ?></td>
		<td>
			<select class="type">
				<option class="text" value="0">Текст</option>
				<option class="numb" value="1">Число</option>
				<option class="listing" value="2">Список</option>
			</select>
		</td>
		<td><input type="text" class="title" value=""></td>
		<td><input type="text" class="unit" value=""></td>
		<td class="files">
			
			<!-- Поле MAX_FILE_SIZE должно быть указано до поля загрузки файла -->
			<input type="hidden" name="file_<?=$lastId ?>" value="30000" />
			<!-- Название элемента input определяет имя в массиве $_FILES -->
			<input name="userfile" type="file" />
			<div class="file_cont">
			
			</div>
			</td>
		<td class="defcont">
			
					<input class="def" type="text" value="">

		</td>
		<td><input class="enabled" type="checkbox" ></td>
		<td><a class="del" data-id="<?=$lastId ?>">Удалить</a></td>
	</tr>
		
		
<?	
	break;		
	
	case "del_par":		
		
		if($_REQUEST['old_file']!="undefined")
		{
		unlink("/var/www/beurerlux" . $_REQUEST['old_file']);
		}
		$stmt = $conn->prepare('DELETE FROM `ultra_param` WHERE id = ?');
		$res = $stmt->execute([$_REQUEST['id_par']]);
		echo $res;
	break;


	case "update_all":		
		
		$dataPar = $_REQUEST['dataPar'];
		//var_dump($_REQUEST['dataPar']);
		foreach($dataPar as $par)
		{
			$stmt = $conn->prepare('UPDATE `ultra_param` SET type=:type, title=:title, unit=:unit, val_def=:def, enabled=:enabled  WHERE id = :id');
			$stmt->execute(array(
				'id'=>$par['id'],
				'type'=>$par['type'],
				'title'=>$par['title'],
				'unit'=>$par['unit'],
				'def'=>trim($par['def']),
				'enabled'=>$par['enabled']
			));
		}
		

	break;		
	
}


?>