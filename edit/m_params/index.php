<?php
include ("../connect.php");
include ("../../func/core.php");
//phpinfo();
include ("../up.php");

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

    $conn = new PDO("mysql:host=$host;dbname=$db;charset=utf8", $login, $pass);

    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);







$stmt = $conn->query('SELECT * FROM `ultra_param`');

$result = $stmt->fetchAll();

 

//var_dump($result);

?>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<script src="script.js"></script>
<form enctype="multipart/form-data" action="?save=1" method="POST">
<table class="table table-striped" id="param_table">
	<thead>
		<tr class="row_par">
		  <th>id</th>
		  <th>Тип</th>
		  <th>Наименование</th>
		  <th>Единица измерения(если есть)</th>
		  <th>Иконка</th>
		  <th>Значение по умолчанию</th>
		  <th>Активность</th>
		  <th>Удалить</th>
		</tr>
	  </thead>
	<? foreach($result as $res){ ?>
	<tr data-id_par="<?=$res['id'] ?>" class="row_par">
		<td><?=$res['id'] ?></td>
		<td>
			<select class="type">
				<option class="text" value="0" <?=$res['type']==0?"selected":"" ?>>Текст</option>
				<option class="numb" value="1" <?=$res['type']==1?"selected":"" ?>>Число</option>
				<option class="listing" value="2" <?=$res['type']==2?"selected":"" ?>>Список</option>
			</select>
		</td>
		<td><input type="text" class="title" value="<?=$res['title'] ?>"></td>
		<td><input type="text" class="unit" value="<?=$res['unit'] ?>"></td>
		<td class="files">
			
			<!-- Поле MAX_FILE_SIZE должно быть указано до поля загрузки файла -->
			<input type="hidden" name="file_<?=$res['id'] ?>" value="30000" />
			<!-- Название элемента input определяет имя в массиве $_FILES -->
			<input name="userfile" type="file" />
			<div class="file_cont">
			<? if(!empty($res['icon'])){ ?>
			
			<img src="/upload/icons/<?=$res['icon'] ?>">
			<? } else {?>
			 Не найден
			<? } ?>
			</div>
			</td>
		<td class="defcont">
			<?php switch($res['type']):
			 case '0': ?>
					<input class="def" type="text" value="<?php echo $res['val_def'] ?>">
				<?php
				break;
		
				case '1': ?>
					<input class="def" type="text" value="<?php echo $res['val_def'] ?>">
				<?php 
				break; 
				case '2': ?>
					<textarea class="def"><?php 
					if(!empty($res['val_def']))
					{
						$arrVal = explode("\n", $res['val_def']);
						foreach($arrVal as $r){ 
						 
						 echo $r."\n";
						
						 } 
						 
					}	 ?>
					
					</textarea>
				<?php break;
				
				default: ?>
					<input class="def" type="text" value="<?php echo $res['val_def'] ?>">
			
			<?php endswitch; ?>
		</td>
		<td><input class="enabled" type="checkbox" <?=$res['enabled']==1?"checked":"" ?>></td>
		<td><a class="del" data-id="<?=$res['id'] ?>">Удалить</a></td>
	</tr>
		
	<? } ?>
</table>
<a id="add_par">Добавить</a>
<input type="button" value="Сохранить" id="save_all"/>
</form>
<?php include ("../down.php");	?>