﻿<?php 
require('connect.php');
session_start();
$errors = array();

if (isset($_POST['login'])){

    if ($_POST['username'] != "" && $_POST['password'] != ""){
        $username = mysql_real_escape_string(htmlspecialchars(substr($_POST['username'],0, 50)));
        $password = mysql_real_escape_string(htmlspecialchars(substr($_POST['password'],0, 50)));

        $sql = "SELECT id, password, is_active FROM users_auth WHERE username = '{$username}' AND is_active = 1";
        $user = mysql_fetch_assoc(mysql_query($sql));
        if ($user && (md5($password) === $user['password'])) {
            $_SESSION['is_auth'] = true;
        } else {
            $errors['wrong_password'] = "Неверное имя пользователя или пароль";
        }
    } else {
        $errors['wrong_password'] = "Не указано имя пользователя или пароль";
    }

}

if (isset($_GET['action']) AND $_GET['action']=="logout") {
    session_start();
    unset($_SESSION['is_auth']);
    session_destroy();
    header('Location: /edit/');
    exit;
}

// echo $passwordHash = md5('');
// $sql = "INSERT INTO users_auth (username, password, is_active) VALUES ('admin', '".$passwordHash."', 1)";
// $q = mysql_query($sql);

if (!isset($_SESSION['is_auth'])){ ?>
    <div class="errors"><?php echo ($errors) ? $errors['wrong_password'] : "" ?></div>
    <form action="" method="post" style="width:250px; margin:0 auto;">
        <input type="text" name="username" placeholder="Имя пользователя" <?php echo ($errors) ? 'class="error"' : '' ?>>
        <input type="password" name="password" placeholder="Пароль" <?php echo ($errors) ? 'class="error"' : '' ?>>
        <input type="submit" name="login" value="Войти">
    </form>

    <style>
        .errors { width:100%; height:100px; box-sizing: border-box; text-align:center; color:#F26060; font-size:18px; padding:35px 0; }
        input[type="text"], input[type="password"]{ width:250px; height:40px; border: 1px solid #c9c9c9; border-radius:5px; display:block; margin-bottom:12px; color: #000; font-size:16px; outline:none; padding: 0 10px; }
        input[type="submit"]{ width:250px; height:40px; border-radius:5px; display:block; margin-bottom:10px; color:#fff; font-size:14px; cursor:pointer; text-transform:uppercase; border:none; box-shadow:none; background-color:#709DB8; outline:none; }
        .error { border:1px solid #F26060 !important; }
    </style>
<?php die(); } ?>

