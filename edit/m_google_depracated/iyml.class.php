<?php
include_once("../../func/core.php");

class iYMLParam{
}

class iYML{
	
	var $controlAmount;
	var $controlMargin;
	
	var $siteName;
	var $siteCompany;
	var $currencyName;
	var $currencyRate;
	
	var $catalogImageType;
	var $selectedCategories;
	var $selectedCategoriesFakeRests;
	var $onRequest;
	var $includeRests;
	
	var $ymlFilePath;
	
	var $lastUpdate;
	
	var $controlPrice;
	var $bidInStock;
	var $bidNotAvailable;
	
	
//	var $__params;
	
	function __construct(){
		
		$this->getYMLConfig();
		
		return $this;
	}
	
	function install(){
		
		$query = "
			CREATE TABLE IF NOT EXISTS `igoogle_config` (
			  `name` varchar(50) NOT NULL,
			  `definition` varchar(50) DEFAULT NULL,
			  `description` varchar(200) DEFAULT NULL,
			  `value` varchar(200) DEFAULT '',
			  `sort` int(4) unsigned NOT NULL default '0',
			  PRIMARY KEY (`name`)
			) ENGINE=MyISAM DEFAULT CHARSET=cp1251
		";
		mysql_query($query);
		
		return $this;
	}
	
	function getYMLConfig(){
		
		$query = "SELECT `name`, COALESCE(`value`, '') AS `value` FROM `igoogle_config` ORDER BY `sort` ASC";
		
		$result = @mysql_query($query);
		
		while($line = @mysql_fetch_assoc($result)){
			
			$this->{$line['name']} = $line['value'];
			
		}
		
	}
	
	function saveParam($paramName, $paramValue){
		if($paramName == 'selectedCategories'){
			mysql_query('truncate table `igoogle_cat_links`');
			$arr = explode(',',$paramValue);
			$inStr = 'insert into `igoogle_cat_links`(`cat_id`,`ya_cat_id`) values';
			$inArr = array();
			foreach($arr as $a){
				$inArr[] = '('.$a.',0)';
			}
			$inStr .= implode(',', $inArr);
			unset($inArr);
			mysql_query($inStr);
		}

		$query = "INSERT INTO `igoogle_config` SET `name` = '{$paramName}', `value`= '{$paramValue}'
			ON DUPLICATE KEY UPDATE `value` = '{$paramValue}' ";
			
		mysql_query($query);
		
		return mysql_affected_rows()>0;
	}
		
	function saveParams(){ 
			
		foreach($this as $attrName=>$attrValue){
			if ($paramName != "lastUpdate"){
				$this->saveParam($attrName, $this->{$attrName});
			}
		}
		
		return $this;
	}

	function buildParamHTML(){
		$query = "SELECT `name`, COALESCE(`description`, '') AS `description`, COALESCE(`value`, '') AS `value` FROM `igoogle_config` ORDER BY `sort` ASC";
		
		$result = @mysql_query($query) or die(mysql_error());
		$html = "<table id=\"iYMLParamForm\">";
		while($line = @mysql_fetch_assoc($result)){
			if ($line['name'] == "lastUpdate"){
				continue;
			}			
			//$this->{$line['name']} = $line['value'];
			$descr = $line['description'];
			$caption_class = "description";
			if(!$descr){
				$caption_class = "internal-name";
				$descr = $line['name'];
			}
			$disabled_attribute = "";
			
			$html .= "<tr>";
			$html .= "<td class=\"col1\"><span class=\"{$caption_class}\">{$descr}</span></td>";
			if ($line['name'] == 'catalogImageType') {
				$html .= "<td class=\"col2\"><select name=\"{$line["name"]}\" {$disabled_attribute}>";
				$ar = array();
				$ar[] = array('Превью', 'product_preview');
				$ar[] = array('Средняя', 'product_medium');
				$ar[] = array('Маленькая', 'product_small');
				$ar[] = array('Оригинал', 'product_original');
				foreach ($ar as $option) {
					$html .= "<option ".($option[1] == $line['value']?'selected="selected" ':'')."value=\"{$option[1]}\">{$option[0]}</option>";
				}
				$html .= "</select></td>";
			} elseif (in_array($line['name'], array('selectedCategories', 'selectedCategoriesFakeRests'))) {
				$html .= "<td class=\"col2\"><select name=\"{$line["name"]}\" style=\"width: 380px;\" multiple=\"multiple\" size=\"16\" {$disabled_attribute}>";
				$selected = explode(',', $line['value']);
				$selected = (array) $selected;
				
				foreach (getCats() as $cat) {
					$html .= "<option value=\"{$cat['id']}\"".(in_array($cat['id'], $selected)?' selected="selected"':'').">{$cat['title']}</option>";
				}
				$html .= "</select></td>";
			} else {
				$html .= "<td class=\"col2\"><input {$disabled_attribute} name=\"{$line["name"]}\" type=\"text\" value=\"{$line["value"]}\" /></td>";
			}
			$html .= "</tr>";
			
		}
		$html .= "</table>";
		
		return $html;
	}
	
	function buildYMLFile(){
		
		mysql_query('SET NAMES utf8');
		
		$imp = new DOMImplementation();
		$dtd = $imp->createDocumentType('yml_catalog', '', 'shops.dtd');
		$xml = $imp->createDocument(null, null, $dtd);
		$xml->encoding = 'utf-8';
		$shop = $xml->createElement('shop');
		$cat_ids = Array();

		// Компания
		$shop->appendChild($xml->createElement('name', $this->siteName));
		$shop->appendChild($xml->createElement('company', $this->siteCompany));
		$shop->appendChild($xml->createElement('url', Config::get('site.web_addr')));
		
		// Currencies
		$currency = $xml->createElement('currency');
		$currency->setAttribute('id', $this->currencyName);
		$currency->setAttribute('rate', $this->currencyRate);
		$currencies = $xml->createElement('currencies');
		$currencies->appendChild($currency);
		$shop->appendChild($currencies);
		
		// Категории
		$enabledCatFilter = false;
		if (!empty($this->selectedCategories)) {
			$selectedCats = explode(',', $this->selectedCategories);
			if (count($selectedCats) > 0)
				$enabledCatFilter = true;
			$catsWhereClause = "IN ('" . implode("','", $selectedCats) . "')";
		}
		
		//Категории с фейковом наличии
		if (!empty($this->selectedCategoriesFakeRests)) {
			$fakeCats = explode(',', $this->selectedCategoriesFakeRests);
		} else {
			$fakeCats = array();
		}
		
		$categories = $xml->createElement('categories');
		$query = ("
			SELECT `id`, `pod`, `title` 
			FROM `cat`
			".($enabledCatFilter ? "WHERE `id` ".$catsWhereClause : '')."
			ORDER BY `id` ASC");
		$result = mysql_query($query);
		if ($result && mysql_num_rows($result) != 0) {
			while ($row = mysql_fetch_assoc($result)) {
				$category = $xml->createElement('category', $row['title']);
				$category->setAttribute('id', $row['id']);
				$cat_ids[count($cat_ids)+1] = $row['id'];
				if ($row['pod'] != 0) {
					$category->setAttribute('parentId', $row['pod']);
				}
				$categories->appendChild($category);
			}
		}
		$shop->appendChild($categories);

		// Товары
		$offers = $xml->createElement('offers');
		$query = "
			SELECT 
				`c`.`id` , 
				`c`.`chpu` , 
				`c`.`price_after_discount` as `price` , 
				`c`.`cat` , 
				`c`.`title` , 
				`c`.`seo_title`,
				`c`.`des`,
				`c`.`market_cost_per_click` as `bid`,
				`c`.`net_cost`,
				CASE WHEN 
					(COALESCE(`sr1`.`amount`,0)+COALESCE(`sr2`.`amount`,0))>='{$this->controlAmount}'
					AND
					(COALESCE(`c`.`price_after_discount`,0)-COALESCE(`c`.`net_cost`,0))>='{$this->controlMargin}'
						THEN 'true'
						ELSE 'false'
				END AS `available`,
				(COALESCE(`sr1`.`amount`,0)+COALESCE(`sr2`.`amount`,0)) as `amount`
			FROM 
				`catalog` as `c`
				LEFT JOIN `storage_rests` AS `sr1`
					ON `sr1`.`catalog_id` = `c`.`id`
					AND `sr1`.`storage_id` = '1'				
				LEFT JOIN `storage_rests` AS `sr2`
					ON `sr2`.`catalog_id` = `c`.`id`
					AND `sr2`.`storage_id` = '2'
			WHERE 
				`c`.`id` IS NOT NULL";
		//Если включен фильтр по категориям
		if ($enabledCatFilter) {
			$query .= " AND `c`.`cat` ".$catsWhereClause;
		}
		$query .=  "
		ORDER BY `c`.`cat`
		#GROUP BY `c`.`id`";

		$result = mysql_query($query);
		file_put_contents('no.txt','');

		if ($result && mysql_num_rows($result) != 0){
			while ($row = mysql_fetch_assoc($result))  {
				if(in_array($row['cat'], $cat_ids))	{
					$query = "SELECT DISTINCT `pav`.value 
						FROM `params_available_values`as `pav` 
						INNER JOIN  `params_catalog_links` as `pcl` 
						ON `pav`.id = `pcl`.param_id 
						INNER JOIN `catalog` as `c`
						ON `pcl`.catalog_id = `c`.id
						WHERE `c`.id  = {$row['id']} 
						AND `pav`.id IN (201, 202, 203, 204)";
					$r = mysql_query($query);
					$value =  mysql_fetch_assoc($r);
					$brand = $value['value'];
					
					/*
					Коротко логика: Если товара нет в наличии, то фильтруем по марже и контрольной стоимости, выставляем bid от наличия
					и available true
					Если нет в наличии, или у него фейковое наличие, фильтруем по цене, выставляем bid по "не наличию", фековым available выставляем тру,
					не фейковым тру выставляем только если есть опция на заказ, иначе выкидываем из выборки
					*/
					if ($row['amount'] > 0 && !in_array($row['cat'], $fakeCats)) {
						//фильтр по марже и количеству
						if ($row['amount'] < $this->controlAmount || ($row['price'] - $row['net_cost']) < $this->controlMargin) {
							continue;
						}
						if ($row['bid'] != 0 && $row['bid'] != '') {
							$bid = $row['bid'] * 100;
						} else {
							$bid = $this->bidInStock * 100;
						}
						$available = true;
					} else {
						//Фильтрация по стоимости
						if ($row['price'] >= $this->controlPrice) {
							if ($row['bid'] != 0 && $row['bid'] != '') {
								$bid = $row['bid'] * 100;
							} else {
								$bid = $this->bidNotAvailable * 100;
							}
							//Если он фейковый
							if (in_array($row['cat'], $fakeCats)) {
								$available = true;
							} else {
								//Если есть возможность на заказ
								if ($this->onRequest == '1') {
									$available = false;
								} else {
									//Иначе пропускаем
									continue;
								}
							}
						} else {
							continue;
						}
					}
					
					$new_title = str_replace('&', '&amp;', $row['seo_title']);

					$fullDes = $row['des'];
					$row['des'] = strip_tags(substr($row['des'], 0, strpos($row['des'], '<h3>')));	
					$row['des'] = str_replace("\n", '', $row['des']);
					$row['des'] = str_replace("\t", '', $row['des']);
					$row['des'] = str_replace("\r", '', $row['des']);
					$row['des'] = str_replace('.', '. ', $row['des']);
					$row['des'] = str_replace('  ', ' ', $row['des']);
					$row['des'] = str_replace('&ndash;', '-', $row['des']);
					$row['des'] = str_replace('&mdash;', '-', $row['des']);
					$row['des'] = str_replace('&laquo;', "\"", $row['des']);
					$row['des'] = str_replace('&raquo;', "\"", $row['des']);
					$row['des'] = str_replace('&shy;', "", $row['des']);
					$row['des'] = str_replace('&nbsp;', " ", $row['des']);
					$row['des'] = trim($row['des']);			

					if (mb_strlen($row['des'], 'utf-8') > 512) {
						$row['des'] = mb_substr($row['des'], 0, $this->findNearestSpace($row['des'], 500), 'utf-8').'...';
					}
					$row['des'] = str_replace(',.', '.', $row['des']);
					$row['des'] = $new_title.'. '.$row['des'];
					
					$offer = $xml->createElement('offer');
					$offer->setAttribute('id', $row['id']);
					$offer->setAttribute('available', $available ? 'true' : 'false');
					$offer->setAttribute('bid', $bid);
					$offer->appendChild($xml->createElement('url', getTemplateLink($row, 'catalog', $fullUrl = true)));
					$offer->appendChild($xml->createElement('price', $row['price']));
					$offer->appendChild($xml->createElement('currencyId', 'RUR'));
					$offer->appendChild($xml->createElement('categoryId', $row['cat']));
					$offer->appendChild($xml->createElement('picture', getImageWebPath($this->catalogImageType).$row['id'].'.jpg'));
					$offer->appendChild($xml->createElement('description', $row['des']));
					$offer->appendChild($xml->createElement('brand', $brand));
					if ($this->includeRests == '1') {
						$offer->appendChild($xml->createElement('delivery', 'true'));
						$offer->appendChild($xml->createElement('local_delivery_cost', deliveryPrice::getByCatAndPrice($row['cat'], $row['price'], $row['id'])));
					}
					$offer->appendChild($xml->createElement('name', $new_title));
					$offer->appendChild($xml->createElement('manufacturer_warranty', 'true'));

					// $offer->appendChild($xml->createElement('country_of_origin', 'Германия'));
					if( preg_match('/us\smedica/i',$new_title) ){
						$offer->appendChild($xml->createElement('country_of_origin', 'США'));
					}elseif( preg_match('/anatomico/i',$new_title) ){
						$offer->appendChild($xml->createElement('country_of_origin', 'Италия'));
					}elseif( preg_match('/yamaguch/i',$new_title) ){
						$offer->appendChild($xml->createElement('country_of_origin', 'Япония'));
					}elseif( preg_match('/hakuju/i',$new_title) ){
						$offer->appendChild($xml->createElement('country_of_origin', 'Япония'));
					}elseif( preg_match('/fujiiryoki/i',$new_title) ){
						$offer->appendChild($xml->createElement('country_of_origin', 'Япония'));
					}else{
						if(preg_match_all('/Производство(.*)/i',$fullDes,$re)){
							$co = str_replace(':','',$re[1][0]);
							$co = str_replace(' ','',$co);
							$co = preg_replace('/<.*>/i','',$co);
							$offer->appendChild($xml->createElement('country_of_origin', $co));
						}else{
							file_put_contents('no.txt',$row['id'].") http://usmedic.ru/catalog/".$row['chpu'].".html | ".$new_title." \n", FILE_APPEND);
						}
					}
					$offers->appendChild($offer);
				}  
			}
		}
		$shop->appendChild($offers);
		$yml_catalog = $xml->createElement('yml_catalog');
		$yml_catalog->setAttribute('date', date('Y-m-d H:i'));
		$yml_catalog->appendChild($shop);
		$xml->appendChild($yml_catalog);
		$xml->formatOutput = true;
		
		// Сохраняем в YML
		$xml->save($this->ymlFilePath);		
		mysql_query("SET NAMES 'cp1251'");
		
		
		
		$this->lastUpdate = date('d.m.Y H:i:s', mktime()); 
		// echo $this->lastUpdate;
		// echo $this->ymlFilePath;
		$this->saveParam("lastUpdate", $this->lastUpdate);
		
		return $this;
	}
	
	function findNearestSpace($string, $limit) {
		$substring_limited = mb_substr($string, 0, $limit, 'UTF-8');
		return mb_strrpos($substring_limited, ' ', 'UTF-8'); 
	}	
	
}

?>