<?
	include ('../up.php');
	include ('../connect.php');

	function getTotal($table) {
		$query = 'SELECT COUNT(`id`) AS cnt FROM `'.$table.'`';
		$result = mysql_query($query);
		$r = mysql_fetch_assoc($result);
		return $r['cnt'];
	}

	function getCount($table, $col, $type) {
		if($type = 'text') {
			$query = 'SELECT COUNT(`id`) AS cnt FROM `'.$table.'` WHERE `'.$col.'`="" OR `'.$col.'`=NULL';
		} elseif($type = 'numb') {
			$query = 'SELECT COUNT(`id`) AS cnt FROM `'.$table.'` WHERE `'.$col.'`=0 OR `'.$col.'`=NULL';
		}
		$result = mysql_query($query);
		$r = mysql_fetch_assoc($result);
		return colorVal($r['cnt']);
	}

	function colorVal($val) {
		if($val > 0) {
			return '<span style="color: red">'.$val."</span>";
		} else {
			return $val;
		}
	}
	$cat['total'] = getTotal('cat');

	//	Категории без титла
	$cat['woTitle'] = getCount('cat', 'title', 'text');

    //	Категории без seo_титла
	$cat['woSeoTitle'] = getCount('cat', 'seo_title', 'text');

	//	Категории без товаров
	$query = 'SELECT
				COUNT(`cat_el`.`id`) as woGoods
			FROM
				`cat` as cat_el
				LEFT JOIN `cat` AS cat_par
					ON cat_el.pod = cat_par.id
			WHERE
				(SELECT
					COUNT(`catalog`.`id`)
				FROM
					`catalog`
				WHERE
					`catalog`.`cat` = `cat_el`.`id`) = 0
				AND
					(SELECT COUNT(cat.id) FROM cat WHERE cat.pod = cat_el.id)=0';
	$result = mysql_query($query);
	$r = mysql_fetch_assoc($result);
	$cat['woGoods'] = colorVal($r['woGoods']);

	//	Подкатегории без родительской
	$query = 'SELECT
				cat.id,
				cat.title,
				cat.pod as ZZZ,
				(SELECT COUNT(id) FROM cat WHERE zzz=id) as isOrphan
			FROM
				cat
			WHERE
				NOT cat.pod=0
			ORDER BY `isOrphan` ASC';
	$result = mysql_query($query);
	$c = 0;
	while($r = mysql_fetch_assoc($result)) {
		if($r['isOrphan'] == 0) {
			$c++;
		}
	}
	$cat['woParent'] = colorVal($c);

?>
<style>
.inlTbl {
	display: inline-table;
	width: 250px;
	margin-left: 30px;
	margin-top: 30px;

}

</style>
<center><h1>Проверка сайта</h1>
<div class="inlTbl">
	<table class="table">
		<tr>
			<th colspan="2">Категории (Всего <?=$cat['total']?>  )</th>
		</tr>
		<tr>
			<td>без названия</td>
			<td><?=$cat['woTitle']?>  </td>
		</tr>
		<tr>
			<td>без seo_title</td>
			<td><?=$cat['woSeoTitle']?>  </td>
		</tr>
			<td>без товаров</td>
			<td><?=$cat['woGoods']?>  </td>
		<tr>
			<td>без родителей</td>
			<td><?=$cat['woParent']?>  </td>
		</tr>
	</table>
</div>
<?
	$good['total'] = getTotal('catalog');
	$good['woTitle'] = getCount('catalog', 'title', 'text');
	$good['woSeoTitle'] = getCount('catalog', 'seo_title', 'text');
	$good['woDes'] = getCount('catalog', 'des', 'text');
	$good['woPrice'] = getCount('catalog', 'price', 'numb');
	$good['woCat'] = getCount('catalog', 'cat', 'numb');

	//	Товары-сироты:
	$query = 'SELECT
				catalog.id,
				catalog.title,
				catalog.cat AS tCat,
				(SELECT COUNT(cat.id)
			FROM
				cat
			WHERE
				tCat=cat.id) as isOrphan
			FROM
				catalog
			WHERE 1 ORDER BY `isOrphan` ASC';
	$result = mysql_query($query);
	$c = 0;
	while($r = mysql_fetch_assoc($result)) {
		if($r['isOrphan'] == 0) {
			$c++;
		}
	}
	$good['orphans'] = colorVal($c);
?>
<div class="inlTbl">
	<table class="table">
		<tr>
			<th colspan="2">Товары (Всего <?=$good['total']?>  )</th>
		</tr>
		<tr>
			<td>без названия</td>
			<td><?=$good['woTitle']?>  </td>
		</tr>
		<tr>
			<td>без seo_title</td>
			<td><?=$good['woSeoTitle']?>  </td>
		</tr>
			<td>без описания</td>
			<td><?=$good['woDes']?>  </td>
		<tr>
			<td>без цены</td>
			<td><?=$good['woPrice']?>  </td>
		</tr>
		<tr>
			<td>без категории</td>
			<td><?=$good['woCat']?>  </td>
		</tr>
		<tr>
			<td>с "мертвой" категорией</td>
			<td><a href="/edit/m_checksite/goods_orphans.php"><?=$good['orphans']?></a>  </td>
		</tr>
	</table>
</div>
<?
	$pages['total'] = getTotal('pages');
	$pages['woTitle'] = getCount('pages', 'title', 'text');
	$pages['woSeoTitle'] = getCount('pages', 'seo_title', 'text');
	$pages['woDes'] = getCount('pages', 'des', 'text');
	$pages['woChpu'] = getCount('pages', 'chpu', 'text');
?>
<div class="inlTbl">
	<table class="table">
		<tr>
			<th colspan="2">Страницы (Всего <?=$pages['total']?>  )</th>
		</tr>
		<tr>
			<td>без названия</td>
			<td><?=$pages['woTitle']?>  </td>
		</tr>
		<tr>
			<td>без seo_title</td>
			<td><?=$pages['woSeoTitle']?>  </td>
		</tr>
			<td>без описания</td>
			<td><?=$pages['woDes']?>  </td>
		<tr>
			<td>без ЧПУ</td>
			<td><?=$pages['woChpu']?>  </td>
		</tr>
	</table>
</div>
<?
	$foto['total'] = getTotal('foto');
	$query = 'SELECT count(t1.id) as cnt, t1.id, t1.catalog_id, t2.id, t2.title FROM `foto` AS t1 LEFT JOIN `catalog` AS t2 ON t1.`catalog_id`=t2.`id` WHERE t2.title is NULL';
	$result = mysql_query($query);
	$r = mysql_fetch_assoc($result);
	$foto['dopOrphans'] = colorVal($r['cnt']);
?>
<div class="inlTbl">
	<table class="table">
		<tr>
			<th colspan="2">Фотки (Всего <?=$foto['total']?> доп. фото  )</th>
		</tr>
		<tr>
			<td>доп. фото "мертвых" товаров</td>
			<td><a href="/edit/m_checksite/dop_photo_orphans.php"><?=$foto['dopOrphans']?></a>  </td>
		</tr>
	</table>
</div>
</center>
<?
	include ('../down.php');
?>
