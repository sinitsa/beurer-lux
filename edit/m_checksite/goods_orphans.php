<?
	include ('../up.php');
	include ('../connect.php');
	//	Товары-сироты:
	$query = 'SELECT
						catalog.id,
						catalog.title,
						catalog.chpu,
						catalog.cat AS tCat,
						(SELECT COUNT(cat.id)
					FROM
						cat
					WHERE
						tCat=cat.id) as isOrphan
					FROM
						catalog
					WHERE 1 ORDER BY `isOrphan` ASC';
	$result = mysql_query($query);
	
?>
<div class="modal hide fade" id="myModal">
	<div class="modal-header">
		<a class="close" data-dismiss="modal">x</a>
		<h3>Удалить товар?</h3>
    </div>
    <div class="modal-body">

    </div>
    <div class="modal-footer">
		<a id="modalOk" class="btn btn-danger">
			<i class="icon-remove"></i> Удалить
		</a>
		<a data-dismiss="modal" class="btn">Закрыть</a>
    </div>
</div>

<div style="width:90%; margin-left:5%">
	<center>
		<h1>Товары с несуществующей категорией</h1>
		<table class="table table-striped">
		<tr>
			<th>ID</th>
			<th>Наименование</th>
			<th></th>
		</tr>
		<?
			while($r = mysql_fetch_assoc($result)) {
			if($r['isOrphan'] != 1) {
				?>
				<tr>
					<td><?=$r['id']?></td>
					<td><a href="/catalog/<?=$r['chpu']?>.html" class="good" good_id="<?=$r['id']?>"><?=$r['title']?></a></td>
					<td><a class="btn btn-danger good_del" good_id="<?=$r['id']?>"><i class="icon-remove"></i></a></td>
				</tr>
				<?
				}
			}
		?>
		</table>
	</center>
</div>

<script type="text/javascript">
	$('.good_del').click( function () {
		var goodID = $(this).attr('good_id');
		var goodTitle = $('a.good[good_id='+goodID+']').text();

		$('#modalOk').click( function () {
			location.href = '/edit/m_catalog/sql_del.php?id='+goodID;
		});

		$('.modal-body').text(goodTitle);
		$('#myModal').modal('show');
	});
</script>
<?	include ('../down.php');	?>