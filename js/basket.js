function setCookie(name, value, expires, path, domain, secure) {
	document.cookie = name + "=" + escape(value) + ((expires) ? "; expires=" + expires : "") + ((path) ? "; path=" + path : "") + ((domain) ? "; domain=" + domain : "") + ((secure) ? "; secure" : "");
}

function getCookie(c_name) {
	if (document.cookie.length > 0) {
		c_start = document.cookie.indexOf(c_name + "=");
		if (c_start != -1) {
			c_start = c_start + c_name.length + 1;
			c_end = document.cookie.indexOf(";", c_start);
			if (c_end == -1)
				c_end = document.cookie.length;
			return unescape(document.cookie.substring(c_start, c_end));
		}
	}
	return "";
}

function parseProductsString(string) {
	var returnArray = new Array();

	if (string.length > 0) {
		var products = string.split('|');
		for (var i in products) {
			var temp = products[i].split(':');

			returnArray.push({
				'id' : parseInt(temp[0]),
				'amount' : parseInt(temp[1])
			});
		}
	}
	return returnArray;
}

function generateProductsString(products) {
	var tempArray = new Array();
	for (var i in products) {
		tempArray.push(products[i].id + ':' + products[i].amount);
	}
	return tempArray.join('|');
}

function addProductToBasket(productId, price) {
	//получим данные из кук
	var products = parseProductsString(getCookie('cart_products'));
	var totalAmount = parseInt(getCookie('total_amount'));
	if (!isNaN(totalAmount)) {
		totalAmount += price;
	} else {
		totalAmount = price;
	}

	//Если товар уже есть в куках, добавляем ему количество
	//Заодно подсчитаем количество всех товаров
	var foundSame = false;
	var productsCount = 0;
	for (var i in products) {
		if (products[i].id == productId) {
			products[i].amount++;
			foundSame = true;
		}
		productsCount += products[i].amount;
	}
	if (!foundSame) {
		products.push({
			"id" : productId,
			"amount" : 1
		});
		productsCount += 1;
	}

	setCookie('cart_products', generateProductsString(products), '', '/');
	setCookie('total_amount', totalAmount, '', '/');
	//Изменить оформление
	refreshBasketInfo();
}

function deleteProductFromBasket(productId, price) {
	//получим данные из кук
	var products = parseProductsString(getCookie('cart_products'));
	var totalAmount = parseInt(getCookie('total_amount'));
	if (!isNaN(totalAmount)) {
		totalAmount -= price;
	} else {
		totalAmount = 0;
	}

	//Если товар уже есть в куках, убавляем ему количество
	//Заодно подсчитаем количество всех товаров
	var foundSame = false;
	var productsCount = 0;
	for (var i in products) {
		if (products[i].id == productId) {
			products[i].amount--;
			foundSame = true;
		}
		productsCount += products[i].amount;
	}
	if (!foundSame) {
		//do nothing
	}

	setCookie('cart_products', generateProductsString(products), '', '/');
	setCookie('total_amount', totalAmount, '', '/');
	//Изменить оформление
	refreshBasketInfo();
}

function setBasketInfo(amount, summ) {
	//Обратная совместимость
	refreshBasketInfo();
}

function refreshBasketInfo() {
	//получим данные из кук
	var products = parseProductsString(getCookie('cart_products'));
	var totalAmount = parseInt(getCookie('total_amount'));
	if (isNaN(totalAmount)) {
		totalAmount = 0;
	}
	var productsCount = 0;
	for (var i in products) {
		productsCount += products[i].amount;
	}

	$('#basket-summ-products, .productsCount').text(productsCount);
	$('#basket-summ-price').text(moneyFormat(totalAmount));
	//$('#basket-summ-products-text').text(declOfNum(productsCount, ['товар', 'товара', 'товаров']));
	$('.col span').text(productsCount+" "+declOfNum(productsCount, ['товар', 'товара', 'товаров']));
	$('#add_product_form .sum span').text(moneyFormat(totalAmount)+" руб.");
}

function recalculateBasket() {
	var productsCount = 0;
	var priceSumm = 0;
	var products = new Array();

	var _break = false;
	//Блокируем изменение инпутов до завершения аякса
	$('.amount-input').attr('disabled', 'disabled');

	$('#cart-items .basket-item').each(function(index, element) {
		if (_break == false) {
			var amount = $(element).find('input[name="amount"]').val();
			var price = $(element).find('input[name="price"]').val();
			var pid = $(element).find('input[name="id"]').val();

			if (amount == '0' || amount == '') {
				$(element).find('input[name="amount"]').val(1);
				amount = '1';
			}

			if (amount.match(/^[0-9]{1,3}$/)) {
				amount = parseInt(amount);
				productsCount += amount;
				priceSumm += parseInt(price) * amount;
				//
				$(element).find('.sum span').text(moneyFormat(parseInt(price) * amount));
				//
				
				products.push({
					"id" : pid,
					"amount" : amount
				});
			} else {
				productsCount = '-';
				priceSumm = '-';
				_break = true;
			}
		}
	});

	//Если ошибок не было
	if (_break == false) {
		//Записываем куки
		setCookie('cart_products', generateProductsString(products), '', '/');
		setCookie('total_amount', priceSumm, '', '/');

		//Изменяем данные в блоке корзины
		setBasketInfo(productsCount, priceSumm);

		var coverField = $('#cart-items');
		var cover = $('<div></div>').css({
			'position' : 'absolute',
			'left' : coverField.offset().left,
			'top' : coverField.offset().top,
			'width' : coverField.width(),
			'height' : coverField.height(),
			'zIndex' : 100,
			'background' : '#fff',
			'opacity' : .6
		}).html('<div style="text-align:center;">Обновляются данные...</div>').appendTo('body');

		//Запрос на стоимость доставкаи и размер скидки

		$.ajax({
			url : '/ajax/basket.php?method=get_delivery_and_discount',
			type : 'POST',
			dataType : 'json',
			data : {
				"products" : products,
				"deliveryType":$(".deliveryMethod input:checked").val(),
			},
			beforeSend : function() {
				$('#delivery-1 .value, #delivery-2 .value').html('<img src="/img/ajax_loading.gif" />');
			},
			success : function(data, textStatus, jqXHR) {
				var orderPrice ={};

				orderPrice.delivery = data.deliveryPrice;
				orderPrice.discount = intVal(data.discount);
				orderPrice.summ = intVal(priceSumm);
				$(".itog .num").html(data.deliveryPrice);

				//Изменяем данные на странице корзины
				$('#totalItemPrice').html(moneyFormat(priceSumm)+" руб.");
				$('#totalBasketPrice').html(moneyFormat(data.fullPrice)+" руб.");
				/* $('#cartDiscountPersent').html(orderPrice.discount); */
				/* if (orderPrice.discount > 0) {
					$('#discount-field').show();
				} else {
					$('#discount-field').hide();
				} */

				$('#basket-products-count').html(productsCount);

			},
			complete : function() {
				cover.remove();
				//Разблокируем инпуты для ввода
				$('.amount-input').removeAttr('disabled');
			},
			error : function(jqXHR, textStatus, errorThrown) {
			}
		});

	}
	if (_break == false && productsCount > 0) {
		$('#continue-order').removeAttr('disabled');
	} else {
		$('#continue-order').attr('disabled', 'disabled');
		$('.amount-input').removeAttr('disabled');
	}
}



var deliveryType;

$(function() {
	//Листенер изменение количества товаров в корзине
	var recalcTimer;
	$('.amount-input').bind('keyup', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);

		/*
		 LEFT=   37
		 UP=     38
		 RIGHT=  39
		 DOWN=   40
		 SHIFT=  16
		 CONTROL=    17
		 ALT=    18
		 */
		var keys = [16, 17, 18, 37, 38, 39, 40];
		//Не реагировать на нажатие клавиш, обозначеных выше
		if (recalcTimer != undefined)
			clearInterval(recalcTimer);
		if (!(keys.indexOf(code) >= 0)) {
			recalcTimer = setTimeout('recalculateBasket();', 1000);
		}
	});
	
	//Сила функционального программирования в связке с говнодоком.		
	//Этим говнокодом мы скрываем возможность прикрепления реквизитов для всех ненужных способов оплаты.
	paymentType = $('#order-form select[name="oplata_type"]').val();
	$('#order-form select[name="oplata_type"]').bind('change', function() {
		paymentType = $(this, ':checked').val();	
		$('.orderForm .details').hide();
		if (paymentType == '3') {
			$('.orderForm .details').show();
		}
	});
 
	deliveryType = $('#order-form select[name="delivery_type"]').val();
  $('.orderForm .details').hide();
	$('#order-form select[name="delivery_type"]').bind('change', function() {
		deliveryType = $(this, ':checked').val();
		if (deliveryType == '1' && paymentType != '3') {
		$('.orderForm .details').hide();
	}
		//Короче это жесткий говнокод-обработчик для события выбора пункта доставки. Гореть мне в аду за это, но нужно было сделать быстро.
		$('.sbOptions').last().find('li').show();
		if (deliveryType == '1') {
			$('.sbOptions').last().find('li').eq(1).hide();
			$('.sbSelector').last().text($('.sbOptions').last().find('li').eq(0).text());
		};
		
		if (deliveryType == '2') {
			$('.sbOptions').last().find('li').eq(1).hide();
			$('.sbSelector').last().text($('.sbOptions').last().find('li').eq(0).text());
		};
		
		showPriceInfo();
	});


	$("#order-form button[type='submit']").click(function() {

		var form = $('#order-form');
		var name = form.find('input[name="name"]').val();
		var phone = form.find('input[name="phone"]').val();
		var email = form.find('input[name="email"]').val();
		var loading = form.find('.loading');
		var inProcess = $(this).hasClass('in-process');
		var sendButton = $(this);
		var formData = new FormData;

		$.each($('#details')[0].files, function (i, file){
			formData.append('files[]', file);
		});

		if (inProcess)
			return false;

		if (name.length <= 3 || phone.length < 5) {
			alert('Поля, отмеченные звездочкой, обязательны для заполнения');
			return false;
		}			
			$.ajax({
			url : '/ajax/basket.php?method=make_order&'+form.serialize(),
			data: formData,
			type : 'POST',
			contentType: false,
			processData: false,
			dataType: "json",
			beforeSend : function() {

				loading.show();
				form.find('input').attr('disabled', 'disabled');
				form.find('textarea').attr('disabled', 'disabled');
				sendButton.addClass('in-process');
			},
			success : function(data, textStatus, jqXHR) {

			//	console.log(data);
                if (data.bank) {
                    setCookie("order_id", data.order_id, 0, "/");
                    $(".confirmation.popup.rounded .outer.rounded a").attr({"href": "/cart/credite.html"});
                    $(".confirmation.popup.rounded .outer.rounded a").text("Перейти к оформлению кредита");
                }
				// Order accepted popup
				$('.confirmation .name').text(name);
				$('.confirmation .order-code').text(data.order_code);
				$('.confirmation .closenow').remove();

				$('#theme-content .inner').find('.container').eq(1).html('Ваша корзина пуста.');

				$('.confirmation').css({
					'top' : $(document).scrollTop() + $(window).height() / 2 - $('.confirmation').outerHeight() / 2
				});

				$.magnificPopup.open({
							  items: {
								src: '<div class="white-popup">Ваш заказ принят! Код заказа '+data.order_code+'.</div>',
							  },
							  callbacks: {
								
								close: function() {
								  window.location="/";
								}
								// e.t.c.
							  },
							  type: 'inline'

							  // You may add options here, they're exactly the same as for $.fn.magnificPopup call
							  // Note that some settings that rely on click event (like disableOn or midClick) will not work here
							}, 0);

				$('.overlay').unbind('click');

				var goodsArray = new Array();
				for (var i in data.products) {
					goodsArray.push({
						'id' : data.products[i].id,
						'price' : data.products[i].price,
						'name' : data.products[i].converted_title,
						'quantity' : data.products[i].amount,
					});
				}
				
				var rrArray = new Array();
				for (var i in data.products) {
					rrArray.push({
						'id' : data.products[i].id,
						'price' : data.products[i].price,
						'qnt' : data.products[i].amount,
					});
				}
				//console.log(rrArray);
				rrApiOnReady.push(function() {
					try {
						rrApi.order({
							transaction: data.order_code,
							items: rrArray
						});
					} catch(e) {}
				});
				if (email.length >= 3) {
					console.log(email);
				rrApiOnReady.push(function() { rrApi.setEmail(email);	});
				}
			},
			complete : function() {
				loading.hide();
				form.find('input').removeAttr('disabled');
				form.find('textarea').removeAttr('disabled');
				sendButton.removeClass('in-process');
			},
			error : function(jqXHR, textStatus, errorThrown) {

			}
		});
		return false;
		
	});

	//Обычный заказ
	$('#order-forms').bind('submit', function() {
		var form = $('#order-form');
		var name = form.find('input[name="name"]');
		var phone = form.find('input[name="phone"]');
		var loading = form.find('.loading');
		var inProcess = $(this).hasClass('in-process');
		var sendButton = $(this);

		console.log('Load vars');

		if (inProcess)
			return false;

		if (name.val().length <= 3 || phone.val().length < 5) {
			alert('Поля, отмеченные звездочкой, обязательны для заполнения');
			return false;
		}

		$.ajax({
			url : '/ajax/basket.php?method=make_order',
			type : 'POST',
			dataType : 'json',
			data : form.serialize(),
			beforeSend : function() {
				loading.show();
				form.find('input').attr('disabled', 'disabled');
				form.find('textarea').attr('disabled', 'disabled');
				sendButton.addClass('in-process');
			},
			success : function(data, textStatus, jqXHR) {
				// Order accepted popup
				$('.confirmation .name').text(name.val());
				$('.confirmation .order-code').text(data.order_code);
				$('.confirmation .closenow').remove();

				$('#theme-content .inner').find('.container').eq(1).html('Ваша корзина пуста.');

				$('.confirmation').css({
					'top' : $(document).scrollTop() + $(window).height() / 2 - $('.confirmation').outerHeight() / 2
				});

				$('.overlay, .confirmation').show();

				$('.overlay').unbind('click');

				var goodsArray = new Array();
				for (var i in data.products) {
					goodsArray.push({
						'id' : data.products[i].id,
						'price' : data.products[i].price,
						'name' : data.products[i].converted_title,
						'quantity' : data.products[i].amount,
					});
				}
			},
			complete : function() {
				loading.hide();
				form.find('input').removeAttr('disabled');
				form.find('textarea').removeAttr('disabled');
				sendButton.removeClass('in-process');
			},
			error : function(jqXHR, textStatus, errorThrown) {
			}
		});
		return false;
	});

	// Молниеносный заказ
	// Quick buy
	$('a.oneClickBuy').click(function() {
		//наполняем инфо о товаре
		$('.quickBuy .name').attr('href', '').text($(this).data('name'));
		$('.quickBuy .price span').text(moneyFormat($(this).data('price')));
		$('#fastorder-form input').val('');

		$('#fastorder-form input[name="product_id"]').val($(this).data('product_id'));

	
		
		return false;
	});

	$('a.callback-button').click(function() {
		//наполняем инфо о товаре
		$('.callback .name').attr('href', '').text($(this).data('name'));
		$('.callback .price span').text(moneyFormat($(this).data('price')));
		$('#fastorder-form input').val('');

		$('#fastorder-form input[name="product_id"]').val($(this).data('product_id'));

		$('.callback').css({
			'top' : $(document).scrollTop() + $(window).height() / 2 - $('.callback').outerHeight() / 2
		});
		$('.overlay, .callback').show();
		return false;
	});
	
	
	$(".call_back_order").click(function() {
        var e = $(this).parents('form').find('input[name="name"]'),
            t = $(this).parents('form').find('input[name="phone"]');
			if(e.val()!="" && t.val()!="")
			{
				
				$.ajax({
				url: "/ajax/basket.php?method=call_back_order",
				type: "POST",
				dataType: "json",
				success:function(data)
				{
					//var answer = JSON.parse(data);
					if(data['orderCode'])
					{
						$(this).parents('form').find('input[name="name"], input[name="phone"]').val("");
							$.magnificPopup.open({
							  items: {
								src: '<div class="white-popup">Ваш запрос принят в ближайшее время с вами свяжется менеджер!</div>',
							  },
							  type: 'inline'

							  // You may add options here, they're exactly the same as for $.fn.magnificPopup call
							  // Note that some settings that rely on click event (like disableOn or midClick) will not work here
							}, 0);
					}
				console.log(data);	
				},
				data: {
					name: e.val(),
					phone: t.val()
				}
				});	
		
			}
			else
			{
				e.css({"border":"1px solid red"});
				t.css({"border":"1px solid red"});
				alert("Заполните поля!!");
			}
			return false;
   
    });
	
	$("#fastorder-form").bind("submit", function() {
        var e = $(this),
            t = e.find(".loading"),
            n = e.find('input[name="name"]'),
            i = e.find('input[name="phone"]'),
            a = e.find('input[name="email"]'),
            r = e.find('input[name="product_id"]'),
			
            o = $(this);
			console.log(n+i);
			if(n.val()=="" || i.val()=="")
			{
				
				alert("Заполните поля телефон и имя!");
				return false;
			}
       $.ajax({
            url: "/ajax/basket.php?method=make_order",
            type: "POST",
            dataType: "json",
            data: {
                name: n.val(),
                phone: i.val(),
                product_id: r.val(),
                fast_order: 1
            },
            beforeSend: function() {
                n.attr("disabled", "disabled"), i.attr("disabled", "disabled"), a.attr("disabled", "disabled"), o.attr("disabled", "disabled"), t.show(), e.addClass("in-process")
            },
            success: function() {
				
              console.log("Успешно");
                 $.magnificPopup.open({
					midClick: true,
					 items: {
					  src: '<div class="white-popup">Ваш запрос принят в ближайшее время с вами свяжется менеджер!</div>',
					  type: 'inline'
					}});
            },
           
        });
		return false;
    });
});
function getNewSize(oldWidth, oldHeight, destWidth, destHeight) {
	if (destWidth) {
		var iScaleW = destWidth / oldWidth;
	} else {
		var iScaleW = 1;
	}
	if (destHeight) {
		var iScaleH = destHeight / oldHeight;
	} else {
		var iScaleH = 1;
	}

	var iSizeRelation = ((iScaleW < iScaleH) ? iScaleW : iScaleH);
	var iWidthNew = Math.round(oldWidth * iSizeRelation);
	var iHeightNew = Math.round(oldHeight * iSizeRelation);

	var iSizeW = iWidthNew;
	var iSizeH = iHeightNew;
	iDestX = 0;
	iDestY = 0;

	return {
		"width" : iSizeW,
		"height" : iSizeH
	};
}

function moneyFormat(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ' ' + '$2');
	}
	return x1 + x2;
}

function declOfNum(number, titles) {
	cases = [2, 0, 1, 1, 1, 2];
	return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}

function intVal(mixed_var, base) {
	var tmp;

	if ( typeof (mixed_var ) == 'string') {
		tmp = parseInt(mixed_var);
		if (isNaN(tmp)) {
			return 0;
		} else {
			return tmp.toString(base || 10);
		}
	} else if ( typeof (mixed_var ) == 'number') {
		return Math.floor(mixed_var);
	} else {
		return 0;
	}
}