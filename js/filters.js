function reloadProducts(page, sort, savePrev) {
	console.log(page);
	var formId = '#filter-form';
	var productsFieldId = '.tovarCont';
	
	var loadingImage = $('#filter-loading').clone();
	var ss;
    var defaultPage = defaultPage || 1 ;
    var defaultSort = defaultSort || 'none' ;
	if (page == undefined) 
		page = defaultPage;
	if (sort == undefined) {
		sort = defaultSort;
	}
	if (savePrev == undefined) {
		savePrev = false;
	}
	
	$(formId).ajaxSubmit({
		data : {"page" : page, "sort" : sort, "savePrev":savePrev},
		beforeSend : function () {
			$(productsFieldId).css('opacity', .3);
			//show loading
			/* loadingImage.appendTo('body');
			loadingImage.css({
				'display' : 'block',
				'position' : 'absolute',
				'left' : $(productsFieldId).position().left + 20,
				'top' : $(productsFieldId).position().top
			});
			ss = $('body, html').mousemove( function (e) {
				loadingImage.css({
					'left' : (e.pageX + 10) + 'px',
					'top' : (e.pageY + 20) + 'px'
				});
			}); */
		},
		success : function ( data, statusText, xhr, element) {
				if(savePrev){
					$('#loadMore').before(data);
				}else{
					$(productsFieldId).empty().append(data);
					//$('#loadMore').data('page', 1).show();
				}// $(productsFieldId).empty().append($('<ul>').addClass('catalogue')).find('.catalogue').append(data);
				
				// var paginDiv = $(productsFieldId).find('.navigation').clone();
				$(productsFieldId).find('.navigation').remove();
				
				// $('#paginator-container').html(paginDiv);
				
				$(productsFieldId).css('opacity', 1);
				
				//Hide loading
				/* ss.unbind('mousemove');
				loadingImage.remove(); */
				
				ajaxMode = true;
				
				
				
				
				//Регистрируем функцию на сортировку
				/* $('#sort-filter').unbind('change').bind('change', function() {
					var sort = $(this, ':checked').val();
					reloadProducts(page, sort);
					defaultSort = sort;
					return false;
				}); */
		}
	});
	return false;
}

$(function() {

	var price = price || [];

	console.log("filters");
	
	$('#sort-filter').bind('change', function() {
		
					var sort = $(this).find('option.active').val();
					console.log(sort);
					reloadProducts(1, sort);
					defaultSort = sort;
					return false;
					
				});
	$( "#price-slider" ).slider({
		range: true,
		min: price.min,
		max: price.max,
		step: 50,
		values: [ price.min, price.max ],
		slide: function( event, ui ) {
			$( "#price-min" ).val( ui.values[ 0 ]);
			$( "#price-max" ).val( ui.values[ 1 ]);
		},
		stop: function() { reloadProducts(); }
	});
	$( "#price-slider" ).slider( "values",0, $("#price-min").val() );
	$( "#price-slider" ).slider( "values",1, $("#price-max").val() );
	$('#price-min').keypress(function(event){
			var keyCode = event.keyCode ? event.keyCode :
			event.charCode ? event.charCode :
			event.which ? event.which : void 0;
	 
			//if pressed "Enter" key
			if(keyCode == 13){
				if(parseInt($(this).val())>parseInt($("#price-max").val().replace(' ',''))){$(this).val(parseInt($("#price-max").val().replace(' ','')));}
				$( "#slider-range" ).slider( "values",0, $(this).val() );
				defaultSort = 'price_asc';
				reloadProducts(1, $('#sort-filter', ':checked').val());
			}
	});
	$('#price-max').keypress(function(event){
			var keyCode = event.keyCode ? event.keyCode :
			event.charCode ? event.charCode :
			event.which ? event.which : void 0;
	 
			//if pressed "Enter" key
			if(keyCode == 13){
				if(parseInt($(this).val())<parseInt($("#price-min").val().replace(' ',''))){$(this).val(parseInt($("#price-min").val().replace(' ','')));}
				$( "#slider-range" ).slider( "values",1, $(this).val() );
				defaultSort = 'price_asc';
				reloadProducts(1, $('#sort-filter', ':checked').val());
			}
	});
	$( "#price-min" ).change(function(){
		if(parseInt($(this).val())>parseInt($("#price-max").val().replace(' ',''))){$(this).val(parseInt($("#price-max").val().replace(' ','')));}
		$( "#slider-range" ).slider( "values",0, $(this).val() );
		defaultSort = 'price_asc';
		reloadProducts();
	});
	$( "#price-max" ).change(function(){
		if(parseInt($(this).val())<parseInt($("#price-min").val().replace(' ',''))){$(this).val(parseInt($("#price-min").val().replace(' ','')));}
		$( "#slider-range" ).slider( "values",1, $(this).val() );
		defaultSort = 'price_asc';
		reloadProducts(1, $('#sort-filter', ':checked').val());
	});
	
	//Checkbox
	$('.hidden-checkbox').css('opacity', 0);
	$('.hidden-checkbox').click(function() {
		if ($(this).is(':checked')) {
			$(this).parent('.filter-checkbox').removeClass('filter-checkbox-off filter-checkbox-on').addClass('filter-checkbox-on');
		} else {
			$(this).parent('.filter-checkbox').removeClass('filter-checkbox-off filter-checkbox-on').addClass('filter-checkbox-off');
		}
		reloadProducts(1, $('#sort-filter', ':checked').val());
	});
});