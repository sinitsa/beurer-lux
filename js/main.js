//var price = { 'min':0, 'max':0 };
function setCookie (name, value, expires, path, domain, secure) {
    document.cookie = name + "=" + escape(value) +
    ((expires) ? "; expires=" + expires : "") +
    ((path) ? "; path=" + path : "") +
    ((domain) ? "; domain=" + domain : "") +
    ((secure) ? "; secure" : "");
}
function getCookie(c_name) {
    if (document.cookie.length > 0 ) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length+1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
}

$(document).ready(function(){

	// Tips
	$('ul.tips img').hover(function(){
		var text = $(this).attr('alt');
		$(this).after('<div class="tipsPopup">' + text + '</div>');
		$(this).siblings('.tipsPopup').animate({
			'opacity': 1,
			'bottom': '-=12'
		},200);
	},function(){
		$(this).siblings('.tipsPopup').remove();
	});
	
	// Buy link
	$('.buttonInBasket').click(function(){
		if(!$(this).is('.active')){
			$(this).addClass('active');
			$(this).text('В корзине');
			$('#add_product_form img').attr("src","/upload/small/"+$(this).data('product_id')+".jpg");
			$('#add_product_form .price span').text($(this).data('product_price'));
			$('#add_product_form .name').text($(this).data('name'));
			$('#add_product_form .cont_link a').attr("href", $(this).parents('form').find('.prdbrief_name a').attr('href'));
			addProductToBasket($(this).data('product_id'), $(this).data('product_price'));
			
			return false;
		}
	});
	
	
		$('body').magnificPopup({
			delegate:".video_small a, a.video_listing",
			disableOn: 700,
			type: 'iframe',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,

			fixedContentPos: false
		});
		
	/* 	if($('.video_small iframe').is('iframe')){
	
	var video_id=$('.video_small iframe').attr("src").replace("https://www.youtube.com/embed/", "");
	var ab = "<a href='http://www.youtube.com/watch?v="+video_id+"'><img src='https://i.ytimg.com/vi/"+video_id+"/hqdefault.jpg'><div class='podl'></div></a>";
			$('.video_small').html(ab);
			
		
	} */

	// Intro
	$div = null;
	$('.thumbs').children().each(function(i){
		if (i % 8 == 0) {
			$div = $('<div />');
			$div.appendTo('.thumbs');
		};
		$(this).appendTo($div);
		$(this).addClass('itm'+i);
		$(this).click(function() {
			$('ul.productsSlider').trigger('slideTo', [i, 0, true]);
		});
	});
	$('.thumbs span.itm0').addClass('selected');
	
	$('ul.productsSlider').after('<div class="arrow prev" /><div class="arrow next" />').carouFredSel({
		circular: false,
		infinite: false,
		width: 760,
		height: 443,
		items: 1,
		auto: false,
		prev: '.intro .prev',
		next: '.intro .next',
		scroll: {
			fx: 'directscroll',
			onBefore: function() {
				var pos = $(this).triggerHandler('currentPosition');
				$('.thumbs span').removeClass('selected');
				$('.thumbs span.itm'+pos).addClass('selected');
				var page = Math.floor(pos / 8);
				$('.thumbs').trigger('slideToPage', page);
			}
		}
	});
	
	$('.thumbs').after('<div class="pager" />').carouFredSel({
		circular: false,
		infinite: false,
		width: 182,
		height: 364,
		items: 1,
		align: false,
		auto: false,
		pagination: '.intro .pager'
	});
	$('.next').click(function(){
		
		$.magnificPopup.close();
		return false;
	});
	// Custom select
	/* $('.sort select, .deonryMethod select').selectbox({
		speed: 0,
		onOpen: function(){
			$(this).next().addClass('focus');
		},
		onClose: function(){
			$(this).next().removeClass('focus');
		}
	}); */
	/* 
	$('#sort-filter').bind('change', function() {
		var uri = $(this).find('option:checked').data('uri');
		window.location = uri;
	}); */
	

	// Sliders
	// Price slider
	
	
	
	//Range slider
	$('.range-slider').each(function(index, element) {
		var _this = $(element);
		var sliderBody = _this.find('.slider-body');
		
		var dataField = _this.find('.slider-field');
		var minValue = dataField.data('min');
		var maxValue = dataField.data('max');
		var paramId = dataField.data('param_id');
		
		sliderBody.slider({
			range: true,
			min: minValue,
			max: maxValue,
			step: 1,
			values: [minValue, maxValue],
			slide : function(event, ui){
				sliderBody.find(".weight-min").val(ui.values[0]);
				sliderBody.find(".weight-max").val(ui.values[1]);

				_this.find(".weight-min").val(ui.values[0]);
				_this.find(".weight-max").val(ui.values[1]);
			},
			change : function(event, ui) {
				dataField.empty();
				dataField.append(
					$('<input>').attr({
						"type" : "hidden",
						"name" : "filter_range[" + paramId + "][min]",
						"value" : ui.values[0]
					})
				);
				dataField.append(
					$('<input>').attr({
						"type" : "hidden",
						"name" : "filter_range[" + paramId + "][max]",
						"value" : ui.values[1]
					})
				);
				reloadProducts();
			}
		});
		sliderBody.find('.ui-slider-range').append('<input type="text" class="weight-min" disabled /><input type="text" class="weight-max" disabled />');
		sliderBody.find('.weight-min').val(sliderBody.slider('values', 0));
		sliderBody.find('.weight-max').val(sliderBody.slider('values', 1));
	});
	
	$('.filters-field .checkbox-param').bind('click', function(){
		var _this = $(this);
		
		if (_this.hasClass('active')) {
			_this.removeClass('active');
			_this.find('input[type="checkbox"]').removeAttr('checked');
		} else {
			_this.addClass('active');
			_this.find('input[type="checkbox"]').attr('checked', 'checked');
		}
		reloadProducts();
	});
	
	// On change inputs
	$('.range-min').change(function(){
		var wrapper = $(this).closest('.range-slider');
		var sliderBody = wrapper.find('.slider-body');
		
		var newValue = $(this).val();
		var min = sliderBody.slider('option', 'min');
		var max = sliderBody.slider('option', 'max');
		var values = sliderBody.slider('option', 'values');
		if(newValue <= values[1]){
			if(newValue >= min){
				sliderBody.slider('option', 'values', [ newValue, values[1] ]);
				wrapper.find('.weight-min').val(newValue);
			}else{
				sliderBody.slider('option', 'values', [ min, values[1] ]);
				wrapper.find('.weight-min').val(min);
			}
		}else{
			if(newValue <= max){
				sliderBody.slider('option', 'values', [ newValue, newValue ]);
				wrapper.find('.weight-min').val(newValue);
				wrapper.find('.weight-max').val(newValue);
			}else{
				sliderBody.slider('option', 'values', [ max, max ]);
				wrapper.find('.weight-min').val(max);
				wrapper.find('.weight-max').val(max);
			}
		}
	});
	$('.range-max').change(function(){
		var wrapper = $(this).closest('.range-slider');
		var sliderBody = wrapper.find('.slider-body');
		
		var newValue = $(this).val();
		var min = sliderBody.slider('option', 'min');
		var max = sliderBody.slider('option', 'max');
		var values = sliderBody.slider('option', 'values');
		if(newValue >= values[0]){
			if(newValue <= max){
				sliderBody.slider('option', 'values', [ values[0], newValue ]);
				wrapper.find('.weight-max').val(newValue);
			}else{
				sliderBody.slider('option', 'values', [ values[0], max ]);
				wrapper.find('.weight-max').val(max);
			}
		}else{
			if(newValue >= min){
				sliderBody.slider('option', 'values', [ newValue, newValue ]);
				wrapper.find('.weight-min').val(newValue);
				wrapper.find('.weight-max').val(newValue);
			}else{
				sliderBody.slider('option', 'values', [ min, min ]);
				wrapper.find('.weight-min').val(min);
				wrapper.find('.weight-max').val(min);
			}
		}
	});
	
	// Filter
	function xfilterToggle () {
		if($('.filter').is('.open')){
			$('.filterToggle').find('span').text('Больше параметров');
			$('.filter .hidden').slideUp();
			$('.filter').removeClass('open');
		}else{
			$('.filterToggle').find('span').text('Скрыть параметры');
			$('.filter .hidden').slideDown();
			$('.filter').addClass('open');
		}
	}
	$('.xfilterToggle').click(function(){
		xfilterToggle();
	});
	
	if ($('.xfilter').is('.open')) {
		$('.xfilter').removeClass('open');
		xfilterToggle();
	}
	
	// Catalogue description text
	var timer = null;
	$('ul.catalogue>li').on({
        mouseenter: function() {
			var _this = $(this);
			if(_this.position().left >= 816){
				$(this).addClass('leftDescription');
			};
			timer = setTimeout(function(){
				_this.addClass('active');
			},500);
        },
        mouseleave: function() {
			clearTimeout(timer);
			$(this).removeClass('active');
           }
       }
    );
	/*
	$('ul.catalogue>li').hover(function(){
		var _this = $(this);
		if(_this.position().left >= 816){
			$(this).addClass('leftDescription');
		};
		timer = setTimeout(function(){
			_this.addClass('active');
		},500);
	},function(){
		clearTimeout(timer);
		$(this).removeClass('active');
	});
	*/
	// Product photo thumbs carousel
	/*
	$('#carousel').carouFredSel({
		items: 1,
		width: 940,		
		circular: true,         // Определяет, должен ли быть слайдер круговым
		infinite: true, 
		auto: {play: false},
		direction: "left",
		pagination: {
        container: '#pager'},
		scroll:{
			items: 1,
			duration: 1000,
			fx:'fade'
		}
	});
	*/
	$('.popular_products .tovarCont').carouFredSel({
		items: 4,
		
		circular: true,         // Определяет, должен ли быть слайдер круговым
		infinite: true, 
		auto: {play: false},
		direction: "left",
		pagination: {
        container: '#pager_popular'},
		scroll:{
			items: 4,
			duration: 1000,
			
		}
	});
		$('.small_fhoto_cont ul').carouFredSel({
		items: 4,
		
		circular: true,         // Определяет, должен ли быть слайдер круговым
		infinite: true, 
		auto: {play: false},
		direction: "left",
		prev: "#cl_left",
        next: "#cl_right",
		scroll:{
			items: 4,
			duration: 1000,
			
		}
	});
	
	$(".small_fhoto_cont li a").click(function(){
		var small = $(this);
		$(".small_fhoto_cont li").removeClass('selected');
		$(this).parent('li').addClass('selected');
		
		
			
			var im = new Image();
			
			im.onload = function(){
				
					$(".big_fhoto_cont a.full_fhoto img").fadeOut(function(){
						
					$(".big_fhoto_cont a.full_fhoto").attr("href", im.src);
					$(".big_fhoto_cont a.full_fhoto img").attr("src", im.src);
					$(".big_fhoto_cont a.full_fhoto img").fadeIn();	
					});
					
					
			};
			im.src = small.data("original");
		
		
		return false;
	});
	
	
	/* $('.mPhotoThumbs ul li:first-child').addClass('current').append('<span class="mask" />'); */
	/* $('.mPhotoThumbs ul a').click(function(){
		//if(!$(this).parent('li').is('.current')){
			var curImg = $('<img src="' + $(this).attr("href") + '" alt="' + $(this).attr("alt") + '" style="display:none;">');
			$(this).parents('.mPhotoThumbs ul').find('li.current').removeClass('current').find('.mask').remove();
			$(this).parent('li').addClass('current').append('<span class="mask" />');
			var links = new Array();
			$(this).closest('li').siblings().each(function (index, el) {
				links.push( $(el).find('a').data('original') );
			});
			var a = $('<a>').attr('href', $(this).data('original')).attr('rel', 'fancygroup').addClass('fgallery');
			a.append(curImg);
			
			var container = $(this).parents('.photos').find('.mPhoto');
			
			container.empty();
			container.append(a);
			container.append('<div class="loader" />');
			
			curImg.load(function(){
				$(this).fadeIn(200);
				//$(this).siblings('img').remove();
				//$(this).siblings('.loader').remove();
				$(this).closest('a').siblings().remove();
				
				for (var i in links) {
					container.append(
						$('<a>').attr('href', links[i]).attr('rel', 'fancygroup').addClass('fgallery').css('display','none')
					);
				}
				$("a.fgallery").fancybox();
			});
		//};
		return false;
	}); */
	
	$('.mPhotoThumbs ul a:first').trigger('click');
	// Tabs
	
	
	$('.overlay, .popup .close').click(function(){
		$('.overlay, .popup').hide();
	});
	
	// Order
	$('.order a.doneBtn').click(function(){
		if(!$(this).is('.disabled')){
			$(this).addClass('disabled');
			$('.order .orderForm').slideDown();
		}
		return false;
	});
	
	//Order delete button
	$('#cart-items .basket-item .delete a').click(function(){
	
			$(this).parents('.basket-item').remove();
			recalculateBasket();
		
		
		
		return false;
	});


	//show all tags on click
	$('#showTags').click(function(){

		var hidden = $('.tags_hidden');

		var tags = $('.tags_slide').height();

		if (hidden.hasClass("tags_show"))
		{

			hidden.animate({"max-height" : "440px"}, 600);
			$(this).html('Показать еще');

		}
		else
		{
			hidden.animate({"max-height" : tags}, 600);
			$(this).html('Скрыть');
		}

		hidden.toggleClass("tags_show");

	});

	
	//add feedback
	$('.add-feed-button a').click(function(){
		
		$('#feed-back-form input[name="product_id"]').val( $('.add-feed-button').data('product_id') );
		
		$('.feed-back-popup').css({
			'top': $(document).scrollTop() + $(window).height()/2 - $('.quickBuy').outerHeight()/2
			});
		$('.overlay, .feed-back-popup').show();
		
		var win = $('.feed-back-popup');
		var form = $('#feed-back-form');
		var loading = win.find('.loading');
		
		form.show();
		win.find('.success').hide();
		win.find('.send-button').show();
		
		win.find('.send-button').unbind('click').bind('click', function(){
			loading.show();
			
			
			//var formData = form.serialize();
			
			$.ajax({ 
				url : '/ajax/system.php',
				data : form.serialize(),
				type : "post",
				dataType : "json",
				beforeSend : function () {
					form.find('input, textarea').attr('disabled', 'disabled');
					win.find('.error').hide();
				},
				success: function(data) {
					if (!data.error) {
						//win.find('.close').trigger('click');
					
						form.find('input, textarea').each(function(index, el){
							$(el).val(el.defaultValue);
						});
						form.hide();
						win.find('.success').show();
						win.find('.send-button').hide();
					} else {
						win.find('.error-code-' + data.error_code).show();
					}
				},
				complete : function() {
					loading.hide();
					form.find('input, textarea').removeAttr('disabled');
				}
			});
		});
		
		return false;
	});
	
	
	// Placeholder
	$("#tabs").tabs();
	
	$('.buttonInBasket').magnificPopup({
	type:'inline',
	 midClick: true,
	 items: {
      src: '#add_product_form',
      type: 'inline'
	}
	
	});
	$('.full_fhoto').magnificPopup({
		type:'image',
	});
	
	
	
	
	$(".oneLinkBuy").click(function(){
		
		var forml = $("#fastorder-form");
		forml.find('input[name="product_id"]').val($(this).data('product_id'));
		$("#linkBuyClick .name").text($(this).data('name'));
		$("#linkBuyClick .price span").text($(this).data('product_price'));
		
		$.magnificPopup.open({
					midClick: true,
					 items: {
					  src: '#linkBuyClick',
					  type: 'inline'
					}});
	});
	
	
	$('.call_back').magnificPopup({
					midClick: true,
					 items: {
					  src: '#ans',
					  type: 'inline'
					}});
	
	
	$('.tovarCont').on('click','#loadMore', function() {
        var next = $(this).data('page') + 1;
		
        $(this).data('page', next);
        if (next == $(this).data('pagescount')) {
            $(this).hide();
        }
        return reloadProducts(next, undefined, true);
    });
	
	$('.new_filters input').click(function(){
		
		//console.log($(this).val());
		$('#sort-filter option').removeClass('active').eq($(this).val()).addClass('active');
		$('#sort-filter').change();
	});
	
	var recalcTimer;
	$('.k_font, .deliveryMethod input').click(function(){
		
		clearTimeout(recalcTimer);
		var tec = parseInt($(this).siblings(".amount-input").val());
		//console.log(tec);
		if($(this).hasClass('k_inc'))
		{
			tec++;
			
		}
		else
		{
			if(tec!=1)
			{
			tec--;
			}
		}
		//console.log(tec);
		$(this).siblings(".amount-input").val(tec);
		recalcTimer = setTimeout('recalculateBasket();', 1000);
		
		
	});
	
	$('#searchBox').keyup(function(){
		autocomplet($(this),1);
	});
	
	
	// autocomplet : this function will be executed every time we change the text
	function autocomplet(e,index) {
		var min_length = 2; // min caracters to display the autocomplete
		var keyword = e.val().replace(/[ ]+/g,' '); // убираем лишние пробелы
		if (keyword.length >= min_length) {
			var post_data = {keyword : keyword};

			$.ajax({
				url: '/ajax/products.php?action=autocomplete',
				type: 'POST',
				data: post_data,
				success:function(data){
					//console.log(data);
					
                $('#search_sugestion_'+index).html(data);
                $('#search_sugestion_'+index).slideDown(400);
                $('#search_sugestion_'+index).mouseleave(function(){
                    $(this).slideUp(400);
                    return false;
                });

                $('.search-categories-menu').mouseenter(function(){
                    $('#search_sugestion_'+index).slideUp(0);
                    return false;
                });
				
				}
			});
		} else {
			$('#search_sugestion').hide();
		}
	}

	// маска инпута в форме заказа оптового прайса
	$('.wholesaler-request').find('input[name="phone"]').mask('+7 (999) 999-99-99');

	//запрос оптового прайса
    $('.wholesaler-request').on('submit', function(e){
    	e.preventDefault();

        var form = $(this),
            name = form.find('input[name="name"]').val(),
            phone = form.find('input[name="phone"]').val(),
            email = form.find('input[name="email"]').val(),
            extra = form.find('textarea').val();

        if (form.find('input[type="submit"]').hasClass('disabled')) {
            return false ;
        }

        var params = {
            name:name,
            phone:phone,
            email:email,
            extra_information:extra,
        };

        $.ajax({
            url:'/ajax/basket.php?method=wholesaler',
            data:params,
            method:'post',
            dataType:'json',
            beforeSend:function(){
                form.find('input[type="submit"]').addClass('disabled');
            },
            success:function(data){
                console.log(data);
                if (data.orderId) {
                    alert(name+', ваш запрос успешно отправлен!');
                    setTimeout(function(){
                        location.reload();
                    }, 1500);
                } else {
                    alert(name+', ошибка запроса! Введите правильно данные, или повторите запрос позже.');
                    console.log(data);
                }
            },
            complete:function(){
                form.find('input[type="submit"]').removeClass('disabled');
            }
        });
	});


    // показываем верхнюю плашку при скролле
    $(window).scroll(function() {
        var mainPanel = $('.top_scroll');
        var navheight = 159;

        if ($(this).scrollTop() > navheight && mainPanel.hasClass("default")) {
            mainPanel.removeClass("default").addClass("fixed");
        } else if ($(this).scrollTop() <= navheight && mainPanel.hasClass("fixed")) {
            mainPanel.removeClass("fixed").addClass("default");
        }
    });


    //кнопка перехода на мобильную версию
	$('.to_mobile').click(function(){
		setCookie('theme',2,0,'/');
		location.reload();
	});
});