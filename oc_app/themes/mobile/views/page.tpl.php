 <div class="top">
	<div>
		<a class="back" href="javascript: void(0);">Назад</a>
		<!-- <a href="javascript: void(0);">Фильтр</a> -->
	</div>
</div>
<div class="container">
	<ul class="breadcrumbs">
		<li><a href="/">Главная</a> </li>
		<?php foreach ($data['breadcrumbs'] as $value): ?>
			<li>&nbsp;- <a href="<?=$value['link']?>"><?=$value['title']?></a></li>
		<?php endforeach ?>
	</ul>
	<div class="text_container">
		<?=$data['text']?>
	</div>
</div>
<div>
<div class="clearfix bottom_separator"></div>
<div class="container">
	<?php
        //  блок информации о сайте: телефоны, время работы, способы оплаты
        include 'widgets/site_info.tpl.php'
    ?>
</div>

<!-- временный Костыль -->
<style>
	.text_container .map-wrap {
		display: none;
	}
	.text_container .delivery_block {
		width: 80%;
		float: none;
		padding: 0px; 
		height: 350px;
		overflow: hidden;
		height: 350px;
		margin: 30px auto;
	}
	.text_container .delivery_block div {
		box-sizing: border-box;
		width: 100%;
		height: 50px;
	}
	.text_container img {
		float: none !important;
		display: block;
		margin: 15px auto !important;
		height: auto !important;
		max-width: 100%;
	}
	.text_container .delivery_block ul.cities_list {
		width: 70%;
		box-sizing: border-box;
		height: 300px;
		overflow-x: scroll;
	}
	.text_container p {
		margin: 10px 0px;
	}
	.text_container ul {
		padding-left: 10px;
		list-style-position: inside;
	}
	.text_container .contacts1 {
		text-align: center;
	}
	.text_container #map {
		display: none;
	}
</style>