	<div class="top">
	<div>
		<a class="back" href="javascript: void(0);">Назад</a>
		<!-- <a href="javascript: void(0);">Фильтр</a> -->
	</div>
</div>
<div class="container">
	<ul class="breadcrumbs">
		<li><a href="/">Главная</a> </li>
		<?php foreach ($data['breadcrumbs'] as $value): ?>
			<li>&nbsp;- <a href="<?=$value['link']?>"><?=$value['title']?></a></li>
		<?php endforeach ?>
	</ul>
	<h2 class="cat_title">Ваша корзина (<span class="count_order"><?=$data['title'] ?></span>)</h2>
</div>
<div>
	<form action="" method="POST" class="make_order" style="<?=($data['basket']['count'] <= 0) ? "display:none;" : ""?>">
		<div class="list">
			<?php foreach ($data['products'] as $key => $item): ?>
				<div class="item">
					<div class="head">
						<?=$item['title']?>
					</div>
					<div class="body">
						<a href="/shop/view_goods/<?=$item['chpu']?>.html" class="image">
							<img  class="lazy" src="/upload/small/<?=$item['id']?>.jpg" alt="<?=$item['title']?>">
						</a>
						<div class="control">
							<div>
								<a href="javascript: void(0);" class="dec"></a>
								<input type="text" data-id="<?=$item['id'] ?>" name="count" value="<?=$item['count'] ?>">
								<a href="javascript: void(0);" class="inc"></a>
							</div>
							<div>
								<a class="del_product" data-title="<?=$item['title']?>" data-cat_title="<?=$item['cat_title']?>" data-id="<?=$item['id']?>" data-price="<?=$item['price']?>"href="javascript: void(0);">удалить товар</a>
							</div>
						</div>
						<div class="price">
							<?=number_format($item['price'], 0, ',', ' ')?> руб.
						</div>
					</div>
				</div>
			<?php endforeach ?>
		</div>
		<div class="list_summ">
			<div></div>
			<div>
				<p>Сумма заказа:</p>
				<p><span class="tsumm"><?=number_format($data['products_summ'], 0, ',', ' ')?></span> руб.</p>
			</div>
		
		</div>
		
		<div class="container">
			<h3>Покупатель:</h3>
			<div class="info">
				<input type="text" class="inp_name" name="name" placeholder="Имя *">
				<input type="tel" class="inp_phone" name="phone" placeholder="+7 (___) ___-__-__*">
				<input type="email" class="inp_email" name="email" placeholder="Еmail:">
				<input type="text" class="inp_adress" name="adress" placeholder="Адрес доставки:">
				<?php /* <input type="number" class="inp_index" name="index" placeholder="Индекс:"> */ ?>
			</div>
			<?php /*
			<h3>Оплата:</h3>
			<div class="pay_type">
				<div class="check_pay_type hidden" data-type="2">
					<div class="icon bk"></div>
					<p>Оплата банковской картой</p>
				</div>
				
				<div class="check_pay_type active" data-type="1">
					<div class="icon nal"></div>
					<p>Оплата наличными</p>		
				</div>	
				<div class="check_pay_type" data-type="3">
					<div class="icon bk"></div>
					<p>Безналичный расчет</p>
				</div>
			</div>
			*/ ?>
			<h3>Доставка:</h3>
			<div class="delivery_type">
				<div class="delivery_wrapper min">
					<a href="javascript: void(0)" class="control2"></a>
					<ul class="dropdown ">
					<?php foreach($data['delivery_types'] as $id => $name): 
					
							$active = $id == 1 ? 'active' : '' ; 
							echo '<li data-id="' . $id . '" class="del_type_item ' . $active . '">' . $name . '</li>' ;
							
						endforeach; ?>
					</ul>
				</div>
			</div>
			<h3>Комментарий к заказу:</h3>
			<div class="comment_order">
				<textarea name="order_comment" cols="30" rows="10" placeholder="Комментарий к заказу"></textarea>
			</div>
		
			<div class="finish_summ">
				<div>
					<div>
						Общая сумма:
					</div>
					<div>
						<span class=""><span class="tsumm red"><?=number_format($data['products_summ'], 0, ',', ' ')?></span> руб.</span>
					</div>
				</div>
				<div class="delivery_info" >
					<div >
						Доставка:
					</div>
					<div class="delivery_summ">
						<?php $del =  ( $data['delivery_price'] == 0 ) ? 'Бесплатно' : $data['delivery_price'] ; ?>
							<span class="red"><?=$del ?></span> <? if(is_numeric($del)){ echo "руб."; } ?>
						
					</div>
				</div>
			</div>
			<p class="error_warrning">
				При заполнении формы были допущены ошибки:
			</p>
			<ul class="error_messages">
				<li class="name_mes_warrning">Имя введено не верно, или отсутствует</li>
				<li class="phone_mes_warrning">Телефон введен не верно</li>
				<li class="email_mes_warrning">email введен не верно</li>
				<li class="email2_mes_warrning">при выборе оплаты банковской картой обазательно указывается Email</li>
			</ul>
			<a href="javascript: void(0);" class="finish_order_btn">Заказать</a>
			<input type="hidden" name="orderid" value="">
		</div>
	</form>
</div>
<div class="bottom_separator"></div>
<div class="container">
	<?php
        //  блок информации о сайте: телефоны, время работы, способы оплаты
        include 'widgets/site_info.tpl.php'
    ?>
</div>