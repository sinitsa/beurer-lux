<?php 
    $sort = filter_input(INPUT_GET, 'sort');
?>

<div class="top">
	<div>
		<a class="back" href="javascript: void(0);">Назад</a>
		<!-- <a href="javascript: void(0);">Фильтр</a> -->
	</div>
</div>
<div class="container">
	<ul class="breadcrumbs">
		<li><a href="/">Главная</a> </li>
		<?php foreach ($data['breadcrumbs'] as $value): ?>
			<li>&nbsp;- <a href="<?=$value['link']?>"><?=$value['title']?></a></li>
		<?php endforeach ?>
	</ul>
	<h1 class="cat_title"><?=$data['title'] ?></h1>
	<div class="sort_filters">
		<?php if ($sort == 'news_sale'): ?>
			<a href="javascript: void(0);" data-sort_type="news_sale" class="by_rating active">По популярности</a>
			<a href="javascript: void(0);" data-sort_type="price_asc" class="by_price">По цене</a>
			<a href="javascript: void(0);" data-sort_type="price_asc" class="arrow_up" href="javascript: void(0);"></a>
			<a href="javascript: void(0);" data-sort_type="price_desc" class="arrow_down" href=""></a>
		<?php elseif ($sort == 'price_asc'): ?>
			<a href="javascript: void(0);" data-sort_type="news_sale" class="by_rating">По популярности</a>
			<a href="javascript: void(0);" data-sort_type="price_asc" class="by_price active">По цене</a>
			<a href="javascript: void(0);" data-sort_type="price_asc" class="arrow_up active" href="javascript: void(0);"></a>
			<a href="javascript: void(0);" data-sort_type="price_desc" class="arrow_down" href=""></a>
		<?php elseif ($sort == 'price_desc'): ?>
			<a href="javascript: void(0);" data-sort_type="news_sale" class="by_rating">По популярности</a>
			<a href="javascript: void(0);" data-sort_type="price_asc" class="by_price active">По цене</a>
			<a href="javascript: void(0);" data-sort_type="price_asc" class="arrow_up" href="javascript: void(0);"></a>
			<a href="javascript: void(0);" data-sort_type="price_desc" class="arrow_down active" href=""></a>
		<?php else: ?>
			<a href="javascript: void(0);" data-sort_type="news_sale" class="by_rating">По популярности</a>
			<a href="javascript: void(0);" data-sort_type="price_asc" class="by_price">По цене</a>
			<a href="javascript: void(0);" data-sort_type="price_asc" class="arrow_up" href="javascript: void(0);"></a>
			<a href="javascript: void(0);" data-sort_type="price_desc" class="arrow_down" href=""></a>
		<?php endif ?>
	</div>
	<ul class="catlist">
		<?php
        foreach ($data['products'] as $item){
            include('catalog_element_list.tpl.php');
        }
        ?>
	</ul>
    <?php if ($data['paginator']['max'] > 1): ?>
    <div class="paginator"> 
    	<?php if ($data['paginator']['page'] > 1): ?>
    		<a data-to="<?=$data['paginator']['page']-1 ?>" class="prev" href="javascript: void(0)";><</a>
    	<?php endif ?>
        
        <?php for ($i = $data['paginator']['start'] ; $i <= $data['paginator']['end']; $i++): ?>
        	<a data-to="<?=$i?>" class="<?=$data['paginator']['page'] == $i ? 'active' : ''?>" href="javascript: void(0);"><?=$i?> </a>
        <?php endfor;?>
        <?php if ($data['paginator']['page'] < $data['paginator']['max']): ?>
        	<a data-to="<?=$data['paginator']['page']+1 ?>" class="next" href="javascript: void(0);">></a>
        <?php endif ?>
        
    </div>	
    <?php endif ?>
   
	<!--<a class="show_all_products" href="javascript:void(0);" ><span> Показать еще</span> <span class="arrow">&nbsp;</span></a>-->

    <?php
        //  блок информации о сайте: телефоны, время работы, способы оплаты
        include 'widgets/site_info.tpl.php'
    ?>
</div>