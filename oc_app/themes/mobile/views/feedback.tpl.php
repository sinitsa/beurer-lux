<div class="top">
	<div>
		<a class="back" href="javascript: void(0);">Назад</a>
		<!-- <a href="javascript: void(0);">Фильтр</a> -->
	</div>
</div>

<div class="container">
	<ul class="breadcrumbs">
		<li><a href="/">Главная</a> </li>
		<?php foreach ($data['breadcrumbs'] as $value): ?>
			<li>&nbsp;- <a href="<?=$value['link']?>"><?=$value['title']?></a></li>
		<?php endforeach; ?>
	</ul>
	
	<div class="feedback-wrapper">
		<div class="hidden warning_message">Все поля формы должны быть заполнены!</div>
		<form action="" method="POST" id="feedback" >
				<input class="inp_name" id="username" name="username" type="text"placeholder="Ваше Имя:"/>
				<input class="inp_mail" id="usermail" name="usermail" type="mail"  placeholder="Ваш е-mail:" />
				<textarea class="form-control" name="usermsg" placeholder="Текст сообщения:" ></textarea>
				<input type="hidden" class="hidden" name="success" value="">
				<a href="javascript: void(0);" class="addComment__send" >Отправить</a>
		</form>
		
		<div class="feedback">
			<?php foreach($data['feedbacks'] as $feedback ): ?>
				<div class="feedback_each">
					<div class="feed_head">
						<div class="feed_name"><?=$feedback['name']?></div>
						<div class="feed_date"><?=$feedback['date']?></div>
					</div>
					<div class="feed_body">
						<div class="feed_des"><?=$feedback['des']?></div>
					</div>
					
				<?php if(isset($feedback['otvet']) && $feedback['otvet'] != '' ):  ?>
				
					<div class="feed_head">
						<div class="feed_title">Администратор</div>
					</div>
					<div class="feed_body">
						<div class="feed_des"><?=$feedback['otvet']?></div>
					</div>
					
				<?php endif; ?>
				</div>
			<?php endforeach;?>
		</div>
		<!--<pre><?php print_r($data['paginator']) ; ?></pre>-->
		<?php if ($data['paginator']['max'] > 1): ?>
		<div class="paginator"> 
    	<?php if ($data['paginator']['page'] > 1): ?>
    		<a data-to="<?=$data['paginator']['page']-1 ?>" class="prev" href="javascript: void(0)";><</a>
    	<?php endif ?>
        
        <?php for ($i = $data['paginator']['start'] ; $i <= $data['paginator']['end']; $i++): ?>
        	<a data-to="<?=$i?>" class="<?=$data['paginator']['page'] == $i ? 'active' : ''?>" href="javascript: void(0);"><?=$i?> </a>
        <?php endfor;?>
        <?php if ($data['paginator']['page'] < $data['paginator']['max']): ?>
        	<a data-to="<?=$data['paginator']['page']+1 ?>" class="next" href="javascript: void(0);">></a>
        <?php endif ?>
        
		</div>	
    <?php endif ?>
		
	</div>
	
	
	
</div>

<div>
<div class="clearfix bottom_separator"></div>
<div class="container">
	<?php
        //  блок информации о сайте: телефоны, время работы, способы оплаты
        include 'widgets/site_info.tpl.php'
    ?>
</div>
