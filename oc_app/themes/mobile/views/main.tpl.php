 <div class="catalog_area">
	
	<?php foreach ($data['menu'] as $section): ?>
		<?php foreach ($section['submenu'] as $category): ?>
            <?php $link = ($category['tag_status'] != NULL) ? "/tags/" . $category['chpu'] : "/category/" . $category['chpu'] ; ?>
			<div class="category">
				<?php if (file_exists($_SERVER["DOCUMENT_ROOT"]."/upload/cat/{$category['cat_id']}.jpg")): ?>
					<a class="image lazy" data-src="/upload/cat/<?=$category['cat_id']?>.jpg" href="<?=$link?>"></a>
				<?php else: ?>
					<a class="image lazy" data-src="/mobile/asset/img/noimage.png" href="<?=$link?>"></a>
				<?php endif ?>
				<a class="name" href="<?=$link?>">
					<?=$category['title']?>
				</a>
			</div>
		<?php endforeach ?>
	<?php endforeach ?>

</div>
<div class="main_container">
	<a class="show_all_catalogs" href="javascript:void(0);" ><span> Показать еще</span> <span class="arrow">&nbsp;</span></a>

	<ul class="pages_list">
		<?php foreach ($data['pagesMenu'] as $value): ?>
			<li><a href="<?=getTemplateLink($value, 'page')?>"><?=$value['title']?></a></li>
		<?php endforeach ?>
	</ul>

	<?php
        //  блок информации о сайте: телефоны, время работы, способы оплаты
        include 'widgets/site_info.tpl.php'
    ?>
</div>