<? $point= $data['point']; ?>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript">
ymaps.ready(function () {
	var  myPlacemark;
       var map = new ymaps.Map("map", {
            center: [<?=$point['center'] ?>], 
            zoom:10
			});


	

				ymaps.geocode('<?=$point['adresses'] ?>', {
				    results: 1
				}).then(function (res) {
				        // Выбираем первый результат геокодирования.
				        var firstGeoObject = res.geoObjects.get(0);
				        map.geoObjects.add(firstGeoObject);

				        });




	
      });

</script>
<div class="container">
    <div class="inner">
		<div class="back_white">
			<div class="content">
				<div itemscope itemtype="http://schema.org/Organization">
				<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<h1> Наши пункты самовывоза Market-Sad <span itemprop="addressLocality"><?=$point['title'] ?></span></h1>
				<p class="warning_point">Уважаемые покупатели самовывоз осуществляется по предварительному согласованию, в противном случае не гарантируется наличие товара в нужном объёме и по указанным ценам.</p>
				<div id="map" style="width: 100%; height: 500px;" ></div>

					<div class="pick_one">
									<span itemprop="streetAddress"><?=$point['adresses'] ?> </span>

						</div>	
					</div>	
				</div>
				<div class="clear" style="clear: both;"></div>
			</div>
		</div>
	</div>
</div>