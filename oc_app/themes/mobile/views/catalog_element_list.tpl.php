<?php $link = '/product/'.$item['chpu'];?>
<li>
    <div class="list_element_cart">

        <a class="img" href="<?=$link?>">
            <?php if ($item['best'] > 0) {?>
                <div class="best-icon-mobile"></div>
            <?php } ?>
            <img class="lazy" data-src="/upload/big/<?=$item['id']?>.jpg" src="" alt="<?=$item['title'] ?>">
        </a>

        <div class="info">
            <a href="<?=$link?>" class="title"><?=$item['title'] ?></a>
            <div>
                <?php if ((filter_var($item['amount']) > 0) ): ?>
                    <div class="info_flags_true available">В наличии</div>
                <?php else: ?>
                    <div class="info_flags_false available">на заказ</div>
                <?php endif ?>
                <div class="art"> Артикул: <?=$item['art']?> </div>
                <!--<?php if ($item['price'] > 20000): ?>
								<div class="info_flags_true delivery">Бесплатно</div>
							<?php else: ?>
								<div class="info_flags_false delivery">от 400 руб.</div>
							<?php endif ?>-->

            </div>
            <div class="price">
                <span>Цена:</span>
                <span><?=$item['price'] ?></span>
                <span>руб.</span>
            </div>
            <a class="in_basket" data-id="<?=$item['id']?>" data-title="<?=$item['title']?>"  data-cat_title="<?=$data['title']?>" data-price="<?=$item['price']?>" href="javascript: void(0);">В корзину</a>
            <!--<a href="javascript: void(0);" class="feedback_count">34 отзыва</a>-->
        </div>
    </div>
</li>