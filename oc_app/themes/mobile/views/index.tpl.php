<?php 

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$data['seo']['title']?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,  user-scalable=no">
   	<meta name="keywords" content="<?=$data['seo']['keywords']?>" />
   	<meta name="description" content="<?=$data['seo']['describtion']?>" />	
    <link rel="stylesheet" href="/frontend/mobile/css/slick.css">
    <!-- <link rel="stylesheet" href="/theme/mobile/css/style.css"> -->
    <link rel="stylesheet" type="text/css" href="/frontend/mobile/css/style.css">
	<!-- <link rel="stylesheet" type="text/css" href="/theme/mobile/css/bootstrap.css"> -->
	<?php if ($canonical): ?>
		<link rel="canonical" href="<?=$canonical?>" />
	<?php endif ?>
</head>
<body>
<canvas class="snow"></canvas>
	<header>
		<div class="phones">
			<div>
				<a class="phone_btn" href="tel:<?=preg_replace('/([^0-9])/', '', $data['phone'])?>" ><?=$data['phone']?></a>
				<!-- <a class="phone_btn" href="tel:<?=preg_replace('/([^0-9])/', '', $data['phone_ru'])?>" ><?=$data['phone_ru']?></a>-->
			</div>
		</div>
		<div class="mainpanel">
			
			<div class="top">
				<a href="javascript: void(0);" class="main_menu"></a>
				<a href="/" class="logo"></a>
				<a href="/cart/korzina" class="basket_icon">
					<div class="counter"><?=$data['basket']['count'] ?></div>
				</a>
			</div>
			<nav class="action_nav hide">
				<ul class="section_list">
					<?php foreach ($data['menu'] as $section): ?>
						<li>
							<a href="javascript: void(0);"><?=$section['title']?></a>
							<ul class="category_list">
								<?php foreach ($section['submenu'] as $category): ?>
                                    <?php $link = ($category['tag_status'] != NULL) ? "/tags/" . $category['chpu'] : "/category/" . $category['chpu'] ; ?>
									<li>
										<a href="<?=$link?>"><?=$category['title']?></a>
									</li>
								<?php endforeach ?>
							</ul>
						</li>
					<?php endforeach ?>
				</ul>
			</nav>
			<div class="bottom">
				<form id="searchform" class="search" method="GET" action="/search">
					<div class="search_area">
						<input type="text" name="search_text" placeholder="Поиск">
					</div>
					<button class="search_submit" form="searchform"></button>
				</form>
			</div>
		</div>
	</header>
	<section>

		 <?php include "oc_app/themes/" . THEME . "/views/" . $content . ".tpl.php";?>

	</section>
	<footer>
		<div class="footer_pages_menu">
			<?php foreach ($data['pagesMenu'] as $value): ?>
				<a href="<?=getTemplateLink($value, 'page')?>"><?=$value['title'] ?></a>
			<?php endforeach ?>
		</div>
		<p class="f_text">© <?=date('Y')?>. beurer-lux.ru</p>
		<a class="in_full_vers" href="javascript: void(0);">Полная версия сайта</a>
	</footer>

	<!-- Js including -->
	
	<script type="text/javascript" src="https://yastatic.net/jquery/2.2.0/jquery.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js"></script>
	<script type="text/javascript" src="/frontend/mobile/js/jquery.lazy.min.js"></script>
	<script type="text/javascript" src="/frontend/mobile/js/jquery.maskedinput.min.js"></script>
	<script type="text/javascript" src="/frontend/mobile/js/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="/frontend/mobile/js/main.js"></script>


	<!-- counters -->
	<?php include ('widgets/counters.tpl.php');?>
</body>
</html>