<?php 

namespace app\themes\mobile\controllers;
use core\lib\BaseController;
use app\models\ConfigModel;
use app\models\MenuModel;
use app\models\PageModel;
use app\models\BasketModel;

/**
* 
*/
class CartController extends BaseController
{
	function action_index(){

		// 
		if ($this->content == 'korzina') {

			// если была загружена сорма со сгенерированным номером заказа то переходим в финальный шаблон
			if (filter_input(INPUT_POST, 'orderid') > 0) {
				
				$config = new ConfigModel();
				$menu = new MenuModel();

				$data = [
					'basket' => BasketModel::getData(),
					'phone' => $config->get('site.phone'),
					'phone_global' => $config->get('site.global_phone'),
					'work_mode'=> $config->get('site.work_mode'),
					'menu' => $menu->getMenu(),
					'pagesMenu' => PageModel::getMenu(),
					'orderId' => filter_input(INPUT_POST, 'orderid'),
					'name'	=> filter_input(INPUT_POST, 'name'),
					'seo' => [
						'title' => 'Заказ Оформлен!' ,
						'describtion' => '', 
						'keywords' => ''
					]
				];

				unset($_POST['orderId']);

				$this->view->render('index', 'finish_order', $data);

			} else {
				$config = new ConfigModel();
				$menu = new MenuModel();
				$basket = new BasketModel();
				$products = $basket->getProducts();
				$summ_products = $basket->getSummProducts($products);

				$data = [
					'basket' => $basket->data,
					'phone' => $config->get('site.phone'),
					'phone_global' => $config->get('site.global_phone'),
					'work_mode'=> $config->get('site.work_mode'),
					'title' => $basket->data['count'] > 0 ? $basket->data['count'] :  'пуста',
					'products' => $products,
					'products_summ' => $summ_products,
					'delivery_price' => $basket::deliverySumm($summ_products),
					'delivery_types' => $basket->deliveryTypes,
					'menu' => $menu->getMenu(),
					'breadcrumbs' => [
						['title' =>'Корзина', 'link'  => '/cart/korzina']
					],
					'pagesMenu' => PageModel::getMenu(),
					'seo' => [
						'title' => 'Корзина',
						'describtion' => '', 
						'keywords' => ''
					]
				];	
				$this->view->render('index', 'pay', $data);
			}

			
		} else {
			$this->get404();
		}

		


	}

	function action_order_done() {

	}

}