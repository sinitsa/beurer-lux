<?php
namespace app\themes\mobile\controllers;

use core\lib\BaseController;
use app\models\ConfigModel;
use app\models\MenuModel;
use app\models\PageModel;
use app\models\BasketModel;
use app\models\SearchModel;

class SearchController extends BaseController {
    
    function action_index() {
        $config = new ConfigModel();
        //$limit = $config->get('catalog.products_on_page_mobile');
        $menu = new MenuModel();
        $query = filter_input(INPUT_GET, 'search_text');
        $search = new SearchModel(); 
        
        $data = [
            'basket' => BasketModel::getData(),
            'phone' => $config->get('site.phone'),
            'phone_global' => $config->get('site.global_phone'),
            'work_mode' => $config->get('site.work_mode'),
            'products' => $search->getProducts($query),
            'title' => iconv( 'utf-8', 'cp1251', "Результаты поиска " ) . "\"{$query}\"",
            'menu' => $menu->getMenu(),
            'pagesMenu' => PageModel::getMenu(),
            'seo' => [
                'title' => iconv( 'utf-8', 'cp1251', 'Поиск'),
                'describtion' => iconv( 'utf-8', 'cp1251', 'Поиск'),
                'keywords' => ''
            ],
        ];

        $this->view->render('index', 'search', $data);
    }
    
}