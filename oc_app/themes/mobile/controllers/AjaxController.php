<?php 
namespace app\themes\mobile\controllers;
use core\lib\BaseController;
use app\models\BasketModel;
use app\models\FeedbackModel;
/**
* 
*/
class AjaxController extends BaseController {

	function action_makeorder(){ 

		header('Content-Type: application/json');
		// фильтрация входных данных
		$args = [
			'name' 		=> FILTER_SANITIZE_MAGIC_QUOTES,
			'phone' 	=> FILTER_SANITIZE_MAGIC_QUOTES,
			'email' 	=> FILTER_SANITIZE_EMAIL,
			'adress' 	=> FILTER_SANITIZE_MAGIC_QUOTES,
			'dtype' 	=> FILTER_VALIDATE_INT,
			'ref'		=> FILTER_VALIDATE_URL,
			'comment' 	=> FILTER_SANITIZE_MAGIC_QUOTES, 
			'list' 		=> [
				'filter'	=> FILTER_VALIDATE_INT,
                'flags'     => FILTER_REQUIRE_ARRAY,
			],	
			'other' 		=> [
				'filter'	=> FILTER_SANITIZE_MAGIC_QUOTES,
                'flags'     => FILTER_REQUIRE_ARRAY,
			],	  	
		];
		$input = filter_input_array(INPUT_POST, $args);

		// 
		
		$result = BasketModel::makeOrder(
			$input['name'],
			$input['phone'],
			$input['email'],
			$input['adress'],
			$input['dtype'],
			$input['comment'],
			$input['ref'],
			$input['list'],
			$input['other']);
			
		echo json_encode($result);
	}

	public function action_newfeedback(){
		
		header('Content-Type: application/json');
		
		$input['name'] = $_POST['name'];
		$input['email'] = $_POST['email'];
		$input['des'] = $_POST['des'];
		
		// 
		$result = FeedbackModel::addFeedback( $input['name'], $input['email'], $input['des'] );

		//echo json_encode($result);
		
		echo json_encode($result);
	}

	/**
	 * [action_changeTheme изменение версии сайта]
	 */
	function action_changeTheme() {

		session_start();

		$theme = filter_input(INPUT_GET, 'theme');

		if ($theme == 'default') {
			$_SESSION['mobile'] = 0;
		}elseif ($theme == 'mobile') {
			$_SESSION['mobile'] = 1;
		}
		return false;
	}

}