<?php 

namespace app\themes\mobile\controllers;

use core\lib\BaseController;
use app\models\ConfigModel;
use app\models\MenuModel;
use app\models\PageModel;
use app\models\BasketModel;
use app\models\FeedbackModel;

/**
* 
*/
class FeedbackController extends BaseController
{
	function action_index(){
		
		// если была загружена форма то переходим в финальный шаблон
		if (isset($_POST['success']) && $_POST['success'] == 'ok') {
				
				$config = new ConfigModel();
				$menu = new MenuModel();

				$data = [
					'basket' => BasketModel::getData(),
					'phone' => $config->get('site.phone'),
					'phone_global' => $config->get('site.phone_global'),
					'work_mode'=> $config->get('site.work_mode'),
					'menu' => $menu->getMenu(),
					'pagesMenu' => PageModel::getMenu(),
					'seo' => [
						'title' => iconv('utf-8', 'cp1251' ,'Отзыв отправлен!'), 
						'describtion' => '', 
						'keywords' => ''
					]
				];

				unset($_POST['success']);

				$this->view->render('index', 'finish_feedback', $data);

		} else {
		
			$page = (isset($_GET['page'])) ? $_GET['page'] : 1 ;
			$config = new ConfigModel();
			$menu = new MenuModel();
			$feed = new FeedbackModel($page);
			$paginator = $feed->getPaginator();
		
			$data = [
				'basket' => BasketModel::getData(),
				'breadcrumbs' => [
				],
				'phone' => $config->get('site.phone'),
				'phone_global' => $config->get('site.phone_global'),
				'work_mode'=> $config->get('site.work_mode'),
				'menu' => $menu->getMenu(),
				'title' => 'Оставить отзыв',
				'feedbacks' => $feed->feedbacks,
				'pagesMenu' => PageModel::getMenu(),
				'paginator' => $paginator,
				'breadcrumbs' => [
					['title' =>  iconv('utf-8', 'cp1251' ,'Оставить отзыв') , 'link' => '/feedback/' ]
				],
				'seo' => [
					'title' => iconv('utf-8', 'cp1251' ,'Оставить отзыв'), 
					'describtion' => iconv('utf-8', 'cp1251' ,'Оставить отзыв'),
                    'keywords' => ''
				],
			];
		
			$this->view->render('index', 'feedback', $data);	
		
		}
		
	}



}