<?php 
namespace app\themes\mobile\controllers;
use core\lib\BaseController;
use app\models\ConfigModel;
use app\models\MenuModel;
use app\models\PageModel;
use app\models\CatModel;
use app\models\BasketModel;
use app\models\BreadModel;

/**
* 
*/
class CategoryController extends BaseController
{

	function action_index(){

		$config = new ConfigModel();	
		$limit = $config->get('catalog.products_on_page_mobile');
		$menu = new MenuModel();
		$cat = new CatModel($this->content);
		if (isset($cat->dbdata->id)) {
			$products = $cat->getProductList($cat->page, $limit);
			$paginator = $cat->getPaginator($limit);
            $breads = new BreadModel($cat->dbdata->id);

			$checkFilter = filter_input(INPUT_GET, 'page');

			if ((($checkFilter >= 1) and ($checkFilter <= $paginator['max'])) or $checkFilter == null) {
		        
				$data = [
					'basket' => BasketModel::getData(),
					'phone' => $config->get('site.phone'),
					'phone_global' => $config->get('site.global_phone'),
					'work_mode'=> $config->get('site.work_mode'),
					'products' => $products,
					'paginator' => $paginator,
                    'id' => $cat->dbdata->id,
		            'title' => $cat->dbdata->title,
					'menu' => $menu->getMenu(),
					'pagesMenu' => PageModel::getMenu(),
					'breadcrumbs' => $breads->breads,
					'seo' => [
						'title' => $cat->dbdata->seo_title, 
						'describtion' => $cat->dbdata->seo_des, 
						'keywords' => $cat->dbdata->seo_key
					], 
				];
				
				$this->view->render('index', 'cat', $data);
			} else {
				$this->get404();
			}		
		} else {
			$this->get404();
		}			
	} 
}
?>