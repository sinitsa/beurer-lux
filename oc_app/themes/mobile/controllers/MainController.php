<?php 

namespace app\themes\mobile\controllers;
use core\lib\BaseController;
use app\models\ConfigModel;
use app\models\MenuModel;
use app\models\PageModel;
use app\models\BasketModel;

/**
* 
*/
class MainController extends BaseController {

	public $config;
	
	function action_index() {

		$config = new ConfigModel();
		$menu = new MenuModel();

		$data = [
			'basket' => BasketModel::getData(),
			'phone' => $config->get('site.phone'),
			'phone_global' => $config->get('site.global_phone'),
			'work_mode'=> $config->get('site.work_mode'),
			'menu' => $menu->getMenu(),
			'pagesMenu' => PageModel::getMenu(),
			'seo' => [
				'title' => $config->get('site.name'), 
				'describtion' => $config->get('site.des'), 
				'keywords' => $config->get('site.key')
			],
		];
		$this->view->render('index', 'main', $data);
	}
}
