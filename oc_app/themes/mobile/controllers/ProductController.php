<?php 
namespace app\themes\mobile\controllers;
use core\lib\BaseController;
use app\models\ConfigModel;
use app\models\MenuModel;
use app\models\PageModel;
use app\models\ProductModel;
use app\models\BasketModel;
use app\models\BreadModel;

/**
* 
*/
class ProductController extends BaseController
{

	function action_index(){

		$config = new ConfigModel();	
		$menu = new MenuModel();
        $product = new ProductModel($this->content);
        $breads = new BreadModel($product->dbdata->cat);
        
		$data = [
			'basket' => BasketModel::getData(),
			'phone' => $config->get('site.phone'),
			'phone_global' => $config->get('site.phone_global'),
			'work_mode'=> $config->get('site.work_mode'),
			'id' => $product->dbdata->id,
            'title' => $product->dbdata->title,
            'images' => $product->getImages(),
            //'fake_in_stock' => $product->dbdata->fake_in_stock,
            'amount' => $product->dbdata->amount,
            'price' => $product->dbdata->price,
			'art'   => $product->dbdata->art,
            'cat_title' => $product->dbdata->cat_title,
			'menu' => $menu->getMenu(),
			'pagesMenu' => PageModel::getMenu(),
			'breadcrumbs' => $breads->breads,
			'seo' => [
				'title' => $product->dbdata->seo_title, 
				'describtion' => $product->dbdata->seo_des, 
				'keywords' => $product->dbdata->seo_key
			],
            //'features' => $product->getFeatures(),
            'describtion' => $product->dbdata->des,
            'feedbacks' => $product->getFeedbacks(),
            'anotherProducts' => $product->getAnotherProducts(4),
            //'recommendProducts' => $product->getRecommendProducts($product->dbdata->id, 4),
		];
        // echo '<pre>';
        // print_r($product->getRecommendProducts($product->dbdata->id, 4));
        // echo '</pre>';

		$this->view->render('index', 'catalog', $data);
	} 
}