<?php 

namespace app\themes\mobile\controllers;

use core\lib\BaseController;
use app\models\ConfigModel;
use app\models\MenuModel;
use app\models\PageModel;
use app\models\SamovyvozModel;
use app\models\BasketModel;

/**
* 
*/
class SamovyvozController extends BaseController
{
	function action_index(){

		$config = new ConfigModel();
		$menu = new MenuModel();
		$page = new SamovyvozModel($this->content);
		//var_dump($page->dbdata);
		if (!isset($page->dbdata['point']) && empty($page->dbdata['points'])) {
			$this->get404();
			
		} else {
			$data = [
				'basket' => BasketModel::getData(),
				'breadcrumbs' => [
						
				],
				'phone' => $config->get('site.phone'),
				'phone_spb' => $config->get('site.phone2'),
				'work_mode'=> $config->get('site.work_mode'),
				'menu' => $menu->getMenu(),
				'title' => !empty($page->dbdata['title'])?$page->dbdata['title']:"Наши пункты самовывоза Market-Sad",
				'text' => !empty($page->dbdata['des'])?$page->dbdata['des']:"Наши пункты самовывоза Market-Sad",
				'pagesMenu' => PageModel::getMenu(),
				'breadcrumbs' => [
					['title' => $page->dbdata['title'], 'link' => '/page/'.$page->dbdata['chpu'].'.html']
				],
				
				'seo' => [
				'title' => !empty($page->dbdata['seo_title'])?$page->dbdata['seo_title']:"Наши пункты самовывоза Market-Sad", 
				'describtion' => !empty($page->dbdata['seo_des'])?$page->dbdata['seo_des']:"Наши пункты самовывоза Market-Sad", 
					'keywords' => $page->dbdata['seo_key']
				],
				'points' => !empty($page->dbdata['points'])?$page->dbdata['points']:"",
				'point'  => $page->dbdata['point']
			];
			$temp = !empty($page->dbdata['points'])?"samovyvozPoints":"samovyvozPoint";
			
			$this->view->render('index', $temp, $data);
						
		}
	}

	function action_404() {
		$this->get404();		
	}


}