<?php
namespace app\models;

use core\lib\BaseModel;
use core\lib\DB;
use app\models\ConfigModel;
use core\plugins\PHPMailer;

class BasketModel extends BaseModel {

	public $data;
	public $deliveryTypes;
	
	function __construct() {
		$this->data = self::getData();
		$this->deliveryTypes = self::getDeliveryTypes();
	}
		
	public static function getData() {

		$basket = json_decode(filter_input(INPUT_COOKIE, 'basket'));
		$list=[];
        
		if ($basket) {
			foreach ($basket->items as $value) {
				$list[$value->id] = $value->count;
			}
			return ['count' => $basket->tcount, 'summ' => $basket->tprice, 'list' => $list];
		}
		return ['count' => 0, 'summ' => 0, 'list' => []];
	}
	
	public static function getDeliveryTypes() {
		
		$types = [] ;
		
		$query = DB::query(
				"SELECT 
					*
				FROM 
					`order_delivery_types`
					");
		while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
			$types[$row['id']] = $row['name'];
		}
		
		return $types ;
	}
	
	public function getProducts() {	
		$list = '';
		$output = [];
		if (count($this->data['list'])) {
			foreach ($this->data['list'] as $key => $value) {
				$list = $list . (string)((int)$key) . ',';
			} 
			$list = substr($list, 0, -1);
			$query = DB::query(
				"SELECT 
					P.`title`,
					P.`price`,
					P.`id`,
					P.`chpu`,
                    C.`title` AS cat_title
				FROM 
					`catalog` AS P
                LEFT JOIN
                    `cat` AS C
                ON
                    P.`cat` = C.`id`
				WHERE 
					P.`id` IN ({$list})");
			while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
				$output[] = array_merge($row, ['count' => $this->data['list'][$row['id']]]);
			}
		}
		return $output; 
	}

	/**
	 * [getSummProducts description]
	 * @param  [type] $list [description]
	 * @return [type]       [description]
	 */
	public function getSummProducts($list) {
		$summ = 0;
		foreach ($list as $value) {
			$summ = $summ + ($value['price'] * $value['count']); 
		}
		return $summ;
	}

	/**
	 * [deliverySumm расчет доставки]
	 * @param  [int] $type [тип доставки] - пока не работает
	 * @param  [int] $summ [сумма заказа]
	 * @return [int]       [стоимость доставки]
	 */
	public static function deliverySumm($summ, $type = 1) {
		switch($type){
		case 1:	
		
			if ($summ < 950) {
				return 300;
			}
			if ($summ >= 950 && $summ < 3000) {
				return 250;
			}
			if ($summ >= 3000 && $summ < 5000) {
				return 200;
			}
			if ($summ >= 5000 && $summ < 7000) {
				return 150;
			}
			
			return 0 ;
			
		break;
		case 2:	
			
			if ($summ < 950) {
				$itog = 300;
			}
			if ($summ >= 950 && $summ < 3000) {
				$itog = 250;
			}
			if ($summ >= 3000 && $summ < 5000) {
				$itog = 200;
			}
			if ($summ >= 5000 && $summ < 7000) {
				$itog = 150;
			}
			if (summ >= 7000) {
				$itog = 0 ;
			}
			
			$itog.= "руб. + 30 руб. за км. от МКАД";
			
			return $itog;
		break;	

		case 3:
			return 'Расчитывается индивидуально';
        break;
        case 4:
            return 'Расчитывается индивидуально';
        break;
        case 5:
            return '';
        break;
        case 6:
            return 'Расчитывается индивидуально';
        break;			
		}
	}

	/**
	 * [sqlSet формирование строки из массива  для  SQL INSERT/UPDATE]
	 * @param  [array] $array
	 * @return [string]       
	 */
	private static function sqlSet($array) {
		$set = '';
		foreach ($array as $key => $value) {
			$set .= "`{$key}` =:{$key}, ";
		}
		return substr($set, 0, -2);
	}

	/**
	 * [mailToClient отправка письма клиенту о совершенном заказе]
	 * @param  [string] $name    [Имя]
	 * @param  [string] $mail    [description]
	 * @param  [int] $orderId [номер заказа]
	 * @return [boolean]          []
	 */
	private static function mailToClient($data, $orderId, $products, $del_type_name ) {

		$config = new ConfigModel();	
		$from =  $config->get('site.email_from');
		$email_support = $config->get('site.email_support');
		$site_name =  iconv( 'cp1251','utf-8' ,$config->get('site.name'));
		$site_url =  $config->get('site.web_addr');	
		$orderKey = $config->get('order.prefix') . $orderId;
		
		// немного кодировки
		$data['name'] = iconv( 'cp1251', 'utf-8' ,$data['name']);
		$data['adress'] = iconv( 'cp1251', 'utf-8' ,$data['adress']);
		$data['comment'] = iconv( 'cp1251', 'utf-8' ,$data['comment']);
		
		
		$subject = "Заказ с сайта {$site_name} {$orderKey}";
		
		$body = "<html>";
		
		$summ = 0 ;
		
    	foreach ($products as $p) { 
			$summ += $p['price'] * $p['quantity'] ;
			$body .= '
			<table width="100%" border="0" cellpadding="7" cellspacing="0" style="border-bottom-width: 1px;border-bottom-style: dashed;border-bottom-color: #d8d8d8;">
			  <tr>
				<td width="80" align="left" valign="top"><a href="' . $site_url . '/catalog/' . $p['chpu'] . '.html"><img src="/upload/small/' . $p['id'] . '.jpg"  border="0" align="left" /></a></td>
				<td align="left" valign="top"><a href="' . $site_url . '/catalog/' .$p['chpu']. '.html" style="color:#333;">' . $p['name'] . '</a> <span style="font-size: 1.3em;">(' . $p['quantity'] . ' шт.)</span><br />
				  <br />
				  <span style="font-size: 17px;color: grey;font-family: Verdana, Geneva, sans-serif;">' . $p['price'] . '</span> <span style="font-size: 12px;color: #A8A8A8;font-weight: bold;font-family: Verdana, Geneva, sans-serif;">руб.</span>
				 </td>
				</tr>
			</table>';
		}
		$body .= '
		<span style="font-size: 1.2em;">Общая сумма: ' . $summ . '</span><br />
		<br />
		<strong>Имя:</strong> ' . $data['name'] . '<br />
		<strong>Тел.:</strong> ' . $data['phone'] . '<br />
		<strong>Доставка:</strong> ' . $del_type_name . '<br />
		<strong>Адрес:</strong> ' . $data['adress'] . '<br />';

		if ($data['email'] != '') {
			$body .= '<strong>E-mail:</strong> ' . $data['email'] . '<br />';
			$body .='<strong>Дополнительно:</strong> ' . $data['comment'] . '<br />';
		}
		$body .= "</html>";
		
		#Отправка письма на почту заказчику
			//$email_support = 'kolchin@ufoweb.ru' ;
			$manager_mail = self::sendMail($email_support, $subject, $body);
			
		#Отправляем письмо пользователю о заказе, если пользователь указал почту. 
		$client_mail = null ;
		if($data['email']){

			$tema = "Вашему заказу присвоен номер {$orderKey} ({$site_name})";
			$pismo = "<html>".
			"Мы рады, что Вы выбрали именно наш интернет магазин.".
			"<br>".
			"Вашему заказу присвоен номер <strong>{$orderKey}</strong><br>".
			"В ближайшее время, для подтверждения заказа, с Вами свяжется наш менеджер.".
			"<br>".
			"Если с обработкой Вашего заказа возникли проблемы - пишите на {$email_support} .".
			"<br>".
			"С уважением, {$site_name}.".
			"</html>";
			
			$client_mail = self::sendMail($data['email'], $tema, $pismo );
		}
    	unset($config);
		return [ 'manager_mail' => $manager_mail, 'client_mail' => $client_mail ];

	}

	/**
	 * [makeOrder оформление заказа]
	 * @param  [string] $name        [description]
	 * @param  [string] $phone       [description]
	 * @param  [string] $email       [description]
	 * @param  [int]    $pay_type    [description]
	 * @param  [int]    $del_type    [description]
	 * @param  [string] $comment     [description]
	 * @param  [string] $ref         [description]
	 * @param  [array]  $basket_list [description]
	 * @param  [array]  $other       [description]
	 * @return [array]               [description]
	 */
	public static function makeOrder($name, $phone, $email, $adress, $del_type, $comment, $ref, $basket_list, $other = []) {
		$messages =[]; 
		$db_products = [];

		// формирование запрса о всех товарах корзины
		
		foreach ($basket_list as $key => $value) {
			$db_products[] = $value['id']; 
		}
		$db_products = implode(',', $db_products);
		$query = DB::query(
			"SELECT
				P.`id`,
				P.`title`,
				P.`price`,
                C.`title` AS cat_title,
				P.`art`,
				P.`chpu`
 			FROM
				`catalog` AS P
            LEFT JOIN
                `cat` AS C
            ON 
                P.`cat` = C.`id` 
			WHERE
				P.`id` IN ({$db_products})");
		$db_products = [];
		while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
			$db_products[$row['id']] = [
				'id' 	=> $row['id'], 
				'price' =>  $row['price'], 
				'title' => $row['title'],
                'cat_title' => $row['cat_title'],
				'art' 		=> $row['art'],
				'chpu' 		=> $row['chpu']
			];
		}
		
		$query = DB::query(
			"SELECT
				*
 			FROM
				`order_delivery_types`
			WHERE
				`id` IN ({$del_type})");
				
		while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
			$del_type_name = $row['name'];
		}

		
		// заполнение данными products
		$products = [];
        
        $output = [];
		$summ = 0;
		foreach ($basket_list as $key => $value) {
			//проверка на существовании цены  товара в базе
			if (!$db_products[$value['id']]['price'] > 0) {
				$message[] = 'Найдена ошибка в данных заказа. Заказ не оформлен!';
				return ['id' => 0, 'error' => true, 'message' => $messages];
			}
			$products[] = [
				'id' => $value['id'],
    			'amount' => $value['count'],
    			'price' => $db_products[$value['id']]['price'],
    			'price_after_discount' => $db_products[$value['id']]['price'],
			];
			$summ += $db_products[$value['id']]['price']*$value['count'];
            
            // массивна выход для статистики
            $output[] = [
				'id' => $value['id'],
                'name' => $db_products[$value['id']]['title'],
                'category' => $db_products[$value['id']]['cat_title'],
    			'quantity' => $value['count'],
    			'price' => $db_products[$value['id']]['price'],
				'art' 	=> $db_products[$value['id']]['art'],
				'chpu' => $db_products[$value['id']]['chpu']
                
			];
		}

		// заполнение данными order price
		$price =[
			'price_after_global_discount' => $summ,
			'price_before_global_discount' => $summ,
			'delivery_price' => self::deliverySumm($summ),
		];

		if ($other['extra'] == 'fast_order') {
			$extra = 'Быстрый заказ';
		} else {
			$extra = '';
		}
		

		$data = [
			'date' 			=> time(),
			'products'	 	=> serialize($products),
			'order_price'	=> serialize($price), 
			'name'			=> $name,
			'delivery_type' => $del_type,  
			'phone'			=> $phone, 
			'email'			=> $email, 
			'adress'		=> $adress,
			'extra_information' =>  $extra,
			'comment' 		=>  $comment,
			'h'				=> $ref,
		];
        
		$query	= DB::prepare("INSERT INTO `orders` SET" . self::sqlSet($data));
		if ($query->execute($data)) {
			$lastId = DB::query("SELECT LAST_INSERT_ID()")->fetch(\PDO::FETCH_NUM);
			
			//if ($email) {
				//$mails = self::mailToClient($data, $lastId[0], $output, $del_type_name );
			//}

			return ['id' => $lastId, 'error' => false, 'message' => $messages, 'products' => $output, 'summ' => $summ  ];
		} else {
			$message[] = 'Данные заказа не записались!';
			return ['id' => 0, 'error' => true, 'message' => $messages];
		}
		return true;
	}
	
	private static function sendMail($email, $subject, $body){
		
		// пароль от ящика 
		$email_password = 'ZH84SjOrg3m9';
		
		$mail = new PHPMailer();
		$mail->isSMTP();         
		$mail->CharSet = 'UTF-8';                       // Set mailer to use SMTP
		$mail->Host =  'smtp.gmail.com' ;                // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                         // Enable SMTP authentication
		$mail->Username = 'info@shopzdrav.ru'; 		// SMTP username
		$mail->Password = $email_password ;               // SMTP password
		$mail->SMTPSecure = 'tls';                      // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                              // TCP port to connect to

		$mail->From = 'info@shopzdrav.ru';
		$mail->FromName = 'Shopzdrav';    
		$mail->addAddress($email);

		$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = $subject;
		$mail->Body  = $body ;
		
		$result = $mail->send() ;

		return $result ;
		
		
	}
	
}

?>