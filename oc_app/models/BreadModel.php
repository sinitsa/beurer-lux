<?php

namespace app\models;

use core\lib\BaseModel;
use core\lib\DB;
/**
 *
 */
class BreadModel extends BaseModel {

    public $breads;

    function __construct($catId) {
        $this->getBreadcrumbs($catId);
    }

    // получаем хлебные крошки по id категории
    public function getBreadcrumbs($catId){
        $this->getBreads($catId) ;

        $breads_tmp = array_reverse($this->breads) ;

        $this->breads = $breads_tmp ;
    }

    // получаем ссылки по id категории
    private function getBreads($catId){

        $query = DB::prepare(
            "SELECT 
                  `menu`.`pod`, 
                  `cat`.`title`, 
                  `menu`.`cat_id`, 
                  `menu`.`chpu`
              FROM `menuleft` AS `menu` 
              LEFT JOIN `cat` ON `menu`.`chpu` = `cat`.`chpu`
              WHERE `cat`.`id` = :id 
              ORDER BY `menu`.`id` ASC
              LIMIT 1");
        $query->execute([':id' => $catId]);
        $cat_in_menu = $query->fetch(\PDO::FETCH_ASSOC);

        $this->breads[] = array(
            'link' => '/category/' . $cat_in_menu['chpu'] ,
            'title' => $cat_in_menu['title'] ,
        );

        $query2 = DB::prepare(
            "SELECT 
                  `id`, 
                  `pod`, 
                  `title`, 
                  `chpu`  
              FROM menuleft 
              WHERE id = :id 
              LIMIT 1");
        $query2->execute([':id' => $cat_in_menu['pod']]);
        $mcat = $query2->fetch(\PDO::FETCH_ASSOC);

        $this->breads[] = array(
            'link' => '/mcat/' . $mcat['chpu'],
            'title' => $mcat['title'] ,
        );


    }
}