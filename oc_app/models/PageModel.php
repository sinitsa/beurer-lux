<?php 

namespace app\models;

use core\lib\BaseModel;
use core\lib\DB;
/**
* 
*/
class PageModel extends BaseModel {

	public $dbdata;

	function __construct($index) {
		$this->getPageData($index);
	}
	
	// вывод списка для меню страниц
	public static function getMenu() {
		$menu = [];
		$query = DB::query("SELECT 
                              id, 
                              title, 
                              chpu 
                            FROM `pages` 
                            WHERE pages_menu = 1");
		while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
			$menu[] = $row;
		}
		return $menu;
	}

	// вывод данных одной страницы по chpu\id
	public function getPageData($index) {
		$key = is_numeric($index) ? 'id' : 'chpu';  
		$query = DB::prepare(
			"SELECT 
				id, title, des, seo_title, seo_des, seo_key, chpu
			FROM
				`pages`
			WHERE 
				`{$key}` = ?
			LIMIT 1");
		$query->execute([$index]);
		$this->dbdata = $query->fetch(\PDO::FETCH_OBJ);
		return $query->fetch(\PDO::FETCH_ASSOC);
	} 

}