<?php

namespace app\models;

use core\lib\BaseModel;
use core\lib\DB;

class SearchModel extends BaseModel 
{
    private $cl;
    
    function __construct() {
        $this->sphinxInit();
    }
    
    /**
     * инициализация  sphinx
     */
    private function sphinxInit() {
        
        // подгрузка библиотеки      
        require_once (ROOT_DIR . 'oc_app/components/sphinxapi.php');
       
        $this->cl = new \SphinxClient();
		// настройки
        $this->cl->SetServer("127.0.0.1", 3312);
		$this->cl->SetConnectTimeout( 1 );
		$this->cl->SetMaxQueryTime(1000);
		$this->cl->SetLimits(0, 5, 5);
		$this->cl->SetMatchMode(SPH_MATCH_ANY);
		// $cl->SetMatchMode(SPH_MATCH_EXTENDED2);
		$this->cl->SetSortMode(SPH_SORT_RELEVANCE);
		$this->cl->SetFieldWeights(array('title' => 20, 'des' => 10));
    }
    /**
     * поиск  sphinx 
     * @param type $query
     * @return array (список id найденых полей)
     */
    private function sphinxSearch($query) {
        
		$result = $this->cl->Query($query,"catalog");
		if( $result === false ){
			if( $this->cl->GetLastWarning() ) {
				die('WARNING: '.$this->cl->GetLastWarning());
			}
            return [];
			//die('ERROR: '.$this->cl->GetLastError());
		}
        return array_keys($result['matches']);
    } 
    
	
	/**
     * поиск по-старинке 
     * @param type $query
     * @return array (список id найденых полей)
     */
    private function defaultSearch($search) {
        
		$result = [] ;
		
		//$search = substr($query, 0, 64);
		//$search = preg_replace("/[^\w\x7F-\xFF\s]/", " ", $query);
		//$good = trim(preg_replace("/\s(\S{1,2})\s/", " ", $search));

		$query = "SELECT * FROM catalog WHERE title LIKE '%". $search . "%' ";
		$art_search = " OR `art` LIKE  '%". $search . "%' ";
		
		$sql = DB::query($query . $art_search);

		while ($row = $sql->fetch(\PDO::FETCH_ASSOC)) {
            $result[] = $row['id'] ;
        } 
		
		return $result ;
    } 
	
    /**
     * Вывод списка товаров по запросу из поиска
     * @param string $query
     * @return array
     */
    public function getProducts($query) {
        
        if (empty($query)) {
            return [];
        } 
            
        //$sphinxResult = $this->sphinxSearch($query);
        $defaultResult = $this->defaultSearch($query);
		
        if (count($defaultResult)) {
            $list = implode(',', $defaultResult);
            $sql = DB::query(
            "SELECT
                P.`id`,
                P.`title`,
                P.`price`,
                P.`chpu` ,
                P.`best`,
                P.`art`,
				IFNULL((SELECT SUM(d.`amount`) FROM `storage_rests` as d 
						LEFT JOIN `storages_new` as e on e.storage_id = d.storage_id 
						WHERE e.is_active = 1 and d.catalog_id = P.`id` ),0) as `amount`
            FROM
                `catalog` AS P
            WHERE 
                `id` in ({$list}) 
            ORDER BY  `amount` DESC ");
            $products=[];
            while ($row = $sql->fetch(\PDO::FETCH_ASSOC)) {
                $products[] = $row;
            }    
            return $products;
        }
        return [];   
    }   
}
