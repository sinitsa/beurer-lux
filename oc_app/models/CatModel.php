<?php 

namespace app\models;

use core\lib\BaseModel;
use core\lib\DB;


/**
* 
*/
class CatModel extends BaseModel
{
    
    public $page;
    public $productList = [];
    public $dbdata;

	function __construct($idOrChpu) {
        $this->loadCatInfo($idOrChpu);
        $this->page = $this->getNumberPage();
        // print_r($this->dbdata);
	}
    
    /**
     * [getNumberPage вывод номер страницы пагинатора]
     * @return [int] 
     */
    private function getNumberPage() {
        if (isset($_REQUEST['page'])) {
            if ($_REQUEST['page'] > 0) {
               return $_REQUEST['page'];
            }
            return 1;
        } 
        return 1;
    }
    
    //  $value = идентификатор Категории (id или chpu)
    /**
     * [loadCatInfo вывод информации о категории]
     * @param  [int/string] $idOrChpu [индекс категории: id или чпу]
     * @return [true] 
     */
    private function loadCatInfo($idOrChpu)
	{
        $key = (is_numeric($idOrChpu) ? 'id' : 'chpu');
		$query = DB::prepare(
            "SELECT 
                `cat`.`id`, 
                `cat`.`title`, 
                `cat`.`seo_title`, 
                `cat`.`seo_des`, 
                `cat`.`seo_key`, 
                `cat`.`seo_text`, 
                `cat`.`chpu`
            FROM 
                `cat` 
            WHERE 
                {$key} = ?");      
        $query->execute([$idOrChpu]);
		$row = $query->FETCH(\PDO::FETCH_OBJ);
        $this->dbdata = $row;
	    return true;
	}
	
    /**
     * [getBreadcrumbs формирование данных для "хлебных крошек"]
     * @return [array] []
     */
	public function getBreadcrumbs() {
		$list = [];
		$query = DB::prepare(
			"SELECT 
                A.title, A.chpu, B.title, (SELECT chpu FROM cat WHERE `id` = A.cat_id) as chpu
			FROM 
                menuleft AS A
			LEFT JOIN 
                menuleft AS B 
            ON 
                A.pod = B.id
			WHERE 
                A.cat_id = ? limit 1");
        $query->execute([$this->dbdata->id]);
        $result = $query->fetch(\PDO::FETCH_NUM);
        return array(
			array('title' => $result[2], 'link' => 'javascript:void(0);'),
			array('title' => $result[0], 'link' => '/' . $result[3] . '/'),
		);
	}

    /**
     * [getProductList вывод данных из БД о товарах категории]
     * @param  integer $page  [текущая страница пагинатора]
     * @param  integer $limit [лимит количества товаров на странице]
     * @return [array]
     */
    function getProductList($page = 1, $limit = 10){
        if(is_numeric($this->dbdata->id)) {	
            
			$sorting = $this->getSortProducts();
            $where = 'cat = :cat ';
            
            // формирование запроса  подсчета количества товаров в категории
            $count_query = DB::prepare(
                "SELECT 
                    count(id) 
                FROM
                    `catalog`
                WHERE
                    {$where}");
            $count_query->bindValue(':cat', $this->dbdata->id,\PDO::PARAM_INT);  
            $count_query->execute();
            $this->count_products = $count_query->fetchColumn();
            
            // формирование запроса вывода списка товаров категории
            $db = DB::instance();               		   
			$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);			
			$query = $db->prepare(
                "SELECT SQL_NO_CACHE
                    P.`id`,
                    P.`title`,
                    P.`price`,
                    P.`chpu`,
					P.`art`,
					P.`best`,
					IFNULL((SELECT SUM(d.`amount`) FROM `storage_rests` as d 
						LEFT JOIN `storages_new` as e on e.storage_id = d.storage_id 
						WHERE e.is_active = 1 and d.catalog_id = P.`id` ),0) as `amount`,
						
					(COALESCE((SELECT SUM(d.`amount`) FROM `storage_rests` as d 
                        LEFT JOIN `storages_new` as e on e.storage_id = d.storage_id 
                        WHERE e.is_active = 1 and d.catalog_id = P.`id` ),0) > 0 AND (P.`best` > 0 ) ) as `best_active`
					
                FROM 
                    `catalog` AS P
                JOIN 
                    (SELECT 
                        id 
                    FROM 
                        `catalog`
                    WHERE 
                        {$where}
					) AS S
                ON S.`id` = P.`id`
				ORDER BY 
					{$sorting}
				LIMIT 
					:limit   
                OFFSET
                    :offset
                "); 

            $query->bindValue(':cat', $this->dbdata->id,\PDO::PARAM_INT);
            $query->bindValue(':limit', (int) $limit, \PDO::PARAM_INT);
            $query->bindValue(':offset', (int) (($page-1)*$limit), \PDO::PARAM_INT);      
			$query->execute();
			
            while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
                $this->productList[] = $row;
				
            }
            return $this->productList;
		}
		else {
			return [];
		}
	}

    /**
     * [getPaginator вычисление и вывод параметров пагинатора]
     * @param  [int] $limit [максимальное количество кнопок пагинатора]
     * @return [array]
     */
    public function getPaginator($limit = 5) {
        $start = $this->page - (int) ($limit/2);
        $max = ceil(($this->count_products/$limit)); 
        
        if ($max < ($start + $limit)) {
           $start = $max - $limit + 1;
        } 

        $output = [
            'limit' => $limit,
            'start' => $start > 0 ? $start : 1,
            'end' => ($start + $limit > $max) ? $max : $start + $limit,
            'page' => $this->page,
            'max' => $max,
        ];
        return $output;
    }
    
    // 
	function getSortProducts() {
	   $querySort="";
	    if(isset($_REQUEST['sort'])) {
	        switch($_REQUEST['sort']) {
                case 'price_asc':
				    $querySort.="`price` ASC";
				    break;
				
	            case 'price_desc':
				    $querySort.="`price` DESC";
				    break;
				
	            default:
				    $querySort.=" `best_active` DESC, `amount` DESC , `best` DESC , `spec_rang` ASC ";
				    break;	
			}		
		} else {
			$querySort.=" `best_active` DESC, `amount` DESC , `best` DESC , `spec_rang` ASC ";
		}	
		return $querySort;
	}
    

}