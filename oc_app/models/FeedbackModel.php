<?php 

namespace app\models;

use core\lib\BaseModel;
use core\lib\DB;

/**
* 
*/
class FeedbackModel extends BaseModel {

	public $feedbacks = [];
	public $page;
	
	private $pagelimit = 5;
	private $paginatorLimit = 6;
	private $count_products;
	
	function __construct($page) {
		$this->getFeedback($page);
		$this->page = $this->getNumberPage();
	}
	
	/**
     * [getNumberPage вывод номер страницы пагинатора]
     * @return [int] 
     */
    private function getNumberPage() {
        if (isset($_REQUEST['page'])) {
            if ($_REQUEST['page'] > 0) {
               return $_REQUEST['page'];
            }
            return 1;
        } 
        return 1;
    }
	
	// получаем отзывы со страницы
    function getFeedback($page = 1){
           
		
		// формирование запроса  подсчета количества товаров в категории
        $count_query = DB::prepare(
                "SELECT 
                    count(id) 
                FROM
                    `shop_otzyv`
                WHERE
                   `confirm` = 1
				   ");
        $count_query->execute();
        $this->count_products = $count_query->fetchColumn();
		
		
		
        // формирование запроса вывода списка отзывов
        $db = DB::instance();                          
        $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);         
        $query = $db->prepare(
            "SELECT SQL_NO_CACHE
                P.`id`, 
                P.`name`, 
                P.`mail`, 
                P.`des`, 
                P.`otvet`, 
                P.`date`
            FROM 
                `shop_otzyv` AS P
			WHERE 
				`confirm` = 1
            LIMIT 
                :limit   
            OFFSET
                :offset
            "); 

        $query->bindValue(':limit', (int)$this->pagelimit, \PDO::PARAM_INT);
        $query->bindValue(':offset', (int)(($page-1) * $this->pagelimit), \PDO::PARAM_INT);      
        $query->execute();
        
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $this->feedbacks[] = $row;
            
        }
        return $this->feedbacks;
	}
	
	
		
	 /**
     * [getPaginator вычисление и вывод параметров пагинатора]
     * @param  [int] $limit [максимальное количество кнопок пагинатора]
     * @return [array]
     */
    public function getPaginator() {
        $start = $this->page - (int)($this->paginatorLimit/2);
        $max = ceil(($this->count_products/$this->pagelimit)); 
        
        if ($max < ($start + $this->pagelimit)) {
           $start = $max - $this->pagelimit + 1;
        } 

        $output = [
            'limit' => $this->pagelimit,
            'start' => $start > 0 ? $start : 1,
            'end' => ($start + $this->pagelimit > $max) ? $max : $start + $this->pagelimit,
            'page' => $this->page,
            'max' => $max,
        ];
        return $output;
    }
	
	
	
	/**
     * Добавляем отзыв в базу
     */
    public static function addFeedback($name, $email, $des) {
        
		$date = date('d') . '.' . date('m') . '.' . date('Y');
		
		$name = iconv('utf-8', 'cp1251', $name);
		$des = iconv('utf-8', 'cp1251', $des);
		
		$q = "INSERT INTO `shop_otzyv` 
		SET 
			`name` = '{$name}' ,
			`mail` = '{$email}' ,
			`des` = '{$des}' ,
			`otvet` = '' ,
			`date` = '{$date}' 
		" ;
		
		$query	= DB::prepare($q);
		
		if ($query->execute()) {
			//$lastId = DB::query("SELECT LAST_INSERT_ID()")->fetch(\PDO::FETCH_NUM);
           
			return ['error' => false ];
		} else {
			$message[] = 'data was not inserted - mysql error !'; 
			return ['error' => true, 'message' => $messages];
		}
		
		return true;

    }

}