<?php 
namespace app\models;

use core\lib\BaseModel;
use core\lib\DB;

/**
* 
*/
class MenuModel extends BaseModel
{
	public function getMenu_old() {

		$menu = [];
		$query = DB::query("SELECT a.id, a.pod, a.title, a.cat_id, b.chpu, a.rang 
							FROM menuleft as a 
							LEFT JOIN cat as b on a.cat_id = b.id  
							WHERE CHAR_LENGTH(a.chpu) > 5 ");
		while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
			$menu[$row['id']] = $row;
		}
		return $this->getTree($menu);
	}


	public function getMenu(){

        $items = [];
        $sel = DB::query("SELECT *,
	(SELECT `chpu` FROM `cat` WHERE `chpu`=`menuleft`.`chpu`) as `cat_url`
	FROM `menuleft` 
	WHERE `cat_id` = '0' AND `pod` = '0' 
	ORDER BY `rang` ASC");
        while($row = $sel->fetch(\PDO::FETCH_ASSOC)) {
            $items[$row['id']] = array(
                'id' => $row['id'],
                'title' => $row['title'],
                'chpu' => $row['chpu'],
                'submenu' => [],
                'cat_url' => $row['cat_url']
            );
        }

        $sel2 = DB::query("
		SELECT
		`menuleft`.*, `tags`.cat_id as tag_status
		FROM
			`menuleft`
		LEFT JOIN `tags` ON `menuleft`.chpu = `tags`.chpu	
		WHERE `menuleft`.`pod` <> '0' ORDER BY `menuleft`.`rang`ASC
	");
        while($row = $sel2->fetch(\PDO::FETCH_ASSOC)) {
            $row['separator'] = false;
            //Если это разделитель
            if ($row['cat_id'] == '0' && $row['pod'] != '0') {
                $row['separator'] = true;
            }
            $items[$row['pod']]['submenu'][] = $row;
        }

        return $items;
    }

	//Функция построения дерева из массива от Tommy Lacroix
	function getTree($dataset) {
		$tree = array();

		foreach ($dataset as $id => &$node) {  

			//Если нет вложений
			if (!$node['pod']){
				$tree[$id] = &$node;
			}else{ 
				//Если есть потомки то перебераем массив
	            $dataset[$node['pod']]['submenu'][$id] = &$node;
			}
		}
		return $tree;
	}

    /*
     * вывод данных главной категории
     */    
    public function getMcatInfo($index) {
        
        // выгрузка данных         
        $sql1 = DB::prepare("SELECT * FROM `menuleft` WHERE chpu = ? LIMIT 1");
        $sql1->execute([$index]);
        $mcat = $sql1->fetch(\PDO::FETCH_ASSOC);

        if (isset($mcat['id'])) {
            $sql2 = DB::prepare(
                "SELECT
                    * 
                FROM 
                    `menuleft` 
                WHERE 
                    pod = ?
                ORDER BY
                    rang");
            $sql2->execute([$mcat['id']]);

            $subcats = [];
            while ($row = $sql2->fetch(\PDO::FETCH_ASSOC)) {
                $subcats[] = $row;
            }
            return [
                'id' => $mcat['id'],
                'title' => $mcat['title'],
                'chpu' => $mcat['chpu'],
                'seo_title' => $mcat['seo_title'],
                'sub_cats' => $subcats,
            ];
        }
        return [];
    }


}