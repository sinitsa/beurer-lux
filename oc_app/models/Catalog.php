<?php


namespace app\models;
use core\lib\DB;
use app\models\ProductModel;
use app\models\ConfigModel; // -  сюда название модели!!!
use core\plugins\PaginationManager;
use core\plugins\PaginationFormat;

class Catalog {
	
	public $id;
	public $title;
	public $seo_title;
	public $seo_des;
	public $seo_text;
	public $chpu;
	public $productList;
	public $paramPDO;
	public $paginationFormat;
	
	public function __construct($idOrChpu) {
      
        $this->loadCategory($idOrChpu);
        //echo "Был создан объект категории";
    }
	
	private function loadCategory($idOrChpu)
	{
		
		$db = DB::instance();
		//$idOrChpu = $db->quote($idOrChpu);
		$where = (is_numeric($idOrChpu) ? '`cat`.`id`' : '`cat`.`chpu`') . " = '{$idOrChpu}'";
		$sel = $db->query("SELECT `cat`.*, `m_l`.`miniart_id` FROM `cat` left join `miniarticles_links` as `m_l` on `m_l`.`cat_id`=`cat`.`id` WHERE " . $where);
		$row = $sel->FETCH(\PDO::FETCH_ASSOC);
		
				$this->id = $row['id'];
				$this->title = $row['title'];
				$this->seo_title = $row['seo_title'];
				$this->seo_des = $row['seo_des'];
				$this->seo_text = $row['seo_text'];
				$this->chpu = $row['chpu'];
	}
	
	function getFiltresProducts()
	{
		
		$queryFiltr="";
		if($_REQUEST['min_price'] && $_REQUEST['max_price'])
		{
		
		$queryFiltr.=" AND `price` <= :max_price AND `price` >= :min_price";
		$this->paramPDO[':min_price'] = $_REQUEST['min_price'];
		$this->paramPDO[':max_price'] = $_REQUEST['max_price'];
		
			return $queryFiltr;
		}
		
	
	}
	function getSortProducts()
	{
		$querySort="";
		if(!empty($_REQUEST['sort']))
		{
			
			switch($_REQUEST['sort'])
			{
				case 'price_asc':
				$querySort.="`price` ASC";
				
				break;
				
				case 'price_desc':
				$querySort.="`price` DESC";
				
				break;
				
				default:
                    $querySort.="`rang` ASC";
                    break;
				
			}		
		}
		else
		{
			$querySort.="`rang` ASC";
		}
			
		return $querySort;
	}
	

	
	function getProductList(){
		if(isset($this->id))
		{	
			$db = DB::instance();
			
			$config = new ConfigModel;
			$countProducts = $config->get('catalog.products_on_page_mobile');
			$paginationManager = new PaginationManager($countProducts, 10, $_REQUEST);
			$filters = $this->getFiltresProducts();
			$sorting = $this->getSortProducts();
			$query = "SELECT
			SQL_CALC_FOUND_ROWS
			*
			FROM `catalog`
			WHERE `cat`= {$this->id}{$filters}
			ORDER BY {$sorting} 
			LIMIT " .
            $paginationManager->getStartLimit() . "," .
            $paginationManager->getStopLimit(); //запрос на выборку товаров пагинации
		   
		
			$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);			
			$sel = $db->prepare($query);
			
			$sel->execute($this->paramPDO);
			$result = $sel->FETCHALL(\PDO::FETCH_CLASS,"app\models\ProductModel");
			$result_found_rows = $db->query("SELECT FOUND_ROWS() as `count`"); //запрос на общее количество товаров в категории
			$count = $result_found_rows->FETCH(\PDO::FETCH_ASSOC);
			$paginationManager->setCount($count["count"]); 
			
			
			$paginationFormat = new paginationFormat($paginationManager);
			$this->productList  = $result;
			$this->paginationFormat = $paginationFormat;
			
			
			
			
		}
		else
		{
			echo "Ошибка загрузите сначала категорию loadCategory($id or Chpu)";
		}
	}
	
}




?>