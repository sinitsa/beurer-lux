<?php 

namespace app\models;

use core\lib\BaseModel;
use core\lib\DB;


/**
* 
*/
class TagModel extends BaseModel
{
    
    public $page;
    public $productList = [];
    public $dbdata;

	function __construct($idOrChpu) {
        $this->loadTagInfo($idOrChpu);
        $this->page = $this->getNumberPage();
        // print_r($this->dbdata);
	}
    
    /**
     * [getNumberPage вывод номер страницы пагинатора]
     * @return [int] 
     */
    private function getNumberPage() {
        if (isset($_REQUEST['page'])) {
            if ($_REQUEST['page'] > 0) {
               return $_REQUEST['page'];
            }
            return 1;
        } 
        return 1;
    }
    
    //  $value = идентификатор Категории (id или chpu)
    /**
     * [loadCatInfo вывод информации о категории]
     * @param  [int/string] $idOrChpu [индекс категории: id или чпу]
     * @return [true] 
     */
    private function loadTagInfo($idOrChpu)
	{
        $key = (is_numeric($idOrChpu) ? 'id' : 'chpu');
		$query = DB::prepare(
            "SELECT 
                T.`id`, 
                T.`cat_id`,
                T.`title`, 
                T.`seo_title`, 
                T.`seo_description`,   
                T.`chpu`
            FROM 
                `tags` AS T
            WHERE 
                {$key} = ?");      
        $query->execute([$idOrChpu]);
		$row = $query->FETCH(\PDO::FETCH_OBJ);
        $this->dbdata = $row;
	    return true;
	}
	
    /**
     * [getBreadcrumbs формирование данных для "хлебных крошек"]
     * @return [array] []
     */
	public function getBreadcrumbs() {
		$list = [];
		$query = DB::prepare(
			"SELECT 
                T.title, T.chpu, C.title, C.chpu
			FROM 
                `tags` AS T
			LEFT JOIN 
                `cat` AS C 
            ON 
                T.cat_id = C.id
			WHERE 
                T.id = ? limit 1");
        $query->execute([$this->dbdata->id]);
        $result = $query->fetch(\PDO::FETCH_NUM);
        return array(
			array('title' => $result[2], 'link' => '/' . $result[3] . '/'),
			array('title' => $result[0], 'link' => '/tags/' . $result[1] . '/'),
		);
	}

    /**
     * [getProductList вывод данных из БД о товарах категории]
     * @param  integer $page  [текущая страница пагинатора]
     * @param  integer $limit [лимит количества товаров на странице]
     * @return [array]
     */
    function getProductList($page = 1, $limit = 10){
        if(is_numeric($this->dbdata->id)) {	
            
			$sorting = $this->getSortProducts();
            $where = 'L.`tag_id` = :id';
            
            // формирование запроса  подсчета количества товаров в категории
            $count_query = DB::prepare(
                "SELECT 
                    count(catalog_id) 
                FROM
                    `tags_links` AS L
                WHERE
                    {$where}");
            $count_query->bindValue(':id', $this->dbdata->id,\PDO::PARAM_INT);  
            $count_query->execute();
            $this->count_products = $count_query->fetchColumn();
            
            // формирование запроса вывода списка товаров в теге
            $db = DB::instance();               		   
			$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);			
			$query = $db->prepare(
                "SELECT SQL_NO_CACHE
                    P.`id`,
                    P.`title`,
                    P.`price`,
                    P.`chpu`,
                    P.`best`,
                    P.`art`,
                    IFNULL((SELECT SUM(d.`amount`) FROM `storage_rests` as d 
						LEFT JOIN `storages_new` as e on e.storage_id = d.storage_id 
						WHERE e.is_active = 1 and d.catalog_id = P.`id` ),0) as `amount`
                FROM 
                    `catalog` AS P
                JOIN 
                    `tags_links` AS L
                ON 
                    P.`id` = L.`catalog_id`
                LEFT JOIN 
                    storage_rests AS R
                ON 
                    R.`catalog_id` = P.`id`
                WHERE
                    {$where}
                ORDER BY
                    {$sorting}
                LIMIT
                    :limit
                OFFSET
                    :offset"); 
            $query->bindValue(':id', $this->dbdata->id,\PDO::PARAM_INT);
            $query->bindValue(':limit', (int) $limit, \PDO::PARAM_INT);
            $query->bindValue(':offset', (int) (($page-1)*$limit), \PDO::PARAM_INT);          
			$query->execute();
            while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
                $this->productList[] = $row;
            }
            return $this->productList;
		}
		else {
			return [];
		}
	}

    /**
     * [getPaginator вычисление и вывод параметров пагинатора]
     * @param  [int] $limit [максимальное количество кнопок пагинатора]
     * @return [array]
     */
    public function getPaginator($limit) {
        $start = $this->page - (int) ($limit/2);
        $max = ceil(($this->count_products/$limit)); 
        
        if ($max < ($start + $limit)) {
           $start = $max - $limit + 1;
        } 

        $output = [
            'limit' => $limit,
            'start' => $start > 0 ? $start : 1,
            'end' => ($start + $limit > $max) ? $max : $start + $limit,
            'page' => $this->page,
            'max' => $max,
        ];
        return $output;
    }
    
    // 
	function getSortProducts() {
	   $querySort="";
	    if(isset($_REQUEST['sort'])) {
	        switch($_REQUEST['sort']) {
                case 'price_asc':
				    $querySort.="`price` ASC";
				    break;
				
	            case 'price_desc':
				    $querySort.="`price` DESC";
				    break;
				
	            default:
				    $querySort.=" `amount` DESC, `rang` ASC ";
				    break;	
			}		
		} else {
			$querySort.="`amount` DESC, `rang` ASC ";
		}	
		return $querySort;
	}
    

}