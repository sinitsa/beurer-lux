<?php
/*
Аякс запросы на получение списка товаров приходят сюда.
Да вроде и так понятно было
*/
include('../connect.php');
include('../func/core.php');


switch ($_GET['method']) {
	case 'getpriceanddiscount' :
		$priceSumm = $_REQUEST['price'];
		$productsAmount = $_REQUEST['amount'];
		
		$globalDiscount = getGlobalDiscount($productsAmount, $priceSumm);

		$priceAfter = getPriceAfterDiscount($priceSumm, $globalDiscount['type'], $globalDiscount['value']);

		echo json_encode(array(
			'global_discount_string' => $globalDiscount['value'].' '.iconv('windows-1251', 'utf-8', getDiscountTypeString($globalDiscount['type'])),
			'price_total' => $priceAfter,
			'price_diff' => ($priceSumm - $priceAfter)
		));
		
	break;
}