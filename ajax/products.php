<?php
/*
Аякс запросы на получение списка товаров приходят сюда.
Да вроде и так понятно было
*/
include('../connect.php');
include('../func/core.php');
include ("../randomizes.php");

mysql_query("SET NAMES utf8");
mysql_query("SET CHARACTER SET utf8");
mysql_query("SET CHARSET utf8");

header('Content-Type: text/html; charset=utf-8');

switch ($_GET['action']) {
	case 'mcat' :
		$filter = array();
		
		if (is_numeric($_REQUEST['min_price'])) {
			$filter['>=price_after_discount'] = $_REQUEST['min_price']; 
		}
		if (is_numeric($_REQUEST['max_price'])) {
			$filter['<=price_after_discount'] = $_REQUEST['max_price']; 
		}
		if ($_REQUEST['filter']['cat']) {
			foreach($_REQUEST['filter']['cat'] as $key => $value) {
				if (is_numeric($key)) $filter['cat'][] = $key;
			}
		}
		if ($_REQUEST['filter']) {
			foreach($_REQUEST['filter'] as $key => $value) {
				if (is_numeric($key) && $value != '') $filter['params'][] = array('type' => PARAM_VALUE, 'data' => $key);
			}
		}
		if ($_REQUEST['filter_range']) {
			foreach($_REQUEST['filter_range'] as $key => $value) {
				if (is_numeric($key) && $value != '')
					$filter['params'][] = array(
						'type' => PARAM_RANGE,
						'data' => array(
							'id' => $key,
							'min' => $value['min'],
							'max' => $value['max']
						)
					);
			}
		}
		if (!isset($filter['cat']) || count($filter['cat']) <= 0) {
			$tempCats = getCatsInMenuItem($_REQUEST['menu_item']);
			foreach ($tempCats as $cat) {
					$filter['cat'][] = $cat['cat_id'];
				}
		}
		$page = array(
			'page' => getPage(),
			'onPage' => Config::get('catalog.products_on_page')
		);
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : false;
		
		//$temp = getProducts(false, $filter, $sort, $page);
		if (isset($filter['params']) && count($filter['params']) > 0) {
			$temp = getProductsWithExtraFilter(false, $filter, $sort, $page);
		} else {
			$temp = getProducts(false, $filter, $sort, $page);
		}
		
		if (count($temp['products']) > 0) {
			foreach ($temp['products'] as $product) {
				include('../templates/blocks/itemcard.tpl.php');
			}
		/* 	switch ($_REQUEST['paginator_type']) {
				case '2':
					showPaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
				case 'uniq':
					UniqGeneratePaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
				default:
					generatePaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
			} */
		} else {
			echo '<div style="margin-left: 20px;">Нет товаров, удовлетворяющих выбранным условиям.</div>';
		}
		break;
	case 'cat' :
		//Фильтр для выбора товаров
		$filter = array();
		if (is_numeric($_REQUEST['min_price'])) {
			$filter['>=price_after_discount'] = $_REQUEST['min_price']; 
		}
		if (is_numeric($_REQUEST['max_price'])) {
			$filter['<=price_after_discount'] = $_REQUEST['max_price']; 
		}
		if ($_REQUEST['filter']) {
			foreach($_REQUEST['filter'] as $key => $value) {
				if (is_numeric($key) && $value != '') $filter['params'][] = array('type' => PARAM_VALUE, 'data' => $key);
			}
		}
		if ($_REQUEST['filter_range']) {
			foreach($_REQUEST['filter_range'] as $key => $value) {
				if (is_numeric($key) && $value != '')
					$filter['params'][] = array(
						'type' => PARAM_RANGE,
						'data' => array(
							'id' => $key,
							'min' => $value['min'],
							'max' => $value['max']
						)
					);
			}
		}

		if (isset($_REQUEST['tag'])) {
			foreach($_REQUEST['tag'] as $key => $value) {
				if (is_numeric($key)) $filter['tag'][] = $key;
			}
		}
		$catInfo = getCatInfo($_REQUEST['cat']);
		if ($catInfo['id']) {
			$filter['cat'] = $catInfo['id'];
		}

		$page = array(
			'page' => getPage(),
			'onPage' => Config::get('catalog.products_on_page')
		);
		//var_dump($page);
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : false;
		//var_dump($sort);
		if (isset($filter['params']) && count($filter['params']) > 0) {
			$temp = getProductsWithExtraFilter(false, $filter, $sort, $page);
		} else {
			$temp = getProducts(false, $filter, $sort, $page);
		}
		if (count($temp['products']) > 0) {
			foreach ($temp['products'] as $item) {
				include('../tpl/blocks/one_product_listing.tpl.php');
			}
			//var_dump($_REQUEST['savePrev']);
			if($temp['page']['currentPage']!=$temp['page']['pagesCount'] && $_REQUEST['savePrev']!="true")
			{
			 echo '<a class="bt_eshe" id="loadMore" data-page="'.$temp['page']['currentPage'].'" data-pagescount="'.$temp['page']['pagesCount'].'" href="#">Показать еще товары</a>';
			}
			/*  switch ($_REQUEST['paginator_type']) {
				case '2':
					showPaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
				case 'uniq':
					UniqGeneratePaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
				default:
					generatePaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
			}  */
		} else {
			echo '<div style="margin-left: 20px;">Нет товаров, удовлетворяющих выбранным условиям.</div>';
		}
		break;
		case 'tag' :

		$filter = array();
		if (is_numeric($_REQUEST['min_price'])) {
			$filter['>=price_after_discount'] = $_REQUEST['min_price']; 
		}
		if (is_numeric($_REQUEST['max_price'])) {
			$filter['<=price_after_discount'] = $_REQUEST['max_price']; 
		}
		if ($_REQUEST['filter']) {
			foreach($_REQUEST['filter'] as $key => $value) {
				if (is_numeric($key) && $value != '') $filter['params'][] = array('type' => PARAM_VALUE, 'data' => $key);
			}
		}
		if ($_REQUEST['filter_range']) {
			foreach($_REQUEST['filter_range'] as $key => $value) {
				if (is_numeric($key) && $value != '')
					$filter['params'][] = array(
						'type' => PARAM_RANGE,
						'data' => array(
							'id' => $key,
							'min' => $value['min'],
							'max' => $value['max']
						)
					);
			}
		}
		
		$filter['tag'] = is_numeric($_REQUEST['tag']) ? $_REQUEST['tag'] : 0;

		$page = array(
			'page' => getPage(),
			'onPage' => Config::get('catalog.products_on_page')
		);
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : false;
		
		if (isset($filter['params']) && count($filter['params']) > 0) {
			$temp = getProductsWithExtraFilter(false, $filter, $sort, $page);
		} else {
			$temp = getProducts(false, $filter, $sort, $page);
		}
	//	var_dump($temp);
		/* if (count($temp['products']) > 0) {
			foreach ($temp['products'] as $product) {
				include('../templates/blocks/itemcard.tpl.php');
			} */
			
				if (count($temp['products']) > 0) {
			foreach ($temp['products'] as $item) {
				include('../tpl/blocks/one_product_listing.tpl.php');
			}
			if($temp['page']['currentPage']!=$temp['page']['pagesCount'] && $_REQUEST['savePrev']!="true")
			{
			 echo '<a class="bt_eshe" id="loadMore" data-page="'.$temp['page']['currentPage'].'" data-pagescount="'.$temp['page']['pagesCount'].'" href="#">Показать еще товары</a>';
			}
			/* switch ($_REQUEST['paginator_type']) {
				case '2':
					showPaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
				case 'uniq':
					UniqGeneratePaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
				default:
					generatePaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
			} */
		} else {
			echo '<div style="margin-left: 20px;">Нет товаров, удовлетворяющих выбранным условиям.</div>';
		}
		break;
	case 'autocomplete':
			// Search autocomplete

			$string = $_POST['keyword'];
			
			mysql_set_charset('utf8');
			
			$keyword = '%'.$string.'%';
			$path = 'http://www.beurer-lux.ru/product/';
			$sql = "SELECT `title`,`id`,`chpu` FROM `catalog` WHERE `title` LIKE '{$keyword}' ORDER BY `spec_rang` ASC LIMIT 5;";
			$res = mysql_query($sql);
			$result = '';
			if($res) {
				while( $rs = mysql_fetch_assoc($res) ) {
					//$sugest =  $rs['title'] ;
					$result .= '<li><a href="'.$path.$rs['chpu'].'/" >'.$rs['title'].'</a></li>';
				}
			}
		    
		    echo $result;

	break;
}