<?php

include('../connect.php');
include('../func/core.php');


switch ($_REQUEST['method']) {
	case 'sendmail' :
		session_start();
		$error = 0;

		if ($_REQUEST['captcha'] == $_SESSION['sendMailCaptcha']) {
			$name = iconv("utf-8", "windows-1251", $_REQUEST['name']);
			$email = iconv("utf-8", "windows-1251", $_REQUEST['email']);
			$messageBody = iconv("utf-8", "windows-1251", $_REQUEST['message_body']);

			do {
				if (!validateEmail($email)) {
					$error = 1;
					break;
				}
				if (isLinksContain($messageBody) || isLinksContain($name)) {
					$error = 2;
					break;
				}

				$name = htmlspecialchars($name);
				$messageBody = htmlspecialchars($messageBody);
				$message = "
					<html>
						<div>Имя: {$name}</div>
						<div>E-mail: {$email}</div>
						<div style=\"margin-top: 10px;\">{$messageBody}</div>
					</html>
				";

				sendMail( array(
						'emailFrom' => $email,
						'emailTo' =>  Config::get('site.email_manager'),
						'subject' => 'Сообщение через форму с сайта "' . Config::get('site.name') . '"',
						'body' => $message
					)
				);

				$_SESSION['sendMailCaptcha'] = rand(9999,999999);
			} while (false);
		} else {
			$error = 3;
		}
		echo json_encode(array(
				'captcha' => $_SESSION['sendMailCaptcha'],
				'error' => $error
				)
			);
	break;
	
	case 'addfeedback' : 
		$name = $_REQUEST['name'];
		$email = $_REQUEST['email'];
		$comment = $_REQUEST['comment'];
		$comment_plus = $_REQUEST['comment_plus'];
		$comment_minus = $_REQUEST['comment_minus'];
		$productId = $_REQUEST['product_id'];
		$productRate = $_REQUEST['product_rate'];
		
		
		if (!is_numeric($productId) || $productId <= 0)
			break;
		
		if (empty($name) || empty($email) || empty($comment)) {
			echo json_encode(array( 'error' => true, 'error_code' => 1));
			break;
		}
		
		if (!validateEmail($email)) {
			echo json_encode(array( 'error' => true, 'error_code' => 2));
			break;
		}
		if (isLinksContain($name) || isLinksContain($comment) || isLinksContain($email)) {
			echo json_encode(array( 'error' => true, 'error_code' => 3));
			break;
		}
		
		file_put_contents('test.txt', $comment);
		
		addProductFeedback($productId, array(
			'name' => $name,
			'email' => $email,
			'comment' => $comment,
			'comment_plus' => $comment_plus,
			'comment_minus' => $comment_minus,
			'stars' => $productRate
		));
		
		echo json_encode(array( 'error' => false));
	break;
}