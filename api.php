<?php
include ('config.php');
include ('connect.php');

 //ini_set('error_reporting', E_ALL);
 //ini_set('display_errors', 1);
 //ini_set('display_startup_errors', 1);

function response($data) {
	echo serialize($data);
}

mysql_query ("SET NAMES 'UTF-8'");

switch($_REQUEST['method']) {
	case 'getNewOrders':
        $lastOrderId = mysql_real_escape_string($_REQUEST['lastOrderId']);
        $limit = mysql_real_escape_string($_REQUEST['limit']);
        $q = mysql_query('SELECT orders.*, order_delivery_types.name as deliveryName 
            FROM orders 
            LEFT JOIN order_delivery_types on order_delivery_types.id=orders.delivery_type 
            WHERE orders.id > '.$lastOrderId.' LIMIT '.$limit) or die(mysql_error());
        $res = array();
        while($r = mysql_fetch_array($q)){
            $prices = unserialize($r['order_price']);
            $products = unserialize($r['products']);
            $idArr = array();
            $prInfo = array();
            $prodArr = array();
            $commentsArr = array();
            foreach( $products as $p ){
                $idArr[] = $p['id'];
                $prInfo[$p['id']] = array(
                    'count' => $p['amount'],
                    'price' => $p['price_after_discount'],
                );
            }
            $idStr = implode(',', $idArr);

            // выбираем инфу по товарам
            $pQ = mysql_query('select * from catalog where id in ('.$idStr.')');
            while( $pR = mysql_fetch_array($pQ) ){
                $prodArr[] = array(
                    'productId' => $pR['id'],
                    'title' => $pR['title'],
                    'productUrl' => 'http://www.beurer-lux.ru/product/'.$pR['chpu'],
                    'imageUrl' => 'http://www.beurer-lux.ru/upload/small/'.$pR['id'].'.jpg',
                    'count' => $prInfo[$pR['id']]['count'],
                    'price' => $prInfo[$pR['id']]['price'],
                );
            }

            // выбираем инфу по комментам
            $oQ = mysql_query('select * from orders_log where order_id = '.$r['id']);
            while( $oR = mysql_fetch_array($oQ) ){
                $commentsArr[] = [
                    'text' => $oR['descr'],
                    'username' => 'manager',
                    'date' => strtotime($oR['date']) * 1000,
                ];
            }

            $promocode = null;
            // if(preg_match('|<strong>Рои Стат Код</strong> - (\d+)|', $r['extra_information'], $match)){
            //     $promocode = $match[1];
            // }
            $extra_information = preg_replace('|<strong>Рои Стат Код</strong> - (\d+)|', '', $r['extra_information']);
            $res[] = array(
                'orderId' => $r['id'],
                'name' => $r['name'],
                'phone' => $r['phone'],
                'email' => $r['email'],
                'adress' => $r['adress'],
                'extraInformation' => strip_tags($extra_information),
                'date' => $r['date'] * 1000,
                'price' => $prices['price_after_global_discount'],
                'deliveryPrice' => (is_numeric($prices['delivery_price']))?$prices['delivery_price']:0,
                'discountPrice' => (is_numeric($prices['discount_value']))?$prices['discount_value']:0,
                'status' => $r['status'],
                'paymentType' => 'Наличными',
                'deliveryType' => ($r['deliveryName']!==null)?$r['deliveryName']:'Курьером по Москве',
                'products' => $prodArr,
                'comments' => $commentsArr,
                'promocode' => $promocode,
                'orderPrefix' => 'B',
                'needInvoice' => ($r['payment_type'] == 2)?true:false,
                'autoreserve' => (is_numeric($r['reservedId']))?$r['reservedId']:null,
                'referer' => $r['h'],
            );
        }

        echo json_encode($res);
    break;

	case 'addProduct':
		$title = mysql_real_escape_string($_REQUEST['title']);
		$art = mysql_real_escape_string($_REQUEST['art']);
		
		$price = (int) $_REQUEST['price'];
		$cat = (int) $_REQUEST['cat'];
		
		if ($cat > 0) {
			//Добавляем товар
			mysql_query("
				INSERT INTO
					catalog
				SET
					`title` = '{$title}',
					`art` = '{$art}',
					`price` = '{$price}',
					`cat` = '{$cat}'
			");
			//Получаем id и создаем ответ
			response(mysql_insert_id());
		} else {
			response('error');
		}
	break;
	case 'import10med' :
		//Запускаем импорт остатков и себестоимости с 10 мед
		$connId = ftp_connect('185.22.60.137');
		$logRes = ftp_login($connId, 'tenmedxml_ftp', 'WvHYyGv9a6QZBRY');
		if( !$connId && !$logRes ){
			break;
		}else{
			if (ftp_get($connId, '10med_files/10med_file.xml', $config['10med_importUrl_filename'], FTP_BINARY)) {
				
			}
		}

		$xml = simplexml_load_file('10med_files/10med_file.xml');
		if ($xml !== false) {
			$plug = new plugin_Import_Data($xml);
			$plug->process();
			response('ok');
		} else {
			response('file not loaded');
		}
	break;
	
	case 'updatelinks' :
		$shopItemId = $_REQUEST['shop_item_id'];
		$_10medItemId = $_REQUEST['10med_item_id'];
		$type = $_REQUEST['type'];
		
		if (!is_numeric($shopItemId) || !is_numeric($_10medItemId) || $shopItemId <= 0 || $_10medItemId <= 0) {
			response('error, bag arguments');
		} else {
			if ($type == 'link') {
				mysql_query("UPDATE `catalog` SET `linked_with_10med` = '{$_10medItemId}' WHERE `id` = '{$shopItemId}'");
			} elseif ($type == 'unlink') {
				mysql_query("UPDATE `catalog` SET `linked_with_10med` = '0' WHERE `id` = '{$shopItemId}'");
			} else {
				response('error, bad arguments');
			}
		}
	break;
}

class plugin_Import_Data{

	var $filename;
	var $nomens;
	var $xml;
	var $_parent;
	
	function __construct($xml){
		$this->nomens = Array();
		$this->xml = $xml;
	}
	
	function parseFile(){
		$xml = $this->xml;

		foreach($xml->catalog as $f){
			//$str .= $f->ПолнНаименование;

			$k = count($this->nomens);
			$this->nomens[$k] = Array();
			$this->nomens[$k]['catalog_id'] = (int)$f["catalog_id"];
			$this->nomens[$k]['remains'] = Array();
			$this->nomens[$k]['net_cost'] = (float)$f['net_cost'];
			$this->nomens[$k]['turnover'] = (int)$f['turnover'];
			$this->nomens[$k]['linked_with_10med'] = (int)$f['linked_with_10med'];
			if( $f['nomencl_10med'] != 'null' ){
				$this->nomens[$k]['nomencl_10med'] = iconv('utf-8','cp1251',(string)$f['nomencl_10med']);
			}else{
				$this->nomens[$k]['nomencl_10med'] = '';
			}
			$kkk = 0;
			//print_r($f);
			foreach($f->storages->storage as $c){
				//print_r($c);
				$this->nomens[$k]['remains'][$kkk] = Array(
					'name' => (string)$c->name,
					'amount' => (int)$c->amount
				);
				$kkk++;
			}			
		}
	}

	function saveData(){
		mysql_query("DELETE FROM `storage_rests`");
		mysql_query("UPDATE catalog SET net_cost = '0', turnover = '0', `linked_with_10med` = '0'");
		foreach($this->nomens as $nomen){

			$_id = $nomen['catalog_id'];
			//echo $_id;

			foreach($nomen['remains'] as $storage){
				$sname = iconv("utf-8", "windows-1251//TRANSLIT", $storage['name']);
				$qresult = mysql_query("SELECT `id` FROM `storages` WHERE `name` = '{$sname}'") or die(mysql_error());
				if($line = mysql_fetch_assoc($qresult)){
					$s_id = $line["id"];
				}else{
					mysql_query("INSERT INTO `storages` (`name`) VALUES ('{$sname}')");
					$s_id = mysql_insert_id();
				}
				
				mysql_query("INSERT INTO `storage_rests`
					(`storage_id`, `catalog_id`, `amount`)
					VALUES
					('{$s_id}', '{$_id}', '{$storage['amount']}')");// or die(mysql_error());
			}
			mysql_query("UPDATE
				`catalog`
				SET
					`net_cost` = '{$nomen['net_cost']}',
					`turnover` = '{$nomen['turnover']}',
					`linked_with_10med` = '{$nomen['linked_with_10med']}',
					`nomencl_10med` = '{$nomen['nomencl_10med']}'
				WHERE
					`id` = '{$_id}'
			");
		}
		return "SUCCESS";
	}
	
	function process(){
		
		$this->parseFile();
		return $this->saveData();
	}
}