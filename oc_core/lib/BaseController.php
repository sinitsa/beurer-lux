<?php

namespace core\lib;
use core\lib\BaseView;
use app\models\ConfigModel;
use app\models\MenuModel;
use app\models\PageModel;
use app\models\BasketModel;

class BaseController {
	
	public $view;
	public $chpu;
	
	function __construct($content = null, $tab = null){
		$this->content = $content;
		$this->tab = $tab;
		$this->view = new BaseView();
	}

	public function get404() {

		header('HTTP/1.1 404 Not Found');


		$config = new ConfigModel();
		$menu = new MenuModel();

		$data = [
			'basket' => BasketModel::getData(),
			'phone' => $config->get('site.phone'),
			'phone_global' => $config->get('site.global_phone'),
			'work_mode'=> $config->get('site.work_mode'),
			'menu' => $menu->getMenu(),
			'pagesMenu' => PageModel::getMenu(),
			'seo' => [
				'title' => 'Страница не найдена', 
				'describtion' => '', 
				'keywords' => ''
			]
		];
		$this->view->render('index', '404', $data);
		die();
	}
	
}