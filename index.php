<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors',1);
error_reporting(E_ALL); 

include("connect.php");
include("func/core.php");
mb_internal_encoding('UTF-8');

define('ROOT_DIR', dirname(__FILE__) . '/');

/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/

/*
 * определяем тип устройства, выбор темы под этот тип
 * Временно: если выбрана мобильная версия , переход на новую версию Движка
*/


$_GET['tab'] = isset($_GET['tab']) ? $_GET['tab'] : '/' ;

// Переводим роутинг в приемлемый вариант
$old_route = explode('/', $_GET['tab']);
$_GET['t'] = isset($old_route[0]) ? $old_route[0] : '' ;
$_GET['c'] = isset($old_route[1]) ? $old_route[1] : '';

if( isset($_REQUEST['type']) && $_REQUEST['type'] == "pda")
{
    setcookie("theme", 2, 0, '/');
}

$theme = filter_input(INPUT_COOKIE, 'theme');

if ($theme == 2) {
    define('THEME', 'mobile');
    // Берем данные о роутинге
    require_once ('oc_core/bootstrap.php');
    die();
} elseif ($theme == 1) {
} else {
    require_once('oc_core/plugins/Mobile_Detect.php');
    $detect = new Mobile_Detect;
    if ($detect->isMobile()/* or $detect->isTablet()*/) {
        setcookie("theme", 2, 0, '/');
        define('THEME', 'mobile');
        require_once ('oc_core/bootstrap.php');
        die();
    } else {
        setcookie("theme", 1, 0, '/');
    }
}


// Берем данные о роутинге
$tab = explode('/', $_GET['tab']);
$_GET['t'] = $tab[0];
//Подгружаем общие данные конфига
query_config();

session_start();

$_GET['debug'] = isset($_GET['debug']) ? $_GET['debug'] : '' ;
$_COOKIE['debug'] = isset($_COOKIE['debug']) ? $_COOKIE['debug'] : '' ;

$my_ip = $_SERVER['REMOTE_ADDR'];
$debug = false;
if ($my_ip == "217.194.255.193" || 1 == $_GET['debug'] || 1 == $_COOKIE['debug']) {
	setcookie("debug", 1, 0, "/");
	$debug = true;
}

//var_dump($_COOKIE);
$_COOKIE['cart_products'] = isset($_COOKIE['cart_products']) ? $_COOKIE['cart_products'] : '';
$summProducts = 0 ;
$summCount = 0 ;
$summPrice =isset($_COOKIE['total_amount']) ? $_COOKIE['total_amount'] : 0;
$arrProducts = explode("|", $_COOKIE['cart_products']);
	foreach($arrProducts as $s)
	{
		 $im = explode(":", $s);
		 $im[1] = isset($im[1]) ? $im[1] : 0 ;
		 $summProducts += $im[1];
	}
if ((isset($_GET['p'])) && (!isset($_COOKIE['fpid']))) {
	setPartnerCookie();
}
global $actionPayIncoming;
$actionPayIncoming = false;
if ((isset($_GET['apclick'])) && ((isset($_GET['apsource']))) && ((isset($_GET['source'])))) {

	if (!isset($_COOKIE['actionpay'])) {
	$actionPayIncoming = true;
	setcookie('actionpay', $_GET['apclick'] . '.' . $_GET['apsource'], time() + 2592000, '/');
	}
}
//Med vera hook
if (file_exists('./hooks/index.php')) {
	include('./hooks/index.php');
}
//Флажек "страница не найдена"
$pageNotFound = false;
// Выполняем запрос данных в зависимости от раздела

//Товары в корзине
	$productsCart = getProductsInBasket();

	$temp = cart::getDeliveryAndDiscount($productsCart);

	$deliveryPrice = $temp['deliveryPrice'];
	$totalPrice = isset($temp['fullPrice']) ? $temp['fullPrice'] : 0 ;
    //$priceSummWithGlobalDiscount = getPriceAfterDiscount($totalPrice, $discount['type'], $temp['discount']);

		if ($deliveryPrice === false) {
		$deliveryText = 'от 500';
		$deliveryUnitShow = true;
		$total = 'false';
	} elseif ($deliveryPrice > 0) {
		$deliveryText = $deliveryPrice;
		$deliveryUnitShow = true;
		//$total = $priceSummWithGlobalDiscount + $deliveryPrice;
	} else {
		$deliveryText = '<strong>Бесплатно</strong>';
		$deliveryUnitShow = false;
		//$total = $priceSummWithGlobalDiscount;
	}
	
	
	//var_dump($productsCart);




switch ($tab[0]) {

	//-------------------------------- Главная
	case "":

	$tab[0] = "main";
	//Тескт в нижней части главной страницы выбираем из page
	$staticPage = getStaticPage('page-on-main');

	$seo_title = Config::get('home.seo_title');
	$seo_des = Config::get('home.seo_des');
	$seo_key = Config::get('home.seo_key');
	$products = getPopularProducts();
	//var_dump($products);
	$cats = getCats();


	break;

	//-------------------------------- Отзывы
	// case "feedback":
	// global $fb_validation;
	// $fb_validation = addFeedBack();
	// $tab = 'feedback';
	// $seo_title = 'Оставить отзыв';
	// $seo_des = 'Оставить отзыв';
	// $seo_key = 'Оставить отзыв';
	// $seo_title_text = '';
	// $seo_text = '';
	// if ($fb_validation == null) {
	// 	$currentPage = (isset($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : 1;
	// 	//Отзывов на странцу
	// 	$onPage = 20;
	// 	if ($_GET['page'] == 'all') {
	// 	$limitString = '';
	// 	} else {
	// 	//Всего отзывов
	// 	$feedsCount = fetchOne("SELECT COUNT(*) FROM `shop_otzyv` WHERE `confirm` = '1'");
	// 	//Всего страниц
	// 	$allPages = ceil($feedsCount / $onPage);

	// 	//Если номер текущей страницы больше, чем страниц всего, то текущая страница 1
	// 	if ($currentPage > $allPages) {
	// 		$currentPage = 1;
	// 	}
	// 	//Высчитываем offset записей
	// 	$limit = ($currentPage - 1) * $onPage;
	// 	//Подготавливаем строку для запроса
	// 	$limitString = 'LIMIT ' . $limit . ',' . $onPage;

	// 	//Подготавливаем данные для скармливания pageGenerator'у
	// 	$paginData = array(
	// 		'pagesCount' => $allPages,
	// 		'currentPage' => $currentPage
	// 	);
	// 	}
	// 	$qString = new queryString();
	// 	$qString = $qString->setHash('read');

	// 	$messages = array();
	// 	$sel = mysql_query("SELECT * FROM `shop_otzyv` WHERE `confirm` = '1' ORDER BY `id` DESC {$limitString}");
	// 	while ($res = mysql_fetch_assoc($sel)) {
	// 	$messages[] = array(
	// 		'id' => $res['id'],
	// 		'name' => $res['name'],
	// 		'des' => $res['des'],
	// 		'otvet' => $res['otvet'],
	// 		'date' => $res['date']
	// 	);
	// 	}
	// }
	// break;

	//-------------------------------- Статьи
	case "articles":
	if ($content == '') {
		$tab = 'articles';
		$seo_title = 'Статьи о здоровье человека';
		$seo_des = 'Интересные статьи о здоровье человека ';
		$seo_key = 'здоровье человека статьи';
		$seo_title_text = '';
		$seo_text = '';
	} else {
		$tab = 'artread';

		$seo_key = 'Интересные статьи';
		$seo_title_text = '';
		$seo_text = '';

		global $article;
		$article = getArticle($content);
		$seo_title = $article['seo_title'];
		$seo_des = strip_tags(htmlspecialchars_decode($article['announce']));
	}
	break;

	case "articles2":
	if ($content == '') {
		$tab = 'articles2';
		$seo_title = 'Интересные статьи';
		$seo_des = 'Интересные статьи';
		$seo_key = 'Интересные статьи';
		$seo_title_text = '';
		$seo_text = '';
	} else {
		$tab = 'artread2';

		$seo_key = 'Интересные статьи';
		$seo_title_text = '';
		$seo_text = '';

		global $article;
		$article = getArticle($content);
		$seo_title = $article['seo_title'];
		$seo_des = strip_tags(htmlspecialchars_decode($article['announce']));
	}
	break;

	//-------------------------------- Категории
	case "category":
//echo "fd";
	global $cur_cat;
	$temp = getCatInfo($tab[1]);
	
	//var_dump($temp);
	if ($temp == false) {
		//Категории нет
		$pageNotFound = true;
		break;
	}
	//var_dump();
	$data['catInfo'] = $temp;
	//var_dump($data['catInfo']);
	$seo_title_text = $temp['seo_title_text'];
	$seo_text = $temp['seo_text'];

	$seo_title = $temp['seo_title'];
	$seo_des = $temp['seo_des'];
	$seo_key = $temp['seo_key'];

	$title = $temp['title'];

	//Если выбрана какая то страница (в урл ?page), то не выводить сео текст и заголовк поменять
	if (isset($_GET['page'])) {
		$seo_title = $data['catInfo']['title'];
		$seo_text = '';
		$seo_title_text = '';
	}
	
	$parentCat = getParentCat($data['catInfo']['pod']);

	// хлебные крошки
    $breadItems = array(
        array(
            'title' => $parentCat['title'],
            'chpu'  => '/mcat/'.$parentCat['chpu'],
        ),
        array(
            'title' => $data['catInfo']['title']
        ),
    );

	$page = array(
		'page' => getPage(),
		'onPage' => Config::get('catalog.products_on_page')
	);
	$filter['cat'] = $temp['id'];
	
	if (isset($_REQUEST['min_price']) && is_numeric($_REQUEST['min_price'])) {
		$filter['>=price_after_discount'] = $_REQUEST['min_price'];
	}
	if (isset($_REQUEST['max_price']) && is_numeric($_REQUEST['max_price'])) {
		$filter['<=price_after_discount'] = $_REQUEST['max_price'];
	}
	$sorting = getSort();
	$temp = getProducts(false, $filter, $sorting, $page);
//var_dump($temp);

    $tag['chpu'] = false;

	$data['products'] = $temp['products'];
	
	$data['page'] = $temp['page'];
	//Страницы под номером N не существует
	if ($data['page']['outOfBounds']) {
		$pageNotFound = true;
		break;
	}
	$data['tags'] = getTagsInCat($filter['cat']);
	$data['tab'] = 'cat';
	//Запрашиваем максимальную и минимальную цены
	$data['price_range'] = $temp['price'];

	//Выберем доступные параметры (для фильтра)
	$data['params'] = getRealParamsAndValuesInCat($filter['cat']);

	$data['paramsFormAction'] = 'cat';
	$data['paramsHidden'] = array('cat' => $data['catInfo']['id']);
	//var_dump($data['paramsHidden']);

	$qString = new queryString;
	// $tab = "category";

	/* // 404 если ничего не нашли
		if ($chekis == '') {
		header("Status: 404 Not Found");
		query_pages('404');
		$tab = "page";
		} */
	break;
	//-------------------------------- Категории
	case "deals":
	$seo_title_text = '';
	$seo_text = '';
	global $cur_cat;
	$cur_cat = $content;

	switch ($content) {
		case 'best' :
		$filter['best'] = 1;
		$title = 'Самое лучшее, новинки';
		break;
		case 'sale' :
		$filter['sale'] = 1;
		$title = 'Сезонные распродажи';
		break;
	}
	$seo_title_text = $temp['seo_title_text'];
	$seo_text = $temp['seo_text'];

	$page = array(
		'page' => getPage(),
		'onPage' => Config::get('catalog.products_on_page')
	);

	//тут то мы и добавим фильтр
	$temp = getProducts(false, $filter, getSort(), $page);
	$data['products'] = $temp['products'];
	$data['page'] = $temp['page'];

	$qString = new queryString;
	$tab = "deals";
	/* // 404 если ничего не нашли
		if ($chekis == '') {
		header("Status: 404 Not Found");
		query_pages('404');
		$tab = "page";
		} */
	break;

	//-------------------------------- Категории в пункте меню
case "mcat":
  
  //данные для Google Adwords
  $ecomm_prodid = '';
  $ecomm_pagetype = 'category';
  $ecomm_totalvalue = '';
  $content = $tab[1];
  $seo_title_text = '';
  $seo_text = '';
  global $cur_cat;
  $cur_cat = $content;
  //echo $content;
  // Запрашиваем данные для страницы

  $data['menuItem'] = getMenuItemInfo($content);
  //print_r($data);
  if ($data['menuItem'] == false) {
   $pageNotFound = true;
   break;
  }
  $data['catInfo']['title'] = $data['menuItem']['title'];
  $title = $data['catInfo']['title'];
  $data['cats'] = getCatsInMenuItem($content);
  //print_r($data['cats']);
  //var_dump($data['cats']);
  $cats = array();
  $data['params'] = array(); 
  
  foreach ($data['cats'] as $cat) {
   $cats[] = $cat;
  }

  $filter['cat'] = $cats;
   if (isset($_REQUEST['min_price']) && is_numeric($_REQUEST['min_price'])) {
   $filter['>=price_after_discount'] = $_REQUEST['min_price'];
  }
  if (isset($_REQUEST['max_price']) && is_numeric($_REQUEST['max_price'])) {
   $filter['<=price_after_discount'] = $_REQUEST['max_price']; 
  }
  $var_sort_mcat='price_asc';

  $data['tab'] = 'mcat';

    // хлебные крошки
    $breadItems = array(
        array(
            'title' => $data['menuItem']['title']
        ),
    );

  $seo_title = $data['menuItem']['seo_title'];
  $seo_text = $data['menuItem']['seo_text'] ;
  
  $seo_des = '';
  $seo_key = '';
  
  //Если выбрана какая то страница (в урл ?page), то не выводить сео текст и заголовк поменять
  if (isset($_GET['page'])) {
   $seo_title = $data['menuItem']['title'];
   $seo_text = '';
  }
  
  //$qString = new queryString;
  //$tab = "catlist";
  $tpltype_mcat=true;
  break;
  
	//Теги\псевдокатегории
case 'tags' :
	//echo 'fdfd';
	$seo_title_text = '';
	$seo_text = '';
	//global $cur_cat;
	//$cur_cat = $content;
	
	$tag = getTagInfo($tab[1]);
	//var_dump($tag);
	if ($tag == false) {
		$pageNotFound = true;
		break;
	}
	$seo_title = $tag['seo_title'];
	$seo_key = $tag['seo_key'];
	$seo_des = $tag['seo_description'];
	//var_dump($seo_title);
	$seo_title_text = '';
	$seo_text = $tag['seo_text'];
	
	$title = $tag['title'];
	$data['tagInfo'] = $tag;
	$data['catInfo'] = getCatInfo($tag['cat_id']);
	
	$page = array(
		'page' => getPage(),
		'onPage' => Config::get('catalog.products_on_page')
	);

	$filter = array();
	$filter['tag'] = $tag['id'];
	$parentCat = getParentCat($data['catInfo']['pod']);
	//var_dump($parentCat);

    $breadItems = array();

	if(!empty($parentCat))
	{
		//$firstBread = "<a href='/mcat/".$parentCat['chpu']."'>".$parentCat['title']."</a> - <a href='/category/".$data['catInfo']['chpu']."'>".$data['catInfo']['title']."</a> - ";
        $breadItems[] = array(
            'title' => $parentCat['title'],
            'chpu'  => '/mcat/'.$parentCat['chpu']
        );
        $breadItems[] = array(
            'title' => $data['catInfo']['title'],
            'chpu'  => '/category/'.$data['catInfo']['chpu']
        );
	}
	else
	{
		//$firstBread = "<a href='/mcat/".$data['catInfo']['chpu']."'>".$data['catInfo']['title']."</a> - ";
        $breadItems[] = array(
            'title' => $data['catInfo']['title'],
            'chpu'  => '/mcat/'.$data['catInfo']['chpu']
        );
	}	
	$temp = getProducts(false, $filter, getSort(), $page);
	//var_dump($filter);
	$data['tab'] = 'tags';
	//$linkBread = $firstBread."<span>".$tag['title']."</span>";

    // хлебные крошки
    $breadItems[] = array(
        'title' => $tag['title']
    );

	//соседние тег
	$data['tags'] = getTagsInCat($data['catInfo']['id']);

	$data['products'] = $temp['products'];
	$data['page'] = $temp['page'];

	//максимальную и минимальную цены

	$data['price_range'] = $temp['price'];

	//Выберем доступные параметры (для фильтра)
	$data['params'] = getRealParamsAndValuesInCat($tag['cat_id']);

	$data['paramsFormAction'] = 'tag';
	$data['paramsHidden'] = array('tag' => $tag['id']);

	$qString = new queryString;
	//$tab = "catlist";
	break;
	//-------------------------------- Спецпредложения
	case "spec":
	$seo_title = '';
	$seo_title_text = 'Какие товары попадают в этот раздел?';
	$seo_text = 'В разделе "Лучшее решение" представлены товары, отобранные из различных категорий. Их можно назвать лучшими и по ценовому уровню и по качеству. Эти товары всегда есть в наличии.  Выражаясь простым языком, эти товары - лучшие из лучших. Они имеют высокую степень надежности. Вы никогда не разочаруетесь в своем выборе.';
	// Запрашиваем данные для страницы
	query_catalog_spec();
	//if ($chekis == ''){ header("Status: 404 Not Found"); query_pages ('404'); $tab = "page";	}
	break;


	//-------------------------------- Каталог
	case "product":
	
	$seo_title_text = '';
	$seo_text = '';
	// Запрашиваем данные для страницы
	//query_catalog_vn($content); выпилина, переписана
	
	$product = getProduct($tab[1]);
	//var_dump($product);
	if ($product == false) {
		$pageNotFound = true;
		break;
	}

	$cat = $product['cat_info'];
	
	//var_dump($cat);
	$data['tags'] = getTagsInCat($cat['id']);
	
	$data['feedAdded'] = false;
	$data['feedError'] = false;
	$data['feedErrorSpam'] = false;

	if (isset($_REQUEST['add_product_feedback']) && $_REQUEST['add_product_feedback']) {
		$name = $_REQUEST['name'];
		$email = $_REQUEST['email'];
		$comment = $_REQUEST['comment'];

		if (strlen($name) > 3 && strlen($comment) > 3 && !isLinksContain($name) && !isLinksContain($email) && !isLinksContain($comment)) {
		addProductFeedback($product['id'], array('name' => $name, 'email' => $email, 'comment' => $comment));
		$data['feedAdded'] = true;
		} else {
		$data['feedError'] = true;
		if (isLinksContain($name) || isLinksContain($comment) || isLinksContain($email)) {
			$data['feedErrorSpam'] = true;
		}
		}
	}

        // хлебные крошки
        $breadItems = array(
            array(
                'chpu'  => '/category/'.$cat['id'],
                'title' => $cat['title']
            ),
            array(
                'title' => $product['title']
            ),
        );

	$data['feedback'] = getProductFeedback($product['id']);
	$products[] = array('id'=>$product['id'], "amount"=>1);
	//$deliveryMoscow = cart::getDeliveryAndDiscount($products,1);
	//$deliveryMoscow = cart::getDeliveryAndDiscount($products,1);
	$seo_title = $product['seo_title'];
	$seo_des = $product['seo_des'];
	$seo_key = $product['seo_key'];
	break;


	//-------------------------------- Обратная связь
	case "feed":
	$seo_title = 'Обратная связь';
	$seo_des = 'Обратная связь';
	$seo_key = 'обратная связь';

	if ($content == "ok_send") {
		$tab = "ok_send";
		$seo_title = 'Ваше сообщение принято';
		$seo_des = 'Ваше сообщение принято';
		$seo_key = 'Ваше сообщение принято';
	}
	if ($content == "ok_obratnyi") {
		$tab = "ok_obratnyi";
		$seo_title = 'Мы вам перезвоним';
		$seo_des = 'Мы вам перезвоним';
		$seo_key = 'Мы вам перезвоним';
	}
	if ($content == "ok_otzyv") {
		$tab = "ok_otzyv";
		$seo_title = 'Ваш отзыв отправлен';
		$seo_des = 'Ваш отзыв отправлен';
		$seo_key = 'Ваш отзыв отправлен';
	}
	$seo_title_text = '';
	$seo_text = '';
	break;

	//-------------------------------- Обратный звонок
	case "obratnyi":
	$seo_title = 'Обратный звонок';
	$seo_des = 'Обратный звонок';
	$seo_key = 'обратный звонок';
	$seo_title_text = '';
	$seo_text = '';
	break;

	//-------------------------------- Поиск
	/* case "search":
	$seo_title = 'Поиск';
	$seo_des = 'Поиск';
	$seo_key = 'Поиск';
	$seo_title_text = '';
	$seo_text = '';
	break;

	case "pay":
	echo "fd";
	$tab = "cart";
	$pageNotFound = false;
	break;case "search":
	$seo_title = 'Поиск';
	$seo_des = 'Поиск';
	$seo_key = 'Поиск';
	$seo_title_text = '';
	$seo_text = '';
	break;

	case "pay":
	echo "fd";
	$tab = "cart";
	$pageNotFound = false;
	break;case "search":
	$seo_title = 'Поиск';
	$seo_des = 'Поиск';
	$seo_key = 'Поиск';
	$seo_title_text = '';
	$seo_text = '';
	break;

	case "pay":
	echo "fd";
	$tab = "cart";
	$pageNotFound = false;
	break; */
	//-------------------------------- Корзина
	case "cart":
	
	
	
	$content = "korzina";
	//session_start();

	//Типы доставки
	$delivery = array();
	$sel = mysql_query("SELECT * FROM `order_delivery_types` ORDER BY `id` ASC");
	while ($row = mysql_fetch_assoc($sel)) {
		$delivery[] = $row;
	}
	$oplata = array();
	$sel_opl = mysql_query("SELECT * FROM `order_oplata_types` ORDER BY `id` ASC");
	while ($row_opl = mysql_fetch_assoc($sel_opl)) {
		$oplata[] = $row_opl;
	}
	

	//Сумма заказа не учитывая главную скидку при оформлении заказа
	$priceSumm = 0;
	$productsCount = count($productsCart);
	$productsAmount = 0;

	$productsInCart = array();

	for ($i = 0; $i < $productsCount; $i++) {
		$productsInCart[$productsCart[$i]['id']] = array(
		'id' => $productsCart[$i]['id'],
		'amount' => $productsCart[$i]['amount']
		);
		$priceSumm += $productsCart[$i]['price_after_discount'] * $productsCart[$i]['amount'];
		$productsAmount += $productsCart[$i]['amount'];
	}
	

	//$linkBread = "<span>Оформление заказа</span>";

        // хлебные крошки
        $breadItems = array(
            array(
                'title' => 'Оформление заказа'
            )
        );

	$discount['value'] = $temp['discount'];
	$discount['type'] = 'percent';

	//depreceted
	//Общая сумма заказа с учетом скидки
	//$discount = getGlobalDiscount($productsAmount, $priceSumm);

	$discountValue = $discount['value'];
	switch ($discount['type']) {
		case 'percent' :
		$discountTypeString = '%';
		break;
		case 'value' :
		$discountTypeString = 'руб';
		break;
		default :
		$discountTypeString = '';
	}
	$priceSummWithGlobalDiscount = getPriceAfterDiscount($priceSumm, $discount['type'], $discount['value']);

	//Обратный звонок (заказ обратного звонка, записывается как
	$backCall = isset($_REQUEST['backcall']);

	//Если открыт первый шаг оформления корзины
	if ($content == 'korzina') {
		// СЕО данные для страницы оформления заказа
		$seo_title = 'Оформление заказа';
		$seo_des = 'Оформление заказа';
		$seo_key = 'Оформление заказа';
		$seo_title_text = '';
		$seo_text = '';
	} elseif ($content == 'vybor') {
		//Второй щаг оформления заказа. Выбор доставки
		$seo_title = 'Оформление заказа';
		$seo_des = 'Оформление заказа';
		$seo_key = 'Оформление заказа';
		$seo_title_text = '';
		$seo_text = '';

		if (count($productsInCart) <= 0) {
		header("Location: " . getTemplateLink(array('chpu' => 'korzina'), 'cart'));
		die();
		}
	} elseif ($content == 'finish') {
		
		//Третий шаг оформления заказа. Заполнение реквизитов
		//$tab = '';
		//Если корзина пустая, какое оформление заказа? ты чо?
		//Не пускать на страницу

		if (count($productsInCart) <= 0) {
		header("Location: " . getTemplateLink(array('chpu' => 'korzina'), 'cart'));
		die();
		}

		$deliveryType = $_POST['radio'];
		if (isset($_POST['delivery_type']))
		$deliveryType = $_POST['delivery_type'];
		elseif (isset($_POST['radio']))
		$deliveryType = $_POST['radio'];
		
		if (isset($_POST['make_order']) || isset($_POST['make_order.x']) || isset($_POST['make_order_x'])) {

		$data = array();
		$data['name'] = schars($_POST['name']);
		$data['phone'] = schars($_POST['phone']);
		$data['email'] = schars($_POST['email']);
		$data['post_index'] = schars($_POST['post_index']);
		$data['adress'] = schars($_POST['adress']);
		$data['extra_information'] = schars($_POST['extra_information']);
		$data['delivery_type'] = $deliveryType;
		$data['backcall'] = $backCall;

		// Вытаскиваем куку "Откуда пришел пользователь"
		$data['ref'] = $_COOKIE["ref"];

		
		if ($_SESSION['capcha'] == $_POST['capcha']) {

			$orderStatus = makeOrder($productsInCart, $data);

			if (!isset($orderStatus['error'])) {
			// Обнуляем корзину
			setcookie("total_amount", "", time() + 7200, "/");
			setcookie("cart_products", "", time() + 7200, "/");

			//На всякий. Мож в шаблоне ok.tpl понадобятся данные о сумме, скидках и тд
			extract($orderStatus);

			$tab = 'ok';
			} else {
			//Поля бы нужно заполнить
			$error = true;
			}
		} else {
			//По капче не прошел
			$error = true;
		}
		}
	}elseif("credite" === $content){
		if (!$_COOKIE['order_id'])
		header("Location: /");
		else {
		$orderId = (int)$_COOKIE['order_id'];
		$orderInfo = getOrderById($orderId);
		unset($_COOKIE['order_id']);
		setcookie('order_id', NULL, time() - 3600, '/');
		setcookie("kredit", NULL, time() - 3600, "/");
		$base64 = getBase64Order($orderInfo);
		$sig = signMessage($base64, "usmedic-secret-x4QCV995");
		}
		break;
	}else {
		$pageNotFound = true;
		break;
	}
	//Блокировка повторного отправления заказа по F5 ("отправить данные")
	$_SESSION['capcha'] = rand(9999, 9999999);
	break;
////////////////////////////////////////////////////////////////////////////////
	//-------------------------------- Новый поиск
	case "invoice":
		// print $_SERVER['QUERY_STRING'];
		//данные для Google Adwords
		$ecomm_prodid = '';
		$ecomm_pagetype = 'other';
		$ecomm_totalvalue = '';

		$seo_title = 'Оплата';
		$seo_des = 'Оплата';
		$seo_key = 'Оплата';
		$seo_title_text = '';
		$seo_text = '';
	break;

	case "search":
		$seo_title = 'Поиск';
		$seo_des = 'Поиск';
		$seo_key = 'Поиск';
		$seo_title_text = '';
		$seo_text = '';
		$search_error = '';
		//$tab = "search";

		include('func/sphinxapi.php');
		
		$search_string = ($_GET['search'])? ($_GET['search']): 1;
		//var_dump($search_string);
		// $search_string = mysql_escape_string(htmlentities($search_string));

		$cl = new SphinxClient ();
		// настройки
		$cl->SetServer("127.0.0.1", 3312);
		$cl->SetConnectTimeout( 1 );
		$cl->SetMaxQueryTime(1000);
		$cl->SetLimits(0, 300, 300);
		$cl->SetMatchMode(SPH_MATCH_ANY);
		// $cl->SetMatchMode(SPH_MATCH_EXTENDED2);
		$cl->SetSortMode(SPH_SORT_RELEVANCE);
		$cl->SetFieldWeights(array('title' => 20, 'des' => 10));

		$result = $cl->Query($search_string,"beurer-lux");
		if( $result === false ){
			if( $cl->GetLastWarning() ) {
				die('WARNING: '.$cl->GetLastWarning());
			}
			die('ERROR: '.$cl->GetLastError());
		}
		
		if( is_array($result['matches']) ){
			$ids = array_keys($result['matches']);
			$id_list = implode(',', $ids);
			$query = "SELECT * FROM `catalog` WHERE `id` in (".$id_list.") ORDER BY FIELD(`id`, ".$id_list.")";
			$result = mysql_query($query) or die(mysql_error());
		}
		
		while($row = mysql_fetch_assoc($result))
		{
		$data['products'][] = $row;
		}
		 //$linkBread = "<span>Поиск товаров</span>";
		//var_dump($result);

        // хлебные крошки
        $breadItems = array(
            array(
                'title' => 'Поиск товаров'
            )
        );

		$tab[0] = 'search';
	break;

	//Обычная страница
	default:

	    if ($tab[1] == 'wholesale') {
	        $data['title'] = 'Запрос оптового прайса';

            $seo_title = 'Запрос оптового прайса';
            $seo_key = 'Запрос оптового прайса';
            $seo_des = 'Запрос оптового прайса';
            $tab[0] = 'wholesale' ;
        } else {
            // Запрашиваем данные для страницы
            $data = getStaticPage($tab[1], true);
            if ($data == false) {
                $pageNotFound = true;
                break;
            }

            $seo_title = $data['seo_title'];
            $seo_key = $data['seo_key'];
            $seo_des = $data['seo_des'];
            $tab[0] = 'page';
        }

        // хлебные крошки
        $breadItems = array(
            array(
                'title' => $data['title']
            )
        );

	break;
}

if ($pageNotFound) {
	// $data = getStaticPage('404', true);
	// $seo_title_text = $data['seo_title'];
	// $seo_text = $data['seo_desc'];
	$seo_title = 'Страница не найдена';
	$seo_key = '';
	$seo_des = 'Страница не найдена';
	$tab = '404';
	//$linkBread = "Страница не найдена 404";

    // хлебные крошки
    $breadItems = array(
        array(
            'title' => 'Страница не найдена 404'
        )
    );
	header('HTTP/1.1 404 Not Found');
	header("Status: 404 Not Found");
}
// ---------------------------------------------   ОТЛАДКА
//Грузим данные для страницы
//echo "<pre>"; print_r($spec_id); echo "</pre>";echo $num_cat;
// Пишем куки откуда пришел пользователь
$h = $_COOKIE["ref"];
if ($h == '') {
	$h = $_SERVER['HTTP_REFERER'];
	setcookie("ref", "$h", time() + 2000000, "/");
}

include('tpl/index.tpl.php');
